﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MylaSchool
{
    public partial class FrmTripM : Form
    {
        public FrmTripM()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        public double sum111 = 0;
        SQLDBHelper db = new SQLDBHelper();
        int slno = 0;
        BindingSource bsParty = new BindingSource();
        public int SelectId = 0;
        private void FrmTripM_Load(object sender, EventArgs e)
        {
            this.Dgv.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dgvlist.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgvlist.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            Dgv.RowHeadersVisible = false;
            Dgvlist.RowHeadersVisible = false;
            grSearch.Visible = false;

            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            chk.Checked = true;
            Getemp();
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            ClearTxt();
            Dgvlist.Rows.Clear();

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnDelete.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select Tuid,TripName,b.Name,routid,busid,start,Endt,a.status,c.name as busname from tripM a inner join routem b on a.routid=b.ruid  inner join busm c on a.busid=c.buid  ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 9;

                Dgv.Columns[0].Name = "Tuid";
                Dgv.Columns[0].HeaderText = "Tuid";
                Dgv.Columns[0].DataPropertyName = "Tuid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "TripName";
                Dgv.Columns[1].HeaderText = "Route Name";
                Dgv.Columns[1].DataPropertyName = "TripName";
                Dgv.Columns[1].Width = 180;

                Dgv.Columns[2].Name = "Name";
                Dgv.Columns[2].HeaderText = "Route Name";
                Dgv.Columns[2].DataPropertyName = "Name";
                Dgv.Columns[2].Width = 180;

                Dgv.Columns[3].Name = "routid";
                Dgv.Columns[3].HeaderText = "routid";
                Dgv.Columns[3].DataPropertyName = "routid";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].Name = "busid";
                Dgv.Columns[4].HeaderText = "busid";
                Dgv.Columns[4].DataPropertyName = "busid";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].Name = "start";
                Dgv.Columns[5].HeaderText = "start";
                Dgv.Columns[5].DataPropertyName = "start";
                Dgv.Columns[5].Width = 50;

                Dgv.Columns[6].Name = "Endt";
                Dgv.Columns[6].HeaderText = "End";
                Dgv.Columns[6].DataPropertyName = "Endt";
                Dgv.Columns[6].Width = 50;

                Dgv.Columns[7].Name = "status";
                Dgv.Columns[7].HeaderText = "status";
                Dgv.Columns[7].DataPropertyName = "status";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "busname";
                Dgv.Columns[8].HeaderText = "busname";
                Dgv.Columns[8].DataPropertyName = "busname";
                Dgv.Columns[8].Width = 120;
                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from tripM where  TripName='" + txtroute.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string ui;
                    if (chk.Checked == true)
                    {
                        ui = "1";
                    }
                    else
                    {
                        ui = "0";
                    }
                    con.Close();
                    con.Open();
                    SqlParameter[] para ={
                        new SqlParameter("@tripName",txtroute.Text),
                        new SqlParameter("@routid",TxtfeeM.Tag),
                        new SqlParameter("@busid",txttime1.Tag),
                        new SqlParameter("@start",txtstart.Text),
                        new SqlParameter("@Endt",txtend.Text),
                        new SqlParameter("@status",ui),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_tripM", para, con);
                }
            }
            else
            {
                string ui;
                if (chk.Checked == true)
                {
                    ui = "1";
                }
                else
                {
                    ui = "0";
                }
                con.Close();
                con.Open();
                SqlParameter[] para ={
                    new SqlParameter("@tripName",txtroute.Text),
                    new SqlParameter("@routid",TxtfeeM.Tag),
                    new SqlParameter("@busid",txttime1.Tag),
                    new SqlParameter("@start",txtstart.Text),
                    new SqlParameter("@Endt",txtend.Text),
                    new SqlParameter("@status",ui),
                    new SqlParameter("@tuid",txtuid.Text),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_tripMUpdate", para, con);
            }
            MessageBox.Show("Record  Saved ");
            ClearTxt();
            con.Close();
            Dgvlist.Rows.Clear();

            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grid();

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnDelete.Visible = true;
            btnaddrcan.Visible = false;

        }
        private void TxtfeeM_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;


                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txttime1.Focus();

                    if (TxtfeeM.Tag.ToString() != "")
                    {
                        LoadBoardingGrid();
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            Dgvlist.Rows.Clear();
            txtroute.Text = string.Empty;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Save";
            chk.Checked = true;
            txtroute.Focus();
        }

        private void TxtfeeM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("name LIKE '%{0}%' ", TxtfeeM.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Titlep()
        {

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = 7;
            Dgvlist.Columns[0].Name = "Ruid";
            Dgvlist.Columns[0].HeaderText = "Ruid";
            Dgvlist.Columns[0].DataPropertyName = "Ruid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "slno";
            Dgvlist.Columns[1].HeaderText = "slno";
            Dgvlist.Columns[1].DataPropertyName = "slno";
            Dgvlist.Columns[1].Width = 50;

            Dgvlist.Columns[2].Name = "BoardName";
            Dgvlist.Columns[2].HeaderText = "BoardName";
            Dgvlist.Columns[2].DataPropertyName = "BoardName";
            Dgvlist.Columns[2].Width = 268;

            Dgvlist.Columns[3].Name = "Time1";
            Dgvlist.Columns[3].HeaderText = "Time1";
            Dgvlist.Columns[3].DataPropertyName = "Time1";
            Dgvlist.Columns[3].Width = 100;

            Dgvlist.Columns[4].Name = "Time2";
            Dgvlist.Columns[4].HeaderText = "Time2";
            Dgvlist.Columns[4].DataPropertyName = "Time2";
            Dgvlist.Columns[4].Width = 100;

            Dgvlist.Columns[5].Name = "Strength";
            Dgvlist.Columns[5].HeaderText = "Strength";
            Dgvlist.Columns[5].DataPropertyName = "Strength";
            Dgvlist.Columns[5].Width = 100;
            Dgvlist.Columns[6].Name = "boardid";
            Dgvlist.Columns[6].HeaderText = "boardid";
            Dgvlist.Columns[6].DataPropertyName = "boardid";
            Dgvlist.Columns[6].Visible = false;
        }

        private void Btnok_Click(object sender, EventArgs e)
        {

            if (TxtfeeM.Text == "")
            {
                MessageBox.Show("Select the Boarding Points");
                TxtfeeM.Focus();
                return;
            }
            if (txtstrength.Text == "")
            {
                MessageBox.Show("Enter the strength");
                txtstrength.Focus();
                return;
            }
            string qur = "select  buid,Boardingp  from boardingpm where buid=" + TxtfeeM.Tag + "";
            cmd = new SqlCommand(qur, con);
            SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {

                slno = Dgvlist.Rows.Count + 1 - 1;


                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = '0';
                Dgvlist.Rows[index].Cells[1].Value = slno;
                Dgvlist.Rows[index].Cells[2].Value = tap1.Rows[i]["Boardingp"].ToString();
                Dgvlist.Rows[index].Cells[3].Value = txttime1.Text;
                Dgvlist.Rows[index].Cells[4].Value = txtend.Text;
                Dgvlist.Rows[index].Cells[5].Value = txtstrength.Text;
                Dgvlist.Rows[index].Cells[6].Value = tap1.Rows[i]["buid"].ToString();
            }
            ClearTxt();
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnDelete.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtroute.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            TxtfeeM.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            TxtfeeM.Tag = Dgv.Rows[i].Cells[3].Value.ToString();
            LoadBoardingGrid();
            txttime1.Tag = Dgv.Rows[i].Cells[4].Value.ToString();
            txtstart.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            txtend.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            txttime1.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            if (Dgv.Rows[i].Cells[7].Value.ToString() == "1")
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Update";
        }

        public void ClearTxt()
        {
            TxtfeeM.Text = "";
            txtend.Text = "";
            txttime1.Text = "";
            txtstrength.Text = "";
        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select Tuid,TripName,b.Name,routid,busid,start,Endt,a.status,c.name as busname from tripM a inner join routem b on a.routid=b.ruid  inner join busm c on a.busid=c.buid  where  a.TripName like '%" + txtscr.Text + "%' or   c.name like '%" + txtscr.Text + "%'  or b.name like '%" + txtscr.Text + "%' or a.start like '%" + txtscr.Text + "%'   or a.endt like '%" + txtscr.Text + "%'     ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 9;

                Dgv.Columns[0].Name = "Tuid";
                Dgv.Columns[0].HeaderText = "Tuid";
                Dgv.Columns[0].DataPropertyName = "Tuid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "TripName";
                Dgv.Columns[1].HeaderText = "Route Name";
                Dgv.Columns[1].DataPropertyName = "TripName";
                Dgv.Columns[1].Width = 180;

                Dgv.Columns[2].Name = "Name";
                Dgv.Columns[2].HeaderText = "Route Name";
                Dgv.Columns[2].DataPropertyName = "Name";
                Dgv.Columns[2].Width = 180;

                Dgv.Columns[3].Name = "routid";
                Dgv.Columns[3].HeaderText = "routid";
                Dgv.Columns[3].DataPropertyName = "routid";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].Name = "busid";
                Dgv.Columns[4].HeaderText = "busid";
                Dgv.Columns[4].DataPropertyName = "busid";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].Name = "start";
                Dgv.Columns[5].HeaderText = "start";
                Dgv.Columns[5].DataPropertyName = "start";
                Dgv.Columns[5].Width = 50;

                Dgv.Columns[6].Name = "Endt";
                Dgv.Columns[6].HeaderText = "End";
                Dgv.Columns[6].DataPropertyName = "Endt";
                Dgv.Columns[6].Width = 50;

                Dgv.Columns[7].Name = "status";
                Dgv.Columns[7].HeaderText = "status";
                Dgv.Columns[7].DataPropertyName = "status";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "busname";
                Dgv.Columns[8].HeaderText = "busname";
                Dgv.Columns[8].DataPropertyName = "busname";
                Dgv.Columns[8].Width = 120;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void TxtfeeM_MouseClick(object sender, MouseEventArgs e)
        {
            DataTable dt = GetParty();
            bsParty.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(TxtfeeM);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void Txtclass_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }
        protected void Getemp()
        {
            try
            {
                con.Close();
                con.Open();
                string Query = "Select buid,name from busM order by buid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["name"].ToString();
                        coll.Add(RegNo);
                    }
                }
                txttime1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txttime1.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txttime1.AutoCompleteCustomSource = coll;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void TxtfeeM_Leave(object sender, EventArgs e)
        {
        }
        protected DataTable GetParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetRoute", con);
                bsParty.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "RUid";
                DataGridCommon.Columns[0].HeaderText = "RUid";
                DataGridCommon.Columns[0].DataPropertyName = "RUid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 200;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        private void LoadBoardingGrid()
        {
            con.Close();
            con.Open();

            if (TxtfeeM.Tag != null)
            {
                string qur = "select b.Ruid,b.slno,c.Boardingp,b.strength from routem a inner join routed b on a.Ruid=b.headid inner join boardingpm c on b.boardid=c.buid where a.Ruid=" + TxtfeeM.Tag + "";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                adpt.SelectCommand = cmd;
                adpt.Fill(dt);

                Dgvlist.AutoGenerateColumns = false;
                Dgvlist.Refresh();
                Dgvlist.DataSource = null;
                Dgvlist.Rows.Clear();
                Dgvlist.ColumnCount = 4;
                Dgvlist.Columns[0].Name = "Ruid";
                Dgvlist.Columns[0].HeaderText = "Ruid";
                Dgvlist.Columns[0].DataPropertyName = "Ruid";
                Dgvlist.Columns[0].Visible = false;

                Dgvlist.Columns[1].Name = "slno";
                Dgvlist.Columns[1].HeaderText = "slno";
                Dgvlist.Columns[1].DataPropertyName = "slno";
                Dgvlist.Columns[1].Width = 100;

                Dgvlist.Columns[2].Name = "Boardingp";
                Dgvlist.Columns[2].HeaderText = "Boarding Name";
                Dgvlist.Columns[2].DataPropertyName = "Boardingp";
                Dgvlist.Columns[2].Width = 250;

                Dgvlist.Columns[3].Name = "strength";
                Dgvlist.Columns[3].HeaderText = "strength";
                Dgvlist.Columns[3].DataPropertyName = "strength";
                Dgvlist.Columns[3].Width = 100;

                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {
                    i = Dgvlist.Rows.Add();
                    Dgvlist.Rows[i].Cells[0].Value = dt.Tables[0].Rows[i][0].ToString();
                    Dgvlist.Rows[i].Cells[1].Value = dt.Tables[0].Rows[i][1].ToString();
                    Dgvlist.Rows[i].Cells[2].Value = dt.Tables[0].Rows[i][2].ToString();
                    Dgvlist.Rows[i].Cells[3].Value = dt.Tables[0].Rows[i][3].ToString();
                }
            }
        }

        private void Txtstrength_TextChanged(object sender, EventArgs e)
        {
            if (txtstrength.Text != string.Empty)
            {
                LoadBoardingGrid();
            }
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();
                if (TxtfeeM.Tag.ToString() != "")
                {
                    LoadBoardingGrid();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();

                if (TxtfeeM.Tag.ToString() != "")
                {
                    LoadBoardingGrid();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();
                if (TxtfeeM.Tag.ToString() != "")
                {
                    LoadBoardingGrid();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void Txttime1_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txttime1.Text != string.Empty)
                {
                    string Query = "Select Buid,Name from busM  Where Name ='" + txttime1.Text + "'";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if (dt.Rows.Count != 0)
                    {
                        txttime1.Tag = dt.Rows[0]["Buid"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the Trip ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());
                    string Query = "Delete from Tripm Where tuid = @tuid";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@tuid", SqlDbType.Int).Value = Uid;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
