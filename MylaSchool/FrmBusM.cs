﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmBusM : Form
    {
        public FrmBusM()
        {
            InitializeComponent();
        }
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
            btnDelete.Visible = true;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            txtclass.Text = string.Empty;
            txtregno.Text = string.Empty;
            txtseatno.Text = string.Empty;
            dtp.Value = DateTime.Now;
            dti.Value = DateTime.Now;

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
            btnDelete.Visible = false;
            chk.Checked = true;
        }

        private void FrmBusM_Load(object sender, EventArgs e)
        {
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panel1);
            panel1.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Buid, Name, Capacity, FC, InsureDate, Status,Regno  FROM BusM  where status='1' order by name asc";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);


                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);
                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;

                Dgv.Columns[0].Name = "Buid";
                Dgv.Columns[0].HeaderText = "Buid";
                Dgv.Columns[0].DataPropertyName = "Buid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Name";
                Dgv.Columns[1].HeaderText = "Bus Name";
                Dgv.Columns[1].DataPropertyName = "Name";
                Dgv.Columns[1].Width = 440;

                Dgv.Columns[2].DataPropertyName = "Capacity";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "FC";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "InsureDate";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Status";
                Dgv.Columns[5].Visible = false;
                Dgv.Columns[6].DataPropertyName = "Regno";
                Dgv.Columns[6].Visible = false;

                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void Load_INAgrid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Buid, Name, Capacity, FC, InsureDate, Status,Regno  FROM BusM   where status='0' order by name asc";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;

                Dgv.Columns[0].Name = "Buid";
                Dgv.Columns[0].HeaderText = "Buid";
                Dgv.Columns[0].DataPropertyName = "Buid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Name";
                Dgv.Columns[1].HeaderText = "Bus Name";
                Dgv.Columns[1].DataPropertyName = "Name";
                Dgv.Columns[1].Width = 440;

                Dgv.Columns[2].DataPropertyName = "Capacity";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "FC";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "InsureDate";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Status";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].DataPropertyName = "Regno";
                Dgv.Columns[6].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (txtclass.Text == "")
            {
                MessageBox.Show("Enter the Class name");
                txtclass.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from BusM where  Name='" + txtclass.Text + "' ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into BusM values(@Name,@regno,@Capacity,@FC,@InsureDate,@Status)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@Name", SqlDbType.NVarChar).Value = txtclass.Text;
                    cmd.Parameters.AddWithValue("@regno", SqlDbType.NVarChar).Value = txtregno.Text;
                    cmd.Parameters.AddWithValue("@Capacity", SqlDbType.NVarChar).Value = txtseatno.Text;
                    cmd.Parameters.AddWithValue("@FC", SqlDbType.DateTime).Value = dtp.Value;
                    cmd.Parameters.AddWithValue("@InsureDate", SqlDbType.DateTime).Value = dti.Value;
                    
                    if (chk.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@Status", SqlDbType.NVarChar).Value = "1";
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Status", SqlDbType.NVarChar).Value = "0";
                    }
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Bus Details Saved ");
                    txtclass.Text = "";
                    txtseatno.Text = "";
                    txtregno.Text = "";

                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnDelete.Visible = true;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    txtclass.Text = "";
                    txtclass.Focus();
                }

            }
            else
            {
                con.Close();
                string qur = "select * from BusM where Name='" + txtclass.Text + "' and buid <> " + txtuid.Text +"";
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    int i;
                    if (chk.Checked == true)
                    {
                        i = 1;
                    }
                    else
                    {
                        i = 0;
                    }
                 
                    string QueryUpdate = "Update BusM set Name='" + txtclass.Text + "',Capacity='" + txtseatno.Text + "',FC='" + dtp.Text + "',InsureDate='" + dti.Text + "',Regno='" + txtregno.Text + "',Status=" + i + " where buid='" + txtuid.Text + "'";
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    txtclass.Text = "";
                    txtseatno.Text = "";
                    txtregno.Text = "";

                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnDelete.Visible = true;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }
                btnsave.Text = "Save";
            }
            chkina.Checked = false;
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtclass.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtseatno.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            dtp.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            dti.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            txtregno.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            if (Dgv.Rows[i].Cells[5].Value.ToString() == "1")
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
            btnsave.Text = "Update";
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Cid, Class_Desc, Sqn_no, s_no, ClassType, Grade,active FROM  Class_Mast where Class_Desc like '%" + txtscr.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;
                Dgv.Columns[0].Name = "Cid";
                Dgv.Columns[0].HeaderText = "Cid";
                Dgv.Columns[0].DataPropertyName = "Cid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Class_Desc";
                Dgv.Columns[1].HeaderText = "Class Name";
                Dgv.Columns[1].DataPropertyName = "Class_Desc";
                Dgv.Columns[1].Width = 250;

                Dgv.Columns[2].DataPropertyName = "Sqn_no";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "s_no";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "ClassType";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Grade";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].DataPropertyName = "active";
                Dgv.Columns[6].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void Cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbogrde_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboser_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Chkina_CheckedChanged(object sender, EventArgs e)
        {
            if (chkina.Checked == true)
            {
                Load_INAgrid();
            }
            else if (chkina.Checked == false)
            {
                Load_grid();
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the class ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());
                    string Query = "Delete from busm Where buid = @buid";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@buid", SqlDbType.Int).Value = Uid;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
