﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmTimetable : Form
    {
        public FrmTimetable()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsClass = new BindingSource();
        BindingSource bsSubject = new BindingSource();
        int SelectId = 0; int Fillid = 0;
        int RowIndex = 0; int ColumnIndex = 0;
        private void FrmTimetable_Load(object sender, EventArgs e)
        {
            loadGrid();
        }
        private void loadGrid()
        {
            DataGridTimeTable.AutoGenerateColumns = false;
            DataGridTimeTable.DataSource = null;
            DataGridTimeTable.ColumnCount = 7;

            int index = DataGridTimeTable.Rows.Add();
            DataGridViewRow row = DataGridTimeTable.Rows[index];
            row.Cells[0].Value = "I";
            row.Cells[1].Value = "";
            row.Cells[2].Value = "";
            row.Cells[3].Value = "";
            row.Cells[4].Value = "";
            row.Cells[5].Value = "";
            row.Cells[6].Value = "";

            int index1 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row1 = DataGridTimeTable.Rows[index1];
            row1.Cells[0].Value = "II";
            row1.Cells[1].Value = "";
            row1.Cells[2].Value = "";
            row1.Cells[3].Value = "";
            row1.Cells[4].Value = "";
            row1.Cells[5].Value = "";
            row1.Cells[6].Value = "";

            int index2 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row2 = DataGridTimeTable.Rows[index2];
            row2.Cells[0].Value = "III";
            row2.Cells[1].Value = "";
            row2.Cells[2].Value = "";
            row2.Cells[3].Value = "";
            row2.Cells[4].Value = "";
            row2.Cells[5].Value = "";
            row2.Cells[6].Value = "";

            int index3 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row3 = DataGridTimeTable.Rows[index3];
            row3.Cells[0].Value = "VI";
            row3.Cells[1].Value = "";
            row3.Cells[2].Value = "";
            row3.Cells[3].Value = "";
            row3.Cells[4].Value = "";
            row3.Cells[5].Value = "";
            row3.Cells[6].Value = "";


            int index4 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row4 = DataGridTimeTable.Rows[index4];
            row4.Cells[0].Value = "V";
            row4.Cells[1].Value = "";
            row4.Cells[2].Value = "";
            row4.Cells[3].Value = "";
            row4.Cells[4].Value = "";
            row4.Cells[5].Value = "";
            row4.Cells[6].Value = "";

            int index5 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row5 = DataGridTimeTable.Rows[index5];
            row5.Cells[0].Value = "VI";
            row5.Cells[1].Value = "";
            row5.Cells[2].Value = "";
            row5.Cells[3].Value = "";
            row5.Cells[4].Value = "";
            row5.Cells[5].Value = "";
            row5.Cells[6].Value = "";

            int index6 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row6 = DataGridTimeTable.Rows[index6];
            row6.Cells[0].Value = "VII";
            row6.Cells[1].Value = "";
            row6.Cells[2].Value = "";
            row6.Cells[3].Value = "";
            row6.Cells[4].Value = "";
            row6.Cells[5].Value = "";
            row6.Cells[6].Value = "";


            int index7 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row7 = DataGridTimeTable.Rows[index7];
            row7.Cells[0].Value = "VIII";
            row7.Cells[1].Value = "";
            row7.Cells[2].Value = "";
            row7.Cells[3].Value = "";
            row7.Cells[4].Value = "";
            row7.Cells[5].Value = "";
            row7.Cells[6].Value = "";

            int index8 = DataGridTimeTable.Rows.Add();
            DataGridViewRow row8 = DataGridTimeTable.Rows[index8];
            row8.Cells[0].Value = "IX";
            row8.Cells[1].Value = "";
            row8.Cells[2].Value = "";
            row8.Cells[3].Value = "";
            row8.Cells[4].Value = "";
            row8.Cells[5].Value = "";
            row8.Cells[6].Value = "";
        }

        private void txtClass_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", con);
                bsClass.DataSource = dt;
                Fillgrid(1, dt);
                Point point = FindLocation(txtClass);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Fillgrid(int id, DataTable data)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "CId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Class_Desc";
                    DataGridCommon.Columns[1].HeaderText = "Class";
                    DataGridCommon.Columns[1].DataPropertyName = "Class_Desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsClass;
                }
                else
                {
                    Fillid = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 4;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "FacId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Uid";
                    DataGridCommon.Columns[1].HeaderText = "Uid";
                    DataGridCommon.Columns[1].DataPropertyName = "Subid";
                    DataGridCommon.Columns[1].Visible = false;

                    DataGridCommon.Columns[2].Name = "Fac_Name";
                    DataGridCommon.Columns[2].HeaderText = "Facultys";
                    DataGridCommon.Columns[2].DataPropertyName = "Fac_Name";
                    DataGridCommon.Columns[2].Width = 160;

                    DataGridCommon.Columns[3].Name = "Sub_desc";
                    DataGridCommon.Columns[3].HeaderText = "Subject";
                    DataGridCommon.Columns[3].DataPropertyName = "Sub_desc";
                    DataGridCommon.Columns[3].Width = 160;
                    DataGridCommon.DataSource = bsSubject;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    string Query = "Select  * from TimeTable Where Class_id =" + txtClass.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if (dt.Rows.Count == 0)
                    {
                        FillTimeTable(dt);
                        InsertTimeTable();
                    }
                    else
                    {
                        SqlParameter[] parameters = { new SqlParameter("@ClassId", Convert.ToInt32(txtClass.Tag.ToString())) };
                        DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPostedTimeTable", parameters, con);
                        if (dataTable.Rows.Count > 0)
                        {
                            FillTimeTable(dataTable);
                        }
                    }
                }
                else
                {
                    DataGridTimeTable.Rows[RowIndex].Cells[ColumnIndex].Value = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    UpdateTimeTable(RowIndex, ColumnIndex, Convert.ToInt32(DataGridCommon.Rows[Index].Cells[1].Value.ToString()));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillTimeTable(DataTable dataTable)
        {
            try
            {
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if(i == 0)
                    {
                        DataGridTimeTable.Rows[0].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[0].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[0].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[0].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[0].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[0].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if(i == 1)
                    {
                        DataGridTimeTable.Rows[1].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[1].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[1].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[1].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[1].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[1].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 2)
                    {
                        DataGridTimeTable.Rows[2].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[2].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[2].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[2].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[2].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[2].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 3)
                    {
                        DataGridTimeTable.Rows[3].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[3].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[3].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[3].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[3].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[3].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 4)
                    {
                        DataGridTimeTable.Rows[4].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[4].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[4].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[4].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[4].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[4].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 5)
                    {
                        DataGridTimeTable.Rows[5].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[5].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[5].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[5].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[5].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[5].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 6)
                    {
                        DataGridTimeTable.Rows[6].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[6].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[6].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[6].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[6].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[6].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 7)
                    {
                        DataGridTimeTable.Rows[7].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[7].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[7].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[7].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[7].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[7].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                    else if (i == 8)
                    {
                        DataGridTimeTable.Rows[8].Cells[1].Value = dataTable.Rows[i]["Monday"].ToString();
                        DataGridTimeTable.Rows[8].Cells[2].Value = dataTable.Rows[i]["Tuesday"].ToString();
                        DataGridTimeTable.Rows[8].Cells[3].Value = dataTable.Rows[i]["WedDay"].ToString();
                        DataGridTimeTable.Rows[8].Cells[4].Value = dataTable.Rows[i]["Thursday"].ToString();
                        DataGridTimeTable.Rows[8].Cells[5].Value = dataTable.Rows[i]["Friday"].ToString();
                        DataGridTimeTable.Rows[8].Cells[6].Value = dataTable.Rows[i]["Satday"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    string Query = "Select  * from TimeTable Where Class_id =" + txtClass.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if(dt.Rows.Count == 0)
                    {
                        FillTimeTable(dt);
                        InsertTimeTable();
                    }
                    else
                    {
                        SqlParameter[] parameters = { new SqlParameter("@ClassId", Convert.ToInt32(txtClass.Tag.ToString())) };
                        DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPostedTimeTable", parameters, con);
                        if (dataTable.Rows.Count > 0)
                        {
                            FillTimeTable(dataTable);
                        }
                    }
                }
                else
                {
                    DataGridTimeTable.Rows[RowIndex].Cells[ColumnIndex].Value = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    UpdateTimeTable(RowIndex, ColumnIndex, Convert.ToInt32(DataGridCommon.Rows[Index].Cells[1].Value.ToString()));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {

        }

        private void DataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                LoadSubjects();
                DataGridViewCell cell = DataGridTimeTable.CurrentCell;
                Rectangle r = DataGridTimeTable.GetCellDisplayRectangle(DataGridTimeTable.CurrentCell.ColumnIndex, DataGridTimeTable.CurrentCell.RowIndex, false);
                if (txtClass.Text != string.Empty)
                {
                    if (e.RowIndex == 0 && e.ColumnIndex == 1 || e.RowIndex == 0 && e.ColumnIndex == 1 || e.RowIndex == 3 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y + 100);
                    }
                    else if (e.RowIndex == 2 && e.ColumnIndex == 1 || e.RowIndex == 2 && e.ColumnIndex == 2 || e.RowIndex == 2 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y + 100);
                    }
                    else if (e.RowIndex == 3 && e.ColumnIndex == 1 || e.RowIndex == 3 && e.ColumnIndex == 2 || e.RowIndex == 3 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y + 100);
                    }
                    else if (e.RowIndex == 4 && e.ColumnIndex == 1 || e.RowIndex == 4 && e.ColumnIndex == 2 || e.RowIndex == 4 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y - 260);
                    }
                    else if (e.RowIndex == 5 && e.ColumnIndex == 1 || e.RowIndex == 5 && e.ColumnIndex == 2 || e.RowIndex == 5 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y - 260);
                    }
                    else if (e.RowIndex == 6 && e.ColumnIndex == 1 || e.RowIndex == 6 && e.ColumnIndex == 2 || e.RowIndex == 6 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y - 260);
                    }
                    else if (e.RowIndex == 7 && e.ColumnIndex == 1 || e.RowIndex == 7 && e.ColumnIndex == 2 || e.RowIndex == 7 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y - 260);
                    }
                    else if (e.RowIndex == 8 && e.ColumnIndex == 1 || e.RowIndex == 8 && e.ColumnIndex == 2 || e.RowIndex == 8 && e.ColumnIndex == 3)
                    {
                        grSearch.Location = new Point(r.X, r.Y - 260);
                    }
                    else if (e.RowIndex == 0 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y + 100);
                    }
                    else if (e.RowIndex == 0 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y + 100);
                    }
                    else if (e.RowIndex == 0 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y + 100);
                    }
                    else if (e.RowIndex == 1 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y + 100);
                    }
                    else if (e.RowIndex == 1 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y + 100);
                    }
                    else if (e.RowIndex == 1 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y + 100);
                    }
                    else if (e.RowIndex == 2 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y + 100);
                    }
                    else if (e.RowIndex == 2 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y + 100);
                    }
                    else if (e.RowIndex == 2 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y + 100);
                    }
                    else if (e.RowIndex == 3 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y + 100);
                    }
                    else if (e.RowIndex == 3 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y + 100);
                    }
                    else if (e.RowIndex == 3 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y + 100);
                    }
                    else if (e.RowIndex == 4 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y - 260);
                    }
                    else if (e.RowIndex == 4 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y - 260);
                    }
                    else if (e.RowIndex == 4 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y - 260);
                    }
                    else if (e.RowIndex == 5 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y - 260);
                    }
                    else if (e.RowIndex == 5 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y - 260);
                    }
                    else if (e.RowIndex == 5 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y - 260);
                    }
                    else if (e.RowIndex == 6 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y - 260);
                    }
                    else if (e.RowIndex == 6 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y - 260);
                    }
                    else if (e.RowIndex == 6 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y - 260);
                    }
                    else if (e.RowIndex == 7 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y - 260);
                    }
                    else if (e.RowIndex == 7 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y - 260);
                    }
                    else if (e.RowIndex == 7 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y - 260);
                    }
                    else if (e.RowIndex == 8 && e.ColumnIndex == 4)
                    {
                        grSearch.Location = new Point(r.X - 100, r.Y - 260);
                    }
                    else if (e.RowIndex == 8 && e.ColumnIndex == 5)
                    {
                        grSearch.Location = new Point(r.X - 200, r.Y - 260);
                    }
                    else if (e.RowIndex == 8 && e.ColumnIndex == 6)
                    {
                        grSearch.Location = new Point(r.X - 300, r.Y - 260);
                    }
                    else
                    {
                        grSearch.Location = new Point(r.X, r.Y + 100);
                    }
                    grSearch.Visible = true;
                    grSearch.Text = "Subject Search";
                    RowIndex = e.RowIndex;
                    ColumnIndex = e.ColumnIndex;
                }
                else
                {
                    MessageBox.Show("Select Class", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadSubjects()
        {
            try
            {
                if (txtClass.Text != string.Empty)
                {
                    SqlParameter[] parameters = { new SqlParameter("@ClassId", txtClass.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetTimeTable", parameters, con);
                    bsSubject.DataSource = dt;
                    Fillgrid(2, dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void InsertTimeTable()
        {
            try
            {
                for (int i = 0; i < 9; i++)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Period",i + 1),
                        new SqlParameter("@Monday",DBNull.Value),
                        new SqlParameter("@Tuesday",DBNull.Value),
                        new SqlParameter("@WedDay",DBNull.Value),
                        new SqlParameter("@Thursday",DBNull.Value),
                        new SqlParameter("@Friday",DBNull.Value),
                        new SqlParameter("@Satday",DBNull.Value),
                        new SqlParameter("@Class_Id",txtClass.Tag)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_TimeTable", parameters, con);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void UpdateTimeTable(int RowIndex,int ColumnIndex,int SubjectId)
        {
            try
            {
                string Query = string.Empty;
                if(RowIndex == 0 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 1";
                }
                else if (RowIndex == 0 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 1";
                }
                else if (RowIndex == 0 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 1";
                }
                else if (RowIndex == 0 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 1";
                }
                else if (RowIndex == 0 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 1";
                }
                else if (RowIndex == 0 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 1";
                }
                else if (RowIndex == 1 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 2";
                }
                else if (RowIndex == 1 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 2";
                }
                else if (RowIndex == 1 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 2";
                }
                else if (RowIndex == 1 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 2";
                }
                else if (RowIndex == 1 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 2";
                }
                else if (RowIndex == 1 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 2";
                }
                else if (RowIndex == 2 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 3";
                }
                else if (RowIndex == 2 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 3";
                }
                else if (RowIndex == 2 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 3";
                }
                else if (RowIndex == 2 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 3";
                }
                else if (RowIndex == 2 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 3";
                }
                else if (RowIndex == 2 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 3";
                }
                else if (RowIndex == 3 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 4";
                }
                else if (RowIndex == 3  && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 4";
                }
                else if (RowIndex == 3 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 4";
                }
                else if (RowIndex == 3 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 4";
                }
                else if (RowIndex == 3 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 4";
                }
                else if (RowIndex == 3 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 4";
                }
                else if (RowIndex == 4 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 5";
                }
                else if (RowIndex == 4 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 5";
                }
                else if (RowIndex == 4 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 5";
                }
                else if (RowIndex == 4 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 5";
                }
                else if (RowIndex == 4 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 5";
                }
                else if (RowIndex == 4 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 5";
                }
                else if (RowIndex == 5 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 6";
                }
                else if (RowIndex == 5 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 6";
                }
                else if (RowIndex == 5 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 6";
                }
                else if (RowIndex == 5 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 6";
                }
                else if (RowIndex == 5 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 6";
                }
                else if (RowIndex == 5 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 6";
                }
                else if (RowIndex == 6 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 7";
                }
                else if (RowIndex == 6 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 7";
                }
                else if (RowIndex == 6 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 7";
                }
                else if (RowIndex == 6 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 7";
                }
                else if (RowIndex == 6 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 7";
                }
                else if (RowIndex == 6 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 7";
                }
                else if (RowIndex == 7 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 8";
                }
                else if (RowIndex == 7 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 8";
                }
                else if (RowIndex == 7 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 8";
                }
                else if (RowIndex == 7 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 8";
                }
                else if (RowIndex == 7 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 8";
                }
                else if (RowIndex == 7 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 8";
                }
                else if (RowIndex == 8 && ColumnIndex == 1)
                {
                    Query = "Update TimeTable Set Monday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 9";
                }
                else if (RowIndex == 8 && ColumnIndex == 2)
                {
                    Query = "Update TimeTable Set Tuesday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 9";
                }
                else if (RowIndex == 8 && ColumnIndex == 3)
                {
                    Query = "Update TimeTable Set WedDay =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 9";
                }
                else if (RowIndex == 8 && ColumnIndex == 4)
                {
                    Query = "Update TimeTable Set Thursday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 9";
                }
                else if (RowIndex == 8 && ColumnIndex == 5)
                {
                    Query = "Update TimeTable Set Friday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 9";
                }
                else if (RowIndex == 8 && ColumnIndex == 6)
                {
                    Query = "Update TimeTable Set Satday =" + SubjectId + " Where Class_id =" + txtClass.Tag + " and Period = 9";
                }
                else
                {
                    Query = string.Empty;
                }
                if(con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                SqlCommand cmd = new SqlCommand(Query, con);
                con.Open();
                cmd.Connection = con;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("Class_Desc LIKE '%{0}%' ", txtClass.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
