﻿namespace MylaSchool
{
    partial class FrmFeeStruct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFeeStruct));
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.GBList = new System.Windows.Forms.GroupBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.txtfuid = new System.Windows.Forms.TextBox();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.btnclass = new System.Windows.Forms.Button();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.DgvClass = new System.Windows.Forms.DataGridView();
            this.Dgvlist = new System.Windows.Forms.DataGridView();
            this.btnok = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.TxtfeeM = new System.Windows.Forms.TextBox();
            this.txtdesc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.GBList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.Color.White;
            this.GBMain.Controls.Add(this.txtscr1);
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.txtscr);
            this.GBMain.Controls.Add(this.Dgv);
            this.GBMain.Location = new System.Drawing.Point(0, 12);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(890, 461);
            this.GBMain.TabIndex = 2;
            this.GBMain.TabStop = false;
            this.GBMain.Enter += new System.EventHandler(this.GBMain_Enter);
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(458, 10);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(329, 26);
            this.txtscr1.TabIndex = 90;
            this.txtscr1.TextChanged += new System.EventHandler(this.Txtscr1_TextChanged);
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(788, 9);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(82, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.Btnser_Click);
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(13, 11);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(442, 26);
            this.txtscr.TabIndex = 87;
            this.txtscr.TextChanged += new System.EventHandler(this.Txtscr_TextChanged);
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(11, 40);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(859, 408);
            this.Dgv.TabIndex = 0;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(827, 1);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(60, 30);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Visible = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(768, 1);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(60, 30);
            this.btnedit.TabIndex = 85;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Visible = false;
            this.btnedit.Click += new System.EventHandler(this.Btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(709, 1);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(60, 30);
            this.btnadd.TabIndex = 84;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.Btnadd_Click);
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.Color.White;
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.label4);
            this.GBList.Controls.Add(this.txttotal);
            this.GBList.Controls.Add(this.txtfuid);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.btnclass);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.DgvClass);
            this.GBList.Controls.Add(this.Dgvlist);
            this.GBList.Controls.Add(this.btnok);
            this.GBList.Controls.Add(this.txtamt);
            this.GBList.Controls.Add(this.TxtfeeM);
            this.GBList.Controls.Add(this.txtdesc);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.cbotype);
            this.GBList.Controls.Add(this.txtuid);
            this.GBList.Location = new System.Drawing.Point(0, 12);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(890, 459);
            this.GBList.TabIndex = 3;
            this.GBList.TabStop = false;
            // 
            // txtssno
            // 
            this.txtssno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtssno.Location = new System.Drawing.Point(818, 24);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 26);
            this.txtssno.TabIndex = 141;
            this.txtssno.Visible = false;
            // 
            // cboTerm
            // 
            this.cboTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(116, 14);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(476, 26);
            this.cboTerm.TabIndex = 0;
            this.cboTerm.SelectedIndexChanged += new System.EventHandler(this.CboTerm_SelectedIndexChanged);
            this.cboTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboTerm_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(68, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 113;
            this.label3.Text = "Term";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(400, 399);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 18);
            this.label4.TabIndex = 140;
            this.label4.Text = "Total";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txttotal
            // 
            this.txttotal.BackColor = System.Drawing.SystemColors.Window;
            this.txttotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotal.Location = new System.Drawing.Point(446, 393);
            this.txttotal.MaxLength = 250;
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(145, 26);
            this.txttotal.TabIndex = 6;
            // 
            // txtfuid
            // 
            this.txtfuid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfuid.Location = new System.Drawing.Point(641, 24);
            this.txtfuid.Name = "txtfuid";
            this.txtfuid.Size = new System.Drawing.Size(35, 26);
            this.txtfuid.TabIndex = 132;
            this.txtfuid.Visible = false;
            // 
            // txtcid
            // 
            this.txtcid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcid.Location = new System.Drawing.Point(682, 24);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 26);
            this.txtcid.TabIndex = 131;
            this.txtcid.Visible = false;
            // 
            // btnclass
            // 
            this.btnclass.BackColor = System.Drawing.Color.White;
            this.btnclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclass.Image = ((System.Drawing.Image)(resources.GetObject("btnclass.Image")));
            this.btnclass.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnclass.Location = new System.Drawing.Point(762, 82);
            this.btnclass.Name = "btnclass";
            this.btnclass.Size = new System.Drawing.Size(60, 30);
            this.btnclass.TabIndex = 130;
            this.btnclass.Text = "Add";
            this.btnclass.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnclass.UseVisualStyleBackColor = false;
            this.btnclass.Click += new System.EventHandler(this.Btnclass_Click);
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(602, 84);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(156, 26);
            this.txtclass.TabIndex = 5;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtclass_MouseClick);
            this.txtclass.TextChanged += new System.EventHandler(this.Txtclass_TextChanged);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtclass_KeyDown);
            // 
            // DgvClass
            // 
            this.DgvClass.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DgvClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvClass.Location = new System.Drawing.Point(602, 113);
            this.DgvClass.Name = "DgvClass";
            this.DgvClass.ReadOnly = true;
            this.DgvClass.RowHeadersVisible = false;
            this.DgvClass.Size = new System.Drawing.Size(220, 274);
            this.DgvClass.TabIndex = 128;
            // 
            // Dgvlist
            // 
            this.Dgvlist.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgvlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgvlist.Location = new System.Drawing.Point(11, 113);
            this.Dgvlist.Name = "Dgvlist";
            this.Dgvlist.RowHeadersVisible = false;
            this.Dgvlist.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Dgvlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgvlist.Size = new System.Drawing.Size(581, 274);
            this.Dgvlist.TabIndex = 127;
            this.Dgvlist.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgvlist_CellClick);
            this.Dgvlist.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgvlist_CellContentClick);
            this.Dgvlist.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgvlist_CellEndEdit);
            this.Dgvlist.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Dgvlist_KeyDown);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.White;
            this.btnok.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Image = ((System.Drawing.Image)(resources.GetObject("btnok.Image")));
            this.btnok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnok.Location = new System.Drawing.Point(543, 83);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(51, 28);
            this.btnok.TabIndex = 126;
            this.btnok.Text = "ok";
            this.btnok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.Btnok_Click);
            // 
            // txtamt
            // 
            this.txtamt.BackColor = System.Drawing.SystemColors.Window;
            this.txtamt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(421, 84);
            this.txtamt.MaxLength = 250;
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(121, 26);
            this.txtamt.TabIndex = 4;
            // 
            // TxtfeeM
            // 
            this.TxtfeeM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TxtfeeM.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtfeeM.Location = new System.Drawing.Point(12, 84);
            this.TxtfeeM.MaxLength = 250;
            this.TxtfeeM.Name = "TxtfeeM";
            this.TxtfeeM.ReadOnly = true;
            this.TxtfeeM.Size = new System.Drawing.Size(268, 26);
            this.TxtfeeM.TabIndex = 2;
            this.TxtfeeM.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtfeeM_MouseClick);
            this.TxtfeeM.TextChanged += new System.EventHandler(this.TxtfeeM_TextChanged);
            this.TxtfeeM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtfeeM_KeyDown);
            // 
            // txtdesc
            // 
            this.txtdesc.BackColor = System.Drawing.SystemColors.Window;
            this.txtdesc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdesc.Location = new System.Drawing.Point(116, 54);
            this.txtdesc.MaxLength = 250;
            this.txtdesc.Name = "txtdesc";
            this.txtdesc.Size = new System.Drawing.Size(237, 26);
            this.txtdesc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 18);
            this.label1.TabIndex = 117;
            this.label1.Text = "Class Structure";
            // 
            // cbotype
            // 
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Items.AddRange(new object[] {
            "ALL",
            "Boys",
            "Girls"});
            this.cbotype.Location = new System.Drawing.Point(284, 84);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(135, 26);
            this.cbotype.TabIndex = 3;
            this.cbotype.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbotype_KeyPress);
            // 
            // txtuid
            // 
            this.txtuid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuid.Location = new System.Drawing.Point(740, 24);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(35, 26);
            this.txtuid.TabIndex = 109;
            this.txtuid.Visible = false;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(748, 1);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 30);
            this.btnsave.TabIndex = 123;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(822, 1);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(65, 30);
            this.btnaddrcan.TabIndex = 122;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Location = new System.Drawing.Point(0, 476);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(894, 32);
            this.panadd.TabIndex = 115;
            // 
            // FrmFeeStruct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(894, 513);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GBList);
            this.Controls.Add(this.GBMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmFeeStruct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fee Structure";
            this.Load += new System.EventHandler(this.FrmFeeStruct_Load);
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).EndInit();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBMain;
        internal System.Windows.Forms.TextBox txtscr1;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        internal System.Windows.Forms.TextBox txtdesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.ComboBox cboTerm;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox TxtfeeM;
        internal System.Windows.Forms.TextBox txtamt;
        internal System.Windows.Forms.Button btnok;
        private System.Windows.Forms.DataGridView Dgvlist;
        private System.Windows.Forms.DataGridView DgvClass;
        internal System.Windows.Forms.TextBox txtclass;
        internal System.Windows.Forms.Button btnclass;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.TextBox txtfuid;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Panel panadd;
    }
}