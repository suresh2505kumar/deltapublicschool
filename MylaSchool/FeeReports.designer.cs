﻿namespace MylaSchool
{
    partial class FeeReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FeeReports));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.cmdprt = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.GBList.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.Color.White;
            this.GBList.Controls.Add(this.cmdprt);
            this.GBList.Controls.Add(this.btnexit);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.dtp1);
            this.GBList.Controls.Add(this.label6);
            this.GBList.Controls.Add(this.dtp);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Location = new System.Drawing.Point(3, 2);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(383, 182);
            this.GBList.TabIndex = 154;
            this.GBList.TabStop = false;
            // 
            // cmdprt
            // 
            this.cmdprt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdprt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt.Image = ((System.Drawing.Image)(resources.GetObject("cmdprt.Image")));
            this.cmdprt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdprt.Location = new System.Drawing.Point(126, 130);
            this.cmdprt.Name = "cmdprt";
            this.cmdprt.Size = new System.Drawing.Size(65, 30);
            this.cmdprt.TabIndex = 171;
            this.cmdprt.Text = "Print";
            this.cmdprt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt.UseVisualStyleBackColor = false;
            this.cmdprt.Click += new System.EventHandler(this.Cmdprt_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(192, 130);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(62, 30);
            this.btnexit.TabIndex = 170;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(216, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 18);
            this.label1.TabIndex = 169;
            this.label1.Text = "To";
            // 
            // dtp1
            // 
            this.dtp1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp1.Location = new System.Drawing.Point(243, 82);
            this.dtp1.Name = "dtp1";
            this.dtp1.Size = new System.Drawing.Size(104, 26);
            this.dtp1.TabIndex = 168;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 18);
            this.label6.TabIndex = 167;
            this.label6.Text = "From";
            // 
            // dtp
            // 
            this.dtp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp.Location = new System.Drawing.Point(75, 82);
            this.dtp.Name = "dtp";
            this.dtp.Size = new System.Drawing.Size(104, 26);
            this.dtp.TabIndex = 166;
            // 
            // cboTerm
            // 
            this.cboTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Items.AddRange(new object[] {
            "Daily Fee Collection Report",
            "Fee Collection summary"});
            this.cboTerm.Location = new System.Drawing.Point(84, 29);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(227, 26);
            this.cboTerm.TabIndex = 165;
            this.cboTerm.SelectedIndexChanged += new System.EventHandler(this.CboTerm_SelectedIndexChanged);
            // 
            // FeeReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(388, 189);
            this.Controls.Add(this.GBList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FeeReports";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FeeReports";
            this.Load += new System.EventHandler(this.FeeReports_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button cmdprt;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtp;
        private System.Windows.Forms.ComboBox cboTerm;
    }
}