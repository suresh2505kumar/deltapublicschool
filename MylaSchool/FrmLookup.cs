﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmLookup : Form
    {
        public FrmLookup()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void FrmLookup_Load(object sender, EventArgs e)
        {
            Module.canclclik = false;
            this.CenterToScreen();
            Hfgp.RowHeadersVisible = false;
            txtscr1.Focus();
        }
        private void OnRightToLeftChanged()
        {
            throw new NotImplementedException();
        }

        private void Btncle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Module.okclick = true;
            Txtinp();
        }
        private void Txtinp()
        {
            foreach (Form Frm in Application.OpenForms)
            {
                if (Frm.Name == Module.frmwhat)
                {
                    foreach (Control ccontrol in Frm.Controls)
                    {
                        if (ccontrol is GroupBox)
                        {
                            foreach (Control c in Module.Parent.Controls)
                            {
                                if (c is TextBox)
                                {
                                    if (c.Name == (string)Module.fieldone)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[0].Value.ToString();
                                    }
                                    else if (c.Name == (string)Module.fieldtwo)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[1].Value.ToString();
                                    }

                                    else if (c.Name == (string)Module.fieldthree)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[2].Value.ToString();
                                    }
                                    else if (c.Name == (string)Module.fieldFour)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[3].Value.ToString();
                                    }
                                    else if (c.Name == (string)Module.fieldFive)
                                    {
                                        int i = Hfgp.SelectedCells[0].RowIndex;
                                        c.Text = Hfgp.Rows[i].Cells[4].Value.ToString();
                                    }

                                }
                            }
                        }
                    }
                }

            }
            this.Dispose();
            txtscr1.Focus();
        }
        private void Lookupload1()
        {
            try
            {
                Module.strsqltmp = Module.strsql;
                Module.FSSQLSortStrtmp = Module.FSSQLSortStr;
                Module.StrSrch = "";
                if (txtscr1.Text != "")
                {
                    if (Module.StrSrch == "")
                    {
                        Module.StrSrch = Module.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                        Module.strsqltmp = Module.strsqltmp + " where  " + Module.StrSrch + " order by " + Module.FSSQLSortStr + "";
                    }
                    else
                    {
                        Module.StrSrch = Module.StrSrch + " and " + Module.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                        Module.strsqltmp = Module.strsqltmp + " where  " + Module.StrSrch + " order by " + Module.FSSQLSortStr + "";
                    }
                }
                else
                {
                    Module.StrSrch = Module.StrSrch + " order by " + Module.FSSQLSortStr + "";
                    Module.strsqltmp = Module.strsqltmp + Module.StrSrch + "";
                }

                Module.cmd = new SqlCommand(Module.strsqltmp, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                this.Hfgp.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Hfgp.AutoGenerateColumns = false;
                Hfgp.DataSource = null;
                Hfgp.ColumnCount = tap.Columns.Count;
                Module.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    Hfgp.Columns[Module.i].Name = column.ColumnName;
                    Hfgp.Columns[Module.i].HeaderText = column.ColumnName;
                    Hfgp.Columns[Module.i].DataPropertyName = column.ColumnName;
                    Module.i = Module.i + 1;
                }


                Hfgp.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Module.cmd.Dispose();
            }
        }


        private void Lookupload()
        {
            try
            {
                Module.strsqltmp = Module.strsql;
                Module.FSSQLSortStrtmp = Module.FSSQLSortStr;
                Module.StrSrch = "";
                if (txtscr1.Text != "")
                {
                    if (Module.StrSrch == "")
                    {

                        if (!int.TryParse(txtscr1.Text, out int parsedValue) && Module.type == 3 || Module.type == 5 || Module.type == 7 || Module.type == 1 || Module.type == 11 || Module.type == 20 || Module.type == 23 || Module.type == 15 || Module.type == 13 || Module.type == 12 || Module.type == 22 || Module.type == 3)
                        {

                            Module.StrSrch = Module.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                            Module.strsqltmp = "select distinct s_no,Class_Desc,cid from class_mast a inner join FeeStructclass b on a.Cid = b.classuid where active = 1";
                            Module.strsqltmp = Module.strsqltmp + " and  " + Module.StrSrch + " order by " + Module.FSSQLSortStr + "";
                        }
                        else if (!int.TryParse(txtscr1.Text, out parsedValue))
                        {
                            Module.StrSrch = Module.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                            Module.strsqltmp = Module.strsqltmp + " and  " + Module.StrSrch + " order by " + Module.FSSQLSortStr + "";
                        }
                        else
                        {
                            Module.StrSrch = Module.FSSQLSortStrA + " like '%" + txtscr1.Text + "%'";
                            Module.strsqltmp = Module.strsqltmp + " and  " + Module.StrSrch + " order by " + Module.FSSQLSortStrA + "";
                        }
                    }
                    else
                    {
                        Module.StrSrch = Module.StrSrch + " and " + Module.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                        Module.strsqltmp = Module.strsqltmp + " or  " + Module.StrSrch + " order by " + Module.FSSQLSortStr + "";
                    }
                }
                else
                {
                    if (Module.type == 3 || Module.type == 5)
                    {
                        Module.strsqltmp = Module.strsqltmp + Module.StrSrch + "order by s_no";
                    }
                    else
                    {
                        Module.strsqltmp = Module.strsqltmp + Module.StrSrch;
                    }
                }
                Module.cmd = new SqlCommand(Module.strsqltmp, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);                
                Hfgp.AutoGenerateColumns = false;
                Hfgp.DataSource = null;
                Hfgp.ColumnCount = tap.Columns.Count;
                Module.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    Hfgp.Columns[Module.i].Name = column.ColumnName;
                    Hfgp.Columns[Module.i].HeaderText = column.ColumnName;
                    Hfgp.Columns[Module.i].DataPropertyName = column.ColumnName;
                    Module.i = Module.i + 1;
                }
                Hfgp.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Error");
                return;
            }
            finally
            {
                conn.Close();
                Module.cmd.Dispose();
            }
        }
        private void Txtscr1_TextChanged(object sender, EventArgs e)
        {
            Lookupload();
        }

        private void Hfgp_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Module.okclick = true;
            Txtinp();
        }
    }
}
