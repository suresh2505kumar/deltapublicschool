﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmBoardIssue : Form
    {
        public FrmBoardIssue()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsParty = new BindingSource();
        BindingSource bsParty1 = new BindingSource();
        BindingSource bsBoard = new BindingSource();
        public int SelectId = 0;
        public int Fill = 0;
        private void FrmBoardIssue_Load(object sender, EventArgs e)
        {
            LoadFeeGrp();
            Load_grids();
            LoadRefT();
            Getemp1();
            checkBox1.Checked = false;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            chk.Checked = true;
            grSearch.Visible = false;
        }


        private void LoadRefT()
        {
            con.Close();
            con.Open();
            SelectId = 1;
            string qur = "select cid,Class_Desc from Class_Mast";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Class_Desc";
            cboTerm.ValueMember = "cid";
            //cboTerm.SelectedIndex = -1;
            con.Close();
            SelectId = 0;

        }

        private void Load_grids()
        {
            try
            {
                string qur = @"Select puid, a.passno,e.Class_Desc,f.SName,d.BoardingP,b.Name,a.startDate,a.Enddate,a.status,a.classid,a.boardid,a.studid,a.tripid,a.adnochk,a.way  from BoardPIssueM a 
	                        inner join RouteM b on a.TripId = b.Ruid
	                        inner join RouteD c on a.boardid = c.Ruid
                        	inner join BoardingPM d on c.Boardid = d.Buid
	                        inner join Class_Mast e on a.classid = e.Cid 
	                        inner join StudentM f on a.studid = f.uid where a.status='1' order by Puid";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);


                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);

                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 15;

                Dgv.Columns[0].Name = "puid";
                Dgv.Columns[0].HeaderText = "puid";
                Dgv.Columns[0].DataPropertyName = "puid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "passno";
                Dgv.Columns[1].HeaderText = "Passno";
                Dgv.Columns[1].DataPropertyName = "passno";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "Class_Desc";
                Dgv.Columns[2].HeaderText = "Class";
                Dgv.Columns[2].DataPropertyName = "Class_Desc";
                Dgv.Columns[2].Width = 100;

                Dgv.Columns[3].Name = "SName";
                Dgv.Columns[3].HeaderText = "Student";
                Dgv.Columns[3].DataPropertyName = "SName";
                Dgv.Columns[3].Width = 220;

                Dgv.Columns[4].Name = "BoardingP";
                Dgv.Columns[4].HeaderText = "Boarding";
                Dgv.Columns[4].DataPropertyName = "BoardingP";
                Dgv.Columns[4].Width = 150;

                Dgv.Columns[5].Name = "TripName";
                Dgv.Columns[5].HeaderText = "TripName";
                Dgv.Columns[5].DataPropertyName = "Name";
                Dgv.Columns[5].Width = 150;

                Dgv.Columns[6].Name = "startDate";
                Dgv.Columns[6].HeaderText = "StartDate";
                Dgv.Columns[6].DataPropertyName = "startDate";
                Dgv.Columns[6].Visible = false;

                Dgv.Columns[7].Name = "Enddate";
                Dgv.Columns[7].HeaderText = "Enddate";
                Dgv.Columns[7].DataPropertyName = "Enddate";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "Status";
                Dgv.Columns[8].HeaderText = "Status";
                Dgv.Columns[8].DataPropertyName = "Status";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].DataPropertyName = "classid";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].DataPropertyName = "boardid";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].DataPropertyName = "studid";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].DataPropertyName = "tripid";
                Dgv.Columns[12].Visible = false;


                Dgv.Columns[13].DataPropertyName = "adnochk";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].DataPropertyName = "way";
                Dgv.Columns[14].Visible = false;

                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void Txtsname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtsname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtboar.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadFeeGrp()
        {
            SelectId = 1;
            string qur = "select * from RouteM order by Ruid";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            CmbRoute.DataSource = null;
            CmbRoute.DataSource = tap;
            CmbRoute.DisplayMember = "Name";
            CmbRoute.ValueMember = "Ruid";
            SelectId = 0;
            con.Close();
        }

        public void ClearTxt()
        {
            SelectId = 1;
            txtboar.Text = "";
            txtpassno.Text = "";
            txtsname.Text = "";
            CmbRoute.SelectedIndex = -1;
            cboTerm.SelectedIndex = -1;
            SelectId = 0;
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            ClearTxt();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {

            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            checkBox1.Checked = false;
            btnsave.Text = "Save";
            AG();
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Save")
            {
                string ui, ui1;
                if (chk.Checked == true)
                {
                    ui = "1";
                }
                else
                {
                    ui = "0";
                }
                if (checkBox1.Checked == true)
                {
                    ui1 = "1";
                }
                else
                {
                    ui1 = "0";
                }
                con.Close();
                con.Open();

                SqlParameter[] para ={
                    new SqlParameter("@Passno",txtpassno.Text),
                    new SqlParameter("@classid",cboTerm.SelectedValue),
                    new SqlParameter("@studid",txtsname.Tag),
                    new SqlParameter("@boardid",txtboar.Tag),
                    new SqlParameter("@tripid",CmbRoute.SelectedValue),
                    new SqlParameter("@startDate",Convert.ToDateTime(txtrent.Text)),
                    new SqlParameter("@Enddate",Convert.ToDateTime(dtp.Text)),
                    new SqlParameter("@status",ui),
                    new SqlParameter("@adnochk",ui1),
                    new SqlParameter("@way",comboBox1.Text),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BoardPIssueM", para, con);
            }
            else
            {
                string ui, ui1;
                if (chk.Checked == true)
                {
                    ui = "1";
                }
                else
                {
                    ui = "0";
                }
                if (checkBox1.Checked == true)
                {
                    ui1 = "1";
                }
                else
                {
                    ui1 = "0";
                }
                con.Close();
                con.Open();
                SqlParameter[] para ={
                    new SqlParameter("@Passno",txtpassno.Text),
                    new SqlParameter("@classid",cboTerm.SelectedValue),
                    new SqlParameter("@studid",txtsname.Tag),
                    new SqlParameter("@boardid",txtboar.Tag),
                    new SqlParameter("@tripid",CmbRoute.SelectedValue),
                    new SqlParameter("@startDate",Convert.ToDateTime(txtrent.Text)),
                    new SqlParameter("@Enddate",Convert.ToDateTime(dtp.Text)),
                    new SqlParameter("@status",ui),
                    new SqlParameter("@adnochk",ui1),
                    new SqlParameter("@way",comboBox1.Text),
                    new SqlParameter("@puid",txtuid.Text),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BoardPIssueMUpdate", para, con);
            }
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            ClearTxt();
            con.Close();
            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grids();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            grSearch.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtpassno.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtboar.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            CmbRoute.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            txtrent.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            dtp.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            if (Dgv.Rows[i].Cells[8].Value.ToString() == "1")
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
            cboTerm.SelectedValue = Dgv.Rows[i].Cells[8].Value.ToString();
            txtboar.Tag = Dgv.Rows[i].Cells[10].Value.ToString();
            txtsname.Tag = Dgv.Rows[i].Cells[11].Value.ToString();
            cboTerm.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            comboBox1.Text = Dgv.Rows[i].Cells[14].Value.ToString();

            if (Dgv.Rows[i].Cells[13].Value.ToString() == "1")
            {
                checkBox1.Checked = true;
                con.Close();
                con.Open();
                string qur = "select  sname + ' - '+ admisno as sname   from studentm  where uid=" + txtsname.Tag + " ";
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtsname.Text = tab.Rows[0]["sname"].ToString();
                }
            }
            else
            {
                checkBox1.Checked = false;
                txtsname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            }
            btnsave.Text = "Update";
            SelectId = 0;
        }

        protected DataTable GetParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Module.Gbtxtid == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetStudentName", con);
                    dt = dt.Select("classid=" + cboTerm.SelectedValue + "", string.Empty).CopyToDataTable();
                    bsParty.DataSource = dt;
                }
                else if (Module.Gbtxtid == 2)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetStudentAdno", con);
                    dt = dt.Select("classid=" + cboTerm.SelectedValue + "", string.Empty).CopyToDataTable();
                    bsParty1.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "SName";
                DataGridCommon.Columns[1].HeaderText = "Student";
                DataGridCommon.Columns[1].DataPropertyName = "SName";
                DataGridCommon.Columns[1].Width = 180;

                DataGridCommon.Columns[2].Name = "AdmissionNo";
                DataGridCommon.Columns[2].HeaderText = "AdmissionNo";
                DataGridCommon.Columns[2].DataPropertyName = "admisno";
                DataGridCommon.Columns[2].Width = 80;

                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "SName";
                DataGridCommon.Columns[1].HeaderText = "Student";
                DataGridCommon.Columns[1].DataPropertyName = "SName";
                DataGridCommon.Columns[1].Width = 200;
                DataGridCommon.DataSource = bsParty1;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        private void Txtsname_MouseClick(object sender, MouseEventArgs e)
        {
            if (cboTerm.Text == "")
            {
                MessageBox.Show("Select The Class Name ");
                cboTerm.Focus();
                return;
            }
            if (checkBox1.Checked == false)
            {
                Module.Gbtxtid = 1;
                Fill = 1;
                DataTable dt = GetParty();
                bsParty.DataSource = dt;
                FillGrid2(dt, 1);
                Point loc = FindLocation(txtsname);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Name Search";
            }
            else if (checkBox1.Checked == true)

            {
                Module.Gbtxtid = 2;
                DataTable dt = GetParty();
                bsParty1.DataSource = dt;
                FillGrid1(dt, 1);
                Point loc = FindLocation(txtsname);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Name Search";
            }
        }
        private void AG()
        {
            con.Close();
            con.Open();
            string qur = "Select isnull(Max(cast(Passno as decimal)),0) + 1 passno from BoardPIssueM";
            SqlCommand cmd = new SqlCommand(qur, con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            if (tab.Rows.Count > 0)
            {
                double str = Convert.ToDouble(tab.Rows[0]["passno"].ToString());
                txtpassno.Text = str.ToString();
            }
            con.Close();
        }

        private void Cbofees_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Txtsname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (Module.Gbtxtid == 1)
                    {
                        bsParty.Filter = string.Format("Sname LIKE '%{0}%' ", txtsname.Text);
                    }
                    else if (Module.Gbtxtid == 2)
                    {
                        bsParty1.Filter = string.Format("Sname LIKE '%{0}%' ", txtsname.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fill == 3)
                {
                    txtboar.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtboar.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtsname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtboar.Focus();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                Button18_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                txtsname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtsname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtboar.Focus();


                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void Txtboar_TextChanged(object sender, EventArgs e)
        {

        }
        protected void Getemp1()
        {
            try
            {
                //con.Close();
                //con.Open();
                //string Query = "Select buid, boardingp from boardingpm order by buid desc";
                //DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                //AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                //if (dt.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        string RegNo = dt.Rows[i]["boardingp"].ToString();
                //        coll.Add(RegNo);
                //    }
                //}
                //txtboar.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                //txtboar.AutoCompleteSource = AutoCompleteSource.CustomSource;
                //txtboar.AutoCompleteCustomSource = coll;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void Txtboar_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtboar.Text != string.Empty)
                {
                    string Query = "Select buid,boardingp from boardingpm  Where boardingp ='" + txtboar.Text + "'";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if (dt.Rows.Count != 0)
                    {
                        txtboar.Tag = dt.Rows[0]["buid"].ToString();


                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            SelectId = 1;
            string qur = "select puid, a.passno,b.Class_Desc,c.SName,d.BoardingP,e.TripName,a.startDate,a.Enddate,a.status,a.classid,a.boardid,a.studid,a.tripid,a.adnochk,a.way from BoardPIssueM a  left join Class_Mast b on a.classid = b.Cid left join StudentM c on a.studid = c.uid left join BoardingPM d on a.boardid = d.Buid left join tripM e on a.tripid = e.tuid   where a.passno like '%" + txtscr.Text + "%' or   b.Class_Desc like '%" + txtscr.Text + "%'  or c.SName like '%" + txtscr.Text + "%' or d.BoardingP like '%" + txtscr.Text + "%'   or e.TripName like '%" + txtscr.Text + "%'  ";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);


            this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);

            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            Dgv.AutoGenerateColumns = false;
            Dgv.DataSource = null;
            Dgv.ColumnCount = 15;

            Dgv.Columns[0].Name = "puid";
            Dgv.Columns[0].HeaderText = "puid";
            Dgv.Columns[0].DataPropertyName = "puid";
            Dgv.Columns[0].Visible = false;

            Dgv.Columns[1].Name = "passno";
            Dgv.Columns[1].HeaderText = "Passno";
            Dgv.Columns[1].DataPropertyName = "passno";
            Dgv.Columns[1].Width = 100;

            Dgv.Columns[2].Name = "Class_Desc";
            Dgv.Columns[2].HeaderText = "Class";
            Dgv.Columns[2].DataPropertyName = "Class_Desc";
            Dgv.Columns[2].Width = 100;

            Dgv.Columns[3].Name = "SName";
            Dgv.Columns[3].HeaderText = "Student";
            Dgv.Columns[3].DataPropertyName = "SName";
            Dgv.Columns[3].Width = 220;

            Dgv.Columns[4].Name = "BoardingP";
            Dgv.Columns[4].HeaderText = "Boarding";
            Dgv.Columns[4].DataPropertyName = "BoardingP";
            Dgv.Columns[4].Width = 150;

            Dgv.Columns[5].Name = "TripName";
            Dgv.Columns[5].HeaderText = "TripName";
            Dgv.Columns[5].DataPropertyName = "TripName";
            Dgv.Columns[5].Width = 150;

            Dgv.Columns[6].Name = "startDate";
            Dgv.Columns[6].HeaderText = "StartDate";
            Dgv.Columns[6].DataPropertyName = "startDate";
            Dgv.Columns[6].Visible = false;

            Dgv.Columns[7].Name = "Enddate";
            Dgv.Columns[7].HeaderText = "Enddate";
            Dgv.Columns[7].DataPropertyName = "Enddate";
            Dgv.Columns[7].Visible = false;

            Dgv.Columns[8].Name = "Status";
            Dgv.Columns[8].HeaderText = "Status";
            Dgv.Columns[8].DataPropertyName = "Status";
            Dgv.Columns[8].Visible = false;

            Dgv.Columns[9].DataPropertyName = "classid";
            Dgv.Columns[9].Visible = false;

            Dgv.Columns[10].DataPropertyName = "boardid";
            Dgv.Columns[10].Visible = false;

            Dgv.Columns[11].DataPropertyName = "studid";
            Dgv.Columns[11].Visible = false;

            Dgv.Columns[12].DataPropertyName = "tripid";
            Dgv.Columns[12].Visible = false;

            Dgv.Columns[13].DataPropertyName = "adnochk";
            Dgv.Columns[13].Visible = false;

            Dgv.Columns[14].DataPropertyName = "way";
            Dgv.Columns[14].Visible = false;
            Dgv.DataSource = tap;
            SelectId = 0;

        }

        private void CmbRoute_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectId == 0)
                {
                    string Query = "select Ruid,BoardingP from RouteD a inner join BoardingPM b on a.Boardid = b.Buid Where a.Headid = " + CmbRoute.SelectedValue + " order by Slno";
                    DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    bsBoard.DataSource = data;
                    FillGrid3(data, 1);
                    Point loc = FindLocation(txtboar);
                    grSearch.Location = new Point(loc.X, loc.Y + 20);
                    grSearch.Visible = true;
                    grSearch.Text = "Name Search";
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                Fill = 3;
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Ruid";
                DataGridCommon.Columns[1].Name = "SName";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "BoardingP";
                DataGridCommon.Columns[1].Width = 200;
                DataGridCommon.DataSource = bsBoard;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Dgv_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Puid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value);
                    string Query = "delete from BoardPIssueM Where Puid =" + Puid + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, con);
                    Load_grids();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
