﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MylaSchool
{
    public partial class FrmSubOrderM : Form
    {
        public FrmSubOrderM()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        public double sum111 = 0;
        SQLDBHelper db = new SQLDBHelper();
        int slno = 0;
        BindingSource bsemp = new BindingSource();
        BindingSource bsclass = new BindingSource();
        BindingSource bssub = new BindingSource();
        public int SelectId = 0;

        private void FrmSubOrderM_Load(object sender, EventArgs e)
        {
            this.Dgv.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dgvlist.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgvlist.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            Dgv.RowHeadersVisible = false;
            Dgvlist.RowHeadersVisible = false;
            //Titlep();

            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnDelete.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            getemp();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            clearTxt();
            //Dgvlist.Rows.Clear();

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnDelete.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select a.uid,b.Class_Desc,c.Sub_Desc,a.sub_sno,a.Std_id,a.Sub_Id from  Sub_det1 a left join class_mast b on a.Std_id=b.cid left join  sub_mast c on a.Sub_Id=c.uid ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 6;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Class_Desc";
                Dgv.Columns[1].HeaderText = "Class";
                Dgv.Columns[1].DataPropertyName = "Class_Desc";
                Dgv.Columns[1].Width = 150;

                Dgv.Columns[2].Name = "Sub_Desc";
                Dgv.Columns[2].HeaderText = "Subject";
                Dgv.Columns[2].DataPropertyName = "Sub_Desc";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "sub_sno";
                Dgv.Columns[3].HeaderText = "Subject Order";
                Dgv.Columns[3].DataPropertyName = "sub_sno";
                Dgv.Columns[3].Width = 150;

                Dgv.Columns[4].DataPropertyName = "Std_id";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Sub_Id";
                Dgv.Columns[5].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                int k = 0;
                foreach (DataGridViewRow row in Dgvlist.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {

                        if (Dgvlist.Rows[k].Cells[3].Value.ToString() != "")
                        {
                            SqlParameter[] paraDet = {
                               new SqlParameter("@Std_id", TxtfeeM.Tag),
                               new SqlParameter("@Sub_Id", Convert.ToInt16(Dgvlist.Rows[k].Cells[3].Value.ToString())),
                               new SqlParameter("@sub_sno", Convert.ToInt16(Dgvlist.Rows[k].Cells[2].Value.ToString())),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Sub_det1", paraDet, con);
                        }
                    }
                    k = k + 1;
                }
            }
            else
            {
                con.Close();
                con.Open();
                cmd.CommandText = "delete from Sub_det1 where Std_id=" + TxtfeeM.Tag + "";
                cmd.ExecuteNonQuery();

                int k = 0;

                foreach (DataGridViewRow row in Dgvlist.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {

                        if (Dgvlist.Rows[k].Cells[2].Value.ToString() != "")
                        {
                            SqlParameter[] paraDet = {
                               new SqlParameter("@Std_id", TxtfeeM.Tag),
                                new SqlParameter("@Sub_Id", Convert.ToInt16(Dgvlist.Rows[k].Cells[3].Value.ToString())),
                                new SqlParameter("@sub_sno", Convert.ToInt16(Dgvlist.Rows[k].Cells[2].Value.ToString())),
                                  };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Sub_det1", paraDet, con);
                        }
                    }
                    k = k + 1;
                }
            }
            MessageBox.Show("Record  Saved ");
            clearTxt();
            con.Close();

            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grid();
            btnDelete.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

        }
        private void TxtfeeM_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txttime1.Focus();
                    Loadclass();
                    for (int l = 0; l < Dgvlist.RowCount - 1; l++)
                    {
                        if (Dgvlist.Rows[l].Cells[3].Value.ToString() != "")
                        {
                            Dgvlist[0, l].Value = true;
                        }
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            txtroute.Text = string.Empty;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Save";
        }

        private void TxtfeeM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsclass.Filter = string.Format("Class_desc LIKE '%{0}%' ", TxtfeeM.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Titlep()
        {
            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = 7;
            Dgvlist.Columns[0].Name = "Ruid";
            Dgvlist.Columns[0].HeaderText = "Ruid";
            Dgvlist.Columns[0].DataPropertyName = "Ruid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "slno";
            Dgvlist.Columns[1].HeaderText = "slno";
            Dgvlist.Columns[1].DataPropertyName = "slno";
            Dgvlist.Columns[1].Width = 50;

            Dgvlist.Columns[2].Name = "BoardName";
            Dgvlist.Columns[2].HeaderText = "BoardName";
            Dgvlist.Columns[2].DataPropertyName = "BoardName";
            Dgvlist.Columns[2].Width = 268;

            Dgvlist.Columns[3].Name = "Time1";
            Dgvlist.Columns[3].HeaderText = "Time1";
            Dgvlist.Columns[3].DataPropertyName = "Time1";
            Dgvlist.Columns[3].Width = 100;

            Dgvlist.Columns[4].Name = "Time2";
            Dgvlist.Columns[4].HeaderText = "Time2";
            Dgvlist.Columns[4].DataPropertyName = "Time2";
            Dgvlist.Columns[4].Width = 100;

            Dgvlist.Columns[5].Name = "Strength";
            Dgvlist.Columns[5].HeaderText = "Strength";
            Dgvlist.Columns[5].DataPropertyName = "Strength";
            Dgvlist.Columns[5].Width = 100;
            Dgvlist.Columns[6].Name = "boardid";
            Dgvlist.Columns[6].HeaderText = "boardid";
            Dgvlist.Columns[6].DataPropertyName = "boardid";
            Dgvlist.Columns[6].Visible = false;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (TxtfeeM.Text == "")
            {
                MessageBox.Show("Select the Boarding Points");
                TxtfeeM.Focus();
                return;
            }
            if (txtstrength.Text == "")
            {
                MessageBox.Show("Enter the strength");
                txtstrength.Focus();
                return;
            }
            string qur = "select  buid,Boardingp  from boardingpm where buid=" + TxtfeeM.Tag + "";
            cmd = new SqlCommand(qur, con);
            SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                slno = Dgvlist.Rows.Count + 1 - 1;
                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = '0';
                Dgvlist.Rows[index].Cells[1].Value = slno;
                Dgvlist.Rows[index].Cells[2].Value = tap1.Rows[i]["Boardingp"].ToString();
                Dgvlist.Rows[index].Cells[3].Value = txttime1.Text;
                Dgvlist.Rows[index].Cells[4].Value = txttime2.Text;
                Dgvlist.Rows[index].Cells[5].Value = txtstrength.Text;
                Dgvlist.Rows[index].Cells[6].Value = tap1.Rows[i]["buid"].ToString();
            }
            clearTxt();

        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            TxtfeeM.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            TxtfeeM.Tag = Dgv.Rows[i].Cells[4].Value.ToString();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Update";
            LoadclassEdit();
            for (int l = 0; l < Dgvlist.RowCount - 1; l++)
            {
                if (Dgvlist.Rows[l].Cells[3].Value.ToString() != "")
                {
                    Dgvlist[0, l].Value = true;
                }
            }
        }

        public void LoadclassEdit()
        {
            con.Close();
            con.Open();
            string quy = "SELECT c.Sub_desc as Subject,a.sub_sno as SlNo,a.Sub_Id,a.Std_id,a.uid  FROM Sub_det1 a left join class_mast b on a.Std_id=b.Cid  left join sub_mast c on a.Sub_Id=c.uid  where a.Std_id=" + TxtfeeM.Tag + "";
            cmd = new SqlCommand(quy, con);
            SqlDataAdapter aptr = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;

            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            Dgvlist.Columns.Add(checkColumn);

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();


            Dgvlist.ColumnCount = tap.Columns.Count + 1;
            Module.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                Dgvlist.Columns[Module.i].Name = column.ColumnName;
                Dgvlist.Columns[Module.i].HeaderText = column.ColumnName;
                Dgvlist.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }

            Dgvlist.Columns[1].Width = 110;
            Dgvlist.Columns[2].Width = 110;
            Dgvlist.Columns[3].Visible = false;

            Dgvlist.Columns[4].Visible = false;
            Dgvlist.Columns[5].Visible = false;
            Dgvlist.DataSource = tap;
        }

        public void clearTxt()
        {

            TxtfeeM.Text = "";
            txttime2.Text = "";
            txttime1.Text = "";
            txtstrength.Text = "";

        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select Ruid,Name from RouteM where  Name like '%" + txtscr1.Text + "%'  ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 2;

                Dgv.Columns[0].Name = "Ruid";
                Dgv.Columns[0].HeaderText = "Ruid";
                Dgv.Columns[0].DataPropertyName = "Ruid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Name";
                Dgv.Columns[1].HeaderText = "Route Name";
                Dgv.Columns[1].DataPropertyName = "Name";
                Dgv.Columns[1].Width = 500;

                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void TxtfeeM_MouseClick(object sender, MouseEventArgs e)
        {
            Module.Dtype = 2;
            DataTable dt = getParty();
            bsclass.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(TxtfeeM);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        
        protected void getemp()
        {
            try
            {
                con.Close();
                con.Open();
                string Query = "Select buid,boardingp from boardingpm order by buid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["boardingp"].ToString();
                        coll.Add(RegNo);
                    }
                }
                TxtfeeM.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                TxtfeeM.AutoCompleteSource = AutoCompleteSource.CustomSource;
                TxtfeeM.AutoCompleteCustomSource = coll;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the Subject ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());


                    string Query1 = "Delete from Sub_det1 Where uid = @uid";
                    SqlCommand cmd1 = new SqlCommand(Query1, con);
                    cmd1.Parameters.AddWithValue("@uid", SqlDbType.Int).Value = Uid;
                    cmd1.ExecuteNonQuery();

                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();
                Loadclass();
                for (int l = 0; l < Dgvlist.RowCount - 1; l++)
                {
                    if (Dgvlist.Rows[l].Cells[3].Value.ToString() != "")
                    {
                        Dgvlist[0, l].Value = true;
                    }
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();
                Loadclass();
                for (int l = 0; l < Dgvlist.RowCount - 1; l++)
                {
                    if (Dgvlist.Rows[l].Cells[3].Value.ToString() != "")
                    {
                        Dgvlist[0, l].Value = true;
                    }
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                txttime1.Focus();
                Loadclass();
                for (int l = 0; l < Dgvlist.RowCount - 1; l++)
                {
                    if (Dgvlist.Rows[l].Cells[3].Value.ToString() != "")
                    {

                        Dgvlist[0, l].Value = true;
                    }

                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        public void Loadclass()
        {
            con.Close();
            con.Open();
            if (TxtfeeM.Tag.ToString() != "")
            {
                string quy = @"Select b.Sub_Desc,c.Sqn_no,b.uid from sub_std a Left join sub_mast b on a.Sub_Id = b.Uid
                            Left join Class_Mast c on a.Std_id = c.cid
                            Where a.Class_Id = " + TxtfeeM.Tag + "";
                cmd = new SqlCommand(quy, con);
                SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
                checkColumn.Name = "CHK";
                checkColumn.HeaderText = "CHK";
                checkColumn.Width = 50;
                checkColumn.ReadOnly = false;
                checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
                Dgvlist.Columns.Add(checkColumn);

                Dgvlist.AutoGenerateColumns = false;
                Dgvlist.Refresh();
                Dgvlist.DataSource = null;
                Dgvlist.Rows.Clear();

                Dgvlist.ColumnCount = tap.Columns.Count + 1;
                Module.i = 1;
                foreach (DataColumn column in tap.Columns)
                {
                    Dgvlist.Columns[Module.i].Name = column.ColumnName;
                    Dgvlist.Columns[Module.i].HeaderText = column.ColumnName;
                    Dgvlist.Columns[Module.i].DataPropertyName = column.ColumnName;
                    Module.i = Module.i + 1;
                }
                Dgvlist.Columns[1].Width = 110;
                Dgvlist.Columns[2].Width = 110;
                Dgvlist.Columns[3].Visible = false;
                Dgvlist.DataSource = tap;
            }

        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "CID";
                DataGridCommon.Columns[0].HeaderText = "CID";
                DataGridCommon.Columns[0].DataPropertyName = "CID";

                DataGridCommon.Columns[1].Name = "Class_desc";
                DataGridCommon.Columns[1].HeaderText = "Class";
                DataGridCommon.Columns[1].DataPropertyName = "Class_desc";
                DataGridCommon.Columns[1].Width = 200;

                DataGridCommon.DataSource = bsclass;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", con);
                bsclass.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void btnser_Click_1(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select a.uid,b.Class_Desc,c.Sub_Desc,a.sub_sno,a.Std_id,a.Sub_Id from  Sub_det1 a left join class_mast b on a.Std_id=b.cid left join  sub_mast c on a.Sub_Id=c.uid  where  Class_Desc like '%" + txtscr.Text + " or   Sub_Desc like '%" + txtscr.Text + " or  sub_sno like '%" + txtscr.Text + " ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 6;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Class_Desc";
                Dgv.Columns[1].HeaderText = "Class";
                Dgv.Columns[1].DataPropertyName = "Class_Desc";
                Dgv.Columns[1].Width = 150;

                Dgv.Columns[2].Name = "Sub_Desc";
                Dgv.Columns[2].HeaderText = "Subject";
                Dgv.Columns[2].DataPropertyName = "Sub_Desc";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "sub_sno";
                Dgv.Columns[3].HeaderText = "Subject Order";
                Dgv.Columns[3].DataPropertyName = "sub_sno";
                Dgv.Columns[3].Width = 150;

                Dgv.Columns[4].DataPropertyName = "Std_id";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Sub_Id";
                Dgv.Columns[5].Visible = false;
                Dgv.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }
    }
}

