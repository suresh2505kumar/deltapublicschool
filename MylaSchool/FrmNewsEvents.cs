﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmNewsEvents : Form
    {
        public FrmNewsEvents()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        DataTable dtNewsEvents = new DataTable();
        string[] fileName;

        private void FrmNewsEvents_Load(object sender, EventArgs e)
        {
            LoadButton(0);
            dtNewsEvents = GetNewsandEvents();
            DateTime dte = Convert.ToDateTime(DtpSearchDate.Text);
            if (this.Text == "News & Events")
            {
                btnFieldTripImages.Visible = false;
                BtnViewImages.Visible = false;
                DataTable dt = new DataTable();
                if (dtNewsEvents.Rows.Count > 0)
                {
                    string cnt = dtNewsEvents.Compute("Count(Ntype)", "Ntype='News&Events'").ToString();
                    if (cnt != "0")
                    {
                        dt = dtNewsEvents.Select("Ntype='News&Events'").CopyToDataTable();
                    }
                }
                else
                {
                    dt = dtNewsEvents.Clone();
                }
                LoadData(dt);
                CmbType.Items.Clear();
                CmbType.Items.Add("News&Events");
            }
            else if (this.Text == "FieldTrip")
            {
                btnFieldTripImages.Visible = true;
                BtnViewImages.Visible = true;
                CmbType.Items.Clear();
                CmbType.Items.Add("FieldTrip");
                DataTable dt = new DataTable();
                if (dtNewsEvents.Rows.Count > 0)
                {
                    string cnt = dtNewsEvents.Compute("Count(Ntype)", "Ntype='FieldTrip'").ToString();
                    if (cnt != "0")
                    {
                        dt = dtNewsEvents.Select("Ntype='FieldTrip'").CopyToDataTable();
                    }
                }
                else
                {
                    dt = dtNewsEvents.Clone();
                }
                LoadData(dt);
            }
            else
            {
                btnFieldTripImages.Visible = false;
                BtnViewImages.Visible = false;
                CmbType.Items.Clear();
                CmbType.Items.Add("Calendar");
                this.Text = "Calendar";
                DataTable dt = new DataTable();
                if (dtNewsEvents.Rows.Count > 0)
                {
                    string cnt = dtNewsEvents.Compute("Count(Ntype)", "Ntype='Calendar'").ToString();
                    if (cnt != "0")
                    {
                        dt = dtNewsEvents.Select("Ntype='Calendar'").CopyToDataTable();
                    }
                }
                else
                {
                    dt = dtNewsEvents.Clone();
                }
                LoadData(dt);
            }
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
        }

        protected void ClearControl()
        {
            try
            {
                txtContent.Text = string.Empty;
                txtContent.Tag = string.Empty;
                CmbType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadData(DataTable dataTable)
        {
            try
            {
                DataGridNewsEvents.DataSource = null;
                DataGridNewsEvents.AutoGenerateColumns = false;
                DataGridNewsEvents.ColumnCount = 4;
                DataGridNewsEvents.Columns[0].Name = "ID";
                DataGridNewsEvents.Columns[0].HeaderText = "ID";
                DataGridNewsEvents.Columns[0].DataPropertyName = "ID";
                DataGridNewsEvents.Columns[0].Visible = false;

                DataGridNewsEvents.Columns[1].Name = "Dte";
                DataGridNewsEvents.Columns[1].HeaderText = "Date";
                DataGridNewsEvents.Columns[1].DataPropertyName = "Dte";
                DataGridNewsEvents.Columns[1].DefaultCellStyle.Format = "dd-MMM-yyyy";

                DataGridNewsEvents.Columns[2].Name = "Contents";
                DataGridNewsEvents.Columns[2].HeaderText = "Contents";
                DataGridNewsEvents.Columns[2].DataPropertyName = "Contents";
                DataGridNewsEvents.Columns[2].Width = 560;

                DataGridNewsEvents.Columns[3].Name = "Ntype";
                DataGridNewsEvents.Columns[3].HeaderText = "Ntype";
                DataGridNewsEvents.Columns[3].DataPropertyName = "Ntype";
                DataGridNewsEvents.Columns[3].Visible = false;
                DataGridNewsEvents.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)//Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else if (id == 1) // Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnadd.Visible = false;
                    btnedit.Visible = false;
                    btnexit.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Save";
                }
                else if (id == 2)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnadd.Visible = false;
                    btnedit.Visible = false;
                    btnexit.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DataTable GetNewsandEvents()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetNewsEvents", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            txtContent.Tag = "0";
            ClearControl();
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            LoadButton(0);
            txtContent.Tag = "0";
            DateTime dte = Convert.ToDateTime(DtpSearchDate.Text);
            DataTable dt = new DataTable();
            dt = GetNewsandEvents();
            if (this.Text == "News & Events")
            {
                dt = dtNewsEvents.Select("Ntype='News&Events'").CopyToDataTable();
            }
            else if (this.Text == "FieldTrip")
            {
                dt = dtNewsEvents.Select("Ntype='FieldTrip'").CopyToDataTable();
            }
            else
            {
                dt = dtNewsEvents.Select("Ntype='Calendar'").CopyToDataTable();
            }
            LoadData(dt);
            ClearControl();
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtContent.Text != string.Empty || CmbType.SelectedIndex != -1)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Id",txtContent.Tag),
                        new SqlParameter("@Dte",Convert.ToDateTime(DtpDate.Text)),
                        new SqlParameter("@Contents",txtContent.Text),
                        new SqlParameter("@Ntype",CmbType.Text),
                        new SqlParameter("@ReturnID",SqlDbType.Int),
                    };
                    parameters[4].Direction = ParameterDirection.Output;
                    int Uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_NewsEvents", parameters, conn,4);
                    if (this.Text == "FieldTrip")
                    {
                        int i = 0;
                        foreach (string item in fileName)
                        {
                            i += 1;
                            FileInfo fileInfo = new FileInfo(item);
                            string newfile = "" + Uid.ToString() + "-" + i.ToString() + "";
                            UploadPhoto(newfile, item);
                            SqlParameter[] parametersImages = { new SqlParameter("@FtripUid", Uid), new SqlParameter("@ImagesPath", "http://www.mylaeasybiz.net/delta/FieldTrip/" + newfile + fileInfo.Extension) };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_FieldTripImage", parametersImages, conn);
                        }
                    }
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtContent.Text = string.Empty;
                    CmbType.SelectedIndex = -1;
                    DateTime dte = Convert.ToDateTime(DtpSearchDate.Text);
                    DataTable dt = new DataTable();
                    if (this.Text == "News&Events")
                    {
                        dt = dtNewsEvents.Select("Ntype='News&Events'").CopyToDataTable();
                    }
                    else if (this.Text == "FieldTrip")
                    {
                        dt = dtNewsEvents.Select("Ntype='FieldTrip'").CopyToDataTable();
                    }
                    else
                    {
                        dt = dtNewsEvents.Select("Ntype='News&Events'").CopyToDataTable();
                    }
                    LoadData(dt);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void UploadPhoto(string remoteFile, string localFile)
        {
            try
            {
                FileInfo fileInf = new FileInfo(localFile);
                FtpWebRequest ftpRequest;
                string Path1 = "ftp://mylaeasybiz.net/";
                string Folder = "FieldTrip/";
                ftpRequest = (FtpWebRequest)WebRequest.Create(Path1 + Folder + remoteFile + fileInf.Extension);
                ftpRequest.Credentials = new NetworkCredential("delta", "delta1234");
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                Stream ftpStream = ftpRequest.GetRequestStream();
                FileStream localFileStream = fileInf.OpenRead();
                int bufferSize = 2048;
                byte[] byteBuffer = new byte[bufferSize];
                int bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                try
                {
                    while (bytesSent != 0)
                    {
                        ftpStream.Write(byteBuffer, 0, bytesSent);
                        bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                localFileStream.Close();
                ftpStream.Close();
                ftpRequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            return;
        }

        private void DtpSearchDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime dte = Convert.ToDateTime(DtpSearchDate.Text);
                GetNewsandEvents();
                DataTable dt = new DataTable();
                if (this.Text == "News&Events")
                {
                    dt = dtNewsEvents.Select("Ntype='News&Events'").CopyToDataTable();
                }
                else if (this.Text == "FieldTrip")
                {
                    dt = dtNewsEvents.Select("Ntype='FieldTrip'").CopyToDataTable();
                }
                else
                {
                    dt = dtNewsEvents.Select("Ntype='News&Events'").CopyToDataTable();
                }
                DataTable dt1 = null;
                var rows = dt.AsEnumerable()
                    .Where(x => ((DateTime)x["Dte"]).Date >= Convert.ToDateTime(DtpSearchDate.Text).Date);
                if (rows.Any())
                    dt1 = rows.CopyToDataTable();
                LoadData(dt1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                int Index = DataGridNewsEvents.SelectedCells[0].RowIndex;
                txtContent.Tag = DataGridNewsEvents.Rows[Index].Cells[0].Value.ToString();
                txtContent.Text = DataGridNewsEvents.Rows[Index].Cells[2].Value.ToString();
                DtpDate.Text = DataGridNewsEvents.Rows[Index].Cells[1].Value.ToString();
                CmbType.Text = DataGridNewsEvents.Rows[Index].Cells[3].Value.ToString();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnFieldTripImages_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog
                {
                    Multiselect = true,
                    Title = "Field Trip Images",
                    Filter = "JPG|*.jpg|JPEG|*.jpeg|GIF|*.gif|PNG|*.png"
                };
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileName = dialog.FileNames;
                    int x = 20;
                    int y = 20;
                    int maxHeight = -1;
                    foreach (string filename in fileName)
                    {
                        PictureBox pic = new PictureBox();
                        pic.Image = Image.FromFile(filename);
                        pic.Location = new Point(x, y);
                        x += pic.Width + 10;
                        maxHeight = Math.Max(pic.Height, maxHeight);
                        pic.SizeMode = PictureBoxSizeMode.StretchImage;
                        if (x > this.ClientSize.Width - 100)
                        {
                            x = 20;
                            y += maxHeight + 10;
                        }
                        this.grImages.Controls.Add(pic);
                        grImages.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            grImages.Visible = false;
        }

        private void BtnViewImages_Click(object sender, EventArgs e)
        {
            grImages.Visible = true;
        }
    }
}