﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            LoadYear();
        }
        protected void LoadYear()
        {
            SQLDBHelper db = new SQLDBHelper();
            DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetYear",conn);
            cmbYear.DataSource = null;
            cmbYear.DisplayMember = "YrName";
            cmbYear.ValueMember = "DBName";
            cmbYear.DataSource = dt;
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtUserName.Text == string.Empty && txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Enter user name and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUserName.Focus();
                    return;
                }
                else if (txtUserName.Text == string.Empty)
                {
                    MessageBox.Show("Enter user name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUserName.Focus();
                    return;
                }
                else if (txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Enter Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPassword.Focus();
                    return;
                }
                else
                {
                    GeneralParameters.ServerName = ConfigurationManager.AppSettings["ServerName"];
                    GeneralParameters.DbName = cmbYear.SelectedValue.ToString();
                    GeneralParameters.UserName = ConfigurationManager.AppSettings["UserName"]; 
                    GeneralParameters.Password = ConfigurationManager.AppSettings["Password"];                    
                    string ConnectionString = "Data Source = '" + GeneralParameters.ServerName + "'; Initial Catalog = '" + GeneralParameters.DbName + "'; User ID = '" + GeneralParameters.UserName + "'; Password = '" + GeneralParameters.Password + "';";
                    GeneralParameters.ConnectionString = ConnectionString;
                    SqlConnection conn = new SqlConnection(ConnectionString);
                    SQLDBHelper db = new SQLDBHelper();
                    SqlParameter[] para = {
                        new SqlParameter("@UserName",txtUserName.Text),
                        new SqlParameter("@Password",txtPassword.Text)
                    };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_UserLogin", para, conn);
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("Enter the correct user name and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUserName.Focus();
                        return;
                    }
                    else
                    {
                        GeneralParameters.UserdId = 1;
                        GeneralParameters.LoginUserName = dt.Rows[0]["UserName"].ToString();
                        this.Hide();
                        FrmMain main = new FrmMain();
                        main.Show();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
