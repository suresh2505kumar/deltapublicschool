﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmRegM : Form
    {
        public FrmRegM()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SQLDBHelper db1 = new SQLDBHelper();
        BindingSource bsParty = new BindingSource();
        public int SelectId = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void Load_grid()
        {
            try
            {
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_REGMLOAD";
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@UID", 0);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 17;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Regdate";
                Dgv.Columns[1].HeaderText = "Date";
                Dgv.Columns[1].DataPropertyName = "Regdate";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "appno";
                Dgv.Columns[2].HeaderText = "AppNo";
                Dgv.Columns[2].DataPropertyName = "appno";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "studName";
                Dgv.Columns[3].HeaderText = "Student Name";
                Dgv.Columns[3].DataPropertyName = "studName";
                Dgv.Columns[3].Width = 200;

                Dgv.Columns[4].Name = "Gender";
                Dgv.Columns[4].HeaderText = "Gender";
                Dgv.Columns[4].DataPropertyName = "Gender";
                Dgv.Columns[4].Width = 100;

                Dgv.Columns[5].Name = "class";
                Dgv.Columns[5].HeaderText = "class";
                Dgv.Columns[5].DataPropertyName = "class";
                Dgv.Columns[5].Visible = false;


                Dgv.Columns[6].Name = "mobno";
                Dgv.Columns[6].HeaderText = "mobno";
                Dgv.Columns[6].DataPropertyName = "mobno";
                Dgv.Columns[6].Visible = false;


                Dgv.Columns[7].Name = "Regno";
                Dgv.Columns[7].HeaderText = "Regno";
                Dgv.Columns[7].DataPropertyName = "Regno";
                Dgv.Columns[7].Width = 100;

                Dgv.Columns[8].Name = "extrance";
                Dgv.Columns[8].HeaderText = "extrance";
                Dgv.Columns[8].DataPropertyName = "extrance";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].Name = "marks";
                Dgv.Columns[9].HeaderText = "marks";
                Dgv.Columns[9].DataPropertyName = "marks";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].Name = "Note";
                Dgv.Columns[10].HeaderText = "Note";
                Dgv.Columns[10].DataPropertyName = "Note";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].Name = "photoav";
                Dgv.Columns[11].HeaderText = "photoav";
                Dgv.Columns[11].DataPropertyName = "photoav";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].Name = "tcav";
                Dgv.Columns[12].HeaderText = "tcav";
                Dgv.Columns[12].DataPropertyName = "tcav";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].Name = "ccav";
                Dgv.Columns[13].HeaderText = "ccav";
                Dgv.Columns[13].DataPropertyName = "ccav";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].Name = "birthav";
                Dgv.Columns[14].HeaderText = "birthav";
                Dgv.Columns[14].DataPropertyName = "birthav";
                Dgv.Columns[14].Visible = false;

                Dgv.Columns[15].Name = "appid";
                Dgv.Columns[15].HeaderText = "appid";
                Dgv.Columns[15].DataPropertyName = "appid";
                Dgv.Columns[15].Visible = false;

                Dgv.Columns[16].DataPropertyName = "groupa";
                Dgv.Columns[16].Visible = false;
                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmRegM_Load(object sender, EventArgs e)
        {
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();

            btnsave.Text = "Save";
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (txtsname.Text == "")
            {
                MessageBox.Show("Enter the Stuednt name");
                txtsname.Focus();
                return;
            }
            else if (txtadno.Text == "")
            {
                MessageBox.Show("select the APPLICATION No");
                txtadno.Focus();
                return;
            }

            else if (txtregNo.Text == "")
            {
                MessageBox.Show("select the regNo");
                txtregNo.Focus();
                return;
            }

            if (txtmarks.Text == "")
            {
                txtmarks.Text = "0";
            }

            if (btnsave.Text == "Save")
            {
                conn.Close();
                string Query = "Select * from RegM Where Regno = '" + txtregNo.Text + "'";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtch = new DataTable();
                da.Fill(dtch);
                if (dtch.Rows.Count == 0)
                {
                    SqlParameter[] para ={
                        new SqlParameter("@Regdate", Convert.ToDateTime(doa.Text)),
                        new SqlParameter("@appno", Convert.ToString(txtadno.Text)),
                        new SqlParameter("@APPID", Convert.ToInt16(txtadno.Tag)),
                        new SqlParameter("@Regno ", Convert.ToString(txtregNo.Text)),
                        new SqlParameter("@extrance", Convert.ToString(cboReligion.Text)),
                        new SqlParameter("@marks",  Convert.ToDecimal(txtmarks.Text)),
                        new SqlParameter("@Note", txtnote.Text),
                        new SqlParameter("@photoav", Convert.ToString(cbophoto.Text)),
                        new SqlParameter("@tcav", Convert.ToString(cbotc.Text)),
                        new SqlParameter("@ccav", Convert.ToString(cbocc.Text)),
                        new SqlParameter("@birthav", Convert.ToString(cbobrith.Text)),
                    };
                    db1.ExecuteNonQuery(CommandType.StoredProcedure, "SP_REGM", para, conn);
                }
                else
                {
                    MessageBox.Show("Record already exists", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                SqlParameter[] para ={
                    new SqlParameter("@Regdate", Convert.ToDateTime(doa.Text)),
                    new SqlParameter("@appno", Convert.ToString(txtadno.Text)),
                    new SqlParameter("@APPID", Convert.ToInt16(txtadno.Tag)),
                    new SqlParameter("@Regno ", Convert.ToString(txtregNo.Text)),
                    new SqlParameter("@extrance", Convert.ToString(cboReligion.Text)),
                    new SqlParameter("@marks", Convert.ToDecimal(txtmarks.Text)),
                    new SqlParameter("@Note", txtnote.Text),
                    new SqlParameter("@photoav", Convert.ToString(cbophoto.Text)),
                    new SqlParameter("@tcav", Convert.ToString(cbotc.Text)),
                    new SqlParameter("@ccav", Convert.ToString(cbocc.Text)),
                    new SqlParameter("@birthav", Convert.ToString(cbobrith.Text)),
                    new SqlParameter("@uid", Convert.ToInt16(Module.i)),
                };
                db1.ExecuteNonQuery(CommandType.StoredProcedure, "SP_REGMUPDATE", para, conn);
            }
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            ClearTxt();
            conn.Close();
            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

        }

        public void ClearTxt()
        {
            txtadno.Text = "";
            txtsname.Text = "";
            txtmarks.Text = "";
            txtregNo.Text = "";
            txtmob.Text = "";
            cbophoto.SelectedIndex = -1;
            cbosex.Text = "";
            cboReligion.SelectedIndex = -1;
            cboclassJ.Text = "";
            txtnote.Text = "";
            cboclass.Text = "";
            cbobrith.SelectedIndex = -1;
            cbotc.SelectedIndex = -1;
            cbocc.SelectedIndex = -1;
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtadno.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtsname.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            cbosex.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            txtmarks.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            doa.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            cboclassJ.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            txtnote.Text = Dgv.Rows[i].Cells[13].Value.ToString();
            cboclass.Text = Dgv.Rows[i].Cells[17].Value.ToString();
            txtmob.Text = Dgv.Rows[i].Cells[19].Value.ToString();
            cboReligion.Text = Dgv.Rows[i].Cells[21].Value.ToString();
            btnsave.Text = "Update";
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Close();
                conn.Open();
                string qur = "select  a.uid,Regdate,a.appno,a.appid,b.studName,b.Gender,b.class,b.mobno,Regno,extrance,marks,Note,photoav,tcav,ccav,birthav from REGM A inner JOIN appm b on a.appid=b.uid   where  studName like '%" + txtscr.Text + "%'  or   regdate like '%" + txtscr.Text + "%' or   appno like '%" + txtscr.Text + "%' or   regno like '%" + txtscr.Text + "%'  or   mobno like '%" + txtscr.Text + "%'  ";
                cmd = new SqlCommand(qur, conn);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 15;
                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Regdate";
                Dgv.Columns[1].HeaderText = "Date";
                Dgv.Columns[1].DataPropertyName = "Regdate";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "appno";
                Dgv.Columns[2].HeaderText = "AppNo";
                Dgv.Columns[2].DataPropertyName = "appno";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "studName";
                Dgv.Columns[3].HeaderText = "Student Name";
                Dgv.Columns[3].DataPropertyName = "studName";
                Dgv.Columns[3].Width = 200;

                Dgv.Columns[4].Name = "Gender";
                Dgv.Columns[4].HeaderText = "Gender";
                Dgv.Columns[4].DataPropertyName = "Gender";
                Dgv.Columns[4].Width = 100;

                Dgv.Columns[5].Name = "class";
                Dgv.Columns[5].HeaderText = "class";
                Dgv.Columns[5].DataPropertyName = "class";
                Dgv.Columns[5].Visible = false;


                Dgv.Columns[6].Name = "mobno";
                Dgv.Columns[6].HeaderText = "mobno";
                Dgv.Columns[6].DataPropertyName = "mobno";
                Dgv.Columns[6].Visible = false;


                Dgv.Columns[7].Name = "Regno";
                Dgv.Columns[7].HeaderText = "Regno";
                Dgv.Columns[7].DataPropertyName = "Regno";
                Dgv.Columns[7].Width = 100;

                Dgv.Columns[8].Name = "extrance";
                Dgv.Columns[8].HeaderText = "extrance";
                Dgv.Columns[8].DataPropertyName = "extrance";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].Name = "marks";
                Dgv.Columns[9].HeaderText = "marks";
                Dgv.Columns[9].DataPropertyName = "marks";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].Name = "Note";
                Dgv.Columns[10].HeaderText = "Note";
                Dgv.Columns[10].DataPropertyName = "Note";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].Name = "photoav";
                Dgv.Columns[11].HeaderText = "photoav";
                Dgv.Columns[11].DataPropertyName = "photoav";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].Name = "tcav";
                Dgv.Columns[12].HeaderText = "tcav";
                Dgv.Columns[12].DataPropertyName = "tcav";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].Name = "ccav";
                Dgv.Columns[13].HeaderText = "ccav";
                Dgv.Columns[13].DataPropertyName = "ccav";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].Name = "birthav";
                Dgv.Columns[14].HeaderText = "birthav";
                Dgv.Columns[14].DataPropertyName = "birthav";
                Dgv.Columns[14].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void CboclassJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboclass_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbosex_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbocom_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboroll_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Btnadd_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";

        }

        private void Btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btnedit_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            ClearTxt();
            int i = Dgv.SelectedCells[0].RowIndex;
            Module.i = Convert.ToInt32(Dgv.Rows[i].Cells[0].Value.ToString());
            doa.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtadno.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtsname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            cbosex.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            cboclassJ.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            txtmob.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            txtregNo.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            cboReligion.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            txtmarks.Text = Dgv.Rows[i].Cells[9].Value.ToString();
            txtnote.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            cbophoto.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            cbotc.Text = Dgv.Rows[i].Cells[12].Value.ToString();
            cbocc.Text = Dgv.Rows[i].Cells[13].Value.ToString();
            cbobrith.Text = Dgv.Rows[i].Cells[14].Value.ToString();
            txtadno.Tag = Dgv.Rows[i].Cells[15].Value.ToString();
            cboclass.Text = Dgv.Rows[i].Cells[16].Value.ToString();
            btnsave.Text = "Update";
        }

        private void Txtadno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("appno LIKE '%{0}%' ", txtadno.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtadno_MouseClick(object sender, MouseEventArgs e)
        {
            DataTable dt = GetParty();
            bsParty.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtadno);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        protected DataTable GetParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db1.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETAPPNO", conn);
                bsParty.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;

                DataGridCommon.ColumnCount = 7;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[0].Visible = false;

                DataGridCommon.Columns[1].Name = "appno";
                DataGridCommon.Columns[1].HeaderText = "appno";
                DataGridCommon.Columns[1].DataPropertyName = "appno";
                DataGridCommon.Columns[1].Width = 150;

                DataGridCommon.Columns[2].DataPropertyName = "studName";
                DataGridCommon.Columns[2].Visible = false;

                DataGridCommon.Columns[3].DataPropertyName = "Gender";
                DataGridCommon.Columns[3].Visible = false;

                DataGridCommon.Columns[4].DataPropertyName = "class";
                DataGridCommon.Columns[4].Visible = false;

                DataGridCommon.Columns[5].DataPropertyName = "mobno";
                DataGridCommon.Columns[5].Visible = false;

                DataGridCommon.Columns[6].DataPropertyName = "Groupa";
                DataGridCommon.Columns[6].Visible = false;
                DataGridCommon.DataSource = bsParty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                txtadno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtadno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtsname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                cbosex.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                cboclassJ.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtmob.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                cboclass.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                cboReligion.Focus();
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                txtadno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtadno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtsname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                cbosex.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                cboclassJ.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtmob.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                cboclass.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                cboReligion.Focus();
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                txtadno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtadno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtsname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                cbosex.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                cboclassJ.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtmob.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                cboclass.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                cboReligion.Focus();
                grSearch.Visible = false;
                SelectId = 0;
            }
        }
    }
}
