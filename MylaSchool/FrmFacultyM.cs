﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MylaSchool
{
    public partial class FrmFacultyM : Form
    {
        public FrmFacultyM()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        public double sum111 = 0;
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsemp = new BindingSource();
        BindingSource bsclass = new BindingSource();
        BindingSource bssub = new BindingSource();
        public int SelectId = 0;

        private void FrmFacultyM_Load(object sender, EventArgs e)
        {
            this.Dgv.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dgvlist.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgvlist.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.DataGridCommon.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridCommon.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            DataGridCommon.RowHeadersVisible = false;
            Dgv.RowHeadersVisible = false;
            Dgvlist.RowHeadersVisible = false;
            Titlep();
            LoadRefT();
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnDelete.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            getemp();
        }

        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "select Cid , Class_desc  from class_mast order by class_desc";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cbocls.DataSource = null;
            cbocls.DataSource = tap;
            cbocls.DisplayMember = "Class_desc";
            cbocls.ValueMember = "Cid";
            cbocls.SelectedIndex = -1;
            con.Close();
        }


        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            clearTxt();
            Dgvlist.Rows.Clear();
            btnsave.Text = "Save";
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnDelete.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select uid,Fac_Name,a.empid,a.class,b.class_desc from Fac_MaSt a left join class_mast b   on a.Class=b.cid left join emp_mast c on a.EmpId=c.EmpId ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 5;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Fac_Name";
                Dgv.Columns[1].HeaderText = "Faculty Name";
                Dgv.Columns[1].DataPropertyName = "Fac_Name";
                Dgv.Columns[1].Width = 300;

                Dgv.Columns[2].DataPropertyName = "empid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "class";
                Dgv.Columns[3].Visible = false;
                Dgv.Columns[4].Name = "class_desc";
                Dgv.Columns[4].HeaderText = "Class Teacher For";
                Dgv.Columns[4].DataPropertyName = "class_desc";
                Dgv.Columns[4].Width = 250;


                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from Fac_MaSt where  Fac_Name='" + txtroute.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    con.Close();
                    con.Open();
                    SqlParameter[] para ={
                        new SqlParameter("@uid","0"),
                        new SqlParameter("@Fac_Name",txtroute.Text),
                        new SqlParameter("@EmpId",Convert.ToInt16(txtroute.Tag)),
                        new SqlParameter("@Class",Convert.ToInt16(cbocls.SelectedValue)),
                        new SqlParameter("@Returnid ",SqlDbType.Int),
                    };
                    para[4].Direction = ParameterDirection.Output;
                    int uid = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Fac_MaSt", para, con, 4));



                    for (int j = 0; j < Dgvlist.RowCount - 1; j++)
                    {
                        SqlParameter[] para1 ={
                            new SqlParameter("@Fac_id",uid),
                            new SqlParameter("@Classid", Convert.ToInt16(Dgvlist.Rows[j].Cells[3].Value)),
                            new SqlParameter("@subid", Convert.ToInt16(Dgvlist.Rows[j].Cells[4].Value)),
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_fac_det", para1, con);
                    }
                }
            }
            else
            {
                con.Close();
                con.Open();
                cmd.CommandText = "delete from fac_det where Fac_id=" + txtuid.Text + "";
                cmd.ExecuteNonQuery();
                SqlParameter[] para ={
                    new SqlParameter("@uid",txtuid.Text),
                    new SqlParameter("@Fac_Name",txtroute.Text),
                    new SqlParameter("@EmpId",Convert.ToInt16(txtroute.Tag)),
                    new SqlParameter("@Class",Convert.ToInt16(cbocls.SelectedValue)),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Fac_MaStUpdate", para, con);

                con.Close();
                con.Open();

                for (int j = 0; j < Dgvlist.RowCount - 1; j++)
                {
                    SqlParameter[] para1 ={
                        new SqlParameter("@Fac_id",txtuid.Text),
                        new SqlParameter("@Classid", Convert.ToInt16(Dgvlist.Rows[j].Cells[3].Value)),
                        new SqlParameter("@subid", Convert.ToInt16(Dgvlist.Rows[j].Cells[4].Value)),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_fac_det", para1, con);
                }
            }
            MessageBox.Show("Record  Saved ");
            clearTxt();
            con.Close();
            Dgvlist.Rows.Clear();
            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grid();
            btnDelete.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

        }
        private void TxtfeeM_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;


                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txttime1.Focus();


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }



        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            Dgvlist.Rows.Clear();
            txtroute.Text = string.Empty;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Save";
        }

        private void TxtfeeM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsclass.Filter = string.Format("Class_desc LIKE '%{0}%' ", TxtfeeM.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Titlep()
        {

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = 5;
            Dgvlist.Columns[0].Name = "uid";
            Dgvlist.Columns[0].HeaderText = "uid";
            Dgvlist.Columns[0].DataPropertyName = "uid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "Class";
            Dgvlist.Columns[1].HeaderText = "Class";
            Dgvlist.Columns[1].DataPropertyName = "Class";
            Dgvlist.Columns[1].Width = 150;

            Dgvlist.Columns[2].Name = "Subject";
            Dgvlist.Columns[2].HeaderText = "Subject";
            Dgvlist.Columns[2].DataPropertyName = "Subject";
            Dgvlist.Columns[2].Width = 150;

            Dgvlist.Columns[3].Name = "classid";
            Dgvlist.Columns[3].HeaderText = "classid";
            Dgvlist.Columns[3].DataPropertyName = "classid";
            Dgvlist.Columns[3].Visible = false;

            Dgvlist.Columns[4].Name = "subid";
            Dgvlist.Columns[4].HeaderText = "subid";
            Dgvlist.Columns[4].DataPropertyName = "subid";
            Dgvlist.Columns[4].Visible = false;

        }

        private void TitleC()
        {

        }

        private void btnok_Click(object sender, EventArgs e)
        {

            if (TxtfeeM.Text == "")
            {
                MessageBox.Show("Select the CLASS");
                TxtfeeM.Focus();
                return;
            }
            if (txttime1.Text == "")
            {
                MessageBox.Show("Enter the SUBJECT");
                txttime1.Focus();
                return;
            }

            string qur = "select  CID,Class_desc  from class_mast where cid=" + TxtfeeM.Tag + "";
            cmd = new SqlCommand(qur, con);
            SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = '0';
                Dgvlist.Rows[index].Cells[1].Value = tap1.Rows[i]["Class_desc"].ToString();
                Dgvlist.Rows[index].Cells[2].Value = txttime1.Text;
                Dgvlist.Rows[index].Cells[3].Value = tap1.Rows[i]["CID"].ToString();
                Dgvlist.Rows[index].Cells[4].Value = txttime1.Tag;
            }
            clearTxt();

        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtroute.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtroute.Tag = Dgv.Rows[i].Cells[2].Value.ToString();
            cbocls.Tag = Dgv.Rows[i].Cells[3].Value.ToString();
            cbocls.Text = Dgv.Rows[i].Cells[4].Value.ToString();

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Update";
            Titlep();

            string quy = "select a.uid,b.Class_desc,c.Sub_desc,a.Classid,a.subid from fac_det a left join class_mast b on a.Classid=b.cid left join sub_mAst c on a.subid=c.uid where a.Fac_id=" + txtuid.Text + "";
            cmd = new SqlCommand(quy, con);

            SqlDataAdapter aptr = new SqlDataAdapter(cmd);
            DataTable tap1 = new DataTable();
            aptr.Fill(tap1);

            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = tap1.Rows[k]["uid"].ToString();
                Dgvlist.Rows[index].Cells[1].Value = tap1.Rows[k]["Class_desc"].ToString();
                Dgvlist.Rows[index].Cells[2].Value = tap1.Rows[k]["Sub_desc"].ToString();
                Dgvlist.Rows[index].Cells[3].Value = tap1.Rows[k]["Classid"].ToString();
                Dgvlist.Rows[index].Cells[4].Value = tap1.Rows[k]["subid"].ToString();
            }
        }

        public void clearTxt()
        {

            TxtfeeM.Text = "";
            txttime2.Text = "";
            txttime1.Text = "";
            txtstrength.Text = "";

        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();

                string qur = "select uid,Fac_Name,a.empid,a.class,b.class_desc from Fac_MaSt a left join class_mast b   on a.Class=b.cid left join emp_mast c on a.EmpId=c.EmpId  where  Fac_Name like '%" + txtscr.Text + "%' or  class_desc like '%" + txtscr.Text + "%' ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 5;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Fac_Name";
                Dgv.Columns[1].HeaderText = "Faculty Name";
                Dgv.Columns[1].DataPropertyName = "Fac_Name";
                Dgv.Columns[1].Width = 300;

                Dgv.Columns[2].DataPropertyName = "empid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "class";
                Dgv.Columns[3].Visible = false;
                Dgv.Columns[4].Name = "class_desc";
                Dgv.Columns[4].HeaderText = "Class Teacher For";
                Dgv.Columns[4].DataPropertyName = "class_desc";
                Dgv.Columns[4].Width = 250;


                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void TxtfeeM_MouseClick(object sender, MouseEventArgs e)
        {
            Module.Dtype = 2;
            DataTable dt = getParty();
            bsclass.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(TxtfeeM);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        protected void getemp()
        {
            try
            {
                con.Close();
                con.Open();
                string Query = "Select buid,boardingp from boardingpm order by buid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["boardingp"].ToString();
                        coll.Add(RegNo);
                    }
                }
                TxtfeeM.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                TxtfeeM.AutoCompleteSource = AutoCompleteSource.CustomSource;
                TxtfeeM.AutoCompleteCustomSource = coll;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void TxtfeeM_Leave(object sender, EventArgs e)
        {
            try
            {
                if (TxtfeeM.Text != string.Empty)
                {
                    string Query = "Select buid,Boardingp from boardingpm  Where Boardingp ='" + TxtfeeM.Text + "'";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if (dt.Rows.Count != 0)
                    {
                        TxtfeeM.Tag = dt.Rows[0]["buid"].ToString();

                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the Faculty ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());


                    string Query1 = "Delete from fac_det Where Fac_id = @Fac_id";
                    SqlCommand cmd1 = new SqlCommand(Query1, con);
                    cmd1.Parameters.AddWithValue("@Fac_id", SqlDbType.Int).Value = Uid;
                    cmd1.ExecuteNonQuery();
                    string Query = "Delete from Fac_MaSt Where uid = @uid";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@uid", SqlDbType.Int).Value = Uid;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtroute_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsemp.Filter = string.Format("empname LIKE '%{0}%' ", txtroute.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txttime1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bssub.Filter = string.Format("Sub_Desc LIKE '%{0}%' ", txttime1.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtroute_MouseClick(object sender, MouseEventArgs e)
        {
            Module.Dtype = 1;
            DataTable dt = getParty();
            bsemp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtroute);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txttime1_MouseClick(object sender, MouseEventArgs e)
        {
            Module.Dtype = 3;
            DataTable dt = getParty();
            bssub.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txttime1);
            grSearch.Location = new Point(loc.X - 80, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Module.Dtype == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETEMP", con);
                    bsemp.DataSource = dt;
                }
                else if (Module.Dtype == 2)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", con);
                    bsclass.DataSource = dt;
                }
                else if (Module.Dtype == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETSUB", con);
                    bssub.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "EmpId";
                DataGridCommon.Columns[0].HeaderText = "EmpId";
                DataGridCommon.Columns[0].DataPropertyName = "EmpId";

                DataGridCommon.Columns[1].Name = "empname";
                DataGridCommon.Columns[1].HeaderText = "Employee";
                DataGridCommon.Columns[1].DataPropertyName = "empname";
                DataGridCommon.Columns[1].Width = 200;

                DataGridCommon.DataSource = bsemp;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "CID";
                DataGridCommon.Columns[0].HeaderText = "CID";
                DataGridCommon.Columns[0].DataPropertyName = "CID";

                DataGridCommon.Columns[1].Name = "Class_desc";
                DataGridCommon.Columns[1].HeaderText = "Class";
                DataGridCommon.Columns[1].DataPropertyName = "Class_desc";
                DataGridCommon.Columns[1].Width = 200;

                DataGridCommon.DataSource = bsclass;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "UID";
                DataGridCommon.Columns[0].HeaderText = "UID";
                DataGridCommon.Columns[0].DataPropertyName = "UID";

                DataGridCommon.Columns[1].Name = "Sub_Desc";
                DataGridCommon.Columns[1].HeaderText = "Subject";
                DataGridCommon.Columns[1].DataPropertyName = "Sub_Desc";
                DataGridCommon.Columns[1].Width = 200;

                DataGridCommon.DataSource = bssub;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Module.Dtype == 1)
                {
                    txtroute.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtroute.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Module.Dtype == 2)
                {
                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Module.Dtype == 3)
                {
                    txttime1.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txttime1.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                txttime1.Focus();
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtroute_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;


                    txtroute.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtroute.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cbocls.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txttime1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txttime1.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txttime1.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    btnok.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Module.Dtype == 1)
                {
                    txtroute.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtroute.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Module.Dtype == 2)
                {
                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Module.Dtype == 3)
                {
                    txttime1.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txttime1.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                txttime1.Focus();
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Module.Dtype == 1)
                {
                    txtroute.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtroute.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Module.Dtype == 2)
                {
                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Module.Dtype == 3)
                {
                    txttime1.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txttime1.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                txttime1.Focus();

                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

