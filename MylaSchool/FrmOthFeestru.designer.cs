﻿namespace MylaSchool
{
    partial class FrmOthFeestru
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOthFeestru));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.chckBulkInsert = new System.Windows.Forms.CheckBox();
            this.btnedit = new System.Windows.Forms.Button();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.txtfee = new System.Windows.Forms.TextBox();
            this.txtstruid = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtsid = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbofees = new System.Windows.Forms.ComboBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtadno = new System.Windows.Forms.TextBox();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.Dgvlist = new System.Windows.Forms.DataGridView();
            this.btnok = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtoid = new System.Windows.Forms.TextBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.grImport = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnImportSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.DataGridImport = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbFees = new System.Windows.Forms.ComboBox();
            this.cmbTerm = new System.Windows.Forms.ComboBox();
            this.GBList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).BeginInit();
            this.grImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).BeginInit();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.chckBulkInsert);
            this.GBList.Controls.Add(this.btnedit);
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.txtscr2);
            this.GBList.Controls.Add(this.txtscr1);
            this.GBList.Controls.Add(this.btnser);
            this.GBList.Controls.Add(this.txtscr);
            this.GBList.Controls.Add(this.txtfee);
            this.GBList.Controls.Add(this.txtstruid);
            this.GBList.Controls.Add(this.btnexit);
            this.GBList.Controls.Add(this.txtsid);
            this.GBList.Controls.Add(this.label7);
            this.GBList.Controls.Add(this.label6);
            this.GBList.Controls.Add(this.cbofees);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.txtsname);
            this.GBList.Controls.Add(this.label2);
            this.GBList.Controls.Add(this.txtadno);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.Dgvlist);
            this.GBList.Controls.Add(this.btnok);
            this.GBList.Controls.Add(this.txtamt);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.txtoid);
            this.GBList.Location = new System.Drawing.Point(5, -1);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(852, 503);
            this.GBList.TabIndex = 4;
            this.GBList.TabStop = false;
            // 
            // chckBulkInsert
            // 
            this.chckBulkInsert.AutoSize = true;
            this.chckBulkInsert.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckBulkInsert.Location = new System.Drawing.Point(757, 367);
            this.chckBulkInsert.Name = "chckBulkInsert";
            this.chckBulkInsert.Size = new System.Drawing.Size(93, 22);
            this.chckBulkInsert.TabIndex = 160;
            this.chckBulkInsert.Text = "Bulk Insert";
            this.chckBulkInsert.UseVisualStyleBackColor = true;
            this.chckBulkInsert.CheckedChanged += new System.EventHandler(this.ChckBulkInsert_CheckedChanged);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnedit.Location = new System.Drawing.Point(770, 256);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(70, 30);
            this.btnedit.TabIndex = 159;
            this.btnedit.Text = "Delete";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Click += new System.EventHandler(this.Btnedit_Click);
            // 
            // txtssno
            // 
            this.txtssno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtssno.Location = new System.Drawing.Point(795, 119);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 26);
            this.txtssno.TabIndex = 158;
            this.txtssno.Visible = false;
            // 
            // txtscr2
            // 
            this.txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(574, 107);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(100, 26);
            this.txtscr2.TabIndex = 8;
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(238, 107);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(334, 26);
            this.txtscr1.TabIndex = 7;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(675, 105);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(76, 30);
            this.btnser.TabIndex = 155;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.Btnser_Click);
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(9, 107);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(226, 26);
            this.txtscr.TabIndex = 6;
            // 
            // txtfee
            // 
            this.txtfee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtfee.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfee.Location = new System.Drawing.Point(739, 41);
            this.txtfee.MaxLength = 250;
            this.txtfee.Name = "txtfee";
            this.txtfee.Size = new System.Drawing.Size(49, 26);
            this.txtfee.TabIndex = 153;
            this.txtfee.Visible = false;
            // 
            // txtstruid
            // 
            this.txtstruid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstruid.Location = new System.Drawing.Point(795, 93);
            this.txtstruid.Name = "txtstruid";
            this.txtstruid.Size = new System.Drawing.Size(35, 26);
            this.txtstruid.TabIndex = 152;
            this.txtstruid.Visible = false;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(770, 302);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(70, 30);
            this.btnexit.TabIndex = 151;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // txtsid
            // 
            this.txtsid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsid.Location = new System.Drawing.Point(792, 67);
            this.txtsid.Name = "txtsid";
            this.txtsid.Size = new System.Drawing.Size(35, 26);
            this.txtsid.TabIndex = 150;
            this.txtsid.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(483, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 18);
            this.label7.TabIndex = 149;
            this.label7.Text = "Amount";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(193, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 18);
            this.label6.TabIndex = 148;
            this.label6.Text = "Fees";
            // 
            // cbofees
            // 
            this.cbofees.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbofees.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofees.FormattingEnabled = true;
            this.cbofees.Location = new System.Drawing.Point(232, 54);
            this.cbofees.Name = "cbofees";
            this.cbofees.Size = new System.Drawing.Size(247, 26);
            this.cbofees.TabIndex = 4;
            this.cbofees.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbofees_KeyPress);
            // 
            // cboTerm
            // 
            this.cboTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(47, 54);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(136, 26);
            this.cboTerm.TabIndex = 3;
            this.cboTerm.SelectedIndexChanged += new System.EventHandler(this.CboTerm_SelectedIndexChanged);
            this.cboTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboTerm_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 18);
            this.label5.TabIndex = 145;
            this.label5.Text = "Term";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(219, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 144;
            this.label3.Text = "Student Name";
            // 
            // txtsname
            // 
            this.txtsname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtsname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsname.Location = new System.Drawing.Point(321, 17);
            this.txtsname.MaxLength = 250;
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(247, 26);
            this.txtsname.TabIndex = 1;
            this.txtsname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtsname_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(568, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 18);
            this.label2.TabIndex = 142;
            this.label2.Text = "Admission No";
            // 
            // txtadno
            // 
            this.txtadno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadno.Location = new System.Drawing.Point(664, 17);
            this.txtadno.MaxLength = 250;
            this.txtadno.Name = "txtadno";
            this.txtadno.Size = new System.Drawing.Size(122, 26);
            this.txtadno.TabIndex = 2;
            // 
            // txtcid
            // 
            this.txtcid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcid.Location = new System.Drawing.Point(792, 41);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 26);
            this.txtcid.TabIndex = 131;
            this.txtcid.Visible = false;
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(47, 17);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 26);
            this.txtclass.TabIndex = 0;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtclass_MouseClick);
            this.txtclass.TextChanged += new System.EventHandler(this.Txtclass_TextChanged);
            // 
            // Dgvlist
            // 
            this.Dgvlist.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgvlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgvlist.Location = new System.Drawing.Point(8, 135);
            this.Dgvlist.Name = "Dgvlist";
            this.Dgvlist.ReadOnly = true;
            this.Dgvlist.RowHeadersVisible = false;
            this.Dgvlist.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Dgvlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgvlist.Size = new System.Drawing.Size(743, 352);
            this.Dgvlist.TabIndex = 127;
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.White;
            this.btnok.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Image = ((System.Drawing.Image)(resources.GetObject("btnok.Image")));
            this.btnok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnok.Location = new System.Drawing.Point(669, 52);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(64, 30);
            this.btnok.TabIndex = 126;
            this.btnok.Text = "Add";
            this.btnok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.Btnok_Click);
            // 
            // txtamt
            // 
            this.txtamt.BackColor = System.Drawing.SystemColors.Window;
            this.txtamt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(543, 54);
            this.txtamt.MaxLength = 250;
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(120, 26);
            this.txtamt.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 18);
            this.label1.TabIndex = 117;
            this.label1.Text = "Class";
            // 
            // txtoid
            // 
            this.txtoid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoid.Location = new System.Drawing.Point(792, 15);
            this.txtoid.Name = "txtoid";
            this.txtoid.Size = new System.Drawing.Size(35, 26);
            this.txtoid.TabIndex = 109;
            this.txtoid.Visible = false;
            // 
            // btnImport
            // 
            this.btnImport.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnImport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.Image = ((System.Drawing.Image)(resources.GetObject("btnImport.Image")));
            this.btnImport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImport.Location = new System.Drawing.Point(639, 23);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(83, 30);
            this.btnImport.TabIndex = 160;
            this.btnImport.Text = "Import";
            this.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // grImport
            // 
            this.grImport.Controls.Add(this.button2);
            this.grImport.Controls.Add(this.btnImportSave);
            this.grImport.Controls.Add(this.btnImport);
            this.grImport.Controls.Add(this.button1);
            this.grImport.Controls.Add(this.DataGridImport);
            this.grImport.Controls.Add(this.label8);
            this.grImport.Controls.Add(this.label4);
            this.grImport.Controls.Add(this.cmbFees);
            this.grImport.Controls.Add(this.cmbTerm);
            this.grImport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grImport.Location = new System.Drawing.Point(5, 5);
            this.grImport.Name = "grImport";
            this.grImport.Size = new System.Drawing.Size(751, 494);
            this.grImport.TabIndex = 161;
            this.grImport.TabStop = false;
            this.grImport.Visible = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(10, 456);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 30);
            this.button2.TabIndex = 162;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // btnImportSave
            // 
            this.btnImportSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnImportSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportSave.Image = ((System.Drawing.Image)(resources.GetObject("btnImportSave.Image")));
            this.btnImportSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImportSave.Location = new System.Drawing.Point(593, 456);
            this.btnImportSave.Name = "btnImportSave";
            this.btnImportSave.Size = new System.Drawing.Size(69, 30);
            this.btnImportSave.TabIndex = 161;
            this.btnImportSave.Text = "Save";
            this.btnImportSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImportSave.UseVisualStyleBackColor = false;
            this.btnImportSave.Click += new System.EventHandler(this.BtnImportSave_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(673, 456);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 30);
            this.button1.TabIndex = 152;
            this.button1.Text = "Close";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // DataGridImport
            // 
            this.DataGridImport.AllowUserToAddRows = false;
            this.DataGridImport.BackgroundColor = System.Drawing.Color.White;
            this.DataGridImport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridImport.Location = new System.Drawing.Point(7, 61);
            this.DataGridImport.Name = "DataGridImport";
            this.DataGridImport.ReadOnly = true;
            this.DataGridImport.RowHeadersVisible = false;
            this.DataGridImport.Size = new System.Drawing.Size(738, 393);
            this.DataGridImport.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "Term";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(295, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Fees";
            // 
            // cmbFees
            // 
            this.cmbFees.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFees.FormattingEnabled = true;
            this.cmbFees.Location = new System.Drawing.Point(353, 23);
            this.cmbFees.Name = "cmbFees";
            this.cmbFees.Size = new System.Drawing.Size(268, 26);
            this.cmbFees.TabIndex = 1;
            // 
            // cmbTerm
            // 
            this.cmbTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerm.FormattingEnabled = true;
            this.cmbTerm.Location = new System.Drawing.Point(73, 23);
            this.cmbTerm.Name = "cmbTerm";
            this.cmbTerm.Size = new System.Drawing.Size(210, 26);
            this.cmbTerm.TabIndex = 0;
            this.cmbTerm.SelectedIndexChanged += new System.EventHandler(this.CmbTerm_SelectedIndexChanged);
            // 
            // FrmOthFeestru
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(861, 504);
            this.Controls.Add(this.grImport);
            this.Controls.Add(this.GBList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOthFeestru";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Other Structure";
            this.Load += new System.EventHandler(this.FrmOthFeestru_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).EndInit();
            this.grImport.ResumeLayout(false);
            this.grImport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.DataGridView Dgvlist;
        internal System.Windows.Forms.Button btnok;
        internal System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtoid;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtadno;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.ComboBox cboTerm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbofees;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtsid;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtstruid;
        internal System.Windows.Forms.TextBox txtfee;
        internal System.Windows.Forms.TextBox txtscr2;
        internal System.Windows.Forms.TextBox txtscr1;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.TextBox txtssno;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.GroupBox grImport;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbFees;
        private System.Windows.Forms.ComboBox cmbTerm;
        private System.Windows.Forms.DataGridView DataGridImport;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chckBulkInsert;
        private System.Windows.Forms.Button btnImportSave;
        private System.Windows.Forms.Button button2;
    }
}