﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace MylaSchool
{
    public partial class FrmReConc : Form
    {
        public FrmReConc()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();

        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.SelectedIndex = -1;
            con.Close();
        }

        private void Loadput()
        {
            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";
            con.Close();
            con.Open();
            if (Module.type == 20)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1";
                Module.FSSQLSortStr = "Class_Desc";
            }
            else if (Module.type == 21)
            {
                FunC.Partylistviewcont3("uid", "name", "classid", Module.strsql, this, txtsid, txtsname, txtcid, GBList);
                Module.strsql = "select uid, sname + '/' + Admisno as name,classid from StudentM where Tag='A' and classid=" + txtcid.Text + " ";
                Module.FSSQLSortStr = "sname";
                Module.FSSQLSortStrA = "Admisno";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 20)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            else if (Module.type == 21)
            {
                dt.Columns[1].Width = 261;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 20)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 21)
                {
                    dt.Columns[Module.i].HeaderText = "Student Name";
                }
                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            con.Close();
        }

        private void FrmReConc_Load(object sender, EventArgs e)
        {
            LoadRefT();
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmdprt_Click(object sender, EventArgs e)
        {            
            if (cboclass.Text == "Studentwise")
            {
                List<ReqBulkUploadProcColl> BulkUploadCollectionList = new List<ReqBulkUploadProcColl>();
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add("@classuid", txtcid.Text);
                    param.Add("@uid", txtsid.Text);
                    param.Add("@termuid", cboTerm.SelectedValue);
                    BulkUploadCollectionList = con.Query<ReqBulkUploadProcColl>("Prc_ExportCollectionToServerDB", param, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                ResUploadCollectionDetailsProc objResUploadCollectionDetails = new ResUploadCollectionDetailsProc();
                if (BulkUploadCollectionList.Count > 0)
                {
                    DataTable DtCollection = new DataTable();
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(int) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(int) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(int) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "Paidamt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(int) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "classid", DataType = typeof(int) });
                    foreach (ReqBulkUploadProcColl ucd in BulkUploadCollectionList)
                    {
                        DataRow row = DtCollection.NewRow();
                        row["studid"] = ucd.Uid;
                        row["Feesturid"] = ucd.Fsuid;
                        row["feeid"] = ucd.Feeid;
                        row["feeamt"] = ucd.Amt;
                        row["concession"] = 0;
                        row["finalamt"] = ucd.Amt;
                        row["Paidamt"] = 0;
                        row["termuid"] = ucd.Termuid;
                        row["classid"] = ucd.Classuid;
                        DtCollection.Rows.Add(row);
                    }
                    using (SqlCommand cmd = new SqlCommand("Prc_Test_Bulk_UploadCollectionDetailsProc"))
                    {
                        DataTable dt = new DataTable();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Colland", DtCollection);
                        try
                        {
                            con.Open();
                            var dataReader = cmd.ExecuteReader();
                            dt.Load(dataReader);
                            objResUploadCollectionDetails.ResponseCode = Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                            objResUploadCollectionDetails.ResponseMessage = Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                            string sql = "select a.uid,classid,a.studid ,a.termuid ,b.amt,b.ConAmt ,b.Feecon,b.feeid   from Feecon a inner join Feecon_det b on a.uid =b.fconid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " and studid=" + txtsid.Text + "";
                            SqlCommand cmnd = new SqlCommand(sql, con);
                            SqlDataAdapter adpt = new SqlDataAdapter(cmnd);
                            DataTable tab = new DataTable();
                            adpt.Fill(tab);
                            for (int i = 0; i < tab.Rows.Count; i++)
                            {
                                double s = Convert.ToDouble(tab.Rows[i]["Amt"].ToString());
                                double s1 = Convert.ToDouble(tab.Rows[i]["ConAmt"].ToString());
                                double j = s - s1;
                                string qu = "update PLFType set concession =" + tab.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + tab.Rows[i]["studid"].ToString() + "  and feeid=" + tab.Rows[i]["feeid"].ToString() + "  and termuid=" + tab.Rows[i]["termuid"].ToString() + " and classid=" + tab.Rows[i]["classid"].ToString() + "";
                                SqlCommand scmd1 = new SqlCommand(qu, con);
                                scmd1.ExecuteNonQuery();
                            }
                            string sql11 = "select feeid  ,sum(paidamt) as paidamt from Coll_Mast a inner join Coll_det b on a.colluid=b.colluid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " and studid=" + txtsid.Text + " group by feeid ";
                            SqlCommand cmnd11 = new SqlCommand(sql11, con);
                            SqlDataAdapter adpt11 = new SqlDataAdapter(cmnd11);
                            DataTable tab11 = new DataTable();
                            adpt11.Fill(tab11);

                            for (int i = 0; i < tab11.Rows.Count; i++)
                            {
                                string strSQL2 = "update PLFType set paidamt =" + tab11.Rows[i]["paidamt"].ToString() + " where studid=" + txtsid.Text + " and classid=" + txtcid.Text + " and termuid =" + cboTerm.SelectedValue + " and feeid= " + tab11.Rows[i]["feeid"].ToString() + "";
                                SqlCommand scmd1 = new SqlCommand(strSQL2, con);
                                scmd1.ExecuteNonQuery();
                            }
                        }
                        catch (Exception)
                        {
                            objResUploadCollectionDetails.ResponseCode = 101;//Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                            objResUploadCollectionDetails.ResponseMessage = "Upload Fails";//Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                        }
                        finally
                        {
                            con.Close();
                            cmd.Dispose();
                        }
                    }
                }
            }
            else
            {
                List<ReqBulkUploadProcCollclass> BulkUploadCollectionList = new List<ReqBulkUploadProcCollclass>();
                DynamicParameters param = new DynamicParameters();
                param.Add("@classuid", txtcid.Text);
                param.Add("@termuid", cboTerm.SelectedValue);
                BulkUploadCollectionList = con.Query<ReqBulkUploadProcCollclass>("Prc_ExportCollectionToServerDBClass", param, null, true, 0, CommandType.StoredProcedure).ToList();
                ResUploadCollectionDetailsProclass objResUploadCollectionDetails = new ResUploadCollectionDetailsProclass();
                if (BulkUploadCollectionList.Count > 0)
                {
                    DataTable DtCollection = new DataTable();
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "Paidamt", DataType = typeof(decimal) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(Int32) });
                    DtCollection.Columns.Add(new DataColumn { ColumnName = "classid", DataType = typeof(Int32) });
                    foreach (ReqBulkUploadProcCollclass ucd in BulkUploadCollectionList)
                    {
                        DataRow row = DtCollection.NewRow();
                        row["studid"] = ucd.Studid;
                        row["Feesturid"] = ucd.Fsuid;
                        row["feeid"] = ucd.Feeid;
                        row["feeamt"] = ucd.Amt;
                        row["concession"] = 0;
                        row["finalamt"] = ucd.Amt;
                        row["Paidamt"] = 0;
                        row["termuid"] = ucd.Termuid;
                        row["classid"] = ucd.Classuid;
                        DtCollection.Rows.Add(row);
                    }
                    using (SqlCommand cmd = new SqlCommand("Prc_Test_Bulk_UploadCollectionDetailsProcclass"))
                    {
                        DataTable dt = new DataTable();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Colland", DtCollection);
                        try
                        {
                            con.Open();
                            var dataReader = cmd.ExecuteReader();
                            dt.Load(dataReader);
                            objResUploadCollectionDetails.ResponseCode = Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                            objResUploadCollectionDetails.ResponseMessage = Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                            string sql = "select a.uid,classid,a.studid ,a.termuid ,b.amt,b.ConAmt ,b.Feecon,b.feeid   from Feecon a inner join Feecon_det b on a.uid =b.fconid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " ";
                            SqlCommand cmnd = new SqlCommand(sql, con);
                            SqlDataAdapter adpt = new SqlDataAdapter(cmnd);
                            DataTable tab = new DataTable();
                            adpt.Fill(tab);
                            for (int i = 0; i < tab.Rows.Count; i++)
                            {
                                double s = Convert.ToDouble(tab.Rows[i]["Amt"].ToString());
                                double s1 = Convert.ToDouble(tab.Rows[i]["ConAmt"].ToString());
                                double j = s - s1;
                                string qu = "update PLFType set concession =" + tab.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + tab.Rows[i]["studid"].ToString() + "  and feeid=" + tab.Rows[i]["feeid"].ToString() + "  and termuid=" + tab.Rows[i]["termuid"].ToString() + " and classid=" + tab.Rows[i]["classid"].ToString() + "";
                                SqlCommand scmd1 = new SqlCommand(qu, con);
                                scmd1.ExecuteNonQuery();
                            }
                            string sql11 = "select feeid  ,sum(paidamt) as paidamt,studid from Coll_Mast a inner join Coll_det b on a.colluid=b.colluid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + "  group by feeid,studid ";
                            SqlCommand cmnd11 = new SqlCommand(sql11, con);
                            SqlDataAdapter adpt11 = new SqlDataAdapter(cmnd11);
                            DataTable tab11 = new DataTable();
                            adpt11.Fill(tab11);
                            for (int i = 0; i < tab11.Rows.Count; i++)
                            {
                                string strSQL2 = "update PLFType set paidamt =" + tab11.Rows[i]["paidamt"].ToString() + " where studid=" + tab11.Rows[i]["studid"].ToString() + " and classid=" + txtcid.Text + " and termuid =" + cboTerm.SelectedValue + " and feeid= " + tab11.Rows[i]["feeid"].ToString() + "";
                                SqlCommand scmd1 = new SqlCommand(strSQL2, con);
                                scmd1.ExecuteNonQuery();
                            }
                        }
                        catch (Exception)
                        {
                            objResUploadCollectionDetails.ResponseCode = 101;//Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                            objResUploadCollectionDetails.ResponseMessage = "Upload Fails";//Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                        }
                        finally
                        {
                            con.Close();
                            cmd.Dispose();
                        }
                    }
                }
            }
            MessageBox.Show("Processed");
        }

        private void Cboclass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboclass.Text == "Classwise")
            {
                txtsname.Visible = false;
                label3.Visible = false;
            }
            else
            {
                txtsname.Visible = true;
                label3.Visible = true;
            }
        }

        private void Txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 20;
            Loadput();
        }

        private void Txtsname_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtclass.Text == "")
            {
                MessageBox.Show("Select The Class Name ");
                txtclass.Focus();
                return;
            }
            Module.type = 21;
            Loadput();
        }

        private void Btnok_Click(object sender, EventArgs e)
        {
            List<ReqBulkUploadProcCollclass> BulkUploadCollectionList = new List<ReqBulkUploadProcCollclass>();
            DynamicParameters param = new DynamicParameters();
            param.Add("@classuid", txtcid.Text);
            param.Add("@termuid", cboTerm.SelectedValue);
            BulkUploadCollectionList = con.Query<ReqBulkUploadProcCollclass>("Prc_ExportCollectionToServerDBClass", param, null, true, 0, CommandType.StoredProcedure).ToList();
            ResUploadCollectionDetailsProc objResUploadCollectionDetails = new ResUploadCollectionDetailsProc();
            if (BulkUploadCollectionList.Count > 0)
            {
                DataTable DtCollection = new DataTable();
                DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });

                DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "Paidamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "classid", DataType = typeof(Int32) });

                foreach (ReqBulkUploadProcCollclass ucd in BulkUploadCollectionList)
                {
                    DataRow row = DtCollection.NewRow();
                    row["studid"] = ucd.Uid;
                    row["Feesturid"] = ucd.Fsuid;
                    row["feeid"] = ucd.Feeid;
                    row["feeamt"] = ucd.Amt;
                    row["concession"] = 0;
                    row["finalamt"] = ucd.Amt;
                    row["Paidamt"] = 0;
                    row["termuid"] = ucd.Termuid;
                    row["classid"] = ucd.Classuid;
                    DtCollection.Rows.Add(row);
                }
                using (SqlCommand cmd = new SqlCommand("Prc_Test_Bulk_UploadCollectionDetailsProcclass"))
                {
                    DataTable dt = new DataTable();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@Colland", DtCollection);
                    try
                    {
                        con.Open();
                        var dataReader = cmd.ExecuteReader();
                        dt.Load(dataReader);
                        objResUploadCollectionDetails.ResponseCode = Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                        objResUploadCollectionDetails.ResponseMessage = Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                        string sql = "select a.uid,classid,a.studid ,a.termuid ,b.amt,b.ConAmt ,b.Feecon,b.feeid   from Feecon a inner join Feecon_det b on a.uid =b.fconid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " and studid=" + txtsid.Text + "";
                        SqlCommand cmnd = new SqlCommand(sql, con);
                        SqlDataAdapter adpt = new SqlDataAdapter(cmnd);
                        DataTable tab = new DataTable();
                        adpt.Fill(tab);

                        for (int i = 0; i < tab.Rows.Count; i++)
                        {
                            double s = Convert.ToDouble(tab.Rows[i]["Amt"].ToString());
                            double s1 = Convert.ToDouble(tab.Rows[i]["ConAmt"].ToString());
                            double j = s - s1;
                            string qu = "update PLFType set concession =" + tab.Rows[i]["ConAmt"].ToString() + ",finalamt=" + j + " where studid =" + tab.Rows[i]["studid"].ToString() + "  and feeid=" + tab.Rows[i]["feeid"].ToString() + "  and termuid=" + tab.Rows[i]["termuid"].ToString() + " and classid=" + tab.Rows[i]["classid"].ToString() + "";
                            SqlCommand scmd1 = new SqlCommand(qu, con);
                            scmd1.ExecuteNonQuery();
                        }
                        string sql11 = "select feeid  ,sum(paidamt) as paidamt from Coll_Mast a inner join Coll_det b on a.colluid=b.colluid where classid=" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + " and studid=" + txtsid.Text + " group by feeid ";
                        SqlCommand cmnd11 = new SqlCommand(sql11, con);
                        SqlDataAdapter adpt11 = new SqlDataAdapter(cmnd11);
                        DataTable tab11 = new DataTable();
                        adpt11.Fill(tab11);

                        for (int i = 0; i < tab11.Rows.Count; i++)
                        {
                            string strSQL2 = "update PLFType set paidamt =" + tab11.Rows[i]["paidamt"].ToString() + " where studid=" + txtsid.Text + " and classid=" + txtcid.Text + " and termuid =" + cboTerm.SelectedValue + " and feeid= " + tab11.Rows[i]["feeid"].ToString() + "";
                            //string qu = "insert into PLFType values(" + tab11.Rows[i]["studid"].ToString() + "," + tab11.Rows[i]["feestructid"].ToString() + " ," + tab11.Rows[i]["feeid"].ToString() + " ," + tab11.Rows[i]["amount"].ToString() + ",0," + tab11.Rows[i]["amount"].ToString() + ",0," + tab11.Rows[i]["termuid"].ToString() + "," + tab11.Rows[i]["classid"].ToString() + "";
                            SqlCommand scmd1 = new SqlCommand(strSQL2, con);
                            scmd1.ExecuteNonQuery();
                        }
                    }
                    catch (Exception)
                    {
                        objResUploadCollectionDetails.ResponseCode = 101;//Convert.ToInt32(dt.Rows[0]["ResponseCode"]);
                        objResUploadCollectionDetails.ResponseMessage = "Upload Fails";//Convert.ToString(dt.Rows[0]["ResponseMessage"]);
                    }
                    finally
                    {
                        con.Close();
                        cmd.Dispose();
                    }
                }
            }
            MessageBox.Show("Processed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }

    public class ReqBulkUploadProcColl
    {

        public int Uid { get; set; }
        public int Fsuid { get; set; }
        public int Feeid { get; set; }
        public decimal Amt { get; set; }
        public decimal Concession { get; set; }
        //public decimal Amt { get; set; }
        public decimal Paidamt { get; set; }
        public int Termuid { get; set; }
        public string Classuid { get; set; }
    }

    public class ReqBulkUploadProcCollclass
    {
        public int Studid { get; set; }
        public int Uid { get; set; }
        public int Fsuid { get; set; }
        public int Feeid { get; set; }
        public decimal Amt { get; set; }
        public decimal Concession { get; set; }
        public decimal Paidamt { get; set; }
        public int Termuid { get; set; }
        public string Classuid { get; set; }
    }

    public class ResUploadCollectionDetailsProclass
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string Dtp { get; set; }
    }

    public class ResUploadCollectionDetailsProc
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string Dtp { get; set; }
    }
}
