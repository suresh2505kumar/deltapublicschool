﻿namespace MylaSchool
{
    public class GeneralParameters
    {
        public static string ServerName = string.Empty;
        public static string Password = string.Empty;
        public static string UserName = string.Empty;
        public static string DbName = string.Empty;
        public static string ConnectionString = string.Empty;
        public static int UserdId;
        public static string LoginUserName = string.Empty;
        public static int MenuId = 0;
    }
}
