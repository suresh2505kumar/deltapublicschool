﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;

namespace MylaSchool
{
    public partial class CRViewer : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private Container components = null;
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        ReportDocument doc = new ReportDocument();
        public CRViewer()
        {
            InitializeComponent();
        }

        private void CRViewer_Load(object sender, EventArgs e)
        {
            if (Module.Dtype == 10)
            {
                CreateReport();
            }
            //else if (Module.Dtype == 20)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport1();
            //}
            else if (Module.Dtype == 30)
            {
                CRViewer Crv = new CRViewer();
                CreateReport2();
            }
            else if (Module.Dtype == 31)
            {
                CRViewer Crv = new CRViewer();
                CreateReport3();
            }
            else if (Module.Dtype == 40)
            {
                CRViewer Crv = new CRViewer();
                CreateReport4();
            }
            else if (Module.Dtype == 41)
            {
                CRViewer Crv = new CRViewer();
                CreateReport5();
            }
            else if (Module.Dtype == 42)
            {
                CRViewer Crv = new CRViewer();
                CreateReport6();
            }
            //else if (Module.Dtype == 43)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport7();
            //}
            //else if (Module.Dtype == 50)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport8();
            //}
            //else if (Module.Dtype == 51)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport9();
            //}
            //else if (Module.Dtype == 25)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport10();

            //}
            //else if (Module.Dtype == 35)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport12();

            //}
            //else if (Module.Dtype == 36)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport13();
            //}
            //else if (Module.Dtype == 37)
            //{
            //    createReport14();
            //}
            //else if (Module.Dtype == 38)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport15();
            //}
            //else if (Module.Dtype == 39)
            //{
            //    CRViewer Crv = new CRViewer();
            //    createReport16();
            //}
        }

        //private void createReport15()
        //{
        //    try
        //    {
        //        if (Module.Dtype == 38)
        //        {
        //            doc = new ReportDocument();
        //            SqlDataAdapter da = new SqlDataAdapter("SP_GetAttandConduct", con);
        //            da.SelectCommand.CommandType = CommandType.StoredProcedure;
        //            da.SelectCommand.Parameters.Add("@Id", SqlDbType.Int).Value = 2;
        //            da.SelectCommand.Parameters.Add("@FromRegNo", SqlDbType.Int).Value = Module.FromAdmisNo;
        //            DataSet ds = new DataSet();
        //            da.Fill(ds, "AttConduct");
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                doc.Load(Application.StartupPath + "\\TC\\CryAttendanceandConduct.rpt");
        //                string SlNo = ds.Tables[0].Rows[i]["SlNo"].ToString() + "/" + ds.Tables[0].Rows[i]["CYear"].ToString();
        //                TextObject txtSlNo = (TextObject)doc.ReportDefinition.ReportObjects["txtSlNo"];
        //                txtSlNo.Text = SlNo;
        //                TextObject txtNofDaysW = (TextObject)doc.ReportDefinition.ReportObjects["txtToalDays"];
        //                txtNofDaysW.Text = ds.Tables[0].Rows[i]["NofDaysW"].ToString() + " Days";
        //                TextObject txtNofDaysP = (TextObject)doc.ReportDefinition.ReportObjects["txtNofP"];
        //                txtNofDaysP.Text = ds.Tables[0].Rows[i]["NofDaysP"].ToString() + " Days";
        //                decimal NofPdatys = Convert.ToDecimal(ds.Tables[0].Rows[i]["NofDaysP"].ToString());
        //                decimal NofWdatys = Convert.ToDecimal(ds.Tables[0].Rows[i]["NofDaysW"].ToString());
        //                decimal percentage = NofPdatys * 100 / NofWdatys;
        //                TextObject txtPercentage = (TextObject)doc.ReportDefinition.ReportObjects["txtPercentage"];
        //                txtPercentage.Text = percentage.ToString("0.0") + " %";
        //                //Connduct certificate
        //                string Sex = ds.Tables[0].Rows[i]["Sex"].ToString();
        //                string sof = string.Empty;
        //                if(Sex == "F")
        //                {
        //                    sof = "D/O";
        //                }
        //                else
        //                {
        //                    sof = "S/O";
        //                }
        //                TextObject txtSof = (TextObject)doc.ReportDefinition.ReportObjects["txtSonOf"];
        //                txtSof.Text = sof;

        //                string[] year = ds.Tables[0].Rows[i]["CYear"].ToString().Split('-');
        //                TextObject txtFrom = (TextObject)doc.ReportDefinition.ReportObjects["txtFrom"];
        //                txtFrom.Text = year[0];
        //                TextObject txtTo = (TextObject)doc.ReportDefinition.ReportObjects["txtTo"];
        //                txtTo.Text = year[1];
        //                string sCon = string.Empty;
        //                string sP = string.Empty;
        //                if (Sex == "F")
        //                {
        //                    sCon = "and that her character and conduct";
        //                    sP = "during the period of her Study.";
        //                }
        //                else
        //                {
        //                    sCon = "and that him character and conduct";
        //                    sP = "during the period of him Study.";
        //                }
        //                TextObject txtsCon = (TextObject)doc.ReportDefinition.ReportObjects["txtC"];
        //                txtsCon.Text = sCon;

        //                TextObject txtsP = (TextObject)doc.ReportDefinition.ReportObjects["txtPeroid"];
        //                txtsP.Text = sP;

        //                doc.SetDataSource(ds);
        //                crystalReportViewer1.RefreshReport();
        //                crystalReportViewer1.ReportSource = doc;
        //                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        return;
        //    }
        //}

        //private void createReport16()
        //{
        //    try
        //    {
        //        if (Module.Dtype == 39)
        //        {
        //            doc = new ReportDocument();
        //            SqlDataAdapter da = new SqlDataAdapter("SP_GetAttandConductBulk", con);
        //            da.SelectCommand.CommandType = CommandType.StoredProcedure;
        //            da.SelectCommand.Parameters.Add("@FromRegNo", SqlDbType.Int).Value = Module.FromAdmisNo;
        //            da.SelectCommand.Parameters.Add("@ToRegNo", SqlDbType.Int).Value = Module.ToAdmisNo;
        //            DataSet ds = new DataSet();
        //            da.Fill(ds, "AttConductBulk");
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                doc.Load(Application.StartupPath + "\\TC\\CryAttendanceandConductBulk.rpt");
        //                doc.SetDataSource(ds);
        //                crystalReportViewer1.RefreshReport();
        //                crystalReportViewer1.ReportSource = doc;
        //                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        return;
        //    }
        //}

        //private void createReport14()
        //{
        //    try
        //    {
        //        if (Module.Dtype == 37)
        //        {
        //            string SchoolName = ConfigurationManager.AppSettings["SclName"];
        //            string EducationalDist = ConfigurationManager.AppSettings["EducationalDist"];
        //            string REvenDist = ConfigurationManager.AppSettings["REvenDist"];
        //            ReportDocument doc8 = new ReportDocument();
        //            ReportDocument doc9 = new ReportDocument();
        //            SqlDataAdapter da = new SqlDataAdapter("SP_GetAdmisnoRange", con);
        //            da.SelectCommand.CommandType = CommandType.StoredProcedure;
        //            da.SelectCommand.Parameters.Add("@FromAdmisno", SqlDbType.Int).Value = Module.FromAdmisNo;
        //            da.SelectCommand.Parameters.Add("@ToAdmisno", SqlDbType.Int).Value = Module.ToAdmisNo;
        //            DataSet ds = new DataSet();
        //            da.Fill(ds, "TC");
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                if (Module.strsql == "Secondary")
        //                {
        //                    doc8.Load(Application.StartupPath + "\\TC\\SrvSamayapuramHSTc.rpt");
        //                    DateTime dob = Convert.ToDateTime(ds.Tables[0].Rows[i]["dob"].ToString());
        //                    //var date = WrittenNumerics.DateToWritten(dob);
        //                    //TextObject dobWords = (TextObject)doc8.ReportDefinition.ReportObjects["dobWords"];
        //                    //dobWords.Text = date;
        //                    //TextObject txtSchoolName = (TextObject)doc8.ReportDefinition.ReportObjects["txtSchoolName"];
        //                    //txtSchoolName.Text = SchoolName;
        //                    //TextObject txtEducationalDist = (TextObject)doc8.ReportDefinition.ReportObjects["txtEducation"];
        //                    //txtEducationalDist.Text = EducationalDist;
        //                    //TextObject txtREvenDist = (TextObject)doc8.ReportDefinition.ReportObjects["txtRevenue"];
        //                    //txtREvenDist.Text = REvenDist;
        //                    string Sex = ds.Tables[0].Rows[i]["Sex"].ToString();
        //                    if (Sex == "M")
        //                    {
        //                        Sex = "Male";
        //                    }
        //                    else
        //                    {
        //                        Sex = "Female";
        //                    }
        //                    TextObject txtSex = (TextObject)doc8.ReportDefinition.ReportObjects["TxtSex"];
        //                    txtSex.Text = Sex;
        //                }
        //                else
        //                {
        //                    doc8.Load(Application.StartupPath + "\\TC\\SrvSamayapuramHSTc.rpt");
        //                    //DateTime dob = Convert.ToDateTime(ds.Tables[0].Rows[i]["dob"].ToString());
        //                    //var date = WrittenNumerics.IntegerToWritten(dob.Day);
        //                    //var Month = dob.ToString("MMMM");
        //                    //var Year = WrittenNumerics.IntegerToWritten1(dob.Year);
        //                    //char[] yearw = dob.Year.ToString().ToCharArray();
        //                    //if (yearw[0].ToString() == "1")
        //                    //{
        //                    //    string Y = (yearw[0].ToString() + yearw[1].ToString()).ToString();
        //                    //    string Z = (yearw[2].ToString() + yearw[3].ToString()).ToString();
        //                    //    string d = WrittenNumerics.IntegerToWritten1((Convert.ToInt32(Y)));
        //                    //    string e = WrittenNumerics.IntegerToWritten1(Convert.ToInt32(Z));
        //                    //    Year = d + " " + e;
        //                    //}

        //                    //TextObject dob1 = (TextObject)doc8.ReportDefinition.ReportObjects["dob"];
        //                    //dob1.Text = dob.ToString("dd.MM.yyyy");

        //                    //TextObject dobWords = (TextObject)doc8.ReportDefinition.ReportObjects["DobWords"];
        //                    //dobWords.Text = date + " - " + Month + " - " + Year;

        //                    //DateTime doA = Convert.ToDateTime(ds.Tables[0].Rows[i]["DOA"].ToString());
        //                    //var Da = WrittenNumerics.IntegerToWritten1(doA.Year);
        //                    //TextObject doAT = (TextObject)doc8.ReportDefinition.ReportObjects["DOA"];
        //                    //doAT.Text = doA.ToString("dd.MM.yyyy") + " (" + Da + " )";
        //                    //string ClassJoin = ds.Tables[0].Rows[i]["Class_join"].ToString();
        //                    //if (ClassJoin == "XI")
        //                    //{
        //                    //    ClassJoin = "XI STANDARD";
        //                    //}

        //                    //TextObject doAWords = (TextObject)doc8.ReportDefinition.ReportObjects["DOAWords"];
        //                    //doAWords.Text = ClassJoin;

        //                    //string Community = ds.Tables[0].Rows[i]["Community"].ToString();
        //                    //string Caste = ds.Tables[0].Rows[i]["Caste"].ToString();
        //                    //TextObject txtcomcst = (TextObject)doc8.ReportDefinition.ReportObjects["txtcomcst"];
        //                    //txtcomcst.Text = Community + " - " + Caste;

        //                    //string Nation = ds.Tables[0].Rows[i]["Nationality"].ToString();
        //                    //string Relig = ds.Tables[0].Rows[i]["Religion"].ToString();
        //                    //TextObject txtNatRlg = (TextObject)doc8.ReportDefinition.ReportObjects["txtNatRlg"];
        //                    //txtNatRlg.Text = Nation + " - " + Relig;



        //                    string Sex = ds.Tables[0].Rows[i]["Sex"].ToString();
        //                    if (Sex == "M")
        //                    {
        //                        Sex = "Male";
        //                    }
        //                    else
        //                    {
        //                        Sex = "Female";
        //                    }
        //                    TextObject txtSex = (TextObject)doc8.ReportDefinition.ReportObjects["TxtSex"];
        //                    txtSex.Text = Sex;
        //                    string Lan = string.Empty;
        //                    string GroupCode = ds.Tables[0].Rows[i]["GroupCode"].ToString();
        //                    //if (GroupCode == "102-COMPUTER SCIENCE")
        //                    //{
        //                    //    Lan = "PHYSICS - CHEMISTRY - COMPUTER SCIENCE - MATHEMATICS";
        //                    //}
        //                    //else if (GroupCode == "103-BIOLOGY")
        //                    //{
        //                    //    Lan = "PHYSICS - CHEMISTRY - BIOLOGY - MATHEMATICS";
        //                    //}
        //                    //else if (GroupCode == "302-COMMERCE")
        //                    //{
        //                    //    Lan = "ACCOUNTANCY - ECONOMICS - COMPUTER SCIENCE";
        //                    //}
        //                    //else if (GroupCode == "308-COMMERCE")
        //                    //{
        //                    //    Lan = "ACCOUNTANCY - ECONOMICS - BUSINESS MATHS";
        //                    //}
        //                    //TextObject txtc = (TextObject)doc8.ReportDefinition.ReportObjects["txtGrCode"];
        //                    //txtc.Text = Lan;
        //                }
        //                doc8.SetDataSource(ds);
        //                crystalReportViewer1.RefreshReport();
        //                crystalReportViewer1.ReportSource = doc8;
        //                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        return;
        //    }
        //}

        private void CreateReport()
        {
            if (Module.Dtype == 10)
            {
                ReportDocument doc1 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_Printtmp", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@Colluid", SqlDbType.Int).Value = Module.ColluidPT;
                DataSet ds = new DataSet();
                da.Fill(ds, "CollMast");
                doc1.Load(Application.StartupPath + "\\Reports\\CollMast.rpt");
                doc1.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc1;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        private void CreateReport1()
        {
            if (Module.Dtype == 20)
            {
                ReportDocument doc4 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_PrintDuetmp", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@termuid", SqlDbType.Int).Value = Module.termuid;
                da.SelectCommand.Parameters.Add("@cid", SqlDbType.Int).Value = Module.cid;
                DataSet ds = new DataSet();
                da.Fill(ds, "OverDue");
                doc4.Load(Application.StartupPath + "\\Reports\\OverDue.rpt");
                doc4.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc4;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }
        private void CreateReport2()
        {
            if (Module.Dtype == 30)
            {
                ReportDocument doc2 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_Dailypnttmp", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@theDate", SqlDbType.NVarChar).Value = Module.theDate;
                da.SelectCommand.Parameters.Add("@toDate", SqlDbType.NVarChar).Value = Module.toDate;
                DataSet ds = new DataSet();
                da.Fill(ds, "Dailycollrpt");
                doc2.Load(Application.StartupPath + "\\Reports\\Dailycollrpt.rpt");
                doc2.SetDataSource(ds);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    crystalReportViewer1.RefreshReport();
                    crystalReportViewer1.ReportSource = doc2;
                    crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void CreateReport3()
        {
            if (Module.Dtype == 31)
            {
                ReportDocument doc3 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_FeeCollrpttmp", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@theDate", SqlDbType.NVarChar).Value = Module.theDate;
                da.SelectCommand.Parameters.Add("@toDate", SqlDbType.NVarChar).Value = Module.toDate;
                DataSet ds = new DataSet();
                da.Fill(ds, "FeeSummrpt");
                doc3.Load(Application.StartupPath + "\\Reports\\FeeSummrpt.rpt");
                doc3.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc3;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        private void CreateReport4()
        {
            if (Module.Dtype == 40)
            {
                ReportDocument doc5 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_studrptpntB", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("cid", SqlDbType.NVarChar).Value = Module.cid;
                DataSet ds = new DataSet();
                da.Fill(ds, "Sturpt");
                doc5.Load(Application.StartupPath + "\\Reports\\Sturpt.rpt");
                doc5.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc5;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }
        private void CreateReport5()
        {
            if (Module.Dtype == 41)
            {
                ReportDocument doc6 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_studrptpntG", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("cid", SqlDbType.NVarChar).Value = Module.cid;
                DataSet ds = new DataSet();
                da.Fill(ds, "Sturpt");
                doc6.Load(Application.StartupPath + "\\Reports\\Sturpt.rpt");
                doc6.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc6;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }
        private void CreateReport6()
        {
            if (Module.Dtype == 42)
            {
                ReportDocument doc7 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_studrptpntALL", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("cid", SqlDbType.NVarChar).Value = Module.cid;
                DataSet ds = new DataSet();
                da.Fill(ds, "Sturpt");
                doc7.Load(Application.StartupPath + "\\Reports\\Sturpt.rpt");
                doc7.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc7;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        private void CreateReport7()
        {
            if (Module.Dtype == 43)
            {
                ReportDocument doc8 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("sp_TermWiseFee", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@classid", SqlDbType.NVarChar).Value = Module.cid;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.NVarChar).Value = Module.theDate;
                da.SelectCommand.Parameters.Add("@termuid", SqlDbType.Int).Value = Module.termuid;
                DataSet ds = new DataSet();
                da.Fill(ds, "TermFeeRemrpt");
                doc8.Load(Application.StartupPath + "\\Reports\\TermFeeRemrpt.rpt");
                doc8.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc8;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        private void CreateReport8()
        {
            if (Module.Dtype == 50)
            {
                ReportDocument doc4 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_PrinttmpALL", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@classid", SqlDbType.Int).Value = Module.cid;
                DataSet ds = new DataSet();
                da.Fill(ds, "CollMast");
                doc4.Load(Application.StartupPath + "\\Reports\\CollMast.rpt");
                doc4.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc4;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        private void CreateReport9()
        {
            if (Module.Dtype == 51)
            {
                ReportDocument doc4 = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_PrintDuetmpTerm", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@termuid", SqlDbType.Int).Value = Module.termuid;
                DataSet ds = new DataSet();
                da.Fill(ds, "OverDueterm");
                doc4.Load(Application.StartupPath + "\\Reports\\OverDueterm.rpt");
                doc4.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc4;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }
        private void CreateReport10()
        {
            if (Module.Dtype == 25)
            {
                ReportDocument doc6 = new ReportDocument();


                string sql = "select  'SRVSCHOOL' as SchoolName, 'Trichy' as EducationalDistrict, 'Trichy' as RevenueDistrict, b.SName,a.StudNameTamil,b.FName as FatherName, b.Religion as NationalityReligion,b.Community ,b.Sex,b.dob,b.Identification1 as PersonalIdentification, b.Identification2 as PersonalIdentification1,b.doa as DateofAdmission,a.EMISno as EMISNumber,a.slno as SerialNo,b.Admisno as AdmissionNo,a.TMRcode,a.RegNo as CertificateNo ,a.RegNo as RegNo,c.Class_Desc as StdLeavingSch ,a.qph as PromationHigher,a.ScholorShip as Scholarship,a.MedicalInspection, a.dol as DOLSchl,a.conduct,a.DOTC,Mname,aadharno,feesdue  from TcStudDetails a inner join StudentM b on a.StudId = b.uid  inner join Class_Mast c on b.Classid = c.Cid and b.uid =  " + Module.ColluidPT + "       ";
                SqlDataAdapter da = new SqlDataAdapter(sql, con);
                da.SelectCommand.CommandType = CommandType.Text;

                DataSet ds = new DataSet();
                da.Fill(ds, "TcDataHdet");
                doc6.Load(Application.StartupPath + "\\Reports\\SrvExcelHSFirstPage.rpt");

                doc6.SetDataSource(ds);

                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc6;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        //private void createReport12()
        //{
        //    if (Module.Dtype == 35)
        //    {
        //        string SchoolName = ConfigurationManager.AppSettings["SclName"];
        //        string EducationalDist = ConfigurationManager.AppSettings["EducationalDist"];
        //        string REvenDist = ConfigurationManager.AppSettings["REvenDist"];
        //        ReportDocument doc8 = new ReportDocument();
        //        ReportDocument doc9 = new ReportDocument();
        //        SqlDataAdapter da = new SqlDataAdapter("SP_GetStudentTc", con);
        //        da.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        da.SelectCommand.Parameters.Add("@studid", SqlDbType.Int).Value = Module.ColluidPT;
        //        DataSet ds = new DataSet();
        //        da.Fill(ds, "TC");
        //        if (Module.strsql == "Secondary")
        //        {
        //            doc8.Load(Application.StartupPath + "\\TC\\srvExcell_FIRST_Tamil.rpt");
        //            DateTime dob = Convert.ToDateTime(ds.Tables[0].Rows[0]["dob"].ToString());
        //            var date = WrittenNumerics.DateToWritten(dob);
        //            TextObject dobWords = (TextObject)doc8.ReportDefinition.ReportObjects["dobWords"];
        //            dobWords.Text = date;
        //            TextObject txtSchoolName = (TextObject)doc8.ReportDefinition.ReportObjects["txtSchoolName"];
        //            txtSchoolName.Text = SchoolName;
        //            TextObject txtEducationalDist = (TextObject)doc8.ReportDefinition.ReportObjects["txtEducation"];
        //            txtEducationalDist.Text = EducationalDist;
        //            TextObject txtREvenDist = (TextObject)doc8.ReportDefinition.ReportObjects["txtRevenue"];
        //            txtREvenDist.Text = REvenDist;
        //            string Sex = ds.Tables[0].Rows[0]["Sex"].ToString();
        //            if (Sex == "M")
        //            {
        //                Sex = "Male";
        //            }
        //            else
        //            {
        //                Sex = "Female";
        //            }
        //            TextObject txtSex = (TextObject)doc8.ReportDefinition.ReportObjects["txtSex"];
        //            txtSex.Text = Sex;

        //        }
        //        else
        //        {
        //            doc8.Load(Application.StartupPath + "\\TC\\SrvSamayapuramHSTc.rpt");
        //            //DateTime dob = Convert.ToDateTime(ds.Tables[0].Rows[0]["dob"].ToString());
        //            //var date = WrittenNumerics.IntegerToWritten(dob.Day);
        //            //var Month = dob.ToString("MMMM");
        //            //var Year = WrittenNumerics.IntegerToWritten1(dob.Year);
        //            //char[] yearw = dob.Year.ToString().ToCharArray();
        //            //if (yearw[0].ToString() == "1")
        //            //{
        //            //    string Y = (yearw[0].ToString() + yearw[1].ToString()).ToString();
        //            //    string Z = (yearw[2].ToString() + yearw[3].ToString()).ToString();
        //            //    string d = WrittenNumerics.IntegerToWritten1((Convert.ToInt32(Y)));
        //            //    string e = WrittenNumerics.IntegerToWritten1(Convert.ToInt32(Z));
        //            //    Year = d + " " + e;
        //            //}

        //            //TextObject dob1 = (TextObject)doc8.ReportDefinition.ReportObjects["dob"];
        //            //dob1.Text = dob.ToString("dd.MM.yyyy");

        //            //TextObject dobWords = (TextObject)doc8.ReportDefinition.ReportObjects["DobWords"];
        //            //dobWords.Text = date + " - " + Month + " - " + Year;

        //            //DateTime doA = Convert.ToDateTime(ds.Tables[0].Rows[0]["DOA"].ToString());
        //            //var Da = WrittenNumerics.IntegerToWritten1(doA.Year);
        //            //TextObject doAT = (TextObject)doc8.ReportDefinition.ReportObjects["DOA"];
        //            //doAT.Text = doA.ToString("dd.MM.yyyy") + " (" + Da + " )";
        //            //string ClassJoin = ds.Tables[0].Rows[0]["Class_join"].ToString();
        //            //if (ClassJoin == "XI")
        //            //{
        //            //    ClassJoin = "XI STANDARD";
        //            //}

        //            //TextObject doAWords = (TextObject)doc8.ReportDefinition.ReportObjects["DOAWords"];
        //            //doAWords.Text = ClassJoin;

        //            //string Community = ds.Tables[0].Rows[0]["Community"].ToString();
        //            //string Caste = ds.Tables[0].Rows[0]["Caste"].ToString();
        //            //TextObject txtcomcst = (TextObject)doc8.ReportDefinition.ReportObjects["txtcomcst"];
        //            //txtcomcst.Text = Community + " - " + Caste;

        //            //string Nation = ds.Tables[0].Rows[0]["Nationality"].ToString();
        //            //string Relig = ds.Tables[0].Rows[0]["Religion"].ToString();
        //            //TextObject txtNatRlg = (TextObject)doc8.ReportDefinition.ReportObjects["txtNatRlg"];
        //            //txtNatRlg.Text = Nation + " - " + Relig;



        //            string Sex = ds.Tables[0].Rows[0]["Sex"].ToString();
        //            if (Sex == "M")
        //            {
        //                Sex = "Male";
        //            }
        //            else
        //            {
        //                Sex = "Female";
        //            }
        //            TextObject txtSex = (TextObject)doc8.ReportDefinition.ReportObjects["TxtSex"];
        //            txtSex.Text = Sex;
        //            //string Lan = string.Empty;
        //            //string GroupCode = ds.Tables[0].Rows[0]["GroupCode"].ToString();
        //            //if (GroupCode == "102-COMPUTER SCIENCE")
        //            //{
        //            //    Lan = "PHYSICS - CHEMISTRY - COMPUTER SCIENCE - MATHEMATICS";
        //            //}
        //            //else if (GroupCode == "103-BIOLOGY")
        //            //{
        //            //    Lan = "PHYSICS - CHEMISTRY - BIOLOGY - MATHEMATICS";
        //            //}
        //            //else if (GroupCode == "302-COMMERCE")
        //            //{
        //            //    Lan = "ACCOUNTANCY - ECONOMICS - COMPUTER SCIENCE";
        //            //}
        //            //else if (GroupCode == "308-COMMERCE")
        //            //{
        //            //    Lan = "ACCOUNTANCY - ECONOMICS - BUSINESS MATHS";
        //            //}
        //            //TextObject txtc = (TextObject)doc8.ReportDefinition.ReportObjects["txtGrCode"];
        //            //txtc.Text = Lan;
        //        }
        //        doc8.SetDataSource(ds);
        //        crystalReportViewer1.RefreshReport();
        //        crystalReportViewer1.ReportSource = doc8;
        //        crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;

        //    }
        //}
        private void CreateReport13()
        {
            if (Module.Dtype == 36)
            {
                doc = new ReportDocument();
                SqlDataAdapter da = new SqlDataAdapter("SP_GetStudentTc", con);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@studid", SqlDbType.Int).Value = Module.ColluidPT;
                DataSet ds = new DataSet();
                da.Fill(ds, "TC");
                if (Module.strsql == "Secondary")
                {
                    doc.Load(Application.StartupPath + "\\TC\\srvExcel_sec_Tamil.rpt");

                }
                else
                {
                    doc.Load(Application.StartupPath + "\\TC\\SrvExcelHSSecondPage.rpt");

                    DateTime left = Convert.ToDateTime(ds.Tables[0].Rows[0]["dol"].ToString());
                    TextObject txtdol = (TextObject)doc.ReportDefinition.ReportObjects["txtdol"];
                    txtdol.Text = left.ToString("dd.MM.yyyy");

                    DateTime appliedTC = Convert.ToDateTime(ds.Tables[0].Rows[0]["appliedTC"].ToString());
                    TextObject txtappliedTC = (TextObject)doc.ReportDefinition.ReportObjects["txtappliedTC"];
                    txtappliedTC.Text = appliedTC.ToString("dd.MM.yyyy");

                    DateTime DOTC = Convert.ToDateTime(ds.Tables[0].Rows[0]["dotc"].ToString());
                    TextObject txtdotc = (TextObject)doc.ReportDefinition.ReportObjects["txtdotc"];
                    txtdotc.Text = DOTC.ToString("dd.MM.yyyy");
                }

                doc.SetDataSource(ds);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc;
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            }
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CrystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
