﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmBoardCancel : Form
    {
        public FrmBoardCancel()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsParty = new BindingSource();
        BindingSource bsParty1 = new BindingSource();
        public int SelectId = 0;
        private void FrmBoardCancel_Load(object sender, EventArgs e)
        {
            SelectId = 1;
            Load_grids();
            checkBox1.Checked = false;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            chk.Checked = false;
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void Load_grids()
        {
            try
            {
                con.Close();
                con.Open();

                string qur = "select puid, a.passno,b.Class_Desc,c.SName,d.BoardingP,e.TripName,a.startDate,a.Enddate,a.status,a.classid,a.boardid,a.studid,a.tripid,a.adnochk,a.way from BoardPIssueM a  left join Class_Mast b on a.classid = b.Cid left join StudentM c on a.studid = c.uid left join BoardingPM d on a.boardid = d.Buid left join tripM e on a.tripid = e.tuid  where a.status='0' ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);
                this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 15;

                Dgv.Columns[0].Name = "puid";
                Dgv.Columns[0].HeaderText = "puid";
                Dgv.Columns[0].DataPropertyName = "puid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "passno";
                Dgv.Columns[1].HeaderText = "Passno";
                Dgv.Columns[1].DataPropertyName = "passno";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "Class_Desc";
                Dgv.Columns[2].HeaderText = "Class";
                Dgv.Columns[2].DataPropertyName = "Class_Desc";
                Dgv.Columns[2].Width = 100;

                Dgv.Columns[3].Name = "SName";
                Dgv.Columns[3].HeaderText = "Student";
                Dgv.Columns[3].DataPropertyName = "SName";
                Dgv.Columns[3].Width = 220;

                Dgv.Columns[4].Name = "BoardingP";
                Dgv.Columns[4].HeaderText = "Boarding";
                Dgv.Columns[4].DataPropertyName = "BoardingP";
                Dgv.Columns[4].Width = 150;

                Dgv.Columns[5].Name = "TripName";
                Dgv.Columns[5].HeaderText = "TripName";
                Dgv.Columns[5].DataPropertyName = "TripName";
                Dgv.Columns[5].Width = 150;

                Dgv.Columns[6].Name = "startDate";
                Dgv.Columns[6].HeaderText = "StartDate";
                Dgv.Columns[6].DataPropertyName = "startDate";
                Dgv.Columns[6].Visible = false;

                Dgv.Columns[7].Name = "Enddate";
                Dgv.Columns[7].HeaderText = "Enddate";
                Dgv.Columns[7].DataPropertyName = "Enddate";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "Status";
                Dgv.Columns[8].HeaderText = "Status";
                Dgv.Columns[8].DataPropertyName = "Status";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].DataPropertyName = "classid";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].DataPropertyName = "boardid";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].DataPropertyName = "studid";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].DataPropertyName = "tripid";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].DataPropertyName = "adnochk";
                Dgv.Columns[13].Visible = false;

                Dgv.Columns[14].DataPropertyName = "way";
                Dgv.Columns[14].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadFeeGrp()
        {
            con.Close();
            con.Open();
            string qur = "select puid, a.passno,b.Class_Desc,c.SName,d.BoardingP,e.TripName,a.startDate,a.Enddate,a.status,a.classid,a.boardid,a.studid,a.tripid,a.adnochk,a.way from BoardPIssueM a  left join Class_Mast b on a.classid = b.Cid left join StudentM c on a.studid = c.uid left join BoardingPM d on a.boardid = d.Buid left join tripM e on a.tripid = e.tuid  and a.status='1'  where a.puid=" + txtpassno.Tag + "";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            if (tap.Rows.Count > 0)
            {
                cboterm.Text = tap.Rows[0]["Class_Desc"].ToString();
                txtsname.Text = tap.Rows[0]["SName"].ToString();
                txtboar.Text = tap.Rows[0]["BoardingP"].ToString();
                cbofess.Text = tap.Rows[0]["TripName"].ToString();
                txtrent.Text = tap.Rows[0]["startDate"].ToString();
                dtp.Text = tap.Rows[0]["Enddate"].ToString();
                combobox1.Text = tap.Rows[0]["way"].ToString();

                cboterm.Tag = tap.Rows[0]["classid"].ToString();
                txtboar.Tag = tap.Rows[0]["boardid"].ToString();
                txtsname.Tag = tap.Rows[0]["studid"].ToString();
                cbofess.Tag = tap.Rows[0]["tripid"].ToString();

                if (tap.Rows[0]["adnochk"].ToString() == "1")
                {
                    checkBox1.Checked = true;
                }
                else
                {
                    checkBox1.Checked = false;
                }
            }
        }

        public void ClearTxt()
        {
            txtboar.Text = "";
            txtpassno.Text = "";
            txtsname.Text = "";
            cbofess.Text = "";
            cboterm.Text = "";
            txtrent.Text = "";
            combobox1.Text = "";
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            ClearTxt();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            checkBox1.Checked = false;
            btnsave.Text = "Update";
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            string ui, ui1;
            if (chk.Checked == true)
            {
                ui = "1";
            }
            else
            {
                ui = "0";
            }
            if (checkBox1.Checked == true)
            {
                ui1 = "1";
            }
            else
            {
                ui1 = "0";
            }
            con.Close();
            con.Open();
            SqlParameter[] para ={
                    new SqlParameter("@Passno",txtpassno.Text),
                    new SqlParameter("@classid",cboterm.Tag),
                    new SqlParameter("@studid",txtsname.Tag),
                    new SqlParameter("@boardid",txtboar.Tag),
                    new SqlParameter("@tripid",cbofess.Tag),
                    new SqlParameter("@startDate",Convert.ToDateTime(txtrent.Text)),
                    new SqlParameter("@Enddate",Convert.ToDateTime(dtp.Text)),
                    new SqlParameter("@status",ui),
                    new SqlParameter("@adnochk",ui1),
                    new SqlParameter("@way",combobox1.Text),
                    new SqlParameter("@puid",txtpassno.Tag),
                };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BoardPIssueMUpdate", para, con);
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            ClearTxt();
            con.Close();
            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grids();

            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            grSearch.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtpassno.Tag = Dgv.Rows[i].Cells[0].Value.ToString();
            txtpassno.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtboar.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            cbofess.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            txtrent.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            dtp.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            if (Dgv.Rows[i].Cells[8].Value.ToString() == "1")
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
            cboterm.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            cboterm.Tag = Dgv.Rows[i].Cells[9].Value.ToString();
            txtboar.Tag = Dgv.Rows[i].Cells[10].Value.ToString();
            txtsname.Tag = Dgv.Rows[i].Cells[11].Value.ToString();
            cbofess.Tag = Dgv.Rows[i].Cells[12].Value.ToString();
            combobox1.Text = Dgv.Rows[i].Cells[14].Value.ToString();
            if (Dgv.Rows[i].Cells[13].Value.ToString() == "1")
            {
                checkBox1.Checked = true;
                con.Close();
                con.Open();
                string qur = "select  sname + ' - '+ admisno as sname   from studentm  where uid=" + txtsname.Tag + " ";
                SqlCommand cmd = new SqlCommand(qur, con);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtsname.Text = tab.Rows[0]["sname"].ToString();
                }
            }
            else
            {
                checkBox1.Checked = false;
                txtsname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            }
            btnsave.Text = "Update";
        }

        protected DataTable GetParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetBoardDet", con);
                bsParty.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "pUid";
                DataGridCommon.Columns[0].HeaderText = "pUid";
                DataGridCommon.Columns[0].DataPropertyName = "pUid";
                DataGridCommon.Columns[1].Name = "Passno";
                DataGridCommon.Columns[1].HeaderText = "Passno";
                DataGridCommon.Columns[1].DataPropertyName = "Passno";
                DataGridCommon.Columns[1].Width = 200;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "SName";
                DataGridCommon.Columns[1].HeaderText = "Student";
                DataGridCommon.Columns[1].DataPropertyName = "SName";
                DataGridCommon.Columns[1].Width = 200;
                DataGridCommon.DataSource = bsParty1;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        
        private void AG()
        {
            con.Close();
            con.Open();
            string qur = "select  isnull(max(passno),0) + 1 as passno   from BoardPIssueM";
            SqlCommand cmd = new SqlCommand(qur, con);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            if (tab.Rows.Count > 0)
            {
                double str = Convert.ToDouble(tab.Rows[0]["passno"].ToString());
                txtpassno.Text = str.ToString();
            }
            con.Close();
        }
       
        private void Cbofees_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                txtpassno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpassno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                LoadFeeGrp();
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                txtpassno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpassno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                LoadFeeGrp();
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;


                txtpassno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpassno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                LoadFeeGrp();


                grSearch.Visible = false;
                SelectId = 0;
            }
        }
       
        private void Btnser_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();

            string qur = "select puid, a.passno,b.Class_Desc,c.SName,d.BoardingP,e.TripName,a.startDate,a.Enddate,a.status,a.classid,a.boardid,a.studid,a.tripid,a.adnochk,a.way from BoardPIssueM a  left join Class_Mast b on a.classid = b.Cid left join StudentM c on a.studid = c.uid left join BoardingPM d on a.boardid = d.Buid left join tripM e on a.tripid = e.tuid   where  a.status='0' and a.passno like '%" + txtscr.Text + "%' or   b.Class_Desc like '%" + txtscr.Text + "%'  or c.SName like '%" + txtscr.Text + "%' or d.BoardingP like '%" + txtscr.Text + "%'   or e.TripName like '%" + txtscr.Text + "%'  ";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            this.Dgv.DefaultCellStyle.Font = new Font("Arial", 10);
            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);
            Dgv.AutoGenerateColumns = false;
            Dgv.DataSource = null;
            Dgv.ColumnCount = 15;

            Dgv.Columns[0].Name = "puid";
            Dgv.Columns[0].HeaderText = "puid";
            Dgv.Columns[0].DataPropertyName = "puid";
            Dgv.Columns[0].Visible = false;

            Dgv.Columns[1].Name = "passno";
            Dgv.Columns[1].HeaderText = "Passno";
            Dgv.Columns[1].DataPropertyName = "passno";
            Dgv.Columns[1].Width = 100;

            Dgv.Columns[2].Name = "Class_Desc";
            Dgv.Columns[2].HeaderText = "Class";
            Dgv.Columns[2].DataPropertyName = "Class_Desc";
            Dgv.Columns[2].Width = 100;

            Dgv.Columns[3].Name = "SName";
            Dgv.Columns[3].HeaderText = "Student";
            Dgv.Columns[3].DataPropertyName = "SName";
            Dgv.Columns[3].Width = 220;

            Dgv.Columns[4].Name = "BoardingP";
            Dgv.Columns[4].HeaderText = "Boarding";
            Dgv.Columns[4].DataPropertyName = "BoardingP";
            Dgv.Columns[4].Width = 150;

            Dgv.Columns[5].Name = "TripName";
            Dgv.Columns[5].HeaderText = "TripName";
            Dgv.Columns[5].DataPropertyName = "TripName";
            Dgv.Columns[5].Width = 150;

            Dgv.Columns[6].Name = "startDate";
            Dgv.Columns[6].HeaderText = "StartDate";
            Dgv.Columns[6].DataPropertyName = "startDate";
            Dgv.Columns[6].Visible = false;

            Dgv.Columns[7].Name = "Enddate";
            Dgv.Columns[7].HeaderText = "Enddate";
            Dgv.Columns[7].DataPropertyName = "Enddate";
            Dgv.Columns[7].Visible = false;

            Dgv.Columns[8].Name = "Status";
            Dgv.Columns[8].HeaderText = "Status";
            Dgv.Columns[8].DataPropertyName = "Status";
            Dgv.Columns[8].Visible = false;

            Dgv.Columns[9].DataPropertyName = "classid";
            Dgv.Columns[9].Visible = false;

            Dgv.Columns[10].DataPropertyName = "boardid";
            Dgv.Columns[10].Visible = false;

            Dgv.Columns[11].DataPropertyName = "studid";
            Dgv.Columns[11].Visible = false;

            Dgv.Columns[12].DataPropertyName = "tripid";
            Dgv.Columns[12].Visible = false;


            Dgv.Columns[13].DataPropertyName = "adnochk";
            Dgv.Columns[13].Visible = false;

            Dgv.Columns[14].DataPropertyName = "way";
            Dgv.Columns[14].Visible = false;
            Dgv.DataSource = tap;
        }

        private void Txtpassno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (txtpassno.Text != string.Empty)
                    {
                        bsParty.Filter = string.Format("passno LIKE '%{0}%' ", txtpassno.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtpassno_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtpassno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpassno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    LoadFeeGrp();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtpassno_MouseClick(object sender, MouseEventArgs e)
        {

            DataTable dt = GetParty();
            bsParty.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtpassno);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "passno Search";
        }
    }
}
