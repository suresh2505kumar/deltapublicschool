﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmContact : Form
    {
        public FrmContact()
        {
            InitializeComponent();
        }
        BindingSource bsClass = new BindingSource();
        BindingSource bsStud = new BindingSource();
        BindingSource bsemp = new BindingSource();
        int SelectId = 0; int Fillid = 0;
        private void FrmContact_Load(object sender, EventArgs e)
        {
            LoadButton(0);
            DataTable dt = GetConact();
            LoadData(dt);
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
        }
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)//Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else if (id == 1) // Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnadd.Visible = false;
                    btnedit.Visible = false;
                    btnexit.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Save";
                }
                else if (id == 2)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnadd.Visible = false;
                    btnedit.Visible = false;
                    btnexit.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DataTable GetConact()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetContact", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadData(DataTable dataTable)
        {
            try
            {
                DataGridContact.DataSource = null;
                DataGridContact.AutoGenerateColumns = false;
                DataGridContact.ColumnCount = 8;
                DataGridContact.Columns[0].Name = "ID";
                DataGridContact.Columns[0].HeaderText = "ID";
                DataGridContact.Columns[0].DataPropertyName = "ID";
                DataGridContact.Columns[0].Visible = false;

                DataGridContact.Columns[1].Name = "Date";
                DataGridContact.Columns[1].HeaderText = "Date";
                DataGridContact.Columns[1].DataPropertyName = "Dte";

                DataGridContact.Columns[2].Name = "Class";
                DataGridContact.Columns[2].HeaderText = "Class";
                DataGridContact.Columns[2].DataPropertyName = "Class_Desc";
                DataGridContact.Columns[2].Width = 80;

                DataGridContact.Columns[3].Name = "Student";
                DataGridContact.Columns[3].HeaderText = "Student";
                DataGridContact.Columns[3].DataPropertyName = "SName";
                DataGridContact.Columns[3].Width = 140;

                DataGridContact.Columns[4].Name = "Narration";
                DataGridContact.Columns[4].HeaderText = "Narration";
                DataGridContact.Columns[4].DataPropertyName = "Description";
                DataGridContact.Columns[4].Width = 210;

                DataGridContact.Columns[5].Name = "ClassId";
                DataGridContact.Columns[5].HeaderText = "ClassId";
                DataGridContact.Columns[5].DataPropertyName = "ClassId";
                DataGridContact.Columns[5].Visible = false;

                DataGridContact.Columns[6].Name = "StudId";
                DataGridContact.Columns[6].HeaderText = "StudId";
                DataGridContact.Columns[6].DataPropertyName = "StudId";
                DataGridContact.Columns[6].Visible = false;

                DataGridContact.Columns[7].Name = "TeacherId";
                DataGridContact.Columns[7].HeaderText = "TeacherId";
                DataGridContact.Columns[7].DataPropertyName = "TeacherId";
                DataGridContact.Columns[7].Visible = false;
                DataGridContact.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ClearControl()
        {
            try
            {
                txtStudent.Text = string.Empty;
                txtClass.Text = string.Empty;
                txtFaculty.Text = string.Empty;
                txtStudent.Tag = string.Empty;
                txtClass.Tag = string.Empty;
                txtFaculty.Tag = string.Empty;
                txtReason.Text = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            txtReason.Tag = "0";
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            DataTable dt = GetConact();
            LoadData(dt);
            LoadButton(0);
        }

        private void dtpDateSearch_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = GetConact();
                LoadData(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtClass_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", conn);
                bsClass.DataSource = dt;
                Fillgrid(1, dt);
                Point point = FindLocation(txtClass);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Fillgrid(int id, DataTable data)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "CId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Class_Desc";
                    DataGridCommon.Columns[1].HeaderText = "Class";
                    DataGridCommon.Columns[1].DataPropertyName = "Class_Desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsClass;
                }
                else if (id == 2)
                {
                    Fillid = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";

                    DataGridCommon.Columns[1].Name = "SName";
                    DataGridCommon.Columns[1].HeaderText = "Student Name";
                    DataGridCommon.Columns[1].DataPropertyName = "SName";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsStud;
                    DataGridCommon.Columns[0].Visible = false;
                }
                else if (id == 3)
                {
                    Fillid = 3;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;

                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "EmpId";
                    DataGridCommon.Columns[0].HeaderText = "EmpId";
                    DataGridCommon.Columns[0].DataPropertyName = "EmpId";

                    DataGridCommon.Columns[1].Name = "empname";
                    DataGridCommon.Columns[1].HeaderText = "Employee";
                    DataGridCommon.Columns[1].DataPropertyName = "empname";
                    DataGridCommon.Columns[1].Width = 200;

                    DataGridCommon.DataSource = bsemp;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtStudent_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClass.Text != string.Empty)
                {
                    SqlParameter[] parameters = { new SqlParameter("@ClassId", txtClass.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStudByClassId", parameters, conn);
                    bsStud.DataSource = dt;
                    Fillgrid(2, dt);
                    Point point = FindLocation(txtStudent);
                    grSearch.Location = new Point(point.X, point.Y + 20);
                    grSearch.Visible = true;
                    grSearch.Text = "Class Search";
                }

                else
                {
                    MessageBox.Show("Select Class", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtFaculty_Click(object sender, EventArgs e)
        {
            try
            {

                Module.Dtype = 1;
                DataTable dt = getParty();
                bsemp.DataSource = dt;
                Fillgrid(3, dt);
                Point loc = FindLocation(txtFaculty);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Name Search";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETEMP", conn);
                bsemp.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtStudent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStudent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtFaculty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFaculty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtStudent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStudent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtFaculty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFaculty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("Class_Desc LIKE '%{0}%' ", txtClass.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtStudent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsStud.Filter = string.Format("SName LIKE '%{0}%' ", txtStudent.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtFaculty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsemp.Filter = string.Format("empname LIKE '%{0}%' ", txtFaculty.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtClass.Text != string.Empty || txtStudent.Text != string.Empty || txtFaculty.Text != string.Empty || txtReason.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@ID",txtReason.Tag),
                        new SqlParameter("@Dte",Convert.ToDateTime(DtpDate.Text)),
                        new SqlParameter("@ClassId",txtClass.Tag),
                        new SqlParameter("@StudId",txtStudent.Tag),
                        new SqlParameter("@TeacherId",txtFaculty.Tag),
                        new SqlParameter("@Narration",txtReason.Text),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Contact", parameters, conn);
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                    DataTable dt = GetConact();
                    LoadData(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridContact.SelectedCells[0].RowIndex;
                txtStudent.Text = DataGridContact.Rows[Index].Cells[3].Value.ToString();
                txtClass.Text = DataGridContact.Rows[Index].Cells[2].Value.ToString();
                txtFaculty.Text = DataGridContact.Rows[Index].Cells[7].Value.ToString();
                txtReason.Text = DataGridContact.Rows[Index].Cells[4].Value.ToString();
                txtStudent.Tag = DataGridContact.Rows[Index].Cells[6].Value.ToString();
                txtClass.Tag = DataGridContact.Rows[Index].Cells[5].Value.ToString();
                txtFaculty.Tag = DataGridContact.Rows[Index].Cells[7].Value.ToString();
                txtReason.Tag = DataGridContact.Rows[Index].Cells[0].Value.ToString();
                DtpDate.Text = Convert.ToDateTime(DataGridContact.Rows[Index].Cells[1].Value.ToString()).ToString();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}