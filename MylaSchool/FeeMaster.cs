﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FeeMaster : Form
    {
        public FeeMaster()
        {
            InitializeComponent();
        }
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            chkRf.Checked = false;
            chkbus.Checked = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void FeeMaster_Load(object sender, EventArgs e)
        {
            Load_grid();
            LoadFeeGrp();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panel1);
            panel1.Visible = true;
        }
        private void LoadFeeGrp()
        {
            con.Close();
            con.Open();
            string qur = "SELECT uid,FGroup FROM Generalm";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cbogrp.DataSource = null;
            cbogrp.DataSource = tap;
            cbogrp.DisplayMember = "FGroup";
            cbogrp.ValueMember = "uid";
            cbogrp.SelectedIndex = -1;
            con.Close();
        }

        private void Load_grid()
        {
            try
            {
                con.Open();
                string qur = "select a.fid,a.Descp,a.Fgpid ,Forder,a.RefBus ,FGroup from FeeMast a inner join Generalm b on a.Fgpid =b.uid ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 6;

                Dgv.Columns[0].Name = "fid";
                Dgv.Columns[0].HeaderText = "fid";
                Dgv.Columns[0].DataPropertyName = "fid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Descp";
                Dgv.Columns[1].HeaderText = "Description";
                Dgv.Columns[1].DataPropertyName = "Descp";
                Dgv.Columns[1].Width = 300;

                Dgv.Columns[2].Name = "Fgpid";
                Dgv.Columns[2].HeaderText = "Fgpid";
                Dgv.Columns[2].DataPropertyName = "Fgpid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "Forder";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "RefBus";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].Name = "FGroup";
                Dgv.Columns[5].HeaderText = "Fee Group";
                Dgv.Columns[5].DataPropertyName = "FGroup";
                Dgv.Columns[5].Width = 130;

                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (txtdesc.Text == "")
            {
                MessageBox.Show("Enter the Description");
                txtdesc.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from FeeMast where  Descp='" + txtdesc.Text + "'  ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into FeeMast values(@Descp,@Fgpid,@Forder,@RefBus,@Active)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@Descp", SqlDbType.NVarChar).Value = txtdesc.Text;
                    cmd.Parameters.AddWithValue("@Fgpid", SqlDbType.NVarChar).Value = cbogrp.SelectedValue;
                    cmd.Parameters.AddWithValue("@Forder", SqlDbType.NVarChar).Value = cboser.Text;

                    if ((chkRf.Checked == true) && (chkbus.Checked == false))
                    {
                        cmd.Parameters.AddWithValue("@RefBus", SqlDbType.NVarChar).Value = "Ref";
                    }
                    else if ((chkbus.Checked == true) && (chkRf.Checked == false))
                    {
                        cmd.Parameters.AddWithValue("@RefBus", SqlDbType.NVarChar).Value = "Bus";
                    }
                    else if ((chkRf.Checked == true) && (chkbus.Checked == true))
                    {
                        cmd.Parameters.AddWithValue("@RefBus", SqlDbType.NVarChar).Value = "Ref/Bus";
                    }
                    else if ((chkRf.Checked == false) && (chkbus.Checked == false))
                    {
                        cmd.Parameters.AddWithValue("@RefBus", SqlDbType.NVarChar).Value = "";
                    }
                    cmd.Parameters.AddWithValue("@Active", SqlDbType.Int).Value = 1;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show(" Saved ");
                    txtdesc.Text = "";
                    cbogrp.SelectedIndex = -1;
                    cboser.SelectedIndex = -1;
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Fee Already Entered");
                    con.Close();
                    cmd1.Dispose();
                    txtdesc.Text = "";
                    txtdesc.Focus();
                }
            }
            else
            {
                con.Close();
                string qur = "select * from FeeMast where Descp='" + txtdesc.Text + "' and fid <> " + txtuid.Text;
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                string re;
                if (tap.Rows.Count == 0)
                {
                    if ((chkRf.Checked == true) && (chkbus.Checked == false))
                    {
                        re = "Ref";
                        string QueryUpdate = "Update FeeMast set Descp='" + txtdesc.Text + "',Fgpid='" + cbogrp.SelectedValue + "',Forder='" + cboser.Text + "',RefBus= '" + re + "' where fid='" + txtuid.Text + "'";
                        cmd = new SqlCommand(QueryUpdate, con);
                    }
                    else if ((chkbus.Checked == true) && (chkRf.Checked == false))
                    {
                        re = "Bus";
                        string QueryUpdate = "Update FeeMast set Descp='" + txtdesc.Text + "',Fgpid='" + cbogrp.SelectedValue + "',Forder='" + cboser.Text + "',RefBus= '" + re + "' where fid='" + txtuid.Text + "'";
                        cmd = new SqlCommand(QueryUpdate, con);
                    }
                    else if ((chkRf.Checked == true) && (chkbus.Checked == true))
                    {
                        re = "Ref/Bus";
                        string QueryUpdate = "Update FeeMast set Descp='" + txtdesc.Text + "',Fgpid='" + cbogrp.SelectedValue + "',Forder='" + cboser.Text + "',RefBus= '" + re + "' where fid='" + txtuid.Text + "'";
                        cmd = new SqlCommand(QueryUpdate, con);
                    }
                    else if ((chkRf.Checked == false) && (chkbus.Checked == false))
                    {
                        re = "";
                        string QueryUpdate = "Update FeeMast set Descp='" + txtdesc.Text + "',Fgpid='" + cbogrp.SelectedValue + "',Forder='" + cboser.Text + "',RefBus= '" + re + "' where fid='" + txtuid.Text + "'";
                        cmd = new SqlCommand(QueryUpdate, con);
                    }
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    txtdesc.Text = "";
                    cbogrp.SelectedIndex = -1;
                    cboser.SelectedIndex = -1;
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }

                btnsave.Text = "Save";

            }
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;

            btnsave.Visible = true;
            btnaddrcan.Visible = true;


            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtdesc.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            cboser.Text = Dgv.Rows[i].Cells[3].Value.ToString();

            if (Dgv.Rows[i].Cells[4].Value.ToString() == "Ref")
            {
                chkRf.Checked = true;
            }
            else if (Dgv.Rows[i].Cells[4].Value.ToString() == "Bus")
            {
                chkbus.Checked = true;
            }

            else if (Dgv.Rows[i].Cells[4].Value.ToString() == "Ref/Bus")
            {
                chkbus.Checked = true;
                chkRf.Checked = true;
            }
            else if (Dgv.Rows[i].Cells[4].Value.ToString() == "")
            {
                chkbus.Checked = false;
                chkRf.Checked = false;
            }

            cbogrp.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            btnsave.Text = "Update";
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select a.fid,a.Descp,a.Fgpid ,Forder,a.RefBus ,FGroup from FeeMast a inner join Generalm b on a.Fgpid =b.uid where Descp like '%" + txtscr.Text + "%' and FGroup like '%" + txtscr1.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 6;

                Dgv.Columns[0].Name = "fid";
                Dgv.Columns[0].HeaderText = "fid";
                Dgv.Columns[0].DataPropertyName = "fid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Descp";
                Dgv.Columns[1].HeaderText = "Description";
                Dgv.Columns[1].DataPropertyName = "Descp";
                Dgv.Columns[1].Width = 300;

                Dgv.Columns[2].Name = "Fgpid";
                Dgv.Columns[2].HeaderText = "Fgpid";
                Dgv.Columns[2].DataPropertyName = "Fgpid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "Forder";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "RefBus";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].Name = "FGroup";
                Dgv.Columns[5].HeaderText = "Fee Group";
                Dgv.Columns[5].DataPropertyName = "FGroup";
                Dgv.Columns[5].Width = 130;

                Dgv.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void Cbogrp_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboser_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
