﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmFaculty : Form
    {
        public FrmFaculty()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsEmp = new BindingSource();
        private void FrmFaculty_Load(object sender, EventArgs e)
        {
            grFront.Visible = true;
            GrBack.Visible = false;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
            btnDelete.Visible = true;
            LoadEmp();
        }


        protected void LoadEmp()
        {
            try
            {
                string Query = "Select EmpId,Empname as FacultyName from Emp_mast Order by EMpId";
                SqlCommand sqlCommand = new SqlCommand(Query, con);
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataGridFaculty.DataSource = null;
                DataGridFaculty.AutoGenerateColumns = false;
                DataGridFaculty.ColumnCount = 2;
                DataGridFaculty.Columns[0].Name = "Empid";
                DataGridFaculty.Columns[0].HeaderText = "Empid";
                DataGridFaculty.Columns[0].DataPropertyName = "Empid";
                DataGridFaculty.Columns[0].Visible = false;

                DataGridFaculty.Columns[1].Name = "Empid";
                DataGridFaculty.Columns[1].HeaderText = "Empid";
                DataGridFaculty.Columns[1].DataPropertyName = "FacultyName";
                DataGridFaculty.Columns[1].Width = 450;
                DataGridFaculty.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                GrBack.Visible = true;
                grFront.Visible = false;
                txtFacultyName.Text = string.Empty;

                btnadd.Visible = false;
                btnedit.Visible = false;
                btnexit.Visible = false;
                btnsave.Visible = true;
                btnaddrcan.Visible = true;
                btnsave.Text = "Save";
                btnDelete.Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridFaculty.SelectedCells[0].RowIndex;
                txtFacultyName.Text = DataGridFaculty.Rows[Index].Cells[1].Value.ToString();
                txtFacultyName.Tag = DataGridFaculty.Rows[Index].Cells[0].Value.ToString();
                GrBack.Visible = true;
                grFront.Visible = false;
                btnadd.Visible = false;
                btnedit.Visible = false;
                btnexit.Visible = false;
                btnsave.Visible = true;
                btnaddrcan.Visible = true;
                btnDelete.Visible = false;
                btnsave.Text = "Update";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridFaculty.SelectedCells[0].RowIndex;
                string Query = "Delete from Emp_mast Where EmpId = " + DataGridFaculty.Rows[Index].Cells[0].Value.ToString() + "";
                SqlCommand sqlCommand = new SqlCommand(Query, con);
                sqlCommand.Connection = con;
                con.Open();
                sqlCommand.ExecuteNonQuery();
                con.Close();
                LoadEmp();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = string.Empty;
                if(btnsave.Text == "Save")
                {
                    Query = "Insert into Emp_mast Values ('" + txtFacultyName.Text + "')";
                }
                else
                {
                    Query = "Update Emp_mast Set  empname='" + txtFacultyName.Text + "'  Where EmpId=" + txtFacultyName.Tag + "";
                }
                SqlCommand sqlCommand = new SqlCommand(Query, con);
                sqlCommand.Connection = con;
                con.Open();
                sqlCommand.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFacultyName.Text = string.Empty;
                LoadEmp();
                btnsave.Text = "Save";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GrBack.Visible = false;
            grFront.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
            btnDelete.Visible = true;
        }
    }
}
