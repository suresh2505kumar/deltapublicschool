﻿namespace MylaSchool
{
    partial class FrmStudrpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStudrpt));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboorder = new System.Windows.Forms.ComboBox();
            this.cmdprt = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GBList.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.Color.White;
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.cboorder);
            this.GBList.Controls.Add(this.cmdprt);
            this.GBList.Controls.Add(this.btnexit);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Location = new System.Drawing.Point(-2, 3);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(377, 195);
            this.GBList.TabIndex = 154;
            this.GBList.TabStop = false;
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(334, 45);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 20);
            this.txtssno.TabIndex = 169;
            this.txtssno.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(87, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 16);
            this.label5.TabIndex = 156;
            this.label5.Text = "Sort";
            // 
            // cboorder
            // 
            this.cboorder.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboorder.FormattingEnabled = true;
            this.cboorder.Items.AddRange(new object[] {
            "Boys",
            "Girls",
            "ALL"});
            this.cboorder.Location = new System.Drawing.Point(128, 85);
            this.cboorder.Name = "cboorder";
            this.cboorder.Size = new System.Drawing.Size(136, 24);
            this.cboorder.TabIndex = 1;
            // 
            // cmdprt
            // 
            this.cmdprt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdprt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt.Image = ((System.Drawing.Image)(resources.GetObject("cmdprt.Image")));
            this.cmdprt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdprt.Location = new System.Drawing.Point(126, 139);
            this.cmdprt.Name = "cmdprt";
            this.cmdprt.Size = new System.Drawing.Size(61, 30);
            this.cmdprt.TabIndex = 161;
            this.cmdprt.Text = "Print";
            this.cmdprt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt.UseVisualStyleBackColor = false;
            this.cmdprt.Click += new System.EventHandler(this.Cmdprt_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(188, 139);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(61, 30);
            this.btnexit.TabIndex = 160;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(334, 19);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 20);
            this.txtcid.TabIndex = 159;
            this.txtcid.Visible = false;
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(125, 37);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(170, 22);
            this.txtclass.TabIndex = 0;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtclass_MouseClick);
            this.txtclass.TextChanged += new System.EventHandler(this.Txtclass_TextChanged);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtclass_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(85, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 16);
            this.label1.TabIndex = 153;
            this.label1.Text = "Class";
            // 
            // FrmStudrpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 197);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStudrpt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.Load += new System.EventHandler(this.FrmStudrpt_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button cmdprt;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.TextBox txtcid;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboorder;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtssno;
    }
}