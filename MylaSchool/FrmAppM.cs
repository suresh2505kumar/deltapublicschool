﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmAppM : Form
    {
        public FrmAppM()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SQLDBHelper db1 = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void Load_grid()
        {
            try
            {
                cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "SP_APPMLOAD",
                    Connection = conn
                };
                cmd.Parameters.AddWithValue("@UID", 0);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 17;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "appdate";
                Dgv.Columns[1].HeaderText = "Date";
                Dgv.Columns[1].DataPropertyName = "appdate";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "appno";
                Dgv.Columns[2].HeaderText = "AppNo";
                Dgv.Columns[2].DataPropertyName = "appno";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "studName";
                Dgv.Columns[3].HeaderText = "Student Name";
                Dgv.Columns[3].DataPropertyName = "studName";
                Dgv.Columns[3].Width = 200;

                Dgv.Columns[4].Name = "FName";
                Dgv.Columns[4].HeaderText = "Father Name";
                Dgv.Columns[4].DataPropertyName = "FName";
                Dgv.Columns[4].Width = 200;

                Dgv.Columns[5].Name = "Gender";
                Dgv.Columns[5].HeaderText = "Gender";
                Dgv.Columns[5].DataPropertyName = "Gender";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].Name = "class";
                Dgv.Columns[6].HeaderText = "class";
                Dgv.Columns[6].DataPropertyName = "class";
                Dgv.Columns[6].Width = 60;

                Dgv.Columns[7].Name = "Dob";
                Dgv.Columns[7].HeaderText = "DoB";
                Dgv.Columns[7].DataPropertyName = "Dob";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "hostel";
                Dgv.Columns[8].HeaderText = "hostel";
                Dgv.Columns[8].DataPropertyName = "hostel";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].Name = "bos";
                Dgv.Columns[9].HeaderText = "bos";
                Dgv.Columns[9].DataPropertyName = "bos";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].Name = "lang";
                Dgv.Columns[10].HeaderText = "lang";
                Dgv.Columns[10].DataPropertyName = "lang";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].Name = "Add1";
                Dgv.Columns[11].HeaderText = "Add1";
                Dgv.Columns[11].DataPropertyName = "Add1";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].Name = "Add2";
                Dgv.Columns[12].HeaderText = "Add2";
                Dgv.Columns[12].DataPropertyName = "Add2";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].Name = "mobno";
                Dgv.Columns[13].HeaderText = "mobno";
                Dgv.Columns[13].DataPropertyName = "mobno";
                Dgv.Columns[13].Width = 60;

                Dgv.Columns[14].Name = "city";
                Dgv.Columns[14].HeaderText = "city";
                Dgv.Columns[14].DataPropertyName = "city";
                Dgv.Columns[14].Visible = false;

                Dgv.Columns[15].Name = "pincode";
                Dgv.Columns[15].HeaderText = "pincode";
                Dgv.Columns[15].DataPropertyName = "pincode";
                Dgv.Columns[15].Visible = false;

                Dgv.Columns[16].DataPropertyName = "Groupa";
                Dgv.Columns[16].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void LoadClass_join()
        {
            conn.Close();
            conn.Open();
            string qur = "SELECT * FROM class_join";
            cmd = new SqlCommand(qur, conn);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboclassJ.DataSource = null;
            cboclassJ.DataSource = tap;
            cboclassJ.DisplayMember = "classJ";
            cboclassJ.ValueMember = "uid";
            cboclassJ.SelectedIndex = -1;
            conn.Close();
        }

        private void FrmAppM_Load(object sender, EventArgs e)
        {
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            btnsave.Text = "Save";
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (txtsname.Text == "")
            {
                MessageBox.Show("Enter the Stuednt name");
                txtsname.Focus();
                return;
            }
            else if (txtadno.Text == "")
            {
                MessageBox.Show("select the APPLICATION No");
                txtadno.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                conn.Close();
                string Query = "Select * from AppM Where appno = '" + txtadno.Text + "'";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dtch = new DataTable();
                da.Fill(dtch);
                if (dtch.Rows.Count == 0)
                {
                    SqlParameter[] para ={
                        new SqlParameter("@appdate", Convert.ToDateTime(doa.Text)),
                        new SqlParameter("@appno", txtadno.Text),
                        new SqlParameter("@studName",txtsname.Text),
                        new SqlParameter("@Gender ", cbosex.Text),
                        new SqlParameter("@class", cboclassJ.Text),
                        new SqlParameter("@Dob",  Convert.ToDateTime(dob.Text)),
                        new SqlParameter("@hostel", cmbNation.Text),
                        new SqlParameter("@bos", cboReligion.Text),
                        new SqlParameter("@lang", cboroll.Text),
                        new SqlParameter("@FName",txtfname.Text),
                        new SqlParameter("@Add1", txtadd.Text),
                        new SqlParameter("@Add2", txtadd1.Text),
                        new SqlParameter("@mobno", txtmob.Text),
                        new SqlParameter("@city", txtcity.Text),
                        new SqlParameter("@pincode",txtpin.Text),
                        new SqlParameter("@Groupa",cboclass.Text),
                    };
                    db1.ExecuteNonQuery(CommandType.StoredProcedure, "SP_APPM", para, conn);
                }
                else
                {
                    MessageBox.Show("Record already exists", "Information", MessageBoxButtons.OK);
                }
            }
            else
            {
                SqlParameter[] para ={
                    new SqlParameter("@appdate", Convert.ToDateTime(doa.Text)),
                    new SqlParameter("@appno", txtadno.Text),
                    new SqlParameter("@studName",txtsname.Text),
                    new SqlParameter("@Gender ", cbosex.Text),
                    new SqlParameter("@class", cboclassJ.Text),
                    new SqlParameter("@Dob",  Convert.ToDateTime(dob.Text)),
                    new SqlParameter("@hostel",  cmbNation.Text),
                    new SqlParameter("@bos", cboReligion.Text),
                    new SqlParameter("@lang", cboroll.Text),
                    new SqlParameter("@FName",txtfname.Text),
                    new SqlParameter("@Add1", txtadd.Text),
                    new SqlParameter("@Add2", txtadd1.Text),
                    new SqlParameter("@mobno", txtmob.Text),
                    new SqlParameter("@city", txtcity.Text),
                    new SqlParameter("@pincode",txtpin.Text),
                    new SqlParameter("@Groupa",cboclass.Text),
                    new SqlParameter("@uid", Module.i),
                };
                db1.ExecuteNonQuery(CommandType.StoredProcedure, "SP_APPMUPDATE", para, conn);
            }
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);
            ClearTxt();
            conn.Close();
            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
        }

        public void ClearTxt()
        {
            txtadno.Text = "";
            txtsname.Text = "";
            txtfname.Text = "";
            txtcity.Text = "";
            txtadd1.Text = "";
            txtmob.Text = "";
            txtpin.Text = "";
            cmbNation.SelectedIndex = -1;
            cbosex.SelectedIndex = -1;
            cboReligion.SelectedIndex = -1;
            cboclassJ.SelectedIndex = -1;
            txtpin.Text = "";
            txtadd.Text = "";
            cboclass.SelectedIndex = -1;
            cboroll.SelectedIndex = -1;
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtadno.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtsname.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtfname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            txtpin.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            dob.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            cbosex.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            txtcity.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            doa.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            cboclassJ.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            txtadd.Text = Dgv.Rows[i].Cells[13].Value.ToString();
            cboroll.Text = Dgv.Rows[i].Cells[15].Value.ToString();
            cboclass.Text = Dgv.Rows[i].Cells[17].Value.ToString();
            txtadd1.Text = Dgv.Rows[i].Cells[18].Value.ToString();
            txtmob.Text = Dgv.Rows[i].Cells[19].Value.ToString();
            cboReligion.Text = Dgv.Rows[i].Cells[21].Value.ToString();
            btnsave.Text = "Update";
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Close();
                conn.Open();
                string qur = "select  uid,appdate,appno,studName,FName,Gender,class,Dob,hostel,bos,lang,Add1,Add2,mobno,city,pincode,Groupa from AppM  where  studName like '%" + txtscr.Text + "%'  or   appdate like '%" + txtscr.Text + "%' or   appno like '%" + txtscr.Text + "%' or   FName like '%" + txtscr.Text + "%'  or   mobno like '%" + txtscr.Text + "%'  ";
                cmd = new SqlCommand(qur, conn);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                if (tap.Rows.Count == 0)
                {
                    MessageBox.Show("No Records Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 17;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "appdate";
                Dgv.Columns[1].HeaderText = "Date";
                Dgv.Columns[1].DataPropertyName = "appdate";
                Dgv.Columns[1].Width = 100;

                Dgv.Columns[2].Name = "appno";
                Dgv.Columns[2].HeaderText = "AppNo";
                Dgv.Columns[2].DataPropertyName = "appno";
                Dgv.Columns[2].Width = 150;

                Dgv.Columns[3].Name = "studName";
                Dgv.Columns[3].HeaderText = "Student Name";
                Dgv.Columns[3].DataPropertyName = "studName";
                Dgv.Columns[3].Width = 200;

                Dgv.Columns[4].Name = "FName";
                Dgv.Columns[4].HeaderText = "Father Name";
                Dgv.Columns[4].DataPropertyName = "FName";
                Dgv.Columns[4].Width = 200;

                Dgv.Columns[5].Name = "Gender";
                Dgv.Columns[5].HeaderText = "Gender";
                Dgv.Columns[5].DataPropertyName = "Gender";
                Dgv.Columns[5].Visible = false;


                Dgv.Columns[6].Name = "class";
                Dgv.Columns[6].HeaderText = "class";
                Dgv.Columns[6].DataPropertyName = "class";
                Dgv.Columns[6].Width = 60;


                Dgv.Columns[7].Name = "Dob";
                Dgv.Columns[7].HeaderText = "DoB";
                Dgv.Columns[7].DataPropertyName = "Dob";
                Dgv.Columns[7].Visible = false;

                Dgv.Columns[8].Name = "hostel";
                Dgv.Columns[8].HeaderText = "hostel";
                Dgv.Columns[8].DataPropertyName = "hostel";
                Dgv.Columns[8].Visible = false;

                Dgv.Columns[9].Name = "bos";
                Dgv.Columns[9].HeaderText = "bos";
                Dgv.Columns[9].DataPropertyName = "bos";
                Dgv.Columns[9].Visible = false;

                Dgv.Columns[10].Name = "lang";
                Dgv.Columns[10].HeaderText = "lang";
                Dgv.Columns[10].DataPropertyName = "lang";
                Dgv.Columns[10].Visible = false;

                Dgv.Columns[11].Name = "Add1";
                Dgv.Columns[11].HeaderText = "Add1";
                Dgv.Columns[11].DataPropertyName = "Add1";
                Dgv.Columns[11].Visible = false;

                Dgv.Columns[12].Name = "Add2";
                Dgv.Columns[12].HeaderText = "Add2";
                Dgv.Columns[12].DataPropertyName = "Add2";
                Dgv.Columns[12].Visible = false;

                Dgv.Columns[13].Name = "mobno";
                Dgv.Columns[13].HeaderText = "mobno";
                Dgv.Columns[13].DataPropertyName = "mobno";
                Dgv.Columns[13].Width = 60;

                Dgv.Columns[14].Name = "city";
                Dgv.Columns[14].HeaderText = "city";
                Dgv.Columns[14].DataPropertyName = "city";
                Dgv.Columns[14].Visible = false;

                Dgv.Columns[15].Name = "pincode";
                Dgv.Columns[15].HeaderText = "pincode";
                Dgv.Columns[15].DataPropertyName = "pincode";
                Dgv.Columns[15].Visible = false;

                Dgv.Columns[16].DataPropertyName = "Groupa";
                Dgv.Columns[16].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CboclassJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboclass_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbosex_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbocom_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboroll_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Btnadd_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
        }

        private void Btnexit_Click_1(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Btnedit_Click_1(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            ClearTxt();
            int i = Dgv.SelectedCells[0].RowIndex;
            Module.i = Convert.ToInt32(Dgv.Rows[i].Cells[0].Value.ToString());
            doa.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            txtadno.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            txtsname.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            txtfname.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            cbosex.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            cboclassJ.Text = Dgv.Rows[i].Cells[6].Value.ToString();
            dob.Text = Dgv.Rows[i].Cells[7].Value.ToString();
            cmbNation.Text = Dgv.Rows[i].Cells[8].Value.ToString();
            cboReligion.Text = Dgv.Rows[i].Cells[9].Value.ToString();
            cboroll.Text = Dgv.Rows[i].Cells[10].Value.ToString();
            txtadd.Text = Dgv.Rows[i].Cells[11].Value.ToString();
            txtadd1.Text = Dgv.Rows[i].Cells[12].Value.ToString();
            txtmob.Text = Dgv.Rows[i].Cells[13].Value.ToString();
            txtcity.Text = Dgv.Rows[i].Cells[14].Value.ToString();
            txtpin.Text = Dgv.Rows[i].Cells[15].Value.ToString();
            cboclass.Text = Dgv.Rows[i].Cells[16].Value.ToString();
            btnsave.Text = "Update";
        }
    }
}
