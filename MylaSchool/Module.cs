﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.NetworkInformation;

namespace MylaSchool
{
    class Module
    {
        public static object fieldone;
        public static object fieldtwo;
        public static object fieldthree;
        public static object fieldFour;
        public static object fieldFive = null;
        public static string frmwhat = "";
        public static string frmname = "";
        public static string sql = "";
        public static string data6 = "";
        public static string strsql = "";
        public static string strsql1 = "";
        public static string strsqltmp = "";
        public static string Genlbl = "";
        public static string ServerName = string.Empty;
        public static string UserName = string.Empty;
        public static string Password = string.Empty;
        public static string DbName = string.Empty;
        public static bool canclclik = false;
        public static SqlCommand cmd = new SqlCommand();
        public static int i = 0;
        public static int Dtype = 0;
        public static int type = 0;

        public static string StrSrch = "";
        public static string FSSQLSortStr = "";
        public static string FSSQLSortStr1 = "";
        public static string FSSQLSortStr2 = "";
        public static string FSSQLSortStr3 = "";
        public static string FSSQLSortStr4 = "";
        public static string FSSQLSortStr5 = "";
        public static string FSSQLSortStrtmp = "";
        public static string FSSQLSortStrA = "";
        public static int data1 = 0;
        public static string data2 = "";
        public static string data3 = "";
        public static string data4 = "";
        public static string data5 = "";
        public static int Gbtxtid = 0;
        public static Control Parent;
        public static Boolean okclick = false;
        public static string dtcry = "";
        public SqlCommand Sqlcmd = new SqlCommand();
        public static int ColluidPT = 0;
        public static string theDate = "";
        public static string toDate = "";
        public static int termuid = 0;
        public static int cid = 0;
        public static string Intchk = "";
    }

    public static class FunC
    {
        public static void Classmaster(string name, string uid, string str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, GroupBox whatfld)
        {
            Module.fieldone = whatfldone.Name;
            Module.fieldtwo = whatfldtwo.Name;
            Module.Parent = whatfld;
            Module.frmwhat = whatform.Name;
        }
        public static void Classmaster1(string name, string uid, string str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, GroupBox whatfld)
        {
            Module.fieldone = whatfldone.Name;
            Module.fieldtwo = whatfldtwo.Name;
            Module.Parent = whatfld;
            Module.frmwhat = whatform.Name;
        }
        public static void Partylistviewcont2(string name, string uid, string uid1, string str2, string str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, TextBox whatfldFour, GroupBox whatfldFive)
        {
            Module.fieldone = whatfldone.Name;
            Module.fieldtwo = whatfldtwo.Name;
            Module.fieldthree = whatfldthree.Name;
            Module.fieldFour = whatfldFour.Name;
            Module.Parent = whatfldFive;
            Module.frmwhat = whatform.Name;
        }
        public static void Partylistviewcont3(string name, string uid, string str1, string str, Form whatform, TextBox whatfldone, TextBox whatfldtwo, TextBox whatfldthree, GroupBox whatfldFive)
        {
            Module.fieldone = whatfldone.Name;
            Module.fieldtwo = whatfldtwo.Name;
            Module.fieldthree = whatfldthree.Name;
            Module.Parent = whatfldFive;
            Module.frmwhat = whatform.Name;
        }
        public static void CallIntChk()
        {
            bool connI = NetworkInterface.GetIsNetworkAvailable();
            if (connI == true)
            {
                Module.Intchk = "Internet is Connected";
                return;
            }
            else
            {
                Module.Intchk = "Internet is DisConnected";
                return;
            }
        }

        public static void Buttonstyleform(Form Frmname)
        {
            foreach (Control ct in Frmname.Controls)
            {
                if (ct is Button)
                {
                    ct.TabStop = false;
                    (ct as Button).FlatStyle = FlatStyle.Flat;
                    (ct as Button).FlatAppearance.BorderSize = 0;
                    (ct as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }
        }

        public static void Buttonstylepanel(Panel whatfldone)
        {
            Module.Parent = whatfldone;
            foreach (Control bt in Module.Parent.Controls)
            {
                if (bt is Button)
                {

                    bt.TabStop = false;
                    (bt as Button).FlatStyle = FlatStyle.Flat;
                    (bt as Button).FlatAppearance.BorderSize = 0;
                    (bt as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }

        }
    }
}
