﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmReportviewver : Form
    {
        ReportDocument doc = new ReportDocument();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        public FrmReportviewver()
        {
            InitializeComponent();
        }
        private void FrmReportviewver_Load(object sender, EventArgs e)
        {
            LodCombo();
            LodComboTerm();
            cmbClass.SelectedIndex = -1;
            cmbTerm.SelectedIndex = -1;
        }

        protected void LodCombo()
        {
            try
            {
                string Query = "Select Class_Desc,Cid from Class_Mast order by Class_desc";
                SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataRow dr = dt.NewRow();
                dr["Class_Desc"] = "All";
                dr["Cid"] = 0;
                dt.Rows.InsertAt(dr, 0);
                cmbClass.DataSource = null;               
                cmbClass.DisplayMember = "Class_Desc";
                cmbClass.ValueMember = "Cid";
                cmbClass.DataSource = dt;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
        protected void LodComboTerm()
        {
            try
            {
                string Query = "Select Ruid,Refname from FeeRef";
                SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cmbTerm.DataSource = null;
                cmbTerm.DisplayMember = "Refname";
                cmbTerm.ValueMember = "Ruid";
                cmbTerm.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            ReportDocument doc = new ReportDocument();
            SqlDataAdapter da = new SqlDataAdapter("SPFeeTempRecipt", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;            
            da.SelectCommand.Parameters.Add("@ClassId", SqlDbType.Int).Value = cmbClass.SelectedValue;
            da.SelectCommand.Parameters.Add("@TermID", SqlDbType.Int).Value = cmbTerm.SelectedValue;
            da.SelectCommand.Parameters.Add("@SelectDate", SqlDbType.DateTime).Value =Convert.ToDateTime(dtpDate.Text);
            DataSet ds = new DataSet();
            da.Fill(ds, "FeeReceipt");
            var id = string.Empty;
            if (ds.Tables["FeeReceipt"].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables["FeeReceipt"].Rows.Count; i++)
                {
                    if (id == string.Empty)
                    {
                        id += ds.Tables["FeeReceipt"].Rows[i]["Colluid"].ToString();
                    }
                    else
                    {
                        id += ',' + ds.Tables["FeeReceipt"].Rows[i]["Colluid"].ToString();
                    }
                }
                string Query = "Select Coll_det.Colluid,FeeMast.Descp,Coll_det.paidamt from Coll_det inner join FeeMast on Coll_det.feeid = FeeMast.Fid Where Coll_det.paidamt <> '0.00' and Coll_det.Colluid  in(" + id + ")order by Coll_det.Colluid";
                SqlCommand cmd = new SqlCommand(Query, conn);
                SqlDataAdapter dadet = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dadet.Fill(dt);
                doc.Load(Application.StartupPath + "\\Reports\\FreeClasswiseRecipt.rpt");
                doc.SetDataSource(ds);
                doc.Subreports["FeeReciptDetails.rpt"].SetDataSource(dt);
                crystalReportViewer1.RefreshReport();
                crystalReportViewer1.ReportSource = doc;
            }
            else
            {
                MessageBox.Show("No Records Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
