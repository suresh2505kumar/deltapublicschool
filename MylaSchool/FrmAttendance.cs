﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmAttendance : Form
    {
        public FrmAttendance()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsClass = new BindingSource();
        BindingSource bsStud = new BindingSource();
        int SelectId = 0; int Fillid = 0;
        private void txtClass_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", con);
                bsClass.DataSource = dt;
                Fillgrid(1, dt);
                Point point = FindLocation(txtClass);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void Clearcontrol()
        {
            txtClass.Text = string.Empty;
            txtClass.Tag = string.Empty;
            txtStudent.Text = string.Empty;
            txtClass.Tag = string.Empty;
            cmbStatus.SelectedIndex = -1;
        }

        private void Fillgrid(int id, DataTable data)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "CId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Class_Desc";
                    DataGridCommon.Columns[1].HeaderText = "Class";
                    DataGridCommon.Columns[1].DataPropertyName = "Class_Desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsClass;
                }
                else
                {
                    Fillid = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";

                    DataGridCommon.Columns[1].Name = "SName";
                    DataGridCommon.Columns[1].HeaderText = "Student Name";
                    DataGridCommon.Columns[1].DataPropertyName = "SName";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsStud;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtStudent_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClass.Text != string.Empty)
                {
                    SqlParameter[] parameters = { new SqlParameter("@ClassId", txtClass.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStudByClassId", parameters, con);
                    bsStud.DataSource = dt;
                    Fillgrid(2, dt);
                    Point point = FindLocation(txtStudent);
                    grSearch.Location = new Point(point.X, point.Y + 20);
                    grSearch.Visible = true;
                    grSearch.Text = "Class Search";
                }

                else
                {
                    MessageBox.Show("Select Class", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtStudent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStudent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtStudent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStudent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStudent.Text != string.Empty || txtClass.Text != string.Empty || cmbStatus.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@ID","0"),
                        new SqlParameter("@AttDate",dtpDate.Text),
                        new SqlParameter("@ClassId",txtClass.Tag),
                        new SqlParameter("@StudId",txtStudent.Tag),
                        new SqlParameter("@Sts",cmbStatus.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_StudAttendance", parameters, con);
                    MessageBox.Show("Attendance posted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clearcontrol();
                    GetAttendance(Convert.ToDateTime(dtpDate.Text));
                }
                else
                {
                    MessageBox.Show("Class,Student and Status can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadGrid()
        {
            try
            {
                DataGridAtt.DataSource = null;
                DataGridAtt.AutoGenerateColumns = false;
                DataGridAtt.ColumnCount = 7;
                DataGridAtt.Columns[0].Name = "Date";
                DataGridAtt.Columns[0].HeaderText = "Date";
                DataGridAtt.Columns[0].Width = 150;
                DataGridAtt.Columns[0].DefaultCellStyle.Format = "dd-MMM-yyyy";

                DataGridAtt.Columns[1].Name = "Class";
                DataGridAtt.Columns[1].HeaderText = "Class";
                DataGridAtt.Columns[1].Width = 150;

                DataGridAtt.Columns[2].Name = "Student";
                DataGridAtt.Columns[2].HeaderText = "Student";
                DataGridAtt.Columns[2].Width = 260;

                DataGridAtt.Columns[3].Name = "Status";
                DataGridAtt.Columns[3].HeaderText = "Status";
                DataGridAtt.Columns[3].Width = 150;

                DataGridAtt.Columns[4].Name = "ClassId";
                DataGridAtt.Columns[4].HeaderText = "ClassId";
                DataGridAtt.Columns[4].Visible = false;

                DataGridAtt.Columns[5].Name = "StudId";
                DataGridAtt.Columns[5].HeaderText = "StudId";
                DataGridAtt.Columns[5].Visible = false;

                DataGridAtt.Columns[6].Name = "ID";
                DataGridAtt.Columns[6].HeaderText = "ID";
                DataGridAtt.Columns[6].Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FrmAttendance_Load(object sender, EventArgs e)
        {
            LoadGrid();
            GetAttendance(Convert.ToDateTime(dtpDate.Text));
        }

        private void GetAttendance(DateTime dateTime)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@AttDate", dateTime.ToString("yyyy-MM-dd")) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStudAttendance", parameters, con);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int index = DataGridAtt.Rows.Add();
                        DataGridViewRow row = DataGridAtt.Rows[index];
                        row.Cells[0].Value = Convert.ToDateTime(dt.Rows[i]["AttDate"].ToString()).ToString("dd-MMM-yyyy");
                        row.Cells[1].Value = dt.Rows[i]["Class_desc"].ToString();
                        row.Cells[2].Value = dt.Rows[i]["SName"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["Sts"].ToString();
                        row.Cells[4].Value = dt.Rows[i]["ClassId"].ToString();
                        row.Cells[5].Value = dt.Rows[i]["Studid"].ToString();
                        row.Cells[6].Value = dt.Rows[i]["ID"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSMS_Click(object sender, EventArgs e)
        {
            try
            {
                //string Url = "http://api.lionsms.com/api/?username=tlcfpl&password=Newlife@5&cmd=sendSMS&to=9843050500&sender=TLCFPL&message=Test";
                //var client = new WebClient();
                //var content = client.DownloadString(Url);
                //MessageBox.Show("Send");
                //return;
                string id = string.Empty;
                if (DataGridAtt.Rows.Count > 0)
                {
                    for (int i = 0; i < DataGridAtt.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            id = DataGridAtt.Rows[i].Cells[5].Value.ToString();
                        }
                        else
                        {
                            id += ',' + DataGridAtt.Rows[i].Cells[5].Value.ToString();
                        }
                    }
                    string Query = "Select SMS,SName,b.Class_desc from StudentM a inner join Class_mast b on a.Classid = b.Cid Where a.Uid in (" + id + ")";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        DataGridSMS.DataSource = null;
                        DataGridSMS.AutoGenerateColumns = false;
                        DataGridSMS.ColumnCount = 4;
                        DataGridSMS.Columns[0].Name = "Mobile No";
                        DataGridSMS.Columns[0].HeaderText = "Mobile No";
                        DataGridSMS.Columns[0].DataPropertyName = "SMS";

                        DataGridSMS.Columns[1].Name = "Student Name";
                        DataGridSMS.Columns[1].HeaderText = "Student Name";
                        DataGridSMS.Columns[1].DataPropertyName = "SName";
                        DataGridSMS.Columns[1].Width = 100;

                        DataGridSMS.Columns[2].Name = "ClassName";
                        DataGridSMS.Columns[2].HeaderText = "Class";
                        DataGridSMS.Columns[2].DataPropertyName = "Class_desc";
                        DataGridSMS.Columns[2].Width = 100;

                        DataGridSMS.Columns[3].Name = "Message";
                        DataGridSMS.Columns[3].HeaderText = "Message";
                        DataGridSMS.Columns[3].Width = 425;
                        DataGridSMS.DataSource = dt;
                        grAtt.Visible = false;
                        grSearch.Visible = true;
                        grSMS.Visible = true;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridSMS.Rows.Count; i++)
                {
                    string Message = txtSMS.Text;
                    Message = Message.Replace("+ Student Name +", DataGridSMS.Rows[i].Cells[1].Value.ToString() + "/" + DataGridSMS.Rows[i].Cells[2].Value.ToString());
                    Message = Message.Replace("+ Date +", Convert.ToDateTime(dtpDate.Text).ToString("dd.MM.yyyy"));
                    DataGridSMS.Rows[i].Cells[3].Value = Message;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                GetAttendance(Convert.ToDateTime(dtpDate.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSendSMS_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridSMS.Rows.Count; i++)
                {
                    //string Url = "http://sms.mylaeasybiz.net/sendsms.jsp?user=deltas&password=a97274ed39XX&mobiles=" + DataGridSMS.Rows[i].Cells[0].Value.ToString() + "&sms=" + DataGridSMS.Rows[i].Cells[3].Value.ToString() + "&senderid=DELTAS";
                    string Url = "http://api.lionsms.com/api/?username=tlcfpl&password=Newlife@5&cmd=sendSMS&to=9843050500&sender=TLCFPL&message=" + DataGridSMS.Rows[i].Cells[0].Value.ToString() + "&sms=" + DataGridSMS.Rows[i].Cells[3].Value.ToString() + "&senderid=DELTAS";
                    var client = new WebClient();
                    var content = client.DownloadString(Url);
                }
                MessageBox.Show("Message Send Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("Class_Desc LIKE '%{0}%' ", txtClass.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtStudent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsStud.Filter = string.Format("SName LIKE '%{0}%' ", txtStudent.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

    }
}
