﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FeeReports : Form
    {
        public FeeReports()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlCommand qur1 = new SqlCommand();
        private void FeeReports_Load(object sender, EventArgs e)
        {
            qur1.Connection = con;
        }
        private void Cmdprt_Click(object sender, EventArgs e)
        {
            Module.Dtype = 30;
            con.Close();
            con.Open();
            if (cboTerm.Text == "Daily Fee Collection Report")
            {
                Module.theDate = dtp.Value.ToString("dd-MMM-yyyy");
                Module.toDate = dtp1.Value.ToString("dd-MMM-yyyy");
                CRViewer crv = new CRViewer
                {
                    MdiParent = this.MdiParent
                };
                crv.Show();
                con.Close();
            }
            else
            {
                Module.Dtype = 31;
                Module.theDate = dtp.Value.ToString("dd-MMM-yyyy");
                Module.toDate = dtp1.Value.ToString("dd-MMM-yyyy");
                CRViewer crv = new CRViewer
                {
                    MdiParent = this.MdiParent
                };
                crv.Show();
                con.Close();
            }
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
