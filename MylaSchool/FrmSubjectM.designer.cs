﻿namespace MylaSchool
{
    partial class FrmSubjectM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSubjectM));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.GBList = new System.Windows.Forms.GroupBox();
            this.Dgvlist = new System.Windows.Forms.DataGridView();
            this.cbocls = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtImax = new System.Windows.Forms.TextBox();
            this.txtIPass = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtEmax = new System.Windows.Forms.TextBox();
            this.txtEPass = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.txtfuid = new System.Windows.Forms.TextBox();
            this.txtstart = new System.Windows.Forms.TextBox();
            this.txtstrength = new System.Windows.Forms.TextBox();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.TxtfeeM = new System.Windows.Forms.TextBox();
            this.btnok = new System.Windows.Forms.Button();
            this.txtend = new System.Windows.Forms.TextBox();
            this.chk = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtshortname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtroute = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txttime1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.GBList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.txtscr);
            this.GBMain.Controls.Add(this.Dgv);
            this.GBMain.Controls.Add(this.txtscr1);
            this.GBMain.Location = new System.Drawing.Point(8, -1);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(610, 461);
            this.GBMain.TabIndex = 2;
            this.GBMain.TabStop = false;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(517, 10);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(85, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.btnser_Click);
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(5, 12);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(514, 26);
            this.txtscr.TabIndex = 87;
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(5, 41);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(597, 413);
            this.Dgv.TabIndex = 0;
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(331, 136);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(237, 26);
            this.txtscr1.TabIndex = 90;
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(548, 2);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(60, 30);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Visible = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(415, 2);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(60, 30);
            this.btnedit.TabIndex = 85;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Visible = false;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(356, 2);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(60, 30);
            this.btnadd.TabIndex = 84;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GBList.Controls.Add(this.Dgvlist);
            this.GBList.Controls.Add(this.cbocls);
            this.GBList.Controls.Add(this.groupBox2);
            this.GBList.Controls.Add(this.groupBox1);
            this.GBList.Controls.Add(this.checkBox1);
            this.GBList.Controls.Add(this.grSearch);
            this.GBList.Controls.Add(this.chk);
            this.GBList.Controls.Add(this.label7);
            this.GBList.Controls.Add(this.txtshortname);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.txtroute);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.txttime1);
            this.GBList.Controls.Add(this.label2);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Location = new System.Drawing.Point(9, -1);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(608, 460);
            this.GBList.TabIndex = 3;
            this.GBList.TabStop = false;
            // 
            // Dgvlist
            // 
            this.Dgvlist.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dgvlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Dgvlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgvlist.EnableHeadersVisualStyles = false;
            this.Dgvlist.Location = new System.Drawing.Point(40, 220);
            this.Dgvlist.Name = "Dgvlist";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Dgvlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.Dgvlist.Size = new System.Drawing.Size(487, 233);
            this.Dgvlist.TabIndex = 405;
            // 
            // cbocls
            // 
            this.cbocls.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocls.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocls.FormattingEnabled = true;
            this.cbocls.Items.AddRange(new object[] {
            "Nursary",
            "Primary",
            "Secondry",
            "Higher"});
            this.cbocls.Location = new System.Drawing.Point(445, 125);
            this.cbocls.Name = "cbocls";
            this.cbocls.Size = new System.Drawing.Size(138, 27);
            this.cbocls.TabIndex = 6;
            this.cbocls.SelectedIndexChanged += new System.EventHandler(this.cbocls_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtImax);
            this.groupBox2.Controls.Add(this.txtIPass);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(224, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(173, 84);
            this.groupBox2.TabIndex = 403;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Inernal Marks";
            // 
            // txtImax
            // 
            this.txtImax.BackColor = System.Drawing.SystemColors.Window;
            this.txtImax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImax.Location = new System.Drawing.Point(82, 52);
            this.txtImax.MaxLength = 250;
            this.txtImax.Name = "txtImax";
            this.txtImax.Size = new System.Drawing.Size(76, 26);
            this.txtImax.TabIndex = 5;
            // 
            // txtIPass
            // 
            this.txtIPass.BackColor = System.Drawing.SystemColors.Window;
            this.txtIPass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIPass.Location = new System.Drawing.Point(82, 19);
            this.txtIPass.MaxLength = 250;
            this.txtIPass.Name = "txtIPass";
            this.txtIPass.Size = new System.Drawing.Size(76, 26);
            this.txtIPass.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 18);
            this.label9.TabIndex = 153;
            this.label9.Text = "Max. Mark";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 18);
            this.label10.TabIndex = 152;
            this.label10.Text = "Pass Mark";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtEmax);
            this.groupBox1.Controls.Add(this.txtEPass);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(40, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 84);
            this.groupBox1.TabIndex = 402;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "External Marks";
            // 
            // txtEmax
            // 
            this.txtEmax.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmax.Location = new System.Drawing.Point(84, 52);
            this.txtEmax.MaxLength = 250;
            this.txtEmax.Name = "txtEmax";
            this.txtEmax.Size = new System.Drawing.Size(76, 26);
            this.txtEmax.TabIndex = 3;
            // 
            // txtEPass
            // 
            this.txtEPass.BackColor = System.Drawing.SystemColors.Window;
            this.txtEPass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEPass.Location = new System.Drawing.Point(84, 19);
            this.txtEPass.MaxLength = 250;
            this.txtEPass.Name = "txtEPass";
            this.txtEPass.Size = new System.Drawing.Size(76, 26);
            this.txtEPass.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 18);
            this.label8.TabIndex = 151;
            this.label8.Text = "Max. Mark";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 18);
            this.label4.TabIndex = 150;
            this.label4.Text = "Pass Mark";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(365, 53);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(121, 22);
            this.checkBox1.TabIndex = 401;
            this.checkBox1.Text = "Extra Curricular";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.txtssno);
            this.grSearch.Controls.Add(this.txtcid);
            this.grSearch.Controls.Add(this.txtfuid);
            this.grSearch.Controls.Add(this.txtstart);
            this.grSearch.Controls.Add(this.txtstrength);
            this.grSearch.Controls.Add(this.txtuid);
            this.grSearch.Controls.Add(this.TxtfeeM);
            this.grSearch.Controls.Add(this.btnok);
            this.grSearch.Controls.Add(this.txtend);
            this.grSearch.Location = new System.Drawing.Point(54, 311);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(70, 33);
            this.grSearch.TabIndex = 400;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(227, 183);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select (F2)";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(337, 184);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(7, 10);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(430, 168);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // txtssno
            // 
            this.txtssno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtssno.Location = new System.Drawing.Point(337, 43);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 26);
            this.txtssno.TabIndex = 141;
            this.txtssno.Visible = false;
            // 
            // txtcid
            // 
            this.txtcid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcid.Location = new System.Drawing.Point(210, 43);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 26);
            this.txtcid.TabIndex = 131;
            this.txtcid.Visible = false;
            // 
            // txtfuid
            // 
            this.txtfuid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfuid.Location = new System.Drawing.Point(169, 43);
            this.txtfuid.Name = "txtfuid";
            this.txtfuid.Size = new System.Drawing.Size(35, 26);
            this.txtfuid.TabIndex = 132;
            this.txtfuid.Visible = false;
            // 
            // txtstart
            // 
            this.txtstart.BackColor = System.Drawing.SystemColors.Window;
            this.txtstart.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstart.Location = new System.Drawing.Point(261, 28);
            this.txtstart.MaxLength = 250;
            this.txtstart.Name = "txtstart";
            this.txtstart.Size = new System.Drawing.Size(76, 26);
            this.txtstart.TabIndex = 149;
            // 
            // txtstrength
            // 
            this.txtstrength.BackColor = System.Drawing.SystemColors.Window;
            this.txtstrength.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstrength.Location = new System.Drawing.Point(289, 26);
            this.txtstrength.MaxLength = 250;
            this.txtstrength.Name = "txtstrength";
            this.txtstrength.Size = new System.Drawing.Size(121, 26);
            this.txtstrength.TabIndex = 144;
            this.txtstrength.TextChanged += new System.EventHandler(this.txtstrength_TextChanged);
            // 
            // txtuid
            // 
            this.txtuid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuid.Location = new System.Drawing.Point(189, 26);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(35, 26);
            this.txtuid.TabIndex = 109;
            this.txtuid.Visible = false;
            // 
            // TxtfeeM
            // 
            this.TxtfeeM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TxtfeeM.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtfeeM.Location = new System.Drawing.Point(127, 43);
            this.TxtfeeM.MaxLength = 250;
            this.TxtfeeM.Name = "TxtfeeM";
            this.TxtfeeM.Size = new System.Drawing.Size(20, 26);
            this.TxtfeeM.TabIndex = 2;
            this.TxtfeeM.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtfeeM_MouseClick);
            this.TxtfeeM.TextChanged += new System.EventHandler(this.TxtfeeM_TextChanged);
            this.TxtfeeM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtfeeM_KeyDown);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.White;
            this.btnok.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Image = ((System.Drawing.Image)(resources.GetObject("btnok.Image")));
            this.btnok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnok.Location = new System.Drawing.Point(319, 26);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(49, 28);
            this.btnok.TabIndex = 126;
            this.btnok.Text = "ok";
            this.btnok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // txtend
            // 
            this.txtend.BackColor = System.Drawing.SystemColors.Window;
            this.txtend.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtend.Location = new System.Drawing.Point(289, 60);
            this.txtend.MaxLength = 250;
            this.txtend.Name = "txtend";
            this.txtend.Size = new System.Drawing.Size(81, 26);
            this.txtend.TabIndex = 4;
            // 
            // chk
            // 
            this.chk.AutoSize = true;
            this.chk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk.Location = new System.Drawing.Point(40, 192);
            this.chk.Name = "chk";
            this.chk.Size = new System.Drawing.Size(84, 22);
            this.chk.TabIndex = 151;
            this.chk.Text = "Check All";
            this.chk.UseVisualStyleBackColor = true;
            this.chk.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 18);
            this.label7.TabIndex = 145;
            this.label7.Text = "Short Name";
            // 
            // txtshortname
            // 
            this.txtshortname.BackColor = System.Drawing.SystemColors.Window;
            this.txtshortname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtshortname.Location = new System.Drawing.Point(117, 50);
            this.txtshortname.MaxLength = 250;
            this.txtshortname.Name = "txtshortname";
            this.txtshortname.Size = new System.Drawing.Size(214, 26);
            this.txtshortname.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(61, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 145;
            this.label1.Text = "Subject";
            // 
            // txtroute
            // 
            this.txtroute.BackColor = System.Drawing.SystemColors.Window;
            this.txtroute.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtroute.Location = new System.Drawing.Point(117, 23);
            this.txtroute.MaxLength = 250;
            this.txtroute.Name = "txtroute";
            this.txtroute.Size = new System.Drawing.Size(214, 26);
            this.txtroute.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(403, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 113;
            this.label3.Text = "Class";
            // 
            // txttime1
            // 
            this.txttime1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txttime1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttime1.Location = new System.Drawing.Point(399, 311);
            this.txttime1.MaxLength = 250;
            this.txttime1.Name = "txttime1";
            this.txttime1.Size = new System.Drawing.Size(82, 26);
            this.txttime1.TabIndex = 142;
            this.txttime1.Leave += new System.EventHandler(this.txttime1_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(292, 430);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 18);
            this.label2.TabIndex = 146;
            this.label2.Text = "Bus Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(476, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 18);
            this.label5.TabIndex = 147;
            this.label5.Text = "End";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(475, 3);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 30);
            this.btnsave.TabIndex = 123;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(543, 2);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(65, 30);
            this.btnaddrcan.TabIndex = 122;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnDelete);
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Location = new System.Drawing.Point(8, 461);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(610, 34);
            this.panadd.TabIndex = 115;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnDelete.Location = new System.Drawing.Point(475, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(74, 30);
            this.btnDelete.TabIndex = 126;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // FrmSubjectM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(625, 500);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GBMain);
            this.Controls.Add(this.GBList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSubjectM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Subject";
            this.Load += new System.EventHandler(this.FrmSubjectM_Load);
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grSearch.ResumeLayout(false);
            this.grSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBMain;
        internal System.Windows.Forms.TextBox txtscr1;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox TxtfeeM;
        internal System.Windows.Forms.TextBox txtend;
        internal System.Windows.Forms.Button btnok;
        private System.Windows.Forms.TextBox txtcid;
        private System.Windows.Forms.TextBox txtfuid;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Panel panadd;
        internal System.Windows.Forms.TextBox txtstrength;
        internal System.Windows.Forms.TextBox txtroute;
        internal System.Windows.Forms.TextBox txttime1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtstart;
        private System.Windows.Forms.CheckBox chk;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.ComboBox cbocls;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.TextBox txtImax;
        internal System.Windows.Forms.TextBox txtIPass;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.TextBox txtEmax;
        internal System.Windows.Forms.TextBox txtEPass;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txtshortname;
        private System.Windows.Forms.DataGridView Dgvlist;
    }
}