﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmLeaveLetter : Form
    {
        public FrmLeaveLetter()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsClass = new BindingSource();
        BindingSource bsStud = new BindingSource();
        int SelectId = 0; int Fillid = 0;
        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)//Load
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else if (id == 1) // Add
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnadd.Visible = false;
                    btnedit.Visible = false;
                    btnexit.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Save";
                }
                else if (id == 2)//Edit
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnadd.Visible = false;
                    btnedit.Visible = false;
                    btnexit.Visible = false;
                    btnsave.Visible = true;
                    btnaddrcan.Visible = true;
                    btnsave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            txtReason.Tag = "0";
            LoadButton(1);
        }

        private void FrmLeaveLetter_Load(object sender, EventArgs e)
        {
            LoadButton(0);
            DataTable dt = GetConact();
            LoadData(dt);
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
        }

        protected DataTable GetConact()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetLeaveLetter", conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadData(DataTable dataTable)
        {
            try
            {
                DataGridLeaveLetter.DataSource = null;
                DataGridLeaveLetter.AutoGenerateColumns = false;
                DataGridLeaveLetter.ColumnCount = 7;
                DataGridLeaveLetter.Columns[0].Name = "ID";
                DataGridLeaveLetter.Columns[0].HeaderText = "ID";
                DataGridLeaveLetter.Columns[0].DataPropertyName = "ID";
                DataGridLeaveLetter.Columns[0].Visible = false;

                DataGridLeaveLetter.Columns[1].Name = "Date";
                DataGridLeaveLetter.Columns[1].HeaderText = "Date";
                DataGridLeaveLetter.Columns[1].DataPropertyName = "Dte";
                
                DataGridLeaveLetter.Columns[2].Name = "Class";
                DataGridLeaveLetter.Columns[2].HeaderText = "Class";
                DataGridLeaveLetter.Columns[2].DataPropertyName = "Class_Desc";
                DataGridLeaveLetter.Columns[2].Width = 80;
                
                DataGridLeaveLetter.Columns[3].Name = "Student";
                DataGridLeaveLetter.Columns[3].HeaderText = "Student";
                DataGridLeaveLetter.Columns[3].DataPropertyName = "SName";
                DataGridLeaveLetter.Columns[3].Width = 140;
                
                DataGridLeaveLetter.Columns[4].Name = "Reason";
                DataGridLeaveLetter.Columns[4].HeaderText = "Reason";
                DataGridLeaveLetter.Columns[4].DataPropertyName = "ReasonForLeave";
                DataGridLeaveLetter.Columns[4].Width = 210;
                
                DataGridLeaveLetter.Columns[5].Name = "ClassId";
                DataGridLeaveLetter.Columns[5].HeaderText = "ClassId";
                DataGridLeaveLetter.Columns[5].DataPropertyName = "ClassId";
                DataGridLeaveLetter.Columns[5].Visible = false;
                
                DataGridLeaveLetter.Columns[6].Name = "StudId";
                DataGridLeaveLetter.Columns[6].HeaderText = "StudId";
                DataGridLeaveLetter.Columns[6].DataPropertyName = "StudId";
                DataGridLeaveLetter.Columns[6].Visible = false;
                DataGridLeaveLetter.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtStudent_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClass.Text != string.Empty)
                {
                    SqlParameter[] parameters = { new SqlParameter("@ClassId", txtClass.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStudByClassId", parameters, conn);
                    bsStud.DataSource = dt;
                    Fillgrid(2, dt);
                    Point point = FindLocation(txtStudent);
                    grSearch.Location = new Point(point.X, point.Y + 20);
                    grSearch.Visible = true;
                    grSearch.Text = "Class Search";
                }

                else
                {
                    MessageBox.Show("Select Class", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Fillgrid(int id, DataTable data)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "CId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Class_Desc";
                    DataGridCommon.Columns[1].HeaderText = "Class";
                    DataGridCommon.Columns[1].DataPropertyName = "Class_Desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsClass;
                }
                else if (id == 2)
                {
                    Fillid = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";

                    DataGridCommon.Columns[1].Name = "SName";
                    DataGridCommon.Columns[1].HeaderText = "Student Name";
                    DataGridCommon.Columns[1].DataPropertyName = "SName";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsStud;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtClass_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", conn);
                bsClass.DataSource = dt;
                Fillgrid(1, dt);
                Point point = FindLocation(txtClass);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtStudent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStudent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtStudent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStudent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClass.Text != string.Empty || txtStudent.Text != string.Empty || txtReason.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@ID",txtReason.Tag),
                        new SqlParameter("@Dte",Convert.ToDateTime(DtpDate.Text)),
                        new SqlParameter("@ClassId",txtClass.Tag),
                        new SqlParameter("@StudId",txtStudent.Tag),
                        new SqlParameter("@ReasonForLeave",txtReason.Text),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_LeaveLetter", parameters, conn);
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                    DataTable dt = GetConact();
                    LoadData(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("Class_Desc LIKE '%{0}%' ", txtClass.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtStudent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsStud.Filter = string.Format("SName LIKE '%{0}%' ", txtStudent.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void ClearControl()
        {
            try
            {
                txtStudent.Text = string.Empty;
                txtClass.Text = string.Empty;
                txtStudent.Tag = string.Empty;
                txtClass.Tag = string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridLeaveLetter.SelectedCells[0].RowIndex;
                txtStudent.Text = DataGridLeaveLetter.Rows[Index].Cells[3].Value.ToString();
                txtClass.Text = DataGridLeaveLetter.Rows[Index].Cells[2].Value.ToString();
                txtReason.Text = DataGridLeaveLetter.Rows[Index].Cells[4].Value.ToString();
                txtStudent.Tag = DataGridLeaveLetter.Rows[Index].Cells[6].Value.ToString();
                txtClass.Tag = DataGridLeaveLetter.Rows[Index].Cells[5].Value.ToString();
                txtReason.Tag = DataGridLeaveLetter.Rows[Index].Cells[0].Value.ToString();
                DtpDate.Text = Convert.ToDateTime(DataGridLeaveLetter.Rows[Index].Cells[1].Value.ToString()).ToString();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            LoadButton(0);
            ClearControl();
            DataTable dt = GetConact();
            LoadData(dt);
        }
    }
}
