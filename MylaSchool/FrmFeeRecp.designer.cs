﻿namespace MylaSchool
{
    partial class FrmFeeRecp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFeeRecp));
            this.GBList = new System.Windows.Forms.GroupBox();
            this.txtssno = new System.Windows.Forms.TextBox();
            this.txtLpt = new System.Windows.Forms.TextBox();
            this.txtStMid = new System.Windows.Forms.TextBox();
            this.cbomode = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtpaid = new System.Windows.Forms.TextBox();
            this.txtBAmt = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.chk = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtadmo = new System.Windows.Forms.TextBox();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtrcpt = new System.Windows.Forms.TextBox();
            this.txtsid = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtfine = new System.Windows.Forms.TextBox();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.cboTerm = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcid = new System.Windows.Forms.TextBox();
            this.txtclass = new System.Windows.Forms.TextBox();
            this.Dgvlist = new System.Windows.Forms.DataGridView();
            this.txtAPaid = new System.Windows.Forms.TextBox();
            this.txtstud = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.chkcan = new System.Windows.Forms.CheckBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.txtscr3 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnser = new System.Windows.Forms.Button();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.cmdprt = new System.Windows.Forms.Button();
            this.GBList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).BeginInit();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.Color.White;
            this.GBList.Controls.Add(this.txtssno);
            this.GBList.Controls.Add(this.txtLpt);
            this.GBList.Controls.Add(this.txtStMid);
            this.GBList.Controls.Add(this.cbomode);
            this.GBList.Controls.Add(this.label12);
            this.GBList.Controls.Add(this.label11);
            this.GBList.Controls.Add(this.label10);
            this.GBList.Controls.Add(this.label9);
            this.GBList.Controls.Add(this.label8);
            this.GBList.Controls.Add(this.txtpaid);
            this.GBList.Controls.Add(this.txtBAmt);
            this.GBList.Controls.Add(this.checkBox2);
            this.GBList.Controls.Add(this.chk);
            this.GBList.Controls.Add(this.label7);
            this.GBList.Controls.Add(this.txtadmo);
            this.GBList.Controls.Add(this.dtp);
            this.GBList.Controls.Add(this.label6);
            this.GBList.Controls.Add(this.label5);
            this.GBList.Controls.Add(this.txtrcpt);
            this.GBList.Controls.Add(this.txtsid);
            this.GBList.Controls.Add(this.label4);
            this.GBList.Controls.Add(this.txtfine);
            this.GBList.Controls.Add(this.txttotal);
            this.GBList.Controls.Add(this.cboTerm);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.label2);
            this.GBList.Controls.Add(this.txtcid);
            this.GBList.Controls.Add(this.txtclass);
            this.GBList.Controls.Add(this.Dgvlist);
            this.GBList.Controls.Add(this.txtAPaid);
            this.GBList.Controls.Add(this.txtstud);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.txtuid);
            this.GBList.Location = new System.Drawing.Point(5, -1);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(856, 535);
            this.GBList.TabIndex = 5;
            this.GBList.TabStop = false;
            // 
            // txtssno
            // 
            this.txtssno.Location = new System.Drawing.Point(784, 26);
            this.txtssno.Name = "txtssno";
            this.txtssno.Size = new System.Drawing.Size(35, 20);
            this.txtssno.TabIndex = 159;
            this.txtssno.Visible = false;
            // 
            // txtLpt
            // 
            this.txtLpt.Location = new System.Drawing.Point(663, 87);
            this.txtLpt.Name = "txtLpt";
            this.txtLpt.Size = new System.Drawing.Size(95, 20);
            this.txtLpt.TabIndex = 158;
            this.txtLpt.Visible = false;
            // 
            // txtStMid
            // 
            this.txtStMid.Location = new System.Drawing.Point(663, 61);
            this.txtStMid.Name = "txtStMid";
            this.txtStMid.Size = new System.Drawing.Size(35, 20);
            this.txtStMid.TabIndex = 157;
            this.txtStMid.Visible = false;
            // 
            // cbomode
            // 
            this.cbomode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbomode.FormattingEnabled = true;
            this.cbomode.Items.AddRange(new object[] {
            "Cash",
            "DD",
            "Cheque"});
            this.cbomode.Location = new System.Drawing.Point(107, 438);
            this.cbomode.Name = "cbomode";
            this.cbomode.Size = new System.Drawing.Size(169, 26);
            this.cbomode.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(509, 421);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 18);
            this.label12.TabIndex = 155;
            this.label12.Text = "Fine";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(424, 478);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 18);
            this.label11.TabIndex = 154;
            this.label11.Text = "Amount To be Paid";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(413, 450);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 18);
            this.label10.TabIndex = 153;
            this.label10.Text = "Amount Paid Already";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(459, 507);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 18);
            this.label9.TabIndex = 152;
            this.label9.Text = "Amount Paid";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 441);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 18);
            this.label8.TabIndex = 151;
            this.label8.Text = "Mode of Pay";
            // 
            // txtpaid
            // 
            this.txtpaid.BackColor = System.Drawing.SystemColors.Window;
            this.txtpaid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpaid.Location = new System.Drawing.Point(558, 446);
            this.txtpaid.MaxLength = 250;
            this.txtpaid.Name = "txtpaid";
            this.txtpaid.Size = new System.Drawing.Size(156, 26);
            this.txtpaid.TabIndex = 9;
            this.txtpaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txtpaid_KeyPress);
            // 
            // txtBAmt
            // 
            this.txtBAmt.BackColor = System.Drawing.SystemColors.Window;
            this.txtBAmt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBAmt.Location = new System.Drawing.Point(558, 503);
            this.txtBAmt.MaxLength = 250;
            this.txtBAmt.Name = "txtBAmt";
            this.txtBAmt.Size = new System.Drawing.Size(156, 26);
            this.txtBAmt.TabIndex = 11;
            this.txtBAmt.TextChanged += new System.EventHandler(this.TxtBAmt_TextChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(313, 40);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(143, 22);
            this.checkBox2.TabIndex = 148;
            this.checkBox2.Text = "Admission No First";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // chk
            // 
            this.chk.AutoSize = true;
            this.chk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk.Location = new System.Drawing.Point(498, 43);
            this.chk.Name = "chk";
            this.chk.Size = new System.Drawing.Size(143, 22);
            this.chk.TabIndex = 146;
            this.chk.Text = "TC Issued Students";
            this.chk.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(495, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 18);
            this.label7.TabIndex = 145;
            this.label7.Text = "Admission No";
            this.label7.Visible = false;
            // 
            // txtadmo
            // 
            this.txtadmo.BackColor = System.Drawing.SystemColors.Window;
            this.txtadmo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadmo.Location = new System.Drawing.Point(597, 16);
            this.txtadmo.MaxLength = 250;
            this.txtadmo.Name = "txtadmo";
            this.txtadmo.Size = new System.Drawing.Size(161, 26);
            this.txtadmo.TabIndex = 2;
            this.txtadmo.Visible = false;
            // 
            // dtp
            // 
            this.dtp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp.Location = new System.Drawing.Point(336, 15);
            this.dtp.Name = "dtp";
            this.dtp.Size = new System.Drawing.Size(137, 26);
            this.dtp.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(284, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 18);
            this.label6.TabIndex = 142;
            this.label6.Text = "Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 18);
            this.label5.TabIndex = 141;
            this.label5.Text = "Receipt No.";
            // 
            // txtrcpt
            // 
            this.txtrcpt.BackColor = System.Drawing.SystemColors.Window;
            this.txtrcpt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrcpt.Location = new System.Drawing.Point(94, 12);
            this.txtrcpt.MaxLength = 250;
            this.txtrcpt.Name = "txtrcpt";
            this.txtrcpt.Size = new System.Drawing.Size(161, 26);
            this.txtrcpt.TabIndex = 3;
            // 
            // txtsid
            // 
            this.txtsid.Location = new System.Drawing.Point(817, 78);
            this.txtsid.Name = "txtsid";
            this.txtsid.Size = new System.Drawing.Size(35, 20);
            this.txtsid.TabIndex = 139;
            this.txtsid.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(506, 393);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 18);
            this.label4.TabIndex = 138;
            this.label4.Text = "Total";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtfine
            // 
            this.txtfine.BackColor = System.Drawing.SystemColors.Window;
            this.txtfine.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfine.Location = new System.Drawing.Point(558, 417);
            this.txtfine.MaxLength = 250;
            this.txtfine.Name = "txtfine";
            this.txtfine.Size = new System.Drawing.Size(156, 26);
            this.txtfine.TabIndex = 8;
            // 
            // txttotal
            // 
            this.txttotal.BackColor = System.Drawing.SystemColors.Window;
            this.txttotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotal.Location = new System.Drawing.Point(558, 389);
            this.txttotal.MaxLength = 250;
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(156, 26);
            this.txttotal.TabIndex = 7;
            this.txttotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txttotal_KeyPress);
            // 
            // cboTerm
            // 
            this.cboTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTerm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTerm.FormattingEnabled = true;
            this.cboTerm.Location = new System.Drawing.Point(94, 93);
            this.cboTerm.Name = "cboTerm";
            this.cboTerm.Size = new System.Drawing.Size(169, 26);
            this.cboTerm.TabIndex = 2;
            this.cboTerm.SelectedIndexChanged += new System.EventHandler(this.CboTerm_SelectedIndexChanged);
            this.cboTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CboTerm_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(46, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 133;
            this.label3.Text = "Term";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 132;
            this.label2.Text = "Student";
            // 
            // txtcid
            // 
            this.txtcid.Location = new System.Drawing.Point(774, 52);
            this.txtcid.Name = "txtcid";
            this.txtcid.Size = new System.Drawing.Size(35, 20);
            this.txtcid.TabIndex = 131;
            this.txtcid.Visible = false;
            // 
            // txtclass
            // 
            this.txtclass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtclass.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclass.Location = new System.Drawing.Point(94, 39);
            this.txtclass.MaxLength = 250;
            this.txtclass.Name = "txtclass";
            this.txtclass.ReadOnly = true;
            this.txtclass.Size = new System.Drawing.Size(199, 26);
            this.txtclass.TabIndex = 0;
            this.txtclass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtclass_MouseClick);
            this.txtclass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtclass_KeyDown);
            // 
            // Dgvlist
            // 
            this.Dgvlist.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgvlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgvlist.Location = new System.Drawing.Point(7, 122);
            this.Dgvlist.Name = "Dgvlist";
            this.Dgvlist.ReadOnly = true;
            this.Dgvlist.RowHeadersVisible = false;
            this.Dgvlist.Size = new System.Drawing.Size(845, 264);
            this.Dgvlist.TabIndex = 127;
            // 
            // txtAPaid
            // 
            this.txtAPaid.BackColor = System.Drawing.SystemColors.Window;
            this.txtAPaid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPaid.Location = new System.Drawing.Point(558, 474);
            this.txtAPaid.MaxLength = 250;
            this.txtAPaid.Name = "txtAPaid";
            this.txtAPaid.Size = new System.Drawing.Size(156, 26);
            this.txtAPaid.TabIndex = 10;
            this.txtAPaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtAPaid_KeyPress);
            // 
            // txtstud
            // 
            this.txtstud.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtstud.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstud.Location = new System.Drawing.Point(94, 66);
            this.txtstud.MaxLength = 250;
            this.txtstud.Name = "txtstud";
            this.txtstud.Size = new System.Drawing.Size(354, 26);
            this.txtstud.TabIndex = 1;
            this.txtstud.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Txtstud_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 18);
            this.label1.TabIndex = 117;
            this.label1.Text = "Class ";
            // 
            // txtuid
            // 
            this.txtuid.Location = new System.Drawing.Point(774, 78);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(35, 20);
            this.txtuid.TabIndex = 109;
            this.txtuid.Visible = false;
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(739, 0);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(63, 30);
            this.btnsave.TabIndex = 123;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(799, 0);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(63, 30);
            this.btnaddrcan.TabIndex = 122;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.Color.White;
            this.GBMain.Controls.Add(this.chkcan);
            this.GBMain.Controls.Add(this.txtscr6);
            this.GBMain.Controls.Add(this.txtscr5);
            this.GBMain.Controls.Add(this.txtscr3);
            this.GBMain.Controls.Add(this.txtscr4);
            this.GBMain.Controls.Add(this.txtscr2);
            this.GBMain.Controls.Add(this.txtscr);
            this.GBMain.Controls.Add(this.label13);
            this.GBMain.Controls.Add(this.dateTimePicker1);
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.Dgv);
            this.GBMain.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBMain.Location = new System.Drawing.Point(5, -2);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(856, 536);
            this.GBMain.TabIndex = 6;
            this.GBMain.TabStop = false;
            // 
            // chkcan
            // 
            this.chkcan.AutoSize = true;
            this.chkcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkcan.Location = new System.Drawing.Point(684, 19);
            this.chkcan.Name = "chkcan";
            this.chkcan.Size = new System.Drawing.Size(134, 22);
            this.chkcan.TabIndex = 216;
            this.chkcan.Text = "Canceled Receipt";
            this.chkcan.UseVisualStyleBackColor = true;
            this.chkcan.CheckedChanged += new System.EventHandler(this.Chkcan_CheckedChanged);
            // 
            // txtscr6
            // 
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(636, 48);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(120, 26);
            this.txtscr6.TabIndex = 215;
            // 
            // txtscr5
            // 
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(436, 48);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(200, 26);
            this.txtscr5.TabIndex = 214;
            // 
            // txtscr3
            // 
            this.txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr3.Location = new System.Drawing.Point(234, 48);
            this.txtscr3.Name = "txtscr3";
            this.txtscr3.Size = new System.Drawing.Size(80, 26);
            this.txtscr3.TabIndex = 213;
            // 
            // txtscr4
            // 
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(315, 48);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(120, 26);
            this.txtscr4.TabIndex = 212;
            // 
            // txtscr2
            // 
            this.txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr2.Location = new System.Drawing.Point(139, 48);
            this.txtscr2.Name = "txtscr2";
            this.txtscr2.Size = new System.Drawing.Size(95, 26);
            this.txtscr2.TabIndex = 211;
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(14, 48);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(125, 26);
            this.txtscr.TabIndex = 210;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(350, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 18);
            this.label13.TabIndex = 160;
            this.label13.Text = "Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(391, 18);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(104, 26);
            this.dateTimePicker1.TabIndex = 159;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.DateTimePicker1_ValueChanged);
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.White;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnser.Location = new System.Drawing.Point(756, 46);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(79, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.Btnser_Click);
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(14, 78);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(821, 433);
            this.Dgv.TabIndex = 0;
            this.Dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_CellContentClick);
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.cmdprt);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Location = new System.Drawing.Point(-1, 537);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(867, 32);
            this.panadd.TabIndex = 209;
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnedit.Location = new System.Drawing.Point(684, 0);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(68, 30);
            this.btnedit.TabIndex = 217;
            this.btnedit.Text = "Cancel";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Click += new System.EventHandler(this.Btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(629, 0);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(56, 30);
            this.btnadd.TabIndex = 216;
            this.btnadd.Text = "Add";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.Btnadd_Click_1);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(808, 0);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(54, 30);
            this.btnexit.TabIndex = 219;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click_1);
            // 
            // cmdprt
            // 
            this.cmdprt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdprt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdprt.Image = ((System.Drawing.Image)(resources.GetObject("cmdprt.Image")));
            this.cmdprt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdprt.Location = new System.Drawing.Point(751, 0);
            this.cmdprt.Name = "cmdprt";
            this.cmdprt.Size = new System.Drawing.Size(58, 30);
            this.cmdprt.TabIndex = 218;
            this.cmdprt.Text = "Print";
            this.cmdprt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdprt.UseVisualStyleBackColor = false;
            this.cmdprt.Click += new System.EventHandler(this.Cmdprt_Click_1);
            // 
            // FrmFeeRecp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(864, 575);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GBList);
            this.Controls.Add(this.GBMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmFeeRecp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fee Collection";
            this.Load += new System.EventHandler(this.FrmFeeRecp_Load);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgvlist)).EndInit();
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.panadd.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.TextBox txtsid;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtfine;
        internal System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.ComboBox cboTerm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcid;
        internal System.Windows.Forms.TextBox txtclass;
        private System.Windows.Forms.DataGridView Dgvlist;
        internal System.Windows.Forms.TextBox txtAPaid;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        internal System.Windows.Forms.TextBox txtstud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtrcpt;
        private System.Windows.Forms.DateTimePicker dtp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txtadmo;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox chk;
        internal System.Windows.Forms.TextBox txtpaid;
        internal System.Windows.Forms.TextBox txtBAmt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbomode;
        private System.Windows.Forms.GroupBox GBMain;
        internal System.Windows.Forms.Button btnser;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.TextBox txtStMid;
        private System.Windows.Forms.TextBox txtLpt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txtssno;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button cmdprt;
        private System.Windows.Forms.Button btnexit;
        internal System.Windows.Forms.TextBox txtscr5;
        internal System.Windows.Forms.TextBox txtscr3;
        internal System.Windows.Forms.TextBox txtscr4;
        internal System.Windows.Forms.TextBox txtscr2;
        internal System.Windows.Forms.TextBox txtscr;
        internal System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.CheckBox chkcan;
    }
}