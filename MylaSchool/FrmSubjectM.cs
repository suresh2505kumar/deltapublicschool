﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MylaSchool
{
    public partial class FrmSubjectM : Form
    {
        public FrmSubjectM()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        public double sum111 = 0;
        SQLDBHelper db = new SQLDBHelper();
        int slno = 0;
        BindingSource bsParty = new BindingSource();
        public int SelectId = 0;

        private void FrmSubjectM_Load(object sender, EventArgs e)
        {
            this.Dgv.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dgvlist.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgvlist.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            Dgv.RowHeadersVisible = false;
            Dgvlist.RowHeadersVisible = false;
            grSearch.Visible = false;

            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            chk.Checked = true;
            getemp();

            cbocls.Text = "Nursary";
        }

        public void Loadclass()
        {
            if (checkBox1.Checked == true)
            {
                string quy = "Select class_Desc as StandardGroup,'' as Hours,cid From class_Mast  order by Sqn_No ";
                cmd = new SqlCommand(quy, con);
            }
            else
            {
                if (cbocls.Text == "Nursary")
                {
                    string quy = "Select class_Desc as StandardGroup,'' as Hours,cid From class_Mast where Sqn_No >= 2 and Sqn_No <= 3 ";
                    cmd = new SqlCommand(quy, con);
                }
                else if (cbocls.Text == "Primary")
                {
                    string quy = "Select class_Desc as StandardGroup,'' as Hours,cid  From class_Mast where Sqn_No >= 4 and Sqn_No <= 8  order by Sqn_no,S_No,cid";
                    cmd = new SqlCommand(quy, con);
                }
                else if (cbocls.Text == "Secondry")
                {
                    string quy = "Select class_Desc as StandardGroup,'' as Hours,cid  From class_Mast where Sqn_No >= 9 and Sqn_No <= 13 order by Sqn_no,S_No,cid";
                    cmd = new SqlCommand(quy, con);
                }
                else if (cbocls.Text == "Higher")
                {
                    string quy = "Select class_Desc as StandardGroup,'' as Hours,cid From class_Mast where Sqn_No >= 14 and Sqn_No <= 19 order by Sqn_no,S_No,cid";
                    cmd = new SqlCommand(quy, con);
                }
            }


            SqlDataAdapter aptr = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            Dgvlist.Columns.Add(checkColumn);

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = tap.Columns.Count + 1;
            Module.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                Dgvlist.Columns[Module.i].Name = column.ColumnName;
                Dgvlist.Columns[Module.i].HeaderText = column.ColumnName;
                Dgvlist.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }

            Dgvlist.Columns[1].Width = 110;
            Dgvlist.Columns[2].Width = 110;
            Dgvlist.Columns[3].Visible = false;
            Dgvlist.DataSource = tap;
        }

        protected void LoadClassGrid()
        {
            try
            {
                Dgvlist.DataSource = null;
                Dgvlist.AutoGenerateColumns = false;
                Dgvlist.ColumnCount = 3;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadclassEdit()
        {
            con.Close();
            con.Open();
            string quy = "SELECT b.class_desc as StandardGroup,Hours,Std_id,uid,Sub_Id  FROM Sub_Std a inner join class_mast b on a.Std_id=b.Cid  where Sub_Id=" + txtuid.Text + "";
            cmd = new SqlCommand(quy, con);
            SqlDataAdapter aptr = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;

            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            Dgvlist.Columns.Add(checkColumn);

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = tap.Columns.Count + 1;
            Module.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                Dgvlist.Columns[Module.i].Name = column.ColumnName;
                Dgvlist.Columns[Module.i].HeaderText = column.ColumnName;
                Dgvlist.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            Dgvlist.Columns[1].Width = 110;
            Dgvlist.Columns[2].Width = 110;
            Dgvlist.Columns[3].Visible = false;

            Dgvlist.Columns[4].Visible = false;
            Dgvlist.Columns[5].Visible = false;
            Dgvlist.DataSource = tap;
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            clearTxt();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnDelete.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select uid,Sub_Desc,Sub_sn from sub_mast ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 3;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Sub_Desc";
                Dgv.Columns[1].HeaderText = "Subject Name";
                Dgv.Columns[1].DataPropertyName = "Sub_Desc";
                Dgv.Columns[1].Width = 180;

                Dgv.Columns[2].Name = "Sub_sn";
                Dgv.Columns[2].HeaderText = "ShortName";
                Dgv.Columns[2].DataPropertyName = "Sub_sn";
                Dgv.Columns[2].Width = 180;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void btnsave_Click(object sender, EventArgs e)
        {
            int ExtraCurricular = 0;
            string ui;
            if (checkBox1.Checked == true)
            {
                ui = "1";
                ExtraCurricular = 1;
            }
            else
            {
                ui = "0";
                ExtraCurricular = 0;
            }
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from sub_mast where  Sub_Desc='" + txtroute.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    SqlParameter[] para ={
                        new SqlParameter("@uid","0"),
                        new SqlParameter("@Sub_Desc",txtroute.Text),
                        new SqlParameter("@Sub_sn",txtshortname.Text),
                        new SqlParameter("@Returnid ",SqlDbType.Int),
                        new SqlParameter("@ExtraCurricular",ExtraCurricular),
                    };
                    para[3].Direction = ParameterDirection.Output;
                    int uid = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_sub_mast", para, con, 3));
                    SqlParameter[] Para11 = {
                        new SqlParameter("@sub_did", uid),
                        new SqlParameter("@Cls", Convert.ToString(cbocls.Text)),
                        new SqlParameter("@Ext_Min", Convert.ToDecimal(txtEPass.Text)),
                        new SqlParameter("@Ext_max", Convert.ToDecimal(txtEmax.Text)),
                        new SqlParameter("@Int_Min", Convert.ToDecimal(txtIPass.Text)),
                        new SqlParameter("@Int_Max", Convert.ToDecimal(txtImax.Text)),
                        new SqlParameter("@Type", ui),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Sub_Det", Para11, con);
                    int k1 = 0;

                    foreach (DataGridViewRow row in Dgvlist.Rows)
                    {
                        DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            SqlParameter[] paraDet = {
                                    new SqlParameter("@Sub_Id", uid),
                                    new SqlParameter("@Std_id ", Convert.ToInt16(Dgvlist.Rows[k1].Cells[3].Value.ToString())),
                                    new SqlParameter("@Hours", Convert.ToString(Dgvlist.Rows[k1].Cells[2].Value.ToString())),
                                    new SqlParameter("@ClassId", Convert.ToString(Dgvlist.Rows[k1].Cells[3].Value.ToString())),
                                };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Sub_Std", paraDet, con);
                        }
                        k1 = k1 + 1;
                    }
                }
            }
            else
            {
                con.Close();
                con.Open();
                cmd.CommandText = "delete from Sub_Det where sub_did=" + txtuid.Text + "";
                cmd.ExecuteNonQuery();
                string ui1;
                if (chk.Checked == true)
                {
                    ui1 = "1";
                }
                else
                {
                    ui1 = "0";
                }
                SqlParameter[] para ={
                    new SqlParameter("@uid",txtuid.Text),
                    new SqlParameter("@Sub_Desc",txtroute.Text),
                    new SqlParameter("@Sub_sn",txtshortname.Text),
                    new SqlParameter("@Returnid ",SqlDbType.Int),
                    new SqlParameter("@ExtraCurricular",ExtraCurricular),
                };
                para[3].Direction = ParameterDirection.Output;
                int uid = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_sub_mast", para, con, 3));
                SqlParameter[] Para1 = {
                        new SqlParameter("@sub_did", uid),
                        new SqlParameter("@Cls", Convert.ToString(cbocls.Text)),
                        new SqlParameter("@Ext_Min", Convert.ToDecimal(txtEPass.Text)),
                        new SqlParameter("@Ext_max", Convert.ToDecimal(txtEmax.Text)),
                        new SqlParameter("@Int_Min", Convert.ToDecimal(txtIPass.Text)),
                        new SqlParameter("@Int_Max", Convert.ToDecimal(txtImax.Text)),
                        new SqlParameter("@Type", ui1),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Sub_Det", Para1, con);
                int k = 0;
                string Query = "delete from Sub_Std where Sub_Id=" + uid + "";
                db.ExecuteNonQuery(CommandType.Text, Query, con);
                foreach (DataGridViewRow row in Dgvlist.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        SqlParameter[] paraDet = {
                                    new SqlParameter("@Sub_Id", uid),
                                    new SqlParameter("@Std_id ", Convert.ToInt16(Dgvlist.Rows[k].Cells[3].Value.ToString())),
                                    new SqlParameter("@Hours", Convert.ToString(Dgvlist.Rows[k].Cells[2].Value.ToString())),
                                    new SqlParameter("@ClassId", Convert.ToString(Dgvlist.Rows[k].Cells[3].Value.ToString())),
                                };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Sub_Std", paraDet, con);
                    }
                    k = k + 1;
                }
            }

            MessageBox.Show("Record  Saved ");
            clearTxt();
            con.Close();
            GBList.Visible = false;
            GBMain.Visible = true;
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnDelete.Visible = true;
            btnaddrcan.Visible = false;
        }

        private void TxtfeeM_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txttime1.Focus();

                    if (TxtfeeM.Tag.ToString() != "")
                    {
                        LoadBoardingGrid();
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            clearTxt();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Save";
            chk.Checked = false;
            txtroute.Focus();
            Loadclass();
        }

        private void TxtfeeM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("name LIKE '%{0}%' ", TxtfeeM.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Titlep()
        {

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = 7;
            Dgvlist.Columns[0].Name = "Ruid";
            Dgvlist.Columns[0].HeaderText = "Ruid";
            Dgvlist.Columns[0].DataPropertyName = "Ruid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "slno";
            Dgvlist.Columns[1].HeaderText = "slno";
            Dgvlist.Columns[1].DataPropertyName = "slno";
            Dgvlist.Columns[1].Width = 50;

            Dgvlist.Columns[2].Name = "BoardName";
            Dgvlist.Columns[2].HeaderText = "BoardName";
            Dgvlist.Columns[2].DataPropertyName = "BoardName";
            Dgvlist.Columns[2].Width = 268;

            Dgvlist.Columns[3].Name = "Time1";
            Dgvlist.Columns[3].HeaderText = "Time1";
            Dgvlist.Columns[3].DataPropertyName = "Time1";
            Dgvlist.Columns[3].Width = 100;

            Dgvlist.Columns[4].Name = "Time2";
            Dgvlist.Columns[4].HeaderText = "Time2";
            Dgvlist.Columns[4].DataPropertyName = "Time2";
            Dgvlist.Columns[4].Width = 100;

            Dgvlist.Columns[5].Name = "Strength";
            Dgvlist.Columns[5].HeaderText = "Strength";
            Dgvlist.Columns[5].DataPropertyName = "Strength";
            Dgvlist.Columns[5].Width = 100;
            Dgvlist.Columns[6].Name = "boardid";
            Dgvlist.Columns[6].HeaderText = "boardid";
            Dgvlist.Columns[6].DataPropertyName = "boardid";
            Dgvlist.Columns[6].Visible = false;




        }

        private void TitleC()
        {

        }

        private void btnok_Click(object sender, EventArgs e)
        {

            if (TxtfeeM.Text == "")
            {
                MessageBox.Show("Select the Boarding Points");
                TxtfeeM.Focus();
                return;
            }
            if (txtstrength.Text == "")
            {
                MessageBox.Show("Enter the strength");
                txtstrength.Focus();
                return;
            }

            string qur = "select  buid,Boardingp  from boardingpm where buid=" + TxtfeeM.Tag + "";
            cmd = new SqlCommand(qur, con);
            SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                slno = Dgvlist.Rows.Count + 1 - 1;
                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = '0';
                Dgvlist.Rows[index].Cells[1].Value = slno;
                Dgvlist.Rows[index].Cells[2].Value = tap1.Rows[i]["Boardingp"].ToString();
                Dgvlist.Rows[index].Cells[3].Value = txttime1.Text;
                Dgvlist.Rows[index].Cells[4].Value = txtend.Text;
                Dgvlist.Rows[index].Cells[5].Value = txtstrength.Text;
                Dgvlist.Rows[index].Cells[6].Value = tap1.Rows[i]["buid"].ToString();
            }
            clearTxt();
        }

        private void btnclass_Click(object sender, EventArgs e)
        {

        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnDelete.Visible = false;
            int index = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[index].Cells[0].Value.ToString();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_GetSubjectMasterEdit";
            cmd.Parameters.AddWithValue("@Uid", Convert.ToInt32(txtuid.Text));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dtMaster = ds.Tables[0];
            DataTable dtBondingDet = ds.Tables[1];
            DataTable dtBondingDet1 = ds.Tables[2];
            txtroute.Text = dtMaster.Rows[0]["Sub_Desc"].ToString();
            txtshortname.Text = dtMaster.Rows[0]["Sub_sn"].ToString();
            cbocls.Text = dtBondingDet.Rows[0]["Cls"].ToString();
            txtEPass.Text = dtBondingDet.Rows[0]["Ext_Min"].ToString();
            txtEmax.Text = dtBondingDet.Rows[0]["Ext_max"].ToString();
            txtIPass.Text = dtBondingDet.Rows[0]["Int_Min"].ToString();
            txtImax.Text = dtBondingDet.Rows[0]["Int_Max"].ToString();
            if (dtBondingDet.Rows[0]["Type"].ToString() == "1")
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
            LoadclassEdit();

            for (int l = 0; l < Dgvlist.RowCount - 1; l++)
            {
                if (Dgvlist.Rows[l].Cells[2].Value.ToString() != "")
                {
                    Dgvlist[0, l].Value = true;
                }
            }

            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Update";
        }

        public void clearTxt()
        {
            txtshortname.Text = "";
            txtEPass.Text = "";
            txtEmax.Text = "";
            txtIPass.Text = "";
            txtImax.Text = "";
            TxtfeeM.Text = "";
            txtend.Text = "";
            txttime1.Text = "";
            txtstrength.Text = "";

        }

        private void cboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();

                string qur = "select uid,Sub_Desc,Sub_sn from sub_mast  where  Sub_Desc like '%" + txtscr.Text + " or   Sub_sn like '%" + txtscr.Text + " ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 3;

                Dgv.Columns[0].Name = "uid";
                Dgv.Columns[0].HeaderText = "uid";
                Dgv.Columns[0].DataPropertyName = "uid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Sub_Desc";
                Dgv.Columns[1].HeaderText = "Subject Name";
                Dgv.Columns[1].DataPropertyName = "Sub_Desc";
                Dgv.Columns[1].Width = 180;

                Dgv.Columns[2].Name = "Sub_sn";
                Dgv.Columns[2].HeaderText = "ShortName";
                Dgv.Columns[2].DataPropertyName = "Sub_sn";
                Dgv.Columns[2].Width = 180;


                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Dgvlist_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)Dgvlist.Rows[Dgvlist.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
            {
                ch1.Value = false;
            }
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void TxtfeeM_MouseClick(object sender, MouseEventArgs e)
        {
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(TxtfeeM);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        protected void getemp()
        {
            try
            {
                con.Close();
                con.Open();
                string Query = "Select buid,name from busM order by buid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                AutoCompleteStringCollection coll = new AutoCompleteStringCollection();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string RegNo = dt.Rows[i]["name"].ToString();
                        coll.Add(RegNo);
                    }
                }
                txttime1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txttime1.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txttime1.AutoCompleteCustomSource = coll;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {

                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetRoute", con);
                bsParty.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "RUid";
                DataGridCommon.Columns[0].HeaderText = "RUid";
                DataGridCommon.Columns[0].DataPropertyName = "RUid";

                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 200;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        private void LoadBoardingGrid()
        {
            con.Close();
            con.Open();

            if (TxtfeeM.Tag != null)
            {
                string qur = "select b.Ruid,b.slno,c.Boardingp,b.strength from routem a inner join routed b on a.Ruid=b.headid inner join boardingpm c on b.boardid=c.buid where a.Ruid=" + TxtfeeM.Tag + "";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                adpt.SelectCommand = cmd;
                adpt.Fill(dt);

                Dgvlist.AutoGenerateColumns = false;
                Dgvlist.Refresh();
                Dgvlist.DataSource = null;
                Dgvlist.Rows.Clear();
                Dgvlist.ColumnCount = 4;
                Dgvlist.Columns[0].Name = "Ruid";
                Dgvlist.Columns[0].HeaderText = "Ruid";
                Dgvlist.Columns[0].DataPropertyName = "Ruid";
                Dgvlist.Columns[0].Visible = false;

                Dgvlist.Columns[1].Name = "slno";
                Dgvlist.Columns[1].HeaderText = "slno";
                Dgvlist.Columns[1].DataPropertyName = "slno";
                Dgvlist.Columns[1].Width = 100;

                Dgvlist.Columns[2].Name = "Boardingp";
                Dgvlist.Columns[2].HeaderText = "Boarding Name";
                Dgvlist.Columns[2].DataPropertyName = "Boardingp";
                Dgvlist.Columns[2].Width = 250;

                Dgvlist.Columns[3].Name = "strength";
                Dgvlist.Columns[3].HeaderText = "strength";
                Dgvlist.Columns[3].DataPropertyName = "strength";
                Dgvlist.Columns[3].Width = 100;

                for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
                {
                    i = Dgvlist.Rows.Add();
                    Dgvlist.Rows[i].Cells[0].Value = dt.Tables[0].Rows[i][0].ToString();
                    Dgvlist.Rows[i].Cells[1].Value = dt.Tables[0].Rows[i][1].ToString();
                    Dgvlist.Rows[i].Cells[2].Value = dt.Tables[0].Rows[i][2].ToString();
                    Dgvlist.Rows[i].Cells[3].Value = dt.Tables[0].Rows[i][3].ToString();
                }
            }
        }

        private void txtstrength_TextChanged(object sender, EventArgs e)
        {
            if (txtstrength.Text != string.Empty)
            {
                LoadBoardingGrid();
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();
                if (TxtfeeM.Tag.ToString() != "")
                {

                    LoadBoardingGrid();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();

                if (TxtfeeM.Tag.ToString() != "")
                {

                    LoadBoardingGrid();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txttime1.Focus();

                if (TxtfeeM.Tag != null || TxtfeeM.Tag.ToString() != "")
                {
                    LoadBoardingGrid();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txttime1_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txttime1.Text != string.Empty)
                {
                    string Query = "Select Buid,Name from busM  Where Name ='" + txttime1.Text + "'";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if (dt.Rows.Count != 0)
                    {
                        txttime1.Tag = dt.Rows[0]["Buid"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the Subject ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());

                    string Query = "Delete from Sub_Det Where sub_did = @sub_did";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@sub_did", SqlDbType.Int).Value = Uid;
                    cmd.ExecuteNonQuery();

                    string Query1 = "Delete from Sub_Std Where Sub_Id = @Sub_Id";
                    SqlCommand cmd1 = new SqlCommand(Query1, con);
                    cmd1.Parameters.AddWithValue("@Sub_Id", SqlDbType.Int).Value = Uid;
                    cmd1.ExecuteNonQuery();


                    string Query2 = "Delete from sub_mast Where uid = @uid";
                    SqlCommand cmd2 = new SqlCommand(Query2, con);
                    cmd2.Parameters.AddWithValue("@uid", SqlDbType.Int).Value = Uid;
                    cmd2.ExecuteNonQuery();
                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cbocls_SelectedIndexChanged(object sender, EventArgs e)
        {
            Loadclass();
        }

        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            if (chk.Checked == true)
            {
                for (int i = 0; i < Dgvlist.RowCount - 1; i++)
                {
                    Dgvlist[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < Dgvlist.RowCount - 1; i++)
                {
                    Dgvlist[0, i].Value = false;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                Loadclass();
            }
            else
            {
                Loadclass();
            }
        }
    }
}
