﻿namespace MylaSchool
{
    partial class FrmAttendance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grAtt = new System.Windows.Forms.GroupBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.DataGridAtt = new System.Windows.Forms.DataGridView();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.txtStudent = new System.Windows.Forms.TextBox();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnSMS = new System.Windows.Forms.Button();
            this.grSMS = new System.Windows.Forms.GroupBox();
            this.btnSendSMS = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSMS = new System.Windows.Forms.RichTextBox();
            this.DataGridSMS = new System.Windows.Forms.DataGridView();
            this.btnExt = new System.Windows.Forms.Button();
            this.grAtt.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAtt)).BeginInit();
            this.grSMS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSMS)).BeginInit();
            this.SuspendLayout();
            // 
            // grAtt
            // 
            this.grAtt.Controls.Add(this.btnOk);
            this.grAtt.Controls.Add(this.grSearch);
            this.grAtt.Controls.Add(this.DataGridAtt);
            this.grAtt.Controls.Add(this.cmbStatus);
            this.grAtt.Controls.Add(this.txtStudent);
            this.grAtt.Controls.Add(this.txtClass);
            this.grAtt.Controls.Add(this.label4);
            this.grAtt.Controls.Add(this.label3);
            this.grAtt.Controls.Add(this.label2);
            this.grAtt.Controls.Add(this.label1);
            this.grAtt.Controls.Add(this.dtpDate);
            this.grAtt.Controls.Add(this.btnSMS);
            this.grAtt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grAtt.Location = new System.Drawing.Point(7, 12);
            this.grAtt.Name = "grAtt";
            this.grAtt.Size = new System.Drawing.Size(759, 450);
            this.grAtt.TabIndex = 0;
            this.grAtt.TabStop = false;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(714, 101);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(38, 28);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.btnExit);
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(52, 130);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(405, 301);
            this.grSearch.TabIndex = 9;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.Image = global::MylaSchool.Properties.Resources.close;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(335, 271);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(65, 27);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Close";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.White;
            this.btnSelect.Image = global::MylaSchool.Properties.Resources.check;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect.Location = new System.Drawing.Point(258, 271);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 27);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(6, 17);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Size = new System.Drawing.Size(393, 252);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // DataGridAtt
            // 
            this.DataGridAtt.AllowUserToAddRows = false;
            this.DataGridAtt.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridAtt.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridAtt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAtt.EnableHeadersVisualStyles = false;
            this.DataGridAtt.Location = new System.Drawing.Point(10, 138);
            this.DataGridAtt.Name = "DataGridAtt";
            this.DataGridAtt.RowHeadersVisible = false;
            this.DataGridAtt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridAtt.Size = new System.Drawing.Size(740, 304);
            this.DataGridAtt.TabIndex = 8;
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Ab",
            "FAb",
            "AAb"});
            this.cmbStatus.Location = new System.Drawing.Point(559, 103);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(151, 26);
            this.cmbStatus.TabIndex = 7;
            // 
            // txtStudent
            // 
            this.txtStudent.Location = new System.Drawing.Point(312, 103);
            this.txtStudent.Name = "txtStudent";
            this.txtStudent.Size = new System.Drawing.Size(198, 26);
            this.txtStudent.TabIndex = 6;
            this.txtStudent.Click += new System.EventHandler(this.txtStudent_Click);
            this.txtStudent.TextChanged += new System.EventHandler(this.txtStudent_TextChanged);
            // 
            // txtClass
            // 
            this.txtClass.Location = new System.Drawing.Point(54, 103);
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(198, 26);
            this.txtClass.TabIndex = 5;
            this.txtClass.Click += new System.EventHandler(this.txtClass_Click);
            this.txtClass.TextChanged += new System.EventHandler(this.txtClass_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(510, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Status";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(258, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Student";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Class";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(54, 47);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(122, 26);
            this.dtpDate.TabIndex = 0;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // btnSMS
            // 
            this.btnSMS.BackColor = System.Drawing.Color.White;
            this.btnSMS.Image = global::MylaSchool.Properties.Resources.sms;
            this.btnSMS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSMS.Location = new System.Drawing.Point(641, 25);
            this.btnSMS.Name = "btnSMS";
            this.btnSMS.Size = new System.Drawing.Size(109, 32);
            this.btnSMS.TabIndex = 11;
            this.btnSMS.Text = "Send SMS";
            this.btnSMS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSMS.UseVisualStyleBackColor = false;
            this.btnSMS.Click += new System.EventHandler(this.btnSMS_Click);
            // 
            // grSMS
            // 
            this.grSMS.Controls.Add(this.btnSendSMS);
            this.grSMS.Controls.Add(this.btnPreview);
            this.grSMS.Controls.Add(this.label5);
            this.grSMS.Controls.Add(this.txtSMS);
            this.grSMS.Controls.Add(this.DataGridSMS);
            this.grSMS.Location = new System.Drawing.Point(7, 12);
            this.grSMS.Name = "grSMS";
            this.grSMS.Size = new System.Drawing.Size(759, 451);
            this.grSMS.TabIndex = 12;
            this.grSMS.TabStop = false;
            this.grSMS.Visible = false;
            // 
            // btnSendSMS
            // 
            this.btnSendSMS.BackColor = System.Drawing.Color.White;
            this.btnSendSMS.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendSMS.Image = global::MylaSchool.Properties.Resources.sms;
            this.btnSendSMS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSendSMS.Location = new System.Drawing.Point(602, 48);
            this.btnSendSMS.Name = "btnSendSMS";
            this.btnSendSMS.Size = new System.Drawing.Size(105, 41);
            this.btnSendSMS.TabIndex = 4;
            this.btnSendSMS.Text = "Send SMS";
            this.btnSendSMS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSendSMS.UseVisualStyleBackColor = false;
            this.btnSendSMS.Click += new System.EventHandler(this.btnSendSMS_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.BackColor = System.Drawing.Color.White;
            this.btnPreview.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreview.Image = global::MylaSchool.Properties.Resources.view;
            this.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPreview.Location = new System.Drawing.Point(602, 10);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(105, 34);
            this.btnPreview.TabIndex = 3;
            this.btnPreview.Text = "Preview";
            this.btnPreview.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPreview.UseVisualStyleBackColor = false;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(89, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 18);
            this.label5.TabIndex = 2;
            this.label5.Text = "SMS Content";
            // 
            // txtSMS
            // 
            this.txtSMS.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMS.Location = new System.Drawing.Point(182, 10);
            this.txtSMS.Name = "txtSMS";
            this.txtSMS.Size = new System.Drawing.Size(414, 92);
            this.txtSMS.TabIndex = 1;
            this.txtSMS.Text = "Dear Parents,\nFor your Kind knowledge, Your son /Daughter + Student Name + didn\'t" +
    " come to school today (+ Date +). Kindly acknowledge office. Thank you";
            // 
            // DataGridSMS
            // 
            this.DataGridSMS.AllowUserToAddRows = false;
            this.DataGridSMS.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSMS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSMS.Location = new System.Drawing.Point(6, 107);
            this.DataGridSMS.Name = "DataGridSMS";
            this.DataGridSMS.RowHeadersVisible = false;
            this.DataGridSMS.Size = new System.Drawing.Size(744, 318);
            this.DataGridSMS.TabIndex = 0;
            // 
            // btnExt
            // 
            this.btnExt.BackColor = System.Drawing.Color.White;
            this.btnExt.Image = global::MylaSchool.Properties.Resources.exit;
            this.btnExt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExt.Location = new System.Drawing.Point(718, 464);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(52, 27);
            this.btnExt.TabIndex = 4;
            this.btnExt.Text = "Exit";
            this.btnExt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExt.UseVisualStyleBackColor = false;
            this.btnExt.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(778, 491);
            this.Controls.Add(this.btnExt);
            this.Controls.Add(this.grAtt);
            this.Controls.Add(this.grSMS);
            this.MaximizeBox = false;
            this.Name = "FrmAttendance";
            this.Text = "Student Attendance";
            this.Load += new System.EventHandler(this.FrmAttendance_Load);
            this.grAtt.ResumeLayout(false);
            this.grAtt.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAtt)).EndInit();
            this.grSMS.ResumeLayout(false);
            this.grSMS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSMS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grAtt;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.TextBox txtStudent;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DataGridView DataGridAtt;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.Button btnSMS;
        private System.Windows.Forms.GroupBox grSMS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox txtSMS;
        private System.Windows.Forms.DataGridView DataGridSMS;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnSendSMS;
    }
}