﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Data.SqlClient;
using System.IO;


namespace MylaSchool
{
    public partial class FrmFeeStruct : Form
    {
        public FrmFeeStruct()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        public double sum111 = 0;

        private void FrmFeeStruct_Load(object sender, EventArgs e)
        {
            LoadRefT();
            Titlep();
            TitleC();
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            ClearTxt();
            Dgvlist.Rows.Clear();
            DgvClass.Rows.Clear();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select a.fsuid,ref_desc,termuid,b.Refname,sum(isnull(amt,0)) as amt from FeeStructureM a inner join feeref b on a.termuid =b.Ruid inner join FeeStructureD c on a.FSuid =c.FSuid group by a.fsuid,ref_desc,termuid,b.Refname";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 5;

                Dgv.Columns[0].Name = "fsuid";
                Dgv.Columns[0].HeaderText = "fsuid";
                Dgv.Columns[0].DataPropertyName = "fsuid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "ref_desc";
                Dgv.Columns[1].HeaderText = "Structure Name";
                Dgv.Columns[1].DataPropertyName = "ref_desc";
                Dgv.Columns[1].Width = 350;

                Dgv.Columns[2].DataPropertyName = "termuid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "Refname";
                Dgv.Columns[3].HeaderText = "Group";
                Dgv.Columns[3].DataPropertyName = "Refname";
                Dgv.Columns[3].Width = 270;

                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].HeaderText = "Amount";
                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].Width = 220;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from FeeStructureM where  Ref_desc='" + txtdesc.Text + "' and termuid=" + cboTerm.SelectedValue + "";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into FeeStructureM values(@Ref_desc,@termuid)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@Ref_desc", SqlDbType.NVarChar).Value = txtdesc.Text;
                    cmd.Parameters.AddWithValue("@termuid", SqlDbType.NVarChar).Value = cboTerm.SelectedValue;
                    cmd.ExecuteNonQuery();

                    string strSQL = "Select FSuid From FeeStructureM Where Ref_desc ='" + txtdesc.Text + "' and termuid=" + cboTerm.SelectedValue + "";
                    cmd = new SqlCommand(strSQL, con);
                    int FSuid = (int)cmd.ExecuteScalar();

                    for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                    {
                        string res1 = "insert into FeeStructureD(FSuid,Feeid,Feetype,Amt) values(" + FSuid + "," + Dgvlist.Rows[i].Cells[0].Value + ",'" + Dgvlist.Rows[i].Cells[2].Value + "','" + Dgvlist.Rows[i].Cells[3].Value + "')";
                        SqlCommand scmd = new SqlCommand(res1, con);
                        scmd.ExecuteNonQuery();
                    }

                    for (int i = 0; i < DgvClass.Rows.Count - 1; i++)
                    {
                        string res2 = "insert into FeeStructclass(FSuid,classuid) values(" + FSuid + "," + DgvClass.Rows[i].Cells[0].Value + ")";
                        SqlCommand scmd1 = new SqlCommand(res2, con);
                        scmd1.ExecuteNonQuery();
                    }

                    SqlParameter[] sqlParameters = { new SqlParameter("@Feesturid", FSuid) };
                    SQLDBHelper db = new SQLDBHelper();
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_Insert_PLFTYPE", sqlParameters, con);

                    //string qur1 = "select d.uid as studid,a.fsuid as Feesturid,b.feeid,b.Amt as feeAmt,isnull('',0) as concession,isnull('',0) as finalamt,isnull('',0) as paidamt,a.termuid as termuid,c.classuid as classid from FeeStructureM a inner join FeeStructureD b on a.FSuid =b.FSuid inner join FeeStructclass c on a.FSuid =c.FSuid inner join StudentM d on c.classuid =d.Classid where a.Fsuid=" + FSuid + "";
                    //cmd = new SqlCommand(qur1, con);
                    //adpt = new SqlDataAdapter(cmd);
                    //DataTable dt = new DataTable();
                    //adpt.Fill(dt);
                    //if (dt.Rows.Count > 0)
                    //{
                    //    DataTable DtCollection = new DataTable();
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(Int32) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(Int32) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(Int32) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "paidamt", DataType = typeof(decimal) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(Int32) });
                    //    DtCollection.Columns.Add(new DataColumn { ColumnName = "classuid", DataType = typeof(Int32) });

                    //    foreach (DataRow row1 in dt.Rows)
                    //    {
                    //        DataRow row = DtCollection.NewRow();
                    //        row["studid"] = row1["studid"];
                    //        row["Feesturid"] = row1["Feesturid"];
                    //        row["feeid"] = row1["feeid"];
                    //        row["feeamt"] = row1["feeamt"];
                    //        row["concession"] = 0;
                    //        row["finalamt"] = row1["feeamt"];
                    //        row["paidamt"] = 0;
                    //        row["termuid"] = row1["termuid"];
                    //        row["classuid"] = row1["classid"];
                    //        DtCollection.Rows.Add(row);
                    //    }
                    //    SqlCommand cmnd = new SqlCommand
                    //    {
                    //        Connection = con
                    //    };
                    //    con.Close();
                    //    con.Open();

                    //    for (int i = 0; i < DtCollection.Rows.Count; i++)
                    //    {

                    //        cmnd.CommandText = "exec Insert_PLF2 " + DtCollection.Rows[i]["studid"].ToString() + "," + DtCollection.Rows[i]["Feesturid"].ToString() + "," + DtCollection.Rows[i]["feeid"].ToString() + "," + DtCollection.Rows[i]["feeamt"].ToString() + "," + DtCollection.Rows[i]["concession"].ToString() + "," + DtCollection.Rows[i]["finalamt"].ToString() + "," + DtCollection.Rows[i]["paidamt"].ToString() + "," + DtCollection.Rows[i]["termuid"].ToString() + "," + DtCollection.Rows[i]["classuid"].ToString() + "";

                    //        cmnd.ExecuteNonQuery();
                    //    }
                    //}

                    MessageBox.Show(" Saved ");
                    ClearTxt();
                    con.Close();
                    Dgvlist.Rows.Clear();
                    DgvClass.Rows.Clear();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
            }
            else
            {
                con.Close();
                con.Open();
                string qur11 = " select  c.* from FeeStructureM a inner join FeeStructured b on a.FSuid =b.fsuid inner join Coll_det c on b.FDuid =c.feeDUid  and c.feeid =b.feeid where termuid=" + cboTerm.SelectedValue + " and a.FSuid=" + txtuid.Text;
                SqlCommand cmnd1 = new SqlCommand(qur11, con);
                SqlDataAdapter apt = new SqlDataAdapter(cmnd1);
                DataTable tap1 = new DataTable();
                apt.Fill(tap1);
                con.Close();
                if (tap1.Rows.Count != 0)
                {
                    MessageBox.Show("Fee Collected, cann't edit the Structure !");
                    ClearTxt();
                    Dgvlist.Rows.Clear();
                    DgvClass.Rows.Clear();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    return;
                }
                con.Close();
                string qur = "select * from FeeStructureM where  Ref_desc='" + txtdesc.Text + "' and termuid = " + cboTerm.SelectedValue + " and Fsuid <> " + txtuid.Text;
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string QueryUpdate = "update FeeStructureM  set ref_desc='" + txtdesc.Text + "' ,termuid=" + cboTerm.SelectedValue + " where Fsuid = " + txtuid.Text;
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    int FSuid = Convert.ToInt16(txtuid.Text);
                    string str = "Delete from FeeStructureD where FSuid=" + FSuid;
                    cmd = new SqlCommand(str, con);
                    cmd.ExecuteNonQuery();
                    for (int i = 0; i < Dgvlist.Rows.Count - 1; i++)
                    {
                        string res1 = "insert into FeeStructureD(FSuid,Feeid,Feetype,Amt) values(" + FSuid + "," + Dgvlist.Rows[i].Cells[0].Value + ",'" + Dgvlist.Rows[i].Cells[2].Value + "','" + Dgvlist.Rows[i].Cells[3].Value + "')";
                        SqlCommand scmd = new SqlCommand(res1, con);
                        scmd.ExecuteNonQuery();
                    }
                    string str1 = "Delete from FeeStructclass where FSuid=" + FSuid + "";
                    cmd = new SqlCommand(str1, con);
                    cmd.ExecuteNonQuery();
                    for (int i = 0; i < DgvClass.Rows.Count - 1; i++)
                    {
                        string res2 = "insert into FeeStructclass(FSuid,classuid) values(" + FSuid + "," + DgvClass.Rows[i].Cells[0].Value + ")";
                        SqlCommand scmd1 = new SqlCommand(res2, con);
                        scmd1.ExecuteNonQuery();
                    }
                    string qur1 = "select d.uid as studid,a.fsuid as Feesturid,b.feeid,b.Amt as feeAmt,isnull('',0) as concession,isnull('',0) as finalamt,isnull('',0) as paidamt,a.termuid as termuid,c.classuid as classid   from FeeStructureM a inner join FeeStructureD b on a.FSuid =b.FSuid inner join FeeStructclass c on a.FSuid =c.FSuid inner join StudentM d on c.classuid =d.Classid where a.Fsuid=" + FSuid + "";
                    cmd = new SqlCommand(qur1, con);
                    adpt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    SqlCommand cmnd = new SqlCommand
                    {
                        Connection = con
                    };
                    con.Close();
                    con.Open();
                    string str3 = "Delete from PLFType where Feesturid=" + FSuid + "";
                    cmd = new SqlCommand(str3, con);
                    cmd.ExecuteNonQuery();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        cmnd.CommandText = "exec Insert_PLF2 " + dt.Rows[i]["studid"].ToString() + "," + dt.Rows[i]["Feesturid"].ToString() + "," + dt.Rows[i]["feeid"].ToString() + "," + dt.Rows[i]["feeamt"].ToString() + ",0," + dt.Rows[i]["feeamt"].ToString() + ",0," + dt.Rows[i]["termuid"].ToString() + "," + dt.Rows[i]["classid"].ToString() + "";
                        cmnd.ExecuteNonQuery();
                    }
                    MessageBox.Show("Record Updated Sucessfully", "");
                    ClearTxt();
                    Dgvlist.Rows.Clear();
                    DgvClass.Rows.Clear();
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }
                btnsave.Text = "Save";
            }
        }
        private void TxtfeeM_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Loadput()
        {
            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";
            con.Close();
            con.Open();

            if (Module.type == 1)
            {
                FunC.Classmaster("cid", "Class_Desc", Module.strsql, this, txtcid, txtclass, GBList);
                Module.strsql = "select cid,Class_Desc,S_no  from class_mast where active=1 ";
                Module.FSSQLSortStr = "Class_Desc";
            }
            else if (Module.type == 2)
            {
                FunC.Classmaster("fid", "descp", Module.strsql, this, txtfuid, TxtfeeM, GBList);
                Module.strsql = "select fid,descp from feemast where active=1";
                Module.FSSQLSortStr = "descp";
            }
            Module.cmd = new SqlCommand(Module.strsql, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 1)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[0].Visible = false;
                dt.Columns[2].Visible = false;
            }

            else if (Module.type == 2)
            {
                dt.Columns[1].Width = 270;
                dt.Columns[0].Visible = false;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 1)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 2)
                {
                    dt.Columns[Module.i].HeaderText = "Fee Description";
                }

                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.Show();
            con.Close();
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            Dgvlist.Rows.Clear();
            DgvClass.Rows.Clear();
            cbotype.Text = "ALL";
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
            sum111 = 0;
        }

        private void TxtfeeM_TextChanged(object sender, EventArgs e)
        {

        }

        private void Txtclass_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void LoadRefT()
        {
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);
            cboTerm.DataSource = null;
            cboTerm.DataSource = tap;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.SelectedIndex = -1;

        }

        private void Txtclass_TextChanged(object sender, EventArgs e)
        {

        }

        private void Titlep()
        {
            Dgvlist.ColumnCount = 4;
            Dgvlist.Columns[0].Name = "fid";
            Dgvlist.Columns[0].HeaderText = "fid";
            Dgvlist.Columns[0].DataPropertyName = "fid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "descp";
            Dgvlist.Columns[1].HeaderText = "Description";
            Dgvlist.Columns[1].DataPropertyName = "descp";
            Dgvlist.Columns[1].Width = 268;

            Dgvlist.Columns[2].Name = "AppilesTo";
            Dgvlist.Columns[2].Width = 121;

            Dgvlist.Columns[3].Name = "Amount";
            Dgvlist.Columns[3].Width = 145;
            this.Dgvlist.Columns["Amount"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void TitleC()
        {
            DgvClass.ColumnCount = 2;
            DgvClass.Columns[0].Name = "cid";
            DgvClass.Columns[0].HeaderText = "cid";
            DgvClass.Columns[0].DataPropertyName = "cid";
            DgvClass.Columns[0].Visible = false;

            DgvClass.Columns[1].Name = "Class_Desc";
            DgvClass.Columns[1].HeaderText = "Class";
            DgvClass.Columns[1].DataPropertyName = "Class_Desc";
            DgvClass.Columns[1].Width = 160;
        }

        private void Btnok_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < Dgvlist.RowCount- 1; i++)
            {
                if (txtclass.Text == Dgvlist.Rows[i].Cells[1].Value.ToString())
                {
                    MessageBox.Show("Fees Already Added");
                    TxtfeeM.Text = "";
                    TxtfeeM.Tag = "";
                    TxtfeeM.Focus();
                    return;
                }
            }
            Dgvlist.AllowUserToAddRows = true;
            Dgvlist.AutoGenerateColumns = false;
            if (txtamt.Text != string.Empty)
            {
                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = txtfuid.Text;
                Dgvlist.Rows[index].Cells[1].Value = TxtfeeM.Text;
                Dgvlist.Rows[index].Cells[2].Value = cbotype.Text;
                Dgvlist.Rows[index].Cells[3].Value = txtamt.Text;
                TxtfeeM.Text = "";
                txtamt.Text = "";
                sum111 += Convert.ToDouble(Dgvlist.Rows[index].Cells[3].Value.ToString());
                txttotal.Text = sum111.ToString();
                txttotal.TextAlign = HorizontalAlignment.Right;
            }
            else
            {
                MessageBox.Show("Amount can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnclass_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < DgvClass.RowCount - 1; i++)
            {
                if (txtclass.Text == DgvClass.Rows[i].Cells[1].Value.ToString())
                {
                    MessageBox.Show("Class Already Added");
                    txtclass.Text = "";
                    txtclass.Text = "";
                    txtclass.Focus();
                    return;
                }
            }
            DgvClass.AllowUserToAddRows = true;
            DgvClass.AutoGenerateColumns = false;
            var index = DgvClass.Rows.Add();
            DgvClass.Rows[index].Cells[0].Value = txtcid.Text;
            DgvClass.Rows[index].Cells[1].Value = txtclass.Text;
            txtclass.Text = string.Empty;
            txtcid.Text = string.Empty;
            txtclass.Focus();
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtdesc.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            cboTerm.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            LoadFeeGrid();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Update";
            sum111 = 0;
        }

        private void LoadFeeGrid()
        {
            con.Close();
            con.Open();
            string qur = "select a.Feeid,b.Descp,Feetype ,Amt   from  FeeStructureD a inner join feemast b on a.Feeid =b.Fid where Fsuid=" + Dgv.CurrentRow.Cells[0].Value.ToString() + "";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataSet dt = new DataSet();
            adpt.SelectCommand = cmd;
            adpt.Fill(dt);
            Dgvlist.Refresh();
            Dgvlist.Columns[0].HeaderText = "fid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "descp";
            Dgvlist.Columns[1].HeaderText = "Description";
            Dgvlist.Columns[1].DataPropertyName = "descp";
            Dgvlist.Columns[1].Width = 268;

            Dgvlist.Columns[2].HeaderText = "AppilesTo";
            Dgvlist.Columns[2].Width = 121;

            Dgvlist.Columns[3].HeaderText = "Amount";
            Dgvlist.Columns[3].Width = 140;
            double sum = 0;
            for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
            {
                i = Dgvlist.Rows.Add();
                Dgvlist.Rows[i].Cells[0].Value = dt.Tables[0].Rows[i][0].ToString();
                Dgvlist.Rows[i].Cells[1].Value = dt.Tables[0].Rows[i][1].ToString();
                Dgvlist.Rows[i].Cells[2].Value = dt.Tables[0].Rows[i][2].ToString();
                Dgvlist.Rows[i].Cells[3].Value = dt.Tables[0].Rows[i][3].ToString();
                sum += Convert.ToDouble(dt.Tables[0].Rows[i][3].ToString());
                txttotal.Text = sum.ToString();
                txttotal.TextAlign = HorizontalAlignment.Right;
            }
            con.Close();
            con.Open();
            string qur1 = "select classuid,Class_Desc from FeeStructclass a inner join Class_Mast b on a.classuid =b.Cid  where Fsuid=" + Dgv.CurrentRow.Cells[0].Value.ToString() + "";
            cmd = new SqlCommand(qur1, con);
            adpt = new SqlDataAdapter(cmd);
            DataSet dt1 = new DataSet();
            adpt.SelectCommand = cmd;
            adpt.Fill(dt1);
            DgvClass.Refresh();
            TitleC();
            for (int j = 0; j < dt1.Tables[0].Rows.Count; j++)
            {
                j = DgvClass.Rows.Add();
                DgvClass.Rows[j].Cells[0].Value = dt1.Tables[0].Rows[j][0].ToString();
                DgvClass.Rows[j].Cells[1].Value = dt1.Tables[0].Rows[j][1].ToString();
            }
        }

        public void ClearTxt()
        {
            txtdesc.Text = "";
            TxtfeeM.Text = "";
            txtamt.Text = "";
            txtclass.Text = "";
            txttotal.Text = "";
            cboTerm.SelectedIndex = -1;
            cbotype.SelectedIndex = -1;
        }

        private void CboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select a.fsuid,ref_desc,termuid,b.Refname,sum(isnull(amt,0)) as amt from FeeStructureM a inner join feeref b on a.termuid =b.Ruid inner join FeeStructureD c on a.FSuid =c.FSuid where ref_desc like '%" + txtscr.Text + "%' and Refname like '%" + txtscr1.Text + "%' group by a.fsuid,ref_desc,termuid,b.Refname ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 5;

                Dgv.Columns[0].Name = "fsuid";
                Dgv.Columns[0].HeaderText = "fsuid";
                Dgv.Columns[0].DataPropertyName = "fsuid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "ref_desc";
                Dgv.Columns[1].HeaderText = "Structure Name";
                Dgv.Columns[1].DataPropertyName = "ref_desc";
                Dgv.Columns[1].Width = 350;

                Dgv.Columns[2].DataPropertyName = "termuid";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "Refname";
                Dgv.Columns[3].HeaderText = "Group";
                Dgv.Columns[3].DataPropertyName = "Refname";
                Dgv.Columns[3].Width = 270;

                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].HeaderText = "Amount";
                Dgv.Columns[4].DataPropertyName = "amt";
                Dgv.Columns[4].Width = 220;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Dgvlist_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgvlist_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Dgvlist.Rows[e.RowIndex].ReadOnly = false;

            if (Dgvlist.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
            {
                Dgvlist.Rows[e.RowIndex].ReadOnly = true;
            }
        }

        private void Dgvlist_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            double sum1 = 0;
            for (int i = 0; i < Dgvlist.RowCount - 1; i++)
            {
                sum1 += Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                txttotal.Text = sum1.ToString();
                txttotal.TextAlign = HorizontalAlignment.Right;
            }
        }

        private void Txtscr1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Txtscr_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtfeeM_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 2;
            Loadput();
        }

        private void Txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            Module.type = 1;
            Loadput();
        }

        private void GBMain_Enter(object sender, EventArgs e)
        {

        }

        private void Dgvlist_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Dou tou want to delete this record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int Index = Dgvlist.SelectedCells[0].RowIndex;
                        if (btnsave.Text == "Save")
                        {
                            Dgvlist.Rows.RemoveAt(Index);
                            Dgvlist.ClearSelection();
                        }
                        else
                        {
                            string Query = "delete from FeeStructureD Where FSUid = " + txtuid.Text + " and Feeid = " + Dgvlist.Rows[Index].Cells[0].Value.ToString() + "";
                            SQLDBHelper db = new SQLDBHelper();
                            db.ExecuteNonQuery(CommandType.Text, Query, con);
                            Dgvlist.Rows.RemoveAt(Index);
                            Dgvlist.ClearSelection();
                        }
                    }
                    double sum1 = 0;
                    for (int i = 0; i < Dgvlist.RowCount - 1; i++)
                    {
                        sum1 += Convert.ToDouble(Dgvlist.Rows[i].Cells[3].Value);
                        txttotal.Text = sum1.ToString();
                        txttotal.TextAlign = HorizontalAlignment.Right;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
