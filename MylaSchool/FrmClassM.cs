﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MylaSchool
{
    public partial class FrmClassM : Form
    {
        public FrmClassM()
        {
            InitializeComponent();
        }
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;
            btnDelete.Visible = true;
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            txtclass.Text = string.Empty;
            cbogrde.SelectedIndex = -1;
            cbotype.SelectedIndex = -1;
            cboser.SelectedIndex = -1;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnsave.Text = "Save";
            btnDelete.Visible = false;
        }

        private void FrmClassM_Load(object sender, EventArgs e)
        {
            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panel1);
            panel1.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Cid, Class_Desc, Sqn_no, s_no, ClassType, Grade,active FROM Class_Mast order by Class_Desc asc";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;

                Dgv.Columns[0].Name = "Cid";
                Dgv.Columns[0].HeaderText = "Cid";
                Dgv.Columns[0].DataPropertyName = "Cid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Class_Desc";
                Dgv.Columns[1].HeaderText = "Class Name";
                Dgv.Columns[1].DataPropertyName = "Class_Desc";
                Dgv.Columns[1].Width = 440;

                Dgv.Columns[2].DataPropertyName = "Sqn_no";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "s_no";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "ClassType";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Grade";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].DataPropertyName = "active";
                Dgv.Columns[6].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void Load_INAgrid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Cid, Class_Desc, Sqn_no, s_no, ClassType, Grade,active FROM  Class_Mast where active=0 order by Class_Desc asc";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;

                Dgv.Columns[0].Name = "Cid";
                Dgv.Columns[0].HeaderText = "Cid";
                Dgv.Columns[0].DataPropertyName = "Cid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Class_Desc";
                Dgv.Columns[1].HeaderText = "Class Name";
                Dgv.Columns[1].DataPropertyName = "Class_Desc";
                Dgv.Columns[1].Width = 440;

                Dgv.Columns[2].DataPropertyName = "Sqn_no";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "s_no";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "ClassType";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Grade";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].DataPropertyName = "active";
                Dgv.Columns[6].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Cbotype_Click(object sender, EventArgs e)
        {

        }

        private void Cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbotype.Text == "Nursery")
            {
                Dictionary<string, string> test = new Dictionary<string, string>
                {
                    { "1", "LKG" },
                    { "2", "UKG" },
                    { "3", "PREKG" }
                };
                cbogrde.DataSource = new BindingSource(test, null);
                cbogrde.DisplayMember = "Value";
                cbogrde.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)cbogrde.SelectedItem).Value;
            }
            else if (cbotype.Text == "Primary")
            {
                cbotype.Refresh();
                Dictionary<string, string> test = new Dictionary<string, string>
                {
                    { "3", "I Std" },
                    { "4", "II Std" },
                    { "5", "III Std" },
                    { "6", "IV Std" },
                    { "7", "V Std" }
                };
                cbogrde.DataSource = new BindingSource(test, null);
                cbogrde.DisplayMember = "Value";
                cbogrde.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)cbogrde.SelectedItem).Value;
            }
            else if (cbotype.Text == "Secondary")
            {
                cbotype.Refresh();
                Dictionary<string, string> test = new Dictionary<string, string>
                {
                    { "8", "VI Std" },
                    { "9", "VII Std" },
                    { "10", "VIII Std" },
                    { "11", "IX Std" },
                    { "12", "X Std" }
                };
                cbogrde.DataSource = new BindingSource(test, null);
                cbogrde.DisplayMember = "Value";
                cbogrde.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)cbogrde.SelectedItem).Value;
            }
            else if (cbotype.Text == "Higher Secondary")
            {
                cbotype.Refresh();
                Dictionary<string, string> test = new Dictionary<string, string>
                {
                    { "13", "XI Std" },
                    { "14", "XII Std" }
                };
                cbogrde.DataSource = new BindingSource(test, null);
                cbogrde.DisplayMember = "Value";
                cbogrde.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)cbogrde.SelectedItem).Value;
            }
            else if (cbotype.Text == "Others")
            {
                cbotype.Refresh();
                Dictionary<string, string> test = new Dictionary<string, string>
                {
                    { "15", "J" },
                    { "16", "New" },
                    { "17", "S" }
                };
                cbogrde.DataSource = new BindingSource(test, null);
                cbogrde.DisplayMember = "Value";
                cbogrde.ValueMember = "Key";
                string value = ((KeyValuePair<string, string>)cbogrde.SelectedItem).Value;
            }
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            if (txtclass.Text == "")
            {
                MessageBox.Show("Enter the Class name");
                txtclass.Focus();
                return;
            }
            if (btnsave.Text == "Save")
            {
                con.Close();
                con.Open();
                string qur = "select * from Class_Mast where  Class_Desc='" + txtclass.Text + "' ";
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    string res = "insert into Class_Mast values(@Class_Desc,@Sqn_no,@s_no,@ClassType,@Grade,@Active)";
                    con.Open();
                    cmd = new SqlCommand(res, con);
                    cmd.Parameters.AddWithValue("@Class_Desc", SqlDbType.NVarChar).Value = txtclass.Text;
                    cmd.Parameters.AddWithValue("@Sqn_no", SqlDbType.NVarChar).Value = cbogrde.SelectedValue;
                    cmd.Parameters.AddWithValue("@s_no", SqlDbType.NVarChar).Value = cboser.Text;
                    cmd.Parameters.AddWithValue("@ClassType", SqlDbType.NVarChar).Value = cbotype.Text;
                    cmd.Parameters.AddWithValue("@Grade", SqlDbType.NVarChar).Value = cbogrde.Text;
                    if (chk.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@Active", SqlDbType.NVarChar).Value = 1;
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Active", SqlDbType.NVarChar).Value = 0;
                    }
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Class Saved ");
                    txtclass.Text = "";
                    cbogrde.SelectedIndex = -1;
                    cbotype.SelectedIndex = -1;
                    cboser.SelectedIndex = -1;
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();

                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnDelete.Visible = true;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    txtclass.Text = "";
                    txtclass.Focus();
                }

            }
            else
            {
                con.Close();
                string qur = "select * from Class_Mast where Class_Desc='" + txtclass.Text + "' and cid <> " + txtuid.Text;
                con.Open();
                SqlCommand cmd1 = new SqlCommand(qur, con);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tap = new DataTable();
                apt1.Fill(tap);
                con.Close();
                if (tap.Rows.Count == 0)
                {
                    int i;
                    if (chk.Checked == true)
                    {
                        i = 1;
                    }
                    else
                    {
                        i = 0;
                    }
                    string QueryUpdate = "Update Class_Mast set Class_Desc='" + txtclass.Text + "',Sqn_no='" + cbogrde.SelectedValue + "',s_no='" + cboser.Text + "',ClassType='" + cbotype.Text + "',Grade='" + cbogrde.Text + "',Active=" + i + " where Cid='" + txtuid.Text + "'";
                    cmd = new SqlCommand(QueryUpdate, con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record Updated Sucessfully");
                    txtclass.Text = "";
                    cbogrde.SelectedIndex = -1;
                    cbotype.SelectedIndex = -1;
                    cboser.SelectedIndex = -1;
                    con.Close();
                    GBList.Visible = false;
                    GBMain.Visible = true;
                    Load_grid();
                    btnadd.Visible = true;
                    btnedit.Visible = true;
                    btnexit.Visible = true;
                    btnsave.Visible = false;
                    btnaddrcan.Visible = false;
                    btnDelete.Visible = true;
                }
                else
                {
                    MessageBox.Show("Enterd the Details are already Exist");
                    con.Close();
                    cmd1.Dispose();
                    return;
                }
                btnsave.Text = "Save";
            }
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtclass.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            cbogrde.SelectedValue = Dgv.Rows[i].Cells[2].Value.ToString();
            cboser.Text = Dgv.Rows[i].Cells[3].Value.ToString();
            cbotype.Text = Dgv.Rows[i].Cells[4].Value.ToString();
            cbogrde.Text = Dgv.Rows[i].Cells[5].Value.ToString();
            if (Dgv.Rows[i].Cells[6].Value.ToString() == "1")
            {
                chk.Checked = true;
            }
            else
            {
                chk.Checked = false;
            }
            btnsave.Text = "Update";
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "SELECT Cid, Class_Desc, Sqn_no, s_no, ClassType, Grade,active FROM  Class_Mast where Class_Desc like '%" + txtscr.Text + "%'";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 7;
                Dgv.Columns[0].Name = "Cid";
                Dgv.Columns[0].HeaderText = "Cid";
                Dgv.Columns[0].DataPropertyName = "Cid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Class_Desc";
                Dgv.Columns[1].HeaderText = "Class Name";
                Dgv.Columns[1].DataPropertyName = "Class_Desc";
                Dgv.Columns[1].Width = 250;

                Dgv.Columns[2].DataPropertyName = "Sqn_no";
                Dgv.Columns[2].Visible = false;

                Dgv.Columns[3].DataPropertyName = "s_no";
                Dgv.Columns[3].Visible = false;

                Dgv.Columns[4].DataPropertyName = "ClassType";
                Dgv.Columns[4].Visible = false;

                Dgv.Columns[5].DataPropertyName = "Grade";
                Dgv.Columns[5].Visible = false;

                Dgv.Columns[6].DataPropertyName = "active";
                Dgv.Columns[6].Visible = false;
                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void Cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbogrde_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cboser_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Chkina_CheckedChanged(object sender, EventArgs e)
        {
            if (chkina.Checked == true)
            {
                Load_INAgrid();
            }
            else if (chkina.Checked == false)
            {
                Load_grid();
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the class ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if(res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());
                    string Query = "Delete from Class_mast Where Cid = @Cid";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@Cid", SqlDbType.Int).Value = Uid;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
