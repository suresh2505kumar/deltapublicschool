﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmMarkEntry : Form
    {
        public FrmMarkEntry()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsClass = new BindingSource();
        BindingSource bsStud = new BindingSource();
        int Fillid = 0; int SelectId = 0;
        private void TxtClass_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", conn);
                bsClass.DataSource = dt;
                Fillgrid(1, dt);
                Point point = FindLocation(txtClass);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Fillgrid(int id, DataTable data)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "CId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Class_Desc";
                    DataGridCommon.Columns[1].HeaderText = "Class";
                    DataGridCommon.Columns[1].DataPropertyName = "Class_Desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsClass;
                }
                else if (id == 2)
                {
                    Fillid = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "SubId";

                    DataGridCommon.Columns[1].Name = "SName";
                    DataGridCommon.Columns[1].HeaderText = "Subject Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Sub_desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsStud;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void TxtStudent_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClass.Text != string.Empty)
                {
                    string Query = "Select distinct a.Sub_desc,a.Uid as SubId from Sub_mast a order by Sub_desc";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    bsStud.DataSource = dt;
                    Fillgrid(2, dt);
                    Point point = FindLocation(txtSubject);
                    grSearch.Location = new Point(point.X, point.Y + 20);
                    grSearch.Visible = true;
                    grSearch.Text = "Class Search";
                }

                else
                {
                    MessageBox.Show("Select Class", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadExam()
        {
            try
            {
                string Query = "Select * from ExamsM Order by SeqNo";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                CmbExam.DataSource = null;
                CmbExam.DisplayMember = "ExamName";
                CmbExam.ValueMember = "ID";
                CmbExam.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmMarkEntry_Load(object sender, EventArgs e)
        {
            LoadExam();
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtSubject.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtSubject.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtSubject.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtSubject.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("Class_Desc LIKE '%{0}%' ", txtClass.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtStudent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsStud.Filter = string.Format("Sub_desc LIKE '%{0}%' ", txtSubject.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbExam_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbExam.SelectedIndex != -1)
                {
                    if (txtSubject.Text != string.Empty && txtClass.Text != string.Empty)
                    {
                        string Query = "Select * from ExamClasses Where Examid =" + CmbExam.SelectedValue + " and ClassId =" + txtClass.Tag + "";
                        DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                        if (dt.Rows.Count == 0)
                        {
                            string qur = "Insert into ExamClasses(ExamId,ClassId) Values (" + CmbExam.SelectedValue + "," + txtClass.Tag + ")";
                            db.ExecuteNonQuery(CommandType.Text, qur, conn);
                        }

                        string Qry = "Select * from ExamMarks Where ExamUid =" + CmbExam.SelectedValue + " and ExmClsUid =" + txtClass.Tag + " and SubjectId =" + txtSubject.Tag + "";
                        DataTable dt1 = db.GetDataWithoutParam(CommandType.Text, Qry, conn);
                        DataGridMark.DataSource = null;
                        if (dt1.Rows.Count == 0)
                        {
                            SqlParameter[] parameters = { new SqlParameter("@ClassId", txtClass.Tag), new SqlParameter("@SubjectId", txtSubject.Tag), new SqlParameter("@ExamUid", CmbExam.SelectedValue) };
                            DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_Marks_InsertDup", parameters, conn);
                            SqlParameter[] parameters1 = { new SqlParameter("@ClassId", txtClass.Tag), new SqlParameter("@SubjectId", txtSubject.Tag), new SqlParameter("@ExamUid", CmbExam.SelectedValue) };
                            DataTable data1 = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_Marks", parameters1, conn);
                            DataGridMark.DataSource = null;
                            DataGridMark.AutoGenerateColumns = false;
                            DataGridMark.ColumnCount = 6;
                            DataGridMark.Columns[0].Name = "SubId";
                            DataGridMark.Columns[0].HeaderText = "StudId";
                            DataGridMark.Columns[0].DataPropertyName = "Uid";
                            DataGridMark.Columns[0].Visible = false;

                            DataGridMark.Columns[1].Name = "StudentName";
                            DataGridMark.Columns[1].HeaderText = "Student Name";
                            DataGridMark.Columns[1].DataPropertyName = "SName";
                            DataGridMark.Columns[1].Width = 130;

                            DataGridMark.Columns[2].Name = "AdmisNo";
                            DataGridMark.Columns[2].HeaderText = "Admission No";
                            DataGridMark.Columns[2].DataPropertyName = "AdmisNo";

                            DataGridMark.Columns[3].Name = "EMark";
                            DataGridMark.Columns[3].HeaderText = "SA";
                            DataGridMark.Columns[3].DataPropertyName = "EMark";

                            DataGridMark.Columns[4].Name = "FA";
                            DataGridMark.Columns[4].HeaderText = "FA";
                            DataGridMark.Columns[4].DataPropertyName = "IMark";

                            DataGridMark.Columns[5].Name = "Total";
                            DataGridMark.Columns[5].HeaderText = "Total";
                            DataGridMark.DataSource = data1;
                            CalculateTotal();
                        }
                        else
                        {
                            SqlParameter[] parameters = { new SqlParameter("@Classid", txtClass.Tag), new SqlParameter("@SubjectId", txtSubject.Tag), new SqlParameter("@ExamUid", CmbExam.SelectedValue) };
                            DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_Marks", parameters, conn);
                            DataGridMark.DataSource = null;
                            DataGridMark.AutoGenerateColumns = false;
                            DataGridMark.ColumnCount = 6;
                            DataGridMark.Columns[0].Name = "SubId";
                            DataGridMark.Columns[0].HeaderText = "StudId";
                            DataGridMark.Columns[0].DataPropertyName = "Uid";
                            DataGridMark.Columns[0].Visible = false;

                            DataGridMark.Columns[1].Name = "StudentName";
                            DataGridMark.Columns[1].HeaderText = "Student Name";
                            DataGridMark.Columns[1].DataPropertyName = "SName";
                            DataGridMark.Columns[1].Width = 130;

                            DataGridMark.Columns[2].Name = "AdmisNo";
                            DataGridMark.Columns[2].HeaderText = "Admission No";
                            DataGridMark.Columns[2].DataPropertyName = "AdmisNo";

                            DataGridMark.Columns[3].Name = "EMark";
                            DataGridMark.Columns[3].HeaderText = "SA";
                            DataGridMark.Columns[3].DataPropertyName = "EMark";

                            DataGridMark.Columns[4].Name = "FA";
                            DataGridMark.Columns[4].HeaderText = "FA";
                            DataGridMark.Columns[4].DataPropertyName = "IMark";

                            DataGridMark.Columns[5].Name = "Total";
                            DataGridMark.Columns[5].HeaderText = "Total";
                            DataGridMark.DataSource = data;
                            CalculateTotal();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void CalculateTotal()
        {
            try
            {
                decimal EMark = 0;
                decimal IMark = 0;
                for (int i = 0; i < DataGridMark.Rows.Count; i++)
                {
                    if (DataGridMark.Rows[i].Cells[3].Value.ToString() == "")
                    {
                        EMark = 0;
                    }
                    else
                    {
                        EMark = Convert.ToDecimal(DataGridMark.Rows[i].Cells[3].Value.ToString());
                    }
                    if (DataGridMark.Rows[i].Cells[4].Value.ToString() == "")
                    {
                        IMark = 0;
                    }
                    else
                    {
                        IMark = Convert.ToDecimal(DataGridMark.Rows[i].Cells[4].Value.ToString());
                    }
                    DataGridMark.Rows[i].Cells[5].Value = EMark + IMark;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridMark_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CalculateTotal();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridMark_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Enter)
                {
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(DataGridMark.Rows.Count > 0)
                {
                    for (int i = 0; i < DataGridMark.Rows.Count; i++)
                    {
                        int EMark = 0;int IMark = 0;
                        if (string.IsNullOrEmpty(DataGridMark.Rows[i].Cells[3].Value.ToString()))
                        {
                            EMark = 0;
                        }
                        else
                        {
                            EMark = Convert.ToInt32(DataGridMark.Rows[i].Cells[3].Value.ToString());
                        }
                        if (string.IsNullOrEmpty(DataGridMark.Rows[i].Cells[4].Value.ToString()))
                        {
                            IMark = 0;
                        }
                        else
                        {
                            IMark = Convert.ToInt32(DataGridMark.Rows[i].Cells[4].Value.ToString());
                        }
                        SqlParameter[] parameters = {
                            new SqlParameter("@ExmClsUid",txtClass.Tag),
                            new SqlParameter("@StudId",DataGridMark.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@SubjetcId",txtSubject.Tag),
                            new SqlParameter("@EMark",EMark),
                            new SqlParameter("@IMark",IMark),
                            new SqlParameter("@ExamUid",CmbExam.SelectedValue)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_InsertExamMarks", parameters, conn);
                    }
                    MessageBox.Show("Marks Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataGridMark.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void LblDownloadFormat_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Mark Entry";

                Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[1, 3];
                Microsoft.Office.Interop.Excel.Range row4 = worksheet.Rows.Cells[1, 4];
                Microsoft.Office.Interop.Excel.Range row5 = worksheet.Rows.Cells[1, 5];
                row1.Value = "SlNo";
                row2.Value = "AdmissionNo";
                row3.Value = "Name";
                row4.Value = "FA";
                row5.Value = "SA";
                int j = 2;
                for (int i = 0; i < DataGridMark.Rows.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range SlNo = worksheet.Rows.Cells[j, 1];
                    Microsoft.Office.Interop.Excel.Range AdmissionNo = worksheet.Rows.Cells[j, 2];
                    Microsoft.Office.Interop.Excel.Range StudentName = worksheet.Rows.Cells[j, 3];
                    Microsoft.Office.Interop.Excel.Range FA = worksheet.Rows.Cells[j, 4];
                    Microsoft.Office.Interop.Excel.Range SA = worksheet.Rows.Cells[j, 5];
                    SlNo.Value = i + 1;
                    AdmissionNo.Value = DataGridMark.Rows[i].Cells[2].Value.ToString();
                    StudentName.Value = DataGridMark.Rows[i].Cells[1].Value.ToString();
                    FA.Value = "";
                    SA.Value = "";
                    j++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
