﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Drawing;

namespace MylaSchool
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += FrmMain_FormClosing;
        }
        ContextMenuStrip context;
        SQLDBHelper db = new SQLDBHelper();
        int Userid;
        TreeNode parentNode = null;
        TreeNode childNode = null;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LblLoginUser.Text = GeneralParameters.LoginUserName;
            MdiClient ctlMDI;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlMDI = (MdiClient)ctl;

                    ctlMDI.BackColor = this.BackColor;
                }
                catch
                {
                }
            }
            Userid = GeneralParameters.UserdId;
            LeadParentMenu();
            cmbMenu.Width = this.Width - 1184;
            treeViewMenu.Top = this.Top + 35;
            treeViewMenu.Height = this.Height - 90;
            PanelMenu.Left = this.Left + 200;
            PanelMenu.Width = this.Width - 210;
        }
        protected void LeadParentMenu()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@UserId", Userid) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetParentMenu", para, conn);
                cmbMenu.DataSource = null;
                cmbMenu.DisplayMember = "MAINMNU";
                cmbMenu.ValueMember = "MENUPARVAL";
                cmbMenu.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Show_msg(object sender2, EventArgs e2, string v, int MenuId)
        {
            string Seqchildc = string.Empty;
            if (Userid != 1)
            {
                Seqchildc = "SELECT MENUPARVAL,FRM_NAME,MNUSUBMENU,STATUS FROM MNU_SUBMENU WHERE USERID =" + v.ToString() + " and  MENUPARVAL = '" + MenuId + "' and STATUS='Y'";
            }
            else
            {
                Seqchildc = "SELECT MENUPARVAL,FRM_NAME,MNUSUBMENU,STATUS FROM MNU_SUBMENU WHERE USERID =" + v.ToString() + " and  MENUPARVAL = '" + MenuId + "'and STATUS='Y'";
            }
            DataTable dtchildc = db.GetDataWithoutParam(CommandType.Text, Seqchildc, conn);
            treeViewMenu.Nodes.Clear();
            foreach (DataRow dr in dtchildc.Rows)
            {
                if (parentNode == null)
                {
                    string drn = dr["FRM_NAME"].ToString();
                    foreach (string img in treeviewImgList.Images.Keys)
                    {
                        if (drn == img)
                        {
                            childNode = treeViewMenu.Nodes.Add(img, dr["FRM_NAME"].ToString(), img);
                        }
                    }
                }
                else
                {
                    string drn = dr["FRM_NAME"].ToString();
                    foreach (string img in treeviewImgList.Images.Keys)
                    {
                        if (drn == img)
                        {
                            childNode = treeViewMenu.Nodes.Add(img, dr["FRM_NAME"].ToString(), img);
                        }
                    }
                }
            }
        }
     
        private void TreeViewMenu_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string name = e.Node.Text;
            SqlParameter[] para = { new SqlParameter("@Frm_Name", name) };
            DataTable dtransaction = db.GetDataWithParam(CommandType.StoredProcedure, "SP_getFrom", para, conn);
            Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);
            foreach (Type type in frmAssembly.GetTypes())
            {
                lblMenu.Text = name;
                if (type.BaseType == typeof(Form))
                {
                    if (type.Name == dtransaction.Rows[0][0].ToString())
                    {
                        Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());
                        frmShow.Text = dtransaction.Rows[0][1].ToString();
                        if (name == "Home Work")
                        {
                            GeneralParameters.MenuId = 1;
                        }
                        else if (name == "Assaignments")
                        {
                            GeneralParameters.MenuId = 2;
                        }
                        else if (name == "Project Work")
                        {
                            GeneralParameters.MenuId = 3;
                        }
                        else if (name == "Assessment")
                        {
                            GeneralParameters.MenuId = 4;
                        }
                        else
                        {
                            GeneralParameters.MenuId = 0;
                        }
                        foreach (Form form in this.MdiChildren)
                        {
                            form.Close();
                        }
                       frmShow.MdiParent = this;
                       //frmShow.Left = this.Left + 120;
                       frmShow.StartPosition = FormStartPosition.CenterScreen;
                       //frmShow.Top = this.Top + 60;
                       //frmShow.Height = this.Height - 70;
                       //frmShow.Width = this.Width - 230;
                        frmShow.FormBorderStyle = FormBorderStyle.None;
                        frmShow.Show();
                    }
                }
            }
        }

        private void CmbMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Show_msg(sender, e, Userid.ToString(), Convert.ToInt32(cmbMenu.SelectedValue));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        
        private void ContexMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            if (item.Text == "Sign Out")
            {
                this.Dispose();
                FrmLogin Logi = new FrmLogin
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                Logi.Show();
            }
        }

        private void LblLoginUser_Click(object sender, EventArgs e)
        {
            context = new ContextMenuStrip();
            ContextMenuStrip contexMenu = new ContextMenuStrip();
            contexMenu.Items.Add("Sign Out");
            contexMenu.Show();
            contexMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContexMenu_ItemClicked);
            Point loc = FindLocation(LblLoginUser);
            context = contexMenu;
            context.Location = new Point(loc.X, loc.Y + 50);
        }

        private void LblLoginUser_MouseHover(object sender, EventArgs e)
        {
            context = new ContextMenuStrip();
            ContextMenuStrip contexMenu = new ContextMenuStrip();
            contexMenu.Items.Add("Sign Out");
            contexMenu.Show();
            contexMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContexMenu_ItemClicked);
            Point loc = FindLocation(LblLoginUser);
            context = contexMenu;
            context.Location = new Point(loc.X, loc.Y + 50);
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void LblLoginUser_MouseLeave(object sender, EventArgs e)
        {
            
        }
    }
}
