﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace MylaSchool
{
    public partial class FrmHomeWork : Form
    {
        public FrmHomeWork()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsClass = new BindingSource();
        BindingSource bsSubject = new BindingSource();
        BindingSource bsHomeWork = new BindingSource();
        string FileName = string.Empty;
        int SelectId = 0;
        int Fillid = 0;
        private void FrmHomeWork_Load(object sender, EventArgs e)
        {
            if (GeneralParameters.MenuId == 1)
            {
                CmbType.Items.Add("Home Work");
                this.Name = "Home Work";
                lblMark.Visible = false;
                TxtMark.Visible = false;
            }
            else if (GeneralParameters.MenuId == 2)
            {
                CmbType.Items.Add("Assaignments");
                this.Name = "Assaignments";
                lblMark.Visible = true;
                TxtMark.Visible = true;
            }
            else if (GeneralParameters.MenuId == 3)
            {
                CmbType.Items.Add("Project");
                this.Name = "Project";
                lblMark.Visible = true;
                TxtMark.Visible = true;
            }
            else if (GeneralParameters.MenuId == 4)
            {
                CmbType.Items.Add("Assessment");
                this.Name = "Assessment";
                lblMark.Visible = true;
                TxtMark.Visible = true;
            }
            GetHomeWork(this.Name);
        }

        public void UploadPhoto(string remoteFile, string localFile)
        {
            try
            {
                FileInfo fileInf = new FileInfo(localFile);
                FtpWebRequest ftpRequest;
                string Path1 = "ftp://mylaeasybiz.net/";
                string Folder = "HomeWork/";
                ftpRequest = (FtpWebRequest)WebRequest.Create(Path1 + Folder + remoteFile + fileInf.Extension);
                ftpRequest.Credentials = new NetworkCredential("delta", "delta1234");
                ftpRequest.KeepAlive = true;
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                Stream ftpStream = ftpRequest.GetRequestStream();
                FileStream localFileStream = fileInf.OpenRead();
                int bufferSize = 2048;
                byte[] byteBuffer = new byte[bufferSize];
                int bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                try
                {
                    while (bytesSent != 0)
                    {
                        ftpStream.Write(byteBuffer, 0, bytesSent);
                        bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                localFileStream.Close();
                ftpStream.Close();
                ftpRequest = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            return;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int uid = 0;
                if(txtDesc.Tag == null)
                {
                    uid = 0;
                }
                else
                {
                    uid = Convert.ToInt32(txtDesc.Tag);
                }
                if (TxtClass.Text != string.Empty || Txtsubject.Text != string.Empty || txtDesc.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@HUid",uid),
                        new SqlParameter("@ClassId",TxtClass.Tag),
                        new SqlParameter("@SubjectId",Txtsubject.Tag),
                        new SqlParameter("@Marks",TxtMark.Text),
                        new SqlParameter("@SubmissionDate",Convert.ToDateTime(dtpSubmissiondate.Text)),
                        new SqlParameter("@HType",CmbType.Text),
                        new SqlParameter("@CreateDate",Convert.ToDateTime(DateTime.Now.Date)),
                        new SqlParameter("@HWDesc",txtDesc.Text),
                        new SqlParameter("@ReturnID",SqlDbType.Int)
                    };
                    parameters[8].Direction = ParameterDirection.Output;
                    int Uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_HomeWork", parameters, con, 8);
                    //FileInfo fileInfo = new FileInfo(FileName);
                    //UploadPhoto(Uid.ToString(), FileName);
                    //string Quer = "Update HomeWork Set PhotoPath='http://www.mylaeasybiz.net/delta/HomeWork/" + Uid.ToString() + fileInfo.Extension + "' Where HUid=" + Uid + "";
                    //SqlCommand cmd = new SqlCommand(Quer, con);
                    //con.Open();
                    //cmd.ExecuteNonQuery();
                    //con.Close();
                    MessageBox.Show("Record saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TxtClass.Text = string.Empty;
                    Txtsubject.Text = string.Empty;
                    txtDesc.Text = string.Empty;
                    TxtMark.Text = string.Empty;
                }
                else
                {
                    TxtClass.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            try
            {
                TxtClass.Text = string.Empty;
                Txtsubject.Text = string.Empty;
                txtDesc.Text = string.Empty;
                TxtMark.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            GetHomeWork(this.Name);
        }

        private void TxtClass_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetAllClass", con);
                bsClass.DataSource = dt;
                Fillgrid(1, dt);
                Point point = FindLocation(TxtClass);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void Fillgrid(int id, DataTable data)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "CId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Class_Desc";
                    DataGridCommon.Columns[1].HeaderText = "Class";
                    DataGridCommon.Columns[1].DataPropertyName = "Class_Desc";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsClass;
                }
                else
                {
                    Fillid = 2;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Sub_desc";
                    DataGridCommon.Columns[1].HeaderText = "Subject";
                    DataGridCommon.Columns[1].DataPropertyName = "Sub_desc";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.DataSource = bsSubject;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtsubject_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getSub_mast", con);
                bsSubject.DataSource = dt;
                Fillgrid(2, dt);
                Point point = FindLocation(Txtsubject);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Subject Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    TxtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    Txtsubject.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    Txtsubject.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtClass_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("Class_Desc LIKE '%{0}%' ", TxtClass.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtsubject_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsSubject.Filter = string.Format("Sub_Desc LIKE '%{0}%' ", Txtsubject.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void PicImage_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*";
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picImage.Image = Image.FromFile(fileDialog.FileName);
                    picImage.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                FileName = fileDialog.FileName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                try
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (Fillid == 1)
                    {
                        TxtClass.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        TxtClass.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    else
                    {
                        Txtsubject.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        Txtsubject.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }

        protected void GetHomeWork(string HType)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@HType", HType), new SqlParameter("@Date", Convert.ToDateTime(dtpFDate.Text)) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetHomeWork", parameters, con);
                bsHomeWork.DataSource = dt;
                DataGridHomeWork.DataSource = null;
                DataGridHomeWork.AutoGenerateColumns = false;
                DataGridHomeWork.ColumnCount = 6;
                DataGridHomeWork.Columns[0].Name = "Uid";
                DataGridHomeWork.Columns[0].HeaderText = "Uid";
                DataGridHomeWork.Columns[0].DataPropertyName = "HUid";
                DataGridHomeWork.Columns[0].Visible = false;

                DataGridHomeWork.Columns[1].Name = "Class";
                DataGridHomeWork.Columns[1].HeaderText = "Class";
                DataGridHomeWork.Columns[1].DataPropertyName = "Class_Desc";

                DataGridHomeWork.Columns[2].Name = "Subject";
                DataGridHomeWork.Columns[2].HeaderText = "Subject";
                DataGridHomeWork.Columns[2].DataPropertyName = "Sub_Desc";
                DataGridHomeWork.Columns[2].Width = 150;

                DataGridHomeWork.Columns[3].Name = "SubmissionDate";
                DataGridHomeWork.Columns[3].HeaderText = "SubmissionDate";
                DataGridHomeWork.Columns[3].DataPropertyName = "SubmissionDate";
                DataGridHomeWork.Columns[3].Width = 150;

                DataGridHomeWork.Columns[4].Name = "Create Date";
                DataGridHomeWork.Columns[4].HeaderText = "CreateDate";
                DataGridHomeWork.Columns[4].DataPropertyName = "CreateDate";

                DataGridHomeWork.Columns[5].Name = "Description";
                DataGridHomeWork.Columns[5].HeaderText = "Description";
                DataGridHomeWork.Columns[5].DataPropertyName = "HWDesc";
                DataGridHomeWork.Columns[5].Width = 390;
                DataGridHomeWork.DataSource = bsHomeWork;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }

        private void DtpFDate_ValueChanged(object sender, EventArgs e)
        {
            GetHomeWork(this.Name);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            grFront.Visible = false;
            grBack.Visible = true;
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridHomeWork.SelectedCells[0].RowIndex;
                int Huid = Convert.ToInt32(DataGridHomeWork.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] sqlParameters = { new SqlParameter("@HUid", Huid) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_EditHomeWork", sqlParameters, con);
                if(dt.Rows.Count > 0)
                {
                    TxtClass.Text = dt.Rows[0]["Class_Desc"].ToString();
                    TxtClass.Tag = dt.Rows[0]["ClassId"].ToString();
                    Txtsubject.Text = dt.Rows[0]["Sub_Desc"].ToString();
                    Txtsubject.Tag = dt.Rows[0]["SubjectId"].ToString();
                    txtDesc.Text = dt.Rows[0]["HWDesc"].ToString();
                    dtpSubmissiondate.Text = dt.Rows[0]["SubmissionDate"].ToString();
                    dtpDate.Text = dt.Rows[0]["HWDT"].ToString();
                    TxtMark.Text = dt.Rows[0]["Marks"].ToString();
                    txtDesc.Tag = Huid;
                    CmbType.SelectedIndex = 0;
                    grFront.Visible = false;
                    grBack.Visible = true;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void DataGridHomeWork_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
