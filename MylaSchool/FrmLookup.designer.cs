﻿namespace MylaSchool
{
    partial class FrmLookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Hfgp = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btncle = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).BeginInit();
            this.SuspendLayout();
            // 
            // Hfgp
            // 
            this.Hfgp.BackgroundColor = System.Drawing.Color.Cyan;
            this.Hfgp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Hfgp.Location = new System.Drawing.Point(7, 30);
            this.Hfgp.Name = "Hfgp";
            this.Hfgp.ReadOnly = true;
            this.Hfgp.RowHeadersVisible = false;
            this.Hfgp.Size = new System.Drawing.Size(283, 389);
            this.Hfgp.TabIndex = 129;
            this.Hfgp.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Hfgp_CellMouseDoubleClick);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(124, 425);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 30);
            this.button1.TabIndex = 131;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btncle
            // 
            this.btncle.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncle.Location = new System.Drawing.Point(210, 425);
            this.btncle.Name = "btncle";
            this.btncle.Size = new System.Drawing.Size(80, 30);
            this.btncle.TabIndex = 132;
            this.btncle.Text = "Cancel";
            this.btncle.UseVisualStyleBackColor = true;
            this.btncle.Click += new System.EventHandler(this.Btncle_Click);
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(7, 4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(283, 23);
            this.txtscr1.TabIndex = 0;
            this.txtscr1.TextChanged += new System.EventHandler(this.Txtscr1_TextChanged);
            // 
            // FrmLookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(308, 463);
            this.Controls.Add(this.txtscr1);
            this.Controls.Add(this.btncle);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Hfgp);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLookup";
            this.Text = "Lookup";
            this.Load += new System.EventHandler(this.FrmLookup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Hfgp;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Button btncle;
        internal System.Windows.Forms.TextBox txtscr1;
    }
}