﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;
using System.Data.Common;

namespace MylaSchool
{
    public partial class FrmOthFeestru : Form
    {
        public FrmOthFeestru()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        SQLDBHelper db = new SQLDBHelper();
        public double sum111 = 0;

        private void LoadRefT()
        {
            con.Close();
            con.Open();
            string qur = "SELECT Ruid,Refname FROM feeref";
            cmd = new SqlCommand(qur, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable tap = new DataTable();
            adpt.Fill(tap);

            cboTerm.DataSource = null;
            cboTerm.DisplayMember = "Refname";
            cboTerm.ValueMember = "Ruid";
            cboTerm.DataSource = tap;
            cboTerm.SelectedIndex = -1;
            cmbTerm.DataSource = null;
            cmbTerm.DisplayMember = "Refname";
            cmbTerm.ValueMember = "Ruid";
            cmbTerm.DataSource = tap;
            cmbTerm.SelectedIndex = -1;
            con.Close();

        }

        private void FrmOthFeestru_Load(object sender, EventArgs e)
        {
            LoadRefT();
            Load_grid();
            ClearTxt();
        }


        private void Loadput()
        {
            Module.fieldone = "";
            Module.fieldtwo = "";
            Module.fieldthree = "";
            Module.fieldFour = "";
            Module.fieldFive = "";
            con.Close();
            con.Open();
            string Query = string.Empty;
            if (Module.type == 7)
            {
                FunC.Partylistviewcont3("s_no", "Class_Desc", "cid", Module.strsql, this, txtssno, txtclass, txtcid, GBList);
                Module.strsql = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1";
                Module.FSSQLSortStr = "Class_Desc";
                Query = "select distinct s_no,Class_Desc,cid  from class_mast a inner join FeeStructclass b on a.Cid=b.classuid where active=1 order by Class_desc";
            }
            else if (Module.type == 8)
            {
                FunC.Partylistviewcont3("uid", "name", "Admisno", Module.strsql, this, txtsid, txtsname, txtadno, GBList);
                Module.strsql = "select uid, sname + '/' + Admisno as name,Admisno from StudentM where Tag ='A' and classid=" + txtcid.Text + " ";
                Module.FSSQLSortStr = "sname";
                Module.FSSQLSortStrA = "Admisno";
                Query = "select uid, sname + '/' + Admisno as name,Admisno from StudentM where Tag ='A' and classid=" + txtcid.Text + " ";
            }
            Module.cmd = new SqlCommand(Query, con);
            SqlDataAdapter aptr = new SqlDataAdapter(Module.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            FrmLookup contc = new FrmLookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            if (Module.type == 7)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            else if (Module.type == 8)
            {
                dt.Columns[1].Width = 260;
                dt.Columns[2].Visible = false;
                dt.Columns[0].Visible = false;
            }
            else if (Module.type == 9)
            {
                dt.Columns[1].Width = 300;
            }
            dt.AutoGenerateColumns = false;
            Module.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Module.i].Name = column.ColumnName;
                if (Module.type == 7)
                {
                    dt.Columns[Module.i].HeaderText = "Class";
                }
                else if (Module.type == 8)
                {
                    dt.Columns[Module.i].HeaderText = "Student Name";
                }
                dt.Columns[Module.i].DataPropertyName = column.ColumnName;
                Module.i = Module.i + 1;
            }
            dt.DataSource = tap;
            contc.MdiParent = this.MdiParent;
            contc.Show();
            con.Close();
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            ClearTxt();
            this.Close();
        }

        private void Txtclass_TextChanged(object sender, EventArgs e)
        {
            cmd.Dispose();
            adpt.Dispose();
        }

        private void CboTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtcid.Text != "")
            {
                con.Close();
                con.Open();
                cbofees.Refresh();
                cbofees.SelectedIndex = -1;
                string ar = "select c.FSuid  from feestructurem a inner join feestructclass c on a.FSuid =c.FSuid where classuid =" + txtcid.Text + " and termuid=" + cboTerm.SelectedValue + "";
                SqlCommand cmnd = new SqlCommand(ar, con);
                int fsuid = (int)cmnd.ExecuteScalar();
                txtstruid.Text = fsuid.ToString();
                string qur = " select Descp,fid  from FeeMast  where Fid not in(select feeid from   feestructured where FSuid=" + fsuid + ") ";
                cmd = new SqlCommand(qur, con);
                SqlDataAdapter dr = new SqlDataAdapter();
                DataSet ds = new DataSet();
                dr.SelectCommand = cmd;
                dr.Fill(ds);
                //Check it this works or not
                cbofees.DisplayMember = "Descp";
                cbofees.ValueMember = "fid";
                cbofees.DataSource = ds.Tables[0];
                cmbFees.DisplayMember = "Descp";
                cmbFees.ValueMember = "fid";
                cmbFees.DataSource = ds.Tables[0]; //Check it this works or not
            }
        }

        private void Titlep()
        {
            Dgvlist.ColumnCount = 9;
            Dgvlist.Columns[0].Name = "fid";
            Dgvlist.Columns[0].HeaderText = "fid";
            Dgvlist.Columns[0].DataPropertyName = "fid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "Class";
            Dgvlist.Columns[1].HeaderText = "Class";
            Dgvlist.Columns[1].DataPropertyName = "Class";
            Dgvlist.Columns[1].Width = 100;

            Dgvlist.Columns[2].Name = "name";
            Dgvlist.Columns[2].HeaderText = "Name";
            Dgvlist.Columns[2].DataPropertyName = "name";
            Dgvlist.Columns[2].Width = 200;

            Dgvlist.Columns[3].Name = "term";
            Dgvlist.Columns[3].HeaderText = "Term";
            Dgvlist.Columns[3].DataPropertyName = "term";
            Dgvlist.Columns[3].Width = 100;

            Dgvlist.Columns[4].Name = "descp";
            Dgvlist.Columns[4].HeaderText = "Description";
            Dgvlist.Columns[4].DataPropertyName = "descp";
            Dgvlist.Columns[4].Width = 200;

            Dgvlist.Columns[5].Name = "Amount";
            Dgvlist.Columns[5].Width = 120;

            Dgvlist.Columns[6].Name = "classid";
            Dgvlist.Columns[6].Visible = false;

            Dgvlist.Columns[7].Name = "studid";
            Dgvlist.Columns[7].Visible = false;

            Dgvlist.Columns[8].Name = "termuid";
            Dgvlist.Columns[8].Visible = false;
        }

        private void Btnok_Click(object sender, EventArgs e)
        {
            if (txtclass.Text == "")
            {
                MessageBox.Show("Select the Class name");
                txtclass.Focus();
                return;
            }
            con.Close();
            con.Open();
            string res1 = "select * from otherfeestru where termuid=" + cboTerm.SelectedValue + " and Feeid=" + cbofees.SelectedValue + " and studid=" + txtsid.Text + " and classid=" + txtcid.Text + "";
            SqlCommand cmd11 = new SqlCommand(res1, con);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd11);
            DataTable tb = new DataTable();
            apt1.Fill(tb);
            if (tb.Rows.Count != 0)
            {
                MessageBox.Show("Enterd the Details are already Exist");
                con.Close();
                cmd11.Dispose();
                return;
            }
            string res = "insert into otherfeestru values(@termuid,@Feeid,@studid,@amount,@feestructid,@classid)";
            con.Close();
            con.Open();
            SqlCommand cmd1 = new SqlCommand(res, con);
            cmd1.Parameters.AddWithValue("@termuid", SqlDbType.NVarChar).Value = cboTerm.SelectedValue;
            cmd1.Parameters.AddWithValue("@Feeid", SqlDbType.NVarChar).Value = cbofees.SelectedValue;

            cmd1.Parameters.AddWithValue("@studid", SqlDbType.NVarChar).Value = txtsid.Text;
            cmd1.Parameters.AddWithValue("@amount", SqlDbType.NVarChar).Value = txtamt.Text;
            cmd1.Parameters.AddWithValue("@feestructid", SqlDbType.NVarChar).Value = txtstruid.Text;
            cmd1.Parameters.AddWithValue("@classid", SqlDbType.NVarChar).Value = txtcid.Text;
            cmd1.ExecuteNonQuery();

            string qur1 = "select  studid,feestructid as Feesturid,feeid,amount as feeamt,isnull('',0) as concession,isnull('',0) as finalamt,isnull('',0) as paidamt,termuid,classid as classuid from otherfeestru where feestructid=" + txtstruid.Text + " and studid=" + txtsid.Text + " and classid=" + txtcid.Text + " and feeid=" + cbofees.SelectedValue + "";
            cmd = new SqlCommand(qur1, con);
            adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                DataTable DtCollection = new DataTable();
                DtCollection.Columns.Add(new DataColumn { ColumnName = "studid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "Feesturid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "feeid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "feeamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "concession", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "finalamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "paidamt", DataType = typeof(decimal) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "termuid", DataType = typeof(Int32) });
                DtCollection.Columns.Add(new DataColumn { ColumnName = "classid", DataType = typeof(Int32) });
                foreach (DataRow row1 in dt.Rows)
                {
                    DataRow row = DtCollection.NewRow();
                    row["studid"] = row1["studid"];
                    row["Feesturid"] = row1["Feesturid"];
                    row["feeid"] = row1["feeid"];
                    row["feeamt"] = row1["feeamt"];
                    row["concession"] = 0;
                    row["finalamt"] = row1["feeamt"];
                    row["paidamt"] = 0;
                    row["termuid"] = row1["termuid"];
                    row["classid"] = row1["classuid"];
                    DtCollection.Rows.Add(row);
                }
                SqlCommand cmnd = new SqlCommand
                {
                    Connection = con
                };
                con.Close();
                con.Open();
                for (int i = 0; i < DtCollection.Rows.Count; i++)
                {
                    cmnd.CommandText = "exec Insert_PLF2 " + DtCollection.Rows[i]["studid"].ToString() + "," + DtCollection.Rows[i]["Feesturid"].ToString() + "," + DtCollection.Rows[i]["feeid"].ToString() + "," + DtCollection.Rows[i]["feeamt"].ToString() + "," + DtCollection.Rows[i]["concession"].ToString() + "," + DtCollection.Rows[i]["finalamt"].ToString() + "," + DtCollection.Rows[i]["paidamt"].ToString() + "," + DtCollection.Rows[i]["termuid"].ToString() + "," + DtCollection.Rows[i]["classid"].ToString() + "";
                    cmnd.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Btnser_Click(sender, e);
            //clearTxt();
        }

        public void ClearTxt()
        {
            txtcid.Text = "";
            txtadno.Text = "";
            txtsname.Text = "";
            txtamt.Text = "";
            txtclass.Text = "";
            txtsid.Text = "";
            cboTerm.SelectedIndex = -1;
            cbofees.SelectedIndex = -1;
        }

        private void Load_grid()
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                string qur = "select a.feeid,class_desc,sname,refname,descp,cast(amount as int)amount,a.classid,studid ,termuid   from otherfeestru  a inner join Class_Mast b on a.classid =b.Cid inner join StudentM c on a.studid =c.uid inner join FeeRef d on a.termuid =d.Ruid inner join FeeMast e on a.feeid =e.Fid  order by s_no ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgvlist.AutoGenerateColumns = false;
                Dgvlist.DataSource = null;
                Dgvlist.ColumnCount = 9;

                Dgvlist.Columns[0].Name = "feeid";
                Dgvlist.Columns[0].HeaderText = "feeid";
                Dgvlist.Columns[0].DataPropertyName = "feeid";
                Dgvlist.Columns[0].Visible = false;

                Dgvlist.Columns[1].Name = "class_desc";
                Dgvlist.Columns[1].HeaderText = "Class Name";
                Dgvlist.Columns[1].DataPropertyName = "class_desc";
                Dgvlist.Columns[1].Width = 100;

                Dgvlist.Columns[2].DataPropertyName = "sname";
                Dgvlist.Columns[2].HeaderText = "Name";
                Dgvlist.Columns[2].DataPropertyName = "sname";
                Dgvlist.Columns[2].Width = 200;


                Dgvlist.Columns[3].DataPropertyName = "refname";
                Dgvlist.Columns[3].HeaderText = "Term";
                Dgvlist.Columns[3].DataPropertyName = "refname";
                Dgvlist.Columns[3].Width = 100;


                Dgvlist.Columns[4].DataPropertyName = "descp";
                Dgvlist.Columns[4].HeaderText = "Description";
                Dgvlist.Columns[4].DataPropertyName = "descp";
                Dgvlist.Columns[4].Width = 200;

                Dgvlist.Columns[5].DataPropertyName = "amount";
                Dgvlist.Columns[5].HeaderText = "Amount";
                Dgvlist.Columns[5].DataPropertyName = "amount";
                Dgvlist.Columns[5].Width = 120;

                Dgvlist.Columns[6].DataPropertyName = "classid";
                Dgvlist.Columns[6].Visible = false;

                Dgvlist.Columns[7].DataPropertyName = "studid";
                Dgvlist.Columns[7].Visible = false;

                Dgvlist.Columns[8].DataPropertyName = "termuid";
                Dgvlist.Columns[8].Visible = false;
                Dgvlist.DataSource = tap;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                string qur = "select a.feeid,class_desc,sname,refname,descp,amount,a.classid,studid ,termuid   from otherfeestru  a inner join Class_Mast b on a.classid =b.Cid inner join StudentM c on a.studid =c.uid inner join FeeRef d on a.termuid =d.Ruid inner join FeeMast e on a.feeid =e.Fid  where class_desc like '%" + txtscr.Text + "%' and sname like '%" + txtscr1.Text + "%' and refname like '%" + txtscr2.Text + "%' order by s_no ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgvlist.AutoGenerateColumns = false;
                Dgvlist.DataSource = null;
                Dgvlist.ColumnCount = 9;

                Dgvlist.Columns[0].Name = "feeid";
                Dgvlist.Columns[0].HeaderText = "feeid";
                Dgvlist.Columns[0].DataPropertyName = "feeid";
                Dgvlist.Columns[0].Visible = false;

                Dgvlist.Columns[1].Name = "class_desc";
                Dgvlist.Columns[1].HeaderText = "Class Name";
                Dgvlist.Columns[1].DataPropertyName = "class_desc";
                Dgvlist.Columns[1].Width = 100;

                Dgvlist.Columns[2].DataPropertyName = "sname";
                Dgvlist.Columns[2].HeaderText = "Name";
                Dgvlist.Columns[2].DataPropertyName = "sname";
                Dgvlist.Columns[2].Width = 200;


                Dgvlist.Columns[3].DataPropertyName = "refname";
                Dgvlist.Columns[3].HeaderText = "Term";
                Dgvlist.Columns[3].DataPropertyName = "refname";
                Dgvlist.Columns[3].Width = 100;


                Dgvlist.Columns[4].DataPropertyName = "descp";
                Dgvlist.Columns[4].HeaderText = "Description";
                Dgvlist.Columns[4].DataPropertyName = "descp";
                Dgvlist.Columns[4].Width = 200;

                Dgvlist.Columns[5].DataPropertyName = "amount";
                Dgvlist.Columns[5].HeaderText = "Amount";
                Dgvlist.Columns[5].DataPropertyName = "amount";
                Dgvlist.Columns[5].Width = 120;

                Dgvlist.Columns[6].DataPropertyName = "classid";
                Dgvlist.Columns[6].Visible = false;

                Dgvlist.Columns[7].DataPropertyName = "studid";
                Dgvlist.Columns[7].Visible = false;

                Dgvlist.Columns[8].DataPropertyName = "termuid";
                Dgvlist.Columns[8].Visible = false;
                Dgvlist.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            con.Close();
            con.Open();
            int k = Dgvlist.SelectedCells[0].RowIndex;
            string sql1 = "select * from plftype where studid =" + Dgvlist.Rows[k].Cells[7].Value.ToString() + " and classid=" + Dgvlist.Rows[k].Cells[6].Value.ToString() + " and termuid=" + Dgvlist.Rows[k].Cells[8].Value.ToString() + " and feeid=" + Dgvlist.Rows[k].Cells[0].Value.ToString() + " and paidamt <> 0";
            SqlCommand cd = new SqlCommand(sql1, con);
            SqlDataAdapter ad = new SqlDataAdapter(cd);
            DataTable tap11 = new DataTable();
            ad.Fill(tap11);
            if (tap11.Rows.Count == 0)
            {
                DialogResult dr = MessageBox.Show("Are you sure to delete row?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    int i = Dgvlist.SelectedCells[0].RowIndex;
                    string quy = "select * from plftype where classid=" + Dgvlist.Rows[i].Cells[6].Value.ToString() + " and termuid =" + Dgvlist.Rows[i].Cells[8].Value.ToString() + " and studid =" + Dgvlist.Rows[i].Cells[7].Value.ToString() + " and feeid=" + Dgvlist.Rows[i].Cells[0].Value.ToString() + "";
                    SqlCommand cmnd = new SqlCommand(quy, con);
                    adpt = new SqlDataAdapter(cmnd);
                    DataTable tap = new DataTable();
                    adpt.Fill(tap);
                    if (tap.Rows.Count != 0)
                    {
                        con.Close();
                        con.Open();
                        int puid = (int)cmnd.ExecuteScalar();
                        string sql = "delete from plftype where Puid=" + puid + "";
                        cmd = new SqlCommand(sql, con);
                        cmd.ExecuteNonQuery();
                    }
                    int j = Dgvlist.SelectedCells[0].RowIndex;
                    string othq = "select * from otherfeestru where classid=" + Dgvlist.Rows[j].Cells[6].Value.ToString() + " and termuid =" + Dgvlist.Rows[j].Cells[8].Value.ToString() + " and studid =" + Dgvlist.Rows[j].Cells[7].Value.ToString() + " and feeid=" + Dgvlist.Rows[j].Cells[0].Value.ToString() + "";
                    SqlCommand cmnd1 = new SqlCommand(othq, con);
                    adpt = new SqlDataAdapter(cmnd1);
                    DataTable tap1 = new DataTable();
                    adpt.Fill(tap1);
                    if (tap1.Rows.Count != 0)
                    {
                        con.Close();
                        con.Open();
                        int ouid = (int)cmnd1.ExecuteScalar();
                        string sql = "delete from otherfeestru where ouid=" + ouid + "";
                        cmd = new SqlCommand(sql, con);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show(" Deleted  ");
                    }

                }
                else if (DialogResult == DialogResult.No)
                {

                }
            }
            else
            {
                MessageBox.Show(" Can't delete!,Amount is collected", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Btnser_Click(sender, e);
        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbofees_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Txtsname_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtclass.Text == "")
            {
                MessageBox.Show("Select The Class Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtclass.Focus();
                return;
            }
            Module.type = 8;
            Loadput();
        }

        private void Txtclass_MouseClick(object sender, MouseEventArgs e)
        {
            txtcid.Text = "";
            txtadno.Text = "";
            txtsname.Text = "";
            txtamt.Text = "";
            txtclass.Text = "";
            txtsid.Text = "";
            Module.type = 7;
            Loadput();
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog folder = new OpenFileDialog
                {
                    Title = "Excel File Dialog",
                    InitialDirectory = @"c:\",
                    Filter = "All files (*.xls)|*.xls",
                    FilterIndex = 2,
                    RestoreDirectory = true
                };
                string excelConnectionString = string.Empty;
                if (folder.ShowDialog() == DialogResult.OK)
                {
                    string filename = folder.FileName;
                    string extension = Path.GetExtension(filename);
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03
                            excelConnectionString = (@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filename + ";Extended Properties=Excel 12.0;");
                            break;
                        case ".xlsx": //Excel 07 or higher
                            excelConnectionString = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                            break;
                    }

                    OleDbConnection connection = new OleDbConnection(excelConnectionString);
                    OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$]", connection);
                    OleDbDataAdapter da = new OleDbDataAdapter(command);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_TruncateOtherfeeImport", con);
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                    {
                        con.Close();
                        con.Open();
                        bulkCopy.DestinationTableName = "dbo.OtherFeeImport";
                        bulkCopy.WriteToServer(dt);
                        MessageBox.Show("Data Exoprted To Sql Server Succefully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    con.Close();
                    SqlParameter[] para = { new SqlParameter("@Feeid", cmbFees.SelectedValue), new SqlParameter("@TermId", cmbTerm.SelectedValue) };
                    db.ExecuteNonQuery(CommandType.StoredProcedure,"SP_UpdateFeeIdImport", para, con);
                }
                SqlParameter[] pa = { new SqlParameter("@TermId", cmbTerm.SelectedValue) };
                DataTable dt1 = db.GetDataWithParam(CommandType.StoredProcedure,"SP_getBulkImportData", pa, con);
                FillImportGrid(dt1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillImportGrid(DataTable dt)
        {
            try
            {
                DataGridImport.DataSource = null;
                DataGridImport.AutoGenerateColumns = false;
                DataGridImport.ColumnCount = 11;
                DataGridImport.Columns[0].Name = "SlNo";
                DataGridImport.Columns[0].HeaderText = "SlNo";

                DataGridImport.Columns[1].Name = "Class";
                DataGridImport.Columns[1].HeaderText = "Class";
                DataGridImport.Columns[1].DataPropertyName = "Class_desc";

                DataGridImport.Columns[2].Name = "Name";
                DataGridImport.Columns[2].HeaderText = "Name";
                DataGridImport.Columns[2].DataPropertyName = "SName";
                DataGridImport.Columns[2].Width = 150;
                DataGridImport.Columns[3].Name = "Term";
                DataGridImport.Columns[3].HeaderText = "Term";
                DataGridImport.Columns[3].DataPropertyName = "Refname";

                DataGridImport.Columns[4].Name = "Fees Description";
                DataGridImport.Columns[4].HeaderText = "Fees Description";
                DataGridImport.Columns[4].DataPropertyName = "Descp";
                DataGridImport.Columns[4].Width = 160;
                DataGridImport.Columns[5].Name = "Amount";
                DataGridImport.Columns[5].HeaderText = "Amount";
                DataGridImport.Columns[5].DataPropertyName = "Amount";

                DataGridImport.Columns[6].Name = "StudId";
                DataGridImport.Columns[6].HeaderText = "StudId";
                DataGridImport.Columns[6].DataPropertyName = "StudId";
                DataGridImport.Columns[6].Visible = false;
                DataGridImport.Columns[7].Name = "TermId";
                DataGridImport.Columns[7].HeaderText = "TermId";
                DataGridImport.Columns[7].DataPropertyName = "TermId";
                DataGridImport.Columns[7].Visible = false;
                DataGridImport.Columns[8].Name = "FeeId";
                DataGridImport.Columns[8].HeaderText = "FeeId";
                DataGridImport.Columns[8].DataPropertyName = "FeeId";
                DataGridImport.Columns[8].Visible = false;
                DataGridImport.Columns[9].Name = "ClassId";
                DataGridImport.Columns[9].HeaderText = "ClassId";
                DataGridImport.Columns[9].DataPropertyName = "ClassId";
                DataGridImport.Columns[9].Visible = false;
                DataGridImport.Columns[10].Name = "FSuid";
                DataGridImport.Columns[10].HeaderText = "FSuid";
                DataGridImport.Columns[10].DataPropertyName = "FSuid";
                DataGridImport.Columns[10].Visible = false;
                if(dt.Rows.Count != 0)
                {
                    DataGridImport.DataSource = dt;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataGridImport.Rows[i].Cells[0].Value = i + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            grImport.Visible = false;
        }

        private void CmbTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTerm.SelectedIndex != -1)
            {
                con.Close();
                con.Open();
                string qur = "select  Fid,Descp from FeeMast";
                cmd = new SqlCommand(qur, con);
                SqlDataAdapter dr = new SqlDataAdapter();
                DataTable ds = new DataTable();
                dr.SelectCommand = cmd;
                dr.Fill(ds);
                cmbFees.DisplayMember = "Descp";
                cmbFees.ValueMember = "fid";
                cmbFees.DataSource = ds;
                cmbFees.SelectedIndex = -1;
            }
        }

        private void ChckBulkInsert_CheckedChanged(object sender, EventArgs e)
        {
            if (chckBulkInsert.Checked == true)
            {
                grImport.Visible = true;
            }
            else
            {
                grImport.Visible = false;
            }
        }

        private void BtnImportSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridImport.Rows.Count; i++)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Termuid",Convert.ToInt32(DataGridImport.Rows[i].Cells[7].Value.ToString())),
                        new SqlParameter("@FeeId",Convert.ToInt32(DataGridImport.Rows[i].Cells[8].Value.ToString())),
                        new SqlParameter("@StudId",Convert.ToInt32(DataGridImport.Rows[i].Cells[6].Value.ToString())),
                        new SqlParameter("@Amount",Convert.ToDecimal(DataGridImport.Rows[i].Cells[5].Value.ToString())),
                        new SqlParameter("@FeeStructId",Convert.ToInt32(DataGridImport.Rows[i].Cells[10].Value.ToString())),
                        new SqlParameter("@Classid",Convert.ToInt32(DataGridImport.Rows[i].Cells[9].Value.ToString()))
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure,"SP_OtherFeesStructure", para, con);
                }
                for (int i = 0; i < DataGridImport.Rows.Count; i++)
                {
                    SqlParameter[] ParaPlf = {
                        new SqlParameter("@StudId",Convert.ToInt32(DataGridImport.Rows[i].Cells[6].Value.ToString())),
                        new SqlParameter("@FeeStructId",Convert.ToInt32(DataGridImport.Rows[i].Cells[10].Value.ToString())),
                        new SqlParameter("@FeeId",Convert.ToInt32(DataGridImport.Rows[i].Cells[8].Value.ToString())),
                        new SqlParameter("@Amount",Convert.ToDecimal(DataGridImport.Rows[i].Cells[5].Value.ToString())),
                        new SqlParameter("@Termuid",Convert.ToInt32(DataGridImport.Rows[i].Cells[7].Value.ToString())),
                        new SqlParameter("@Classid",Convert.ToInt32(DataGridImport.Rows[i].Cells[9].Value.ToString()))
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OtherFeesImportPlf", ParaPlf, con);
                }
                MessageBox.Show("Impoted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            FillImportGrid(dt);
        }
    }
}
