﻿namespace MylaSchool
{
    partial class FrmNewsEvents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmNewsEvents));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.BtnViewImages = new System.Windows.Forms.Button();
            this.grImages = new System.Windows.Forms.GroupBox();
            this.BtnBack = new System.Windows.Forms.Button();
            this.btnFieldTripImages = new System.Windows.Forms.Button();
            this.CmbType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DtpDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DtpSearchDate = new System.Windows.Forms.DateTimePicker();
            this.DataGridNewsEvents = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.grImages.SuspendLayout();
            this.panadd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridNewsEvents)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.BtnViewImages);
            this.grBack.Controls.Add(this.grImages);
            this.grBack.Controls.Add(this.btnFieldTripImages);
            this.grBack.Controls.Add(this.CmbType);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtContent);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.DtpDate);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(8, 2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(697, 411);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // BtnViewImages
            // 
            this.BtnViewImages.Location = new System.Drawing.Point(4, 374);
            this.BtnViewImages.Name = "BtnViewImages";
            this.BtnViewImages.Size = new System.Drawing.Size(107, 31);
            this.BtnViewImages.TabIndex = 1;
            this.BtnViewImages.Text = "View Images";
            this.BtnViewImages.UseVisualStyleBackColor = true;
            this.BtnViewImages.Click += new System.EventHandler(this.BtnViewImages_Click);
            // 
            // grImages
            // 
            this.grImages.Controls.Add(this.BtnBack);
            this.grImages.Location = new System.Drawing.Point(6, 21);
            this.grImages.Name = "grImages";
            this.grImages.Size = new System.Drawing.Size(677, 347);
            this.grImages.TabIndex = 7;
            this.grImages.TabStop = false;
            this.grImages.Visible = false;
            // 
            // BtnBack
            // 
            this.BtnBack.Location = new System.Drawing.Point(583, 306);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(82, 31);
            this.BtnBack.TabIndex = 0;
            this.BtnBack.Text = "Back";
            this.BtnBack.UseVisualStyleBackColor = true;
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // btnFieldTripImages
            // 
            this.btnFieldTripImages.Location = new System.Drawing.Point(522, 370);
            this.btnFieldTripImages.Name = "btnFieldTripImages";
            this.btnFieldTripImages.Size = new System.Drawing.Size(162, 35);
            this.btnFieldTripImages.TabIndex = 6;
            this.btnFieldTripImages.Text = "Upload Images";
            this.btnFieldTripImages.UseVisualStyleBackColor = true;
            this.btnFieldTripImages.Visible = false;
            this.btnFieldTripImages.Click += new System.EventHandler(this.BtnFieldTripImages_Click);
            // 
            // CmbType
            // 
            this.CmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbType.FormattingEnabled = true;
            this.CmbType.Items.AddRange(new object[] {
            "News&Events",
            "PTA"});
            this.CmbType.Location = new System.Drawing.Point(209, 259);
            this.CmbType.Name = "CmbType";
            this.CmbType.Size = new System.Drawing.Size(236, 26);
            this.CmbType.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(165, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Type";
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(209, 125);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(370, 96);
            this.txtContent.TabIndex = 3;
            this.txtContent.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(144, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Content";
            // 
            // DtpDate
            // 
            this.DtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpDate.Location = new System.Drawing.Point(204, 76);
            this.DtpDate.Name = "DtpDate";
            this.DtpDate.Size = new System.Drawing.Size(113, 26);
            this.DtpDate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(165, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Controls.Add(this.btnsave);
            this.panadd.Controls.Add(this.btnaddrcan);
            this.panadd.Controls.Add(this.btnedit);
            this.panadd.Controls.Add(this.btnexit);
            this.panadd.Location = new System.Drawing.Point(7, 421);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(690, 35);
            this.panadd.TabIndex = 212;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(491, 3);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(71, 30);
            this.btnadd.TabIndex = 80;
            this.btnadd.Text = "Add ";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.Btnadd_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(550, 3);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(72, 30);
            this.btnsave.TabIndex = 77;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(621, 3);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 30);
            this.btnaddrcan.TabIndex = 76;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnedit.Location = new System.Drawing.Point(561, 3);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(62, 30);
            this.btnedit.TabIndex = 81;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Visible = false;
            this.btnedit.Click += new System.EventHandler(this.Btnedit_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(622, 3);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(63, 30);
            this.btnexit.TabIndex = 83;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Visible = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DtpSearchDate);
            this.grFront.Controls.Add(this.DataGridNewsEvents);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(7, 2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(690, 411);
            this.grFront.TabIndex = 6;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 13);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(551, 26);
            this.txtSearch.TabIndex = 2;
            // 
            // DtpSearchDate
            // 
            this.DtpSearchDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpSearchDate.Location = new System.Drawing.Point(558, 13);
            this.DtpSearchDate.Name = "DtpSearchDate";
            this.DtpSearchDate.Size = new System.Drawing.Size(126, 26);
            this.DtpSearchDate.TabIndex = 1;
            this.DtpSearchDate.ValueChanged += new System.EventHandler(this.DtpSearchDate_ValueChanged);
            // 
            // DataGridNewsEvents
            // 
            this.DataGridNewsEvents.AllowUserToAddRows = false;
            this.DataGridNewsEvents.BackgroundColor = System.Drawing.Color.White;
            this.DataGridNewsEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridNewsEvents.Location = new System.Drawing.Point(6, 41);
            this.DataGridNewsEvents.Name = "DataGridNewsEvents";
            this.DataGridNewsEvents.ReadOnly = true;
            this.DataGridNewsEvents.RowHeadersVisible = false;
            this.DataGridNewsEvents.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridNewsEvents.Size = new System.Drawing.Size(678, 364);
            this.DataGridNewsEvents.TabIndex = 0;
            // 
            // FrmNewsEvents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(709, 462);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.MaximizeBox = false;
            this.Name = "FrmNewsEvents";
            this.Text = "News Events";
            this.Load += new System.EventHandler(this.FrmNewsEvents_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grImages.ResumeLayout(false);
            this.panadd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridNewsEvents)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.ComboBox CmbType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox txtContent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DtpDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DateTimePicker DtpSearchDate;
        private System.Windows.Forms.DataGridView DataGridNewsEvents;
        private System.Windows.Forms.Button btnFieldTripImages;
        private System.Windows.Forms.GroupBox grImages;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.Button BtnViewImages;
    }
}