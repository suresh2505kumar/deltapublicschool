﻿
namespace MylaSchool    
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.treeViewMenu = new System.Windows.Forms.TreeView();
            this.treeviewImgList = new System.Windows.Forms.ImageList(this.components);
            this.cmbMenu = new System.Windows.Forms.ComboBox();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.LblLoginUser = new System.Windows.Forms.LinkLabel();
            this.lblMenu = new System.Windows.Forms.Label();
            this.statusStrip.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1147, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // treeViewMenu
            // 
            this.treeViewMenu.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeViewMenu.FullRowSelect = true;
            this.treeViewMenu.ImageIndex = 0;
            this.treeViewMenu.ImageList = this.treeviewImgList;
            this.treeViewMenu.ItemHeight = 30;
            this.treeViewMenu.Location = new System.Drawing.Point(0, 26);
            this.treeViewMenu.Name = "treeViewMenu";
            this.treeViewMenu.SelectedImageIndex = 0;
            this.treeViewMenu.ShowLines = false;
            this.treeViewMenu.ShowPlusMinus = false;
            this.treeViewMenu.ShowRootLines = false;
            this.treeViewMenu.Size = new System.Drawing.Size(190, 403);
            this.treeViewMenu.TabIndex = 4;
            this.treeViewMenu.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.TreeViewMenu_NodeMouseClick);
            // 
            // treeviewImgList
            // 
            this.treeviewImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeviewImgList.ImageStream")));
            this.treeviewImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeviewImgList.Images.SetKeyName(0, "Application Issue");
            this.treeviewImgList.Images.SetKeyName(1, "Achivements");
            this.treeviewImgList.Images.SetKeyName(2, "Admission");
            this.treeviewImgList.Images.SetKeyName(3, "Leave Details");
            this.treeviewImgList.Images.SetKeyName(4, "Classes");
            this.treeviewImgList.Images.SetKeyName(5, "Class & Subjects");
            this.treeviewImgList.Images.SetKeyName(6, "Class Transfer");
            this.treeviewImgList.Images.SetKeyName(7, "Conduct");
            this.treeviewImgList.Images.SetKeyName(8, "Entrence Exam");
            this.treeviewImgList.Images.SetKeyName(9, "Faculty & Subjects");
            this.treeviewImgList.Images.SetKeyName(10, "Health Details");
            this.treeviewImgList.Images.SetKeyName(11, "Student Profile");
            this.treeviewImgList.Images.SetKeyName(12, "Registration");
            this.treeviewImgList.Images.SetKeyName(13, "Student Information");
            this.treeviewImgList.Images.SetKeyName(14, "Subjects");
            this.treeviewImgList.Images.SetKeyName(15, "Syllabus");
            this.treeviewImgList.Images.SetKeyName(16, "Teacher.jpg");
            this.treeviewImgList.Images.SetKeyName(17, "MIS - Application");
            this.treeviewImgList.Images.SetKeyName(18, "MIS - Registration");
            this.treeviewImgList.Images.SetKeyName(19, "MIS - Entrence Exam");
            this.treeviewImgList.Images.SetKeyName(20, "MIS - Admission");
            this.treeviewImgList.Images.SetKeyName(21, "MIS - Attendance");
            this.treeviewImgList.Images.SetKeyName(22, "MIS - Conduct");
            this.treeviewImgList.Images.SetKeyName(23, "MIS - Achivements");
            this.treeviewImgList.Images.SetKeyName(24, "MIS - Health Data");
            this.treeviewImgList.Images.SetKeyName(25, "Syllabus Scheduler");
            this.treeviewImgList.Images.SetKeyName(26, "SMS");
            this.treeviewImgList.Images.SetKeyName(27, "Faculty");
            this.treeviewImgList.Images.SetKeyName(28, "Muster Roll");
            this.treeviewImgList.Images.SetKeyName(29, "Payroll");
            this.treeviewImgList.Images.SetKeyName(30, "MIS - Staff Profile");
            this.treeviewImgList.Images.SetKeyName(31, "MIS - Leave Details");
            this.treeviewImgList.Images.SetKeyName(32, "Trip Transfer");
            this.treeviewImgList.Images.SetKeyName(33, "Home Work");
            this.treeviewImgList.Images.SetKeyName(34, "Assaignments");
            this.treeviewImgList.Images.SetKeyName(35, "Project Work");
            this.treeviewImgList.Images.SetKeyName(36, "Assessment");
            this.treeviewImgList.Images.SetKeyName(37, "Events");
            this.treeviewImgList.Images.SetKeyName(38, "Circulars");
            this.treeviewImgList.Images.SetKeyName(39, "News & Events");
            this.treeviewImgList.Images.SetKeyName(40, "Notifications");
            this.treeviewImgList.Images.SetKeyName(41, "Buses");
            this.treeviewImgList.Images.SetKeyName(42, "Boarding Points");
            this.treeviewImgList.Images.SetKeyName(43, "Trip");
            this.treeviewImgList.Images.SetKeyName(44, "Trips");
            this.treeviewImgList.Images.SetKeyName(45, "Cancel Boarding Pass");
            this.treeviewImgList.Images.SetKeyName(46, "Boarding Pass");
            this.treeviewImgList.Images.SetKeyName(47, "Fee Group");
            this.treeviewImgList.Images.SetKeyName(48, "Fees Concession");
            this.treeviewImgList.Images.SetKeyName(49, "Fees  Master");
            this.treeviewImgList.Images.SetKeyName(50, "Fees Reconcilation");
            this.treeviewImgList.Images.SetKeyName(51, "Other Fee Posting");
            this.treeviewImgList.Images.SetKeyName(52, "Fee Structure");
            this.treeviewImgList.Images.SetKeyName(53, "Fees Collection");
            this.treeviewImgList.Images.SetKeyName(54, "Cancel Collection");
            this.treeviewImgList.Images.SetKeyName(55, "Fees Refund");
            this.treeviewImgList.Images.SetKeyName(56, "MIS - Fees Collection");
            this.treeviewImgList.Images.SetKeyName(57, "Time Table");
            this.treeviewImgList.Images.SetKeyName(58, "Leave Approval");
            this.treeviewImgList.Images.SetKeyName(59, "Calendar");
            this.treeviewImgList.Images.SetKeyName(60, "FieldTrip");
            this.treeviewImgList.Images.SetKeyName(61, "Mark Entry");
            // 
            // cmbMenu
            // 
            this.cmbMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMenu.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMenu.FormattingEnabled = true;
            this.cmbMenu.Location = new System.Drawing.Point(0, 1);
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.Size = new System.Drawing.Size(190, 26);
            this.cmbMenu.TabIndex = 8;
            this.cmbMenu.SelectedIndexChanged += new System.EventHandler(this.CmbMenu_SelectedIndexChanged);
            // 
            // PanelMenu
            // 
            this.PanelMenu.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.PanelMenu.Controls.Add(this.LblLoginUser);
            this.PanelMenu.Controls.Add(this.lblMenu);
            this.PanelMenu.Location = new System.Drawing.Point(190, 1);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(955, 28);
            this.PanelMenu.TabIndex = 10;
            // 
            // LblLoginUser
            // 
            this.LblLoginUser.AutoSize = true;
            this.LblLoginUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.LblLoginUser.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLoginUser.Location = new System.Drawing.Point(864, 0);
            this.LblLoginUser.Name = "LblLoginUser";
            this.LblLoginUser.Size = new System.Drawing.Size(91, 23);
            this.LblLoginUser.TabIndex = 1;
            this.LblLoginUser.TabStop = true;
            this.LblLoginUser.Text = "linkLabel1";
            this.LblLoginUser.Click += new System.EventHandler(this.LblLoginUser_Click);
            this.LblLoginUser.MouseHover += new System.EventHandler(this.LblLoginUser_MouseHover);
            // 
            // lblMenu
            // 
            this.lblMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMenu.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenu.ForeColor = System.Drawing.Color.White;
            this.lblMenu.Location = new System.Drawing.Point(0, 0);
            this.lblMenu.Name = "lblMenu";
            this.lblMenu.Size = new System.Drawing.Size(955, 28);
            this.lblMenu.TabIndex = 0;
            this.lblMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1147, 453);
            this.Controls.Add(this.PanelMenu);
            this.Controls.Add(this.cmbMenu);
            this.Controls.Add(this.treeViewMenu);
            this.Controls.Add(this.statusStrip);
            this.IsMdiContainer = true;
            this.Name = "FrmMain";
            this.Text = "Delta Public School";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TreeView treeViewMenu;
        private System.Windows.Forms.ImageList treeviewImgList;
        private System.Windows.Forms.ComboBox cmbMenu;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.Label lblMenu;
        private System.Windows.Forms.LinkLabel LblLoginUser;
    }
}



