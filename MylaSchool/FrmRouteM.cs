﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MylaSchool
{
    public partial class FrmRouteM : Form
    {
        public FrmRouteM()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adpt = new SqlDataAdapter();
        public double sum111 = 0;
        SQLDBHelper db = new SQLDBHelper();
        int slno = 0; int Fillid = 0; int SelectId = 0;
        int slno1 = 0;
        BindingSource bsBus = new BindingSource();

        private void FrmRouteM_Load(object sender, EventArgs e)
        {
            this.Dgv.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgv.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dgvlist.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dgvlist.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            Dgv.RowHeadersVisible = false;
            Dgvlist.RowHeadersVisible = false;
            Titlep();

            Load_grid();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnDelete.Visible = true;
            FunC.Buttonstyleform(this);
            FunC.Buttonstylepanel(panadd);
            panadd.Visible = true;
            Getemp();
        }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            GBList.Visible = false;
            GBMain.Visible = true;
            ClearTxt();
            Dgvlist.Rows.Clear();
            btnadd.Visible = true;
            btnedit.Visible = true;
            btnexit.Visible = true;
            btnsave.Visible = false;
            btnDelete.Visible = true;
        }

        private void Load_grid()
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select Ruid,a.Name,b.Name as Bus from RouteM a Left join BusM b on a.BusId = b.Buid order by a.RUid";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 3;

                Dgv.Columns[0].Name = "Ruid";
                Dgv.Columns[0].HeaderText = "Ruid";
                Dgv.Columns[0].DataPropertyName = "Ruid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Name";
                Dgv.Columns[1].HeaderText = "Route Name";
                Dgv.Columns[1].DataPropertyName = "Name";
                Dgv.Columns[1].Width = 450;

                Dgv.Columns[2].Name = "Bus";
                Dgv.Columns[2].HeaderText = "Bus";
                Dgv.Columns[2].DataPropertyName = "Bus";
                Dgv.Columns[2].Width = 200;

                Dgv.DataSource = tap;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsave.Text == "Save")
                {
                    con.Close();
                    con.Open();
                    string qur = "select * from RouteM where  Name='" + TxtfeeM.Text + "'";
                    SqlCommand cmd1 = new SqlCommand(qur, con);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tap = new DataTable();
                    apt1.Fill(tap);
                    con.Close();
                    if (tap.Rows.Count == 0)
                    {
                        con.Close();
                        con.Open();
                        SqlParameter[] para ={
                        new SqlParameter("@Name",txtroute.Text),
                        new SqlParameter("@BusId",CmbBus.SelectedValue),
                    };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RouteM", para, con);
                        string strSQL = "Select Ruid From RouteM Where Name ='" + txtroute.Text + "'";
                        cmd = new SqlCommand(strSQL, con);
                        SqlDataAdapter apt2 = new SqlDataAdapter(cmd);
                        DataTable tap1 = new DataTable();
                        apt2.Fill(tap1);
                        if (tap1.Rows.Count > 0)
                        {
                            txtuid.Text = tap1.Rows[0]["Ruid"].ToString();
                        }
                        for (int j = 0; j < Dgvlist.RowCount - 1; j++)
                        {
                            SqlParameter[] para1 ={
                                new SqlParameter("@headid",txtuid.Text),
                                new SqlParameter("@slno", Dgvlist.Rows[j].Cells[1].Value),
                                new SqlParameter("@Boardid", Convert.ToInt16(Dgvlist.Rows[j].Cells[6].Value)),
                                new SqlParameter("@time1", Convert.ToString(Dgvlist.Rows[j].Cells[3].Value)),
                                new SqlParameter("@time2", Convert.ToString(Dgvlist.Rows[j].Cells[4].Value)),
                                new SqlParameter("@strength",Convert.ToString(Dgvlist.Rows[j].Cells[5].Value)),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RouteD", para1, con);
                        }
                    }
                }
                else
                {
                    con.Close();
                    con.Open();
                    cmd.CommandText = "delete from Routed where headid=" + txtuid.Text + "";
                    cmd.ExecuteNonQuery();
                    SqlParameter[] para ={
                        new SqlParameter("@Name",txtroute.Text),
                        new SqlParameter("@ruid",txtuid.Text),
                        new SqlParameter("@BusId",CmbBus.SelectedValue),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RouteMUpdate", para, con);
                    con.Close();
                    con.Open();
                    for (int j = 0; j < Dgvlist.RowCount - 1; j++)
                    {
                        SqlParameter[] para1 ={
                            new SqlParameter("@headid",txtuid.Text),
                            new SqlParameter("@slno", Dgvlist.Rows[j].Cells[1].Value),
                            new SqlParameter("@Boardid", Convert.ToInt16(Dgvlist.Rows[j].Cells[6].Value)),
                            new SqlParameter("@time1", Convert.ToString(Dgvlist.Rows[j].Cells[3].Value)),
                            new SqlParameter("@time2", Convert.ToString(Dgvlist.Rows[j].Cells[4].Value)),
                            new SqlParameter("@strength",Convert.ToString(Dgvlist.Rows[j].Cells[5].Value)),
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RouteD", para1, con);
                    }
                }
                MessageBox.Show("Record  Saved ");
                ClearTxt();
                con.Close();
                Dgvlist.Rows.Clear();

                GBList.Visible = false;
                GBMain.Visible = true;
                Load_grid();
                btnDelete.Visible = true;
                btnadd.Visible = true;
                btnedit.Visible = true;
                btnexit.Visible = true;
                btnsave.Visible = false;
                btnaddrcan.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            ClearTxt();
            Dgvlist.Rows.Clear();
            txtroute.Text = string.Empty;
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Save";
        }

        private void Titlep()
        {

            Dgvlist.AutoGenerateColumns = false;
            Dgvlist.Refresh();
            Dgvlist.DataSource = null;
            Dgvlist.Rows.Clear();
            Dgvlist.ColumnCount = 7;
            Dgvlist.Columns[0].Name = "Ruid";
            Dgvlist.Columns[0].HeaderText = "Ruid";
            Dgvlist.Columns[0].DataPropertyName = "Ruid";
            Dgvlist.Columns[0].Visible = false;

            Dgvlist.Columns[1].Name = "slno";
            Dgvlist.Columns[1].HeaderText = "slno";
            Dgvlist.Columns[1].DataPropertyName = "slno";
            Dgvlist.Columns[1].Width = 50;

            Dgvlist.Columns[2].Name = "BoardName";
            Dgvlist.Columns[2].HeaderText = "BoardName";
            Dgvlist.Columns[2].DataPropertyName = "BoardName";
            Dgvlist.Columns[2].Width = 550;

            Dgvlist.Columns[3].Name = "Time1";
            Dgvlist.Columns[3].HeaderText = "Time1";
            Dgvlist.Columns[3].DataPropertyName = "Time1";
            Dgvlist.Columns[3].Width = 100;
            Dgvlist.Columns[3].Visible = false;

            Dgvlist.Columns[4].Name = "Time2";
            Dgvlist.Columns[4].HeaderText = "Time2";
            Dgvlist.Columns[4].DataPropertyName = "Time2";
            Dgvlist.Columns[4].Width = 100;
            Dgvlist.Columns[4].Visible = false;

            Dgvlist.Columns[5].Name = "Strength";
            Dgvlist.Columns[5].HeaderText = "Strength";
            Dgvlist.Columns[5].DataPropertyName = "Strength";
            Dgvlist.Columns[5].Width = 100;
            Dgvlist.Columns[5].Visible = false;

            Dgvlist.Columns[6].Name = "boardid";
            Dgvlist.Columns[6].HeaderText = "boardid";
            Dgvlist.Columns[6].DataPropertyName = "boardid";
            Dgvlist.Columns[6].Visible = false;
        }

        private void Btnok_Click(object sender, EventArgs e)
        {
            if (TxtfeeM.Text == "")
            {
                MessageBox.Show("Select the Boarding Points");
                TxtfeeM.Focus();
                return;
            }
            //if (txtstrength.Text == "")
            //{
            //    MessageBox.Show("Enter the strength");
            //    txtstrength.Focus();
            //    return;
            //}
            DataGridViewRow dataGridView =(DataGridViewRow) Dgvlist.Rows[0].Clone();
            dataGridView.Cells[0].Value = "0";
            dataGridView.Cells[1].Value = Dgvlist.Rows.Count;
            dataGridView.Cells[2].Value = TxtfeeM.Text;
            dataGridView.Cells[3].Value = "00:00";
            dataGridView.Cells[4].Value = "00:00";
            dataGridView.Cells[5].Value = "0";
            dataGridView.Cells[6].Value = TxtfeeM.Tag;
            Dgvlist.Rows.Add(dataGridView);
            ClearTxt();
        }

        private void Btnedit_Click(object sender, EventArgs e)
        {
            GBList.Visible = true;
            GBMain.Visible = false;
            int i = Dgv.SelectedCells[0].RowIndex;
            txtuid.Text = Dgv.Rows[i].Cells[0].Value.ToString();
            txtroute.Text = Dgv.Rows[i].Cells[1].Value.ToString();
            CmbBus.Text = Dgv.Rows[i].Cells[2].Value.ToString();
            btnadd.Visible = false;
            btnedit.Visible = false;
            btnexit.Visible = false;
            btnsave.Visible = true;
            btnaddrcan.Visible = true;
            btnDelete.Visible = false;
            btnsave.Text = "Update";
            Titlep();

            string quy = "select  a.ruid,a.slno,b.boardingp,a.time1,a.time2,a.strength,a.boardid from Routed a inner join boardingpm b on a.boardid=b.buid where a.headid=" + txtuid.Text + "";
            cmd = new SqlCommand(quy, con);
            SqlDataAdapter aptr = new SqlDataAdapter(cmd);
            DataTable tap1 = new DataTable();
            aptr.Fill(tap1);
            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = Dgvlist.Rows.Add();
                Dgvlist.Rows[index].Cells[0].Value = tap1.Rows[k]["ruid"].ToString();
                Dgvlist.Rows[index].Cells[1].Value = tap1.Rows[k]["slno"].ToString();
                Dgvlist.Rows[index].Cells[2].Value = tap1.Rows[k]["boardingp"].ToString();
                Dgvlist.Rows[index].Cells[3].Value = tap1.Rows[k]["time1"].ToString();
                Dgvlist.Rows[index].Cells[4].Value = tap1.Rows[k]["time2"].ToString();
                Dgvlist.Rows[index].Cells[5].Value = tap1.Rows[k]["strength"].ToString();
                Dgvlist.Rows[index].Cells[6].Value = tap1.Rows[k]["boardid"].ToString();
            }
            slno1 = tap1.Rows.Count;
        }

        public void ClearTxt()
        {
            TxtfeeM.Text = "";
            txttime2.Text = "";
            txttime1.Text = "";
            txtstrength.Text = "";
        }

        private void CboTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void Btnser_Click(object sender, EventArgs e)
        {
            try
            {
                con.Close();
                con.Open();
                string qur = "select Ruid,Name from RouteM where  Name like '%" + txtscr1.Text + "%'  ";
                cmd = new SqlCommand(qur, con);
                adpt = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                adpt.Fill(tap);
                Dgv.AutoGenerateColumns = false;
                Dgv.DataSource = null;
                Dgv.ColumnCount = 2;

                Dgv.Columns[0].Name = "Ruid";
                Dgv.Columns[0].HeaderText = "Ruid";
                Dgv.Columns[0].DataPropertyName = "Ruid";
                Dgv.Columns[0].Visible = false;

                Dgv.Columns[1].Name = "Name";
                Dgv.Columns[1].HeaderText = "Route Name";
                Dgv.Columns[1].DataPropertyName = "Name";
                Dgv.Columns[1].Width = 500;

                Dgv.DataSource = tap;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        protected void Getemp()
        {
            try
            {
                string Query = "Select Buid,Name from BusM order by Buid";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                CmbBus.DataSource = null;
                CmbBus.DisplayMember = "Name";
                CmbBus.ValueMember = "Buid";
                CmbBus.DataSource = dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void TxtfeeM_Leave(object sender, EventArgs e)
        {
            try
            {
                if (TxtfeeM.Text != string.Empty)
                {
                    string Query = "Select buid,Boardingp from boardingpm  Where Boardingp ='" + TxtfeeM.Text + "'";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, con);
                    if (dt.Rows.Count != 0)
                    {
                        TxtfeeM.Tag = dt.Rows[0]["buid"].ToString();

                    }
                    else
                    {
                        MessageBox.Show("Data Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want to delete the Route ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    int Index = Dgv.SelectedCells[0].RowIndex;
                    int Uid = Convert.ToInt32(Dgv.Rows[Index].Cells[0].Value.ToString());


                    string Query1 = "Delete from Routed Where headid = @headid";
                    SqlCommand cmd1 = new SqlCommand(Query1, con);
                    cmd1.Parameters.AddWithValue("@headid", SqlDbType.Int).Value = Uid;
                    cmd1.ExecuteNonQuery();
                    string Query = "Delete from RouteM Where ruid = @ruid";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    cmd.Parameters.AddWithValue("@ruid", SqlDbType.Int).Value = Uid;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Record deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Load_grid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtfeeM_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "select Buid,BoardingP from BoardingPM Order by Buid";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, con);
                bsBus.DataSource = dataTable;
                Fillgrid(1, dataTable);
                Point point = FindLocation(TxtfeeM);
                grSearch.Location = new Point(point.X, point.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Class Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void Fillgrid(int id, DataTable dt)
        {
            try
            {
                if (id == 1)
                {
                    Fillid = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Buid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "BoardingP";
                    DataGridCommon.Columns[1].HeaderText = "Boarding Point";
                    DataGridCommon.Columns[1].DataPropertyName = "BoardingP";
                    DataGridCommon.Columns[1].Width = 330;
                    DataGridCommon.DataSource = bsBus;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    TxtfeeM.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtfeeM.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                SelectId = 0;
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtfeeM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectId == 0)
                {
                    bsBus.Filter = string.Format("BoardingP Like '%{0}%'", TxtfeeM.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                btnSelect_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}

