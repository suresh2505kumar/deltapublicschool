﻿namespace MylaSchool
{
    partial class FeeMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FeeMaster));
            this.GBMain = new System.Windows.Forms.GroupBox();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.btnser = new System.Windows.Forms.Button();
            this.txtscr = new System.Windows.Forms.TextBox();
            this.Dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.GBList = new System.Windows.Forms.GroupBox();
            this.chkbus = new System.Windows.Forms.CheckBox();
            this.chkRf = new System.Windows.Forms.CheckBox();
            this.txtdesc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboser = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbogrp = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.GBMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.GBList.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBMain
            // 
            this.GBMain.BackColor = System.Drawing.Color.White;
            this.GBMain.Controls.Add(this.txtscr1);
            this.GBMain.Controls.Add(this.btnser);
            this.GBMain.Controls.Add(this.txtscr);
            this.GBMain.Controls.Add(this.Dgv);
            this.GBMain.Location = new System.Drawing.Point(2, 3);
            this.GBMain.Name = "GBMain";
            this.GBMain.Size = new System.Drawing.Size(465, 331);
            this.GBMain.TabIndex = 1;
            this.GBMain.TabStop = false;
            // 
            // txtscr1
            // 
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(219, 11);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(162, 26);
            this.txtscr1.TabIndex = 90;
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Image = ((System.Drawing.Image)(resources.GetObject("btnser.Image")));
            this.btnser.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnser.Location = new System.Drawing.Point(383, 8);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(70, 30);
            this.btnser.TabIndex = 89;
            this.btnser.Text = "Search";
            this.btnser.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.Btnser_Click);
            // 
            // txtscr
            // 
            this.txtscr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr.Location = new System.Drawing.Point(14, 11);
            this.txtscr.Name = "txtscr";
            this.txtscr.Size = new System.Drawing.Size(203, 26);
            this.txtscr.TabIndex = 87;
            // 
            // Dgv
            // 
            this.Dgv.AllowUserToAddRows = false;
            this.Dgv.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv.Location = new System.Drawing.Point(13, 38);
            this.Dgv.Name = "Dgv";
            this.Dgv.ReadOnly = true;
            this.Dgv.RowHeadersVisible = false;
            this.Dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv.Size = new System.Drawing.Size(440, 287);
            this.Dgv.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnedit);
            this.panel1.Controls.Add(this.btnadd);
            this.panel1.Controls.Add(this.btnexit);
            this.panel1.Controls.Add(this.btnaddrcan);
            this.panel1.Controls.Add(this.btnsave);
            this.panel1.Location = new System.Drawing.Point(1, 336);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(466, 32);
            this.panel1.TabIndex = 91;
            // 
            // btnedit
            // 
            this.btnedit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnedit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnedit.Image = ((System.Drawing.Image)(resources.GetObject("btnedit.Image")));
            this.btnedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnedit.Location = new System.Drawing.Point(361, 1);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(53, 30);
            this.btnedit.TabIndex = 85;
            this.btnedit.Text = "Edit";
            this.btnedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnedit.UseVisualStyleBackColor = false;
            this.btnedit.Visible = false;
            this.btnedit.Click += new System.EventHandler(this.Btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(311, 0);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(53, 30);
            this.btnadd.TabIndex = 84;
            this.btnadd.Text = "Add";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Visible = false;
            this.btnadd.Click += new System.EventHandler(this.Btnadd_Click);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(412, 1);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(53, 30);
            this.btnexit.TabIndex = 86;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Visible = false;
            this.btnexit.Click += new System.EventHandler(this.Btnexit_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(399, 0);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(66, 30);
            this.btnaddrcan.TabIndex = 122;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Visible = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(327, 0);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 30);
            this.btnsave.TabIndex = 123;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Visible = false;
            this.btnsave.Click += new System.EventHandler(this.Btnsave_Click);
            // 
            // GBList
            // 
            this.GBList.BackColor = System.Drawing.Color.White;
            this.GBList.Controls.Add(this.chkbus);
            this.GBList.Controls.Add(this.chkRf);
            this.GBList.Controls.Add(this.txtdesc);
            this.GBList.Controls.Add(this.label1);
            this.GBList.Controls.Add(this.cboser);
            this.GBList.Controls.Add(this.label4);
            this.GBList.Controls.Add(this.cbogrp);
            this.GBList.Controls.Add(this.label3);
            this.GBList.Controls.Add(this.txtuid);
            this.GBList.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBList.Location = new System.Drawing.Point(1, 0);
            this.GBList.Name = "GBList";
            this.GBList.Size = new System.Drawing.Size(466, 333);
            this.GBList.TabIndex = 2;
            this.GBList.TabStop = false;
            // 
            // chkbus
            // 
            this.chkbus.AutoSize = true;
            this.chkbus.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkbus.Location = new System.Drawing.Point(229, 252);
            this.chkbus.Name = "chkbus";
            this.chkbus.Size = new System.Drawing.Size(75, 22);
            this.chkbus.TabIndex = 125;
            this.chkbus.Text = "Bus Fee";
            this.chkbus.UseVisualStyleBackColor = true;
            // 
            // chkRf
            // 
            this.chkRf.AutoSize = true;
            this.chkRf.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRf.Location = new System.Drawing.Point(126, 252);
            this.chkRf.Name = "chkRf";
            this.chkRf.Size = new System.Drawing.Size(99, 22);
            this.chkRf.TabIndex = 124;
            this.chkRf.Text = "Refundable";
            this.chkRf.UseVisualStyleBackColor = true;
            // 
            // txtdesc
            // 
            this.txtdesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtdesc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdesc.Location = new System.Drawing.Point(126, 66);
            this.txtdesc.MaxLength = 250;
            this.txtdesc.Name = "txtdesc";
            this.txtdesc.Size = new System.Drawing.Size(335, 26);
            this.txtdesc.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 18);
            this.label1.TabIndex = 117;
            this.label1.Text = "Description";
            // 
            // cboser
            // 
            this.cboser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboser.FormattingEnabled = true;
            this.cboser.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25"});
            this.cboser.Location = new System.Drawing.Point(126, 182);
            this.cboser.Name = "cboser";
            this.cboser.Size = new System.Drawing.Size(121, 26);
            this.cboser.TabIndex = 2;
            this.cboser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cboser_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(53, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 18);
            this.label4.TabIndex = 113;
            this.label4.Text = "Fee Order";
            // 
            // cbogrp
            // 
            this.cbogrp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbogrp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbogrp.FormattingEnabled = true;
            this.cbogrp.Location = new System.Drawing.Point(126, 124);
            this.cbogrp.Name = "cbogrp";
            this.cbogrp.Size = new System.Drawing.Size(273, 26);
            this.cbogrp.TabIndex = 1;
            this.cbogrp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Cbogrp_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(51, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 18);
            this.label3.TabIndex = 111;
            this.label3.Text = "Fee Group";
            // 
            // txtuid
            // 
            this.txtuid.Location = new System.Drawing.Point(429, 19);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(35, 22);
            this.txtuid.TabIndex = 109;
            this.txtuid.Visible = false;
            // 
            // FeeMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(467, 375);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.GBMain);
            this.Controls.Add(this.GBList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FeeMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fee Master";
            this.Load += new System.EventHandler(this.FeeMaster_Load);
            this.GBMain.ResumeLayout(false);
            this.GBMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.GBList.ResumeLayout(false);
            this.GBList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBMain;
        internal System.Windows.Forms.Button btnser;
        internal System.Windows.Forms.TextBox txtscr;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridView Dgv;
        private System.Windows.Forms.GroupBox GBList;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btnaddrcan;
        internal System.Windows.Forms.TextBox txtdesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbogrp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.CheckBox chkbus;
        private System.Windows.Forms.CheckBox chkRf;
        internal System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.Panel panel1;
    }
}